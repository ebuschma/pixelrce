-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscDtmCore.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-05-16
-- Last update: 2017-03-10
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:  
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AtlasTtcRxPkg.all;

library unisim;
use unisim.vcomponents.all;

entity AtlasCscDtmCore is
   generic (
      TPD_G              : time             := 1 ns;
      AXIL_BASEADDR_G    : slv(31 downto 0) := X"A0000000";
      AXI_ERROR_RESP_G   : slv(1 downto 0)  := AXI_RESP_OK_C;
      CASCADE_SIZE_G     : positive         := 1;
      FIFO_FIXED_THES_G  : boolean          := false;
      FIFO_AFULL_THRES_G : positive         := 256); 
   port (
      -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
      axiClk         : in    sl;
      axiRst         : in    sl;
      axiReadMaster  : in    AxiLiteReadMasterType;
      axiReadSlave   : out   AxiLiteReadSlaveType;
      axiWriteMaster : in    AxiLiteWriteMasterType;
      axiWriteSlave  : out   AxiLiteWriteSlaveType;
      -- RTM Low Speed
      dtmToRtmLsP    : slv(2 downto 0);
      dtmToRtmLsM    : slv(2 downto 0);
      busyOutP       : out sl;
      busyOutM       : out sl;
      cdr_Locked     : in sl;
      dtm_Locked     : out sl;
      -- I2C to RMB
      ttc_sda        : inout sl;
      ttc_scl        : inout sl;
      -- DPM Signals
      dpmClkP        : out   slv(2 downto 0);
      dpmClkM        : out   slv(2 downto 0);
      dpmFbP         : in    slv(7 downto 0);
      dpmFbM         : in    slv(7 downto 0);
      -- Reference 200 MHz clock
      sysClk200      : in    sl;
      sysClk200Rst   : in    sl;
      sysClk125      : in    sl;
      sysClk125Rst   : in    sl;
      stableClk125   : in    sl;
      -- Backplane Clocks
      bpClkIn     : in    slv(5 downto 0);
      bpClkOut    : out   slv(5 downto 0);      
      -- Debug
      led            : out   slv(1 downto 0));      
end AtlasCscDtmCore;

architecture rtl of AtlasCscDtmCore is
   
   constant NUM_AXI_MASTERS_C : natural := 3;

   constant TRIG_AXI_INDEX_C   : natural := 0;
   constant EMU_V1_AXI_INDEX_C : natural := 1;
   constant EMU_V2_AXI_INDEX_C : natural := 2;

   constant TRIG_BASE_ADDR_C   : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00000000");
   constant EMU_V1_BASE_ADDR_C : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00001000");
   constant EMU_V2_BASE_ADDR_C : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00002000");

   constant AXI_CROSSBAR_MASTERS_CONFIG_C : AxiLiteCrossbarMasterConfigArray(NUM_AXI_MASTERS_C-1 downto 0) := (
      TRIG_AXI_INDEX_C   => (
         baseAddr        => TRIG_BASE_ADDR_C,
         addrBits        => 12,
         connectivity    => X"0001"),
      EMU_V1_AXI_INDEX_C => (
         baseAddr        => EMU_V1_BASE_ADDR_C,
         addrBits        => 12,
         connectivity    => X"0001"),
      EMU_V2_AXI_INDEX_C => (
         baseAddr        => EMU_V2_BASE_ADDR_C,
         addrBits        => 12,
         connectivity    => X"0001"));         

   signal mAxiWriteMasters : AxiLiteWriteMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiWriteSlaves  : AxiLiteWriteSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadMasters  : AxiLiteReadMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadSlaves   : AxiLiteReadSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);

   component AtlasTtcTxEmuPll
      port(
         -- Clock in ports
         sysClk200MHz : in  sl;
         -- Clock out ports
         emuClk160MHz : out sl;
         -- Status and control signals
         reset        : in  sl;
         locked       : out sl);
   end component;

   attribute SYN_BLACK_BOX                     : boolean;
   attribute SYN_BLACK_BOX of AtlasTtcTxEmuPll : component is true;

   attribute BLACK_BOX_PAD_PIN                     : string;
   attribute BLACK_BOX_PAD_PIN of AtlasTtcTxEmuPll : component is "sysClk200MHz,emuClk160MHz,reset,locked";

   signal atlasClk160MHz,
      atlasClk160MHzRst,
      sysClkLocked,
      locPllRef,
      clk,
      data,
      emuDataV1,
      emuDataV2,
      emuVersionSelect,
      emuClk,
      emuData,
      emuBusy,
      cdrHeartbeat,
      toggleL1Trig,
      gbtclock120,
      gbtout,
      calibdone,
      gbtclock120nb,
      gbtclock120out,
      clkfb,
      sumExtBusy : sl;
   signal busy          : slv(7 downto 0);
   signal atlasTtcRxOut : AtlasTTCRxOutType;

   attribute KEEP_HIERARCHY : string;
   attribute KEEP_HIERARCHY of
      U_AtlasCscBusy,
      U_AtlasTtcRxEmuV1,
      U_AtlasTtcRxEmuV2,
      AtlasTtcTxEmuPll_Inst : label is "TRUE";
   
begin

   -- AXI-Lite Crossbar
   AxiLiteCrossbar_Inst : entity work.AxiLiteCrossbar
      generic map (
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => NUM_AXI_MASTERS_C,
         DEC_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         MASTERS_CONFIG_G   => AXI_CROSSBAR_MASTERS_CONFIG_C)
      port map (
         axiClk              => axiClk,
         axiClkRst           => axiRst,
         sAxiWriteMasters(0) => axiWriteMaster,
         sAxiWriteSlaves(0)  => axiWriteSlave,
         sAxiReadMasters(0)  => axiReadMaster,
         sAxiReadSlaves(0)   => axiReadSlave,
         mAxiWriteMasters    => mAxiWriteMasters,
         mAxiWriteSlaves     => mAxiWriteSlaves,
         mAxiReadMasters     => mAxiReadMasters,
         mAxiReadSlaves      => mAxiReadSlaves); 

   -- TTC-RX Module
   U_AtlasCscBusy : entity work.AtlasCscBusy
      generic map (
         TPD_G              => TPD_G,
         STATUS_CNT_WIDTH_G => 32,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         IDELAY_VALUE_G     => toSlv(0, 5),
         IODELAY_GROUP_G    => "atlas_csc_busy_delay_group")     
      port map (
         -- External CDR Ports
         clkP              => dtmToRtmLsP(1),  -- From ADN2816 IC
         clkN              => dtmToRtmLsM(1),  -- From ADN2816 IC
         clk               => clk,             -- Copy of the ADN2816 IC's clock
         gbtclock120       => gbtclock120,
         dataP             => dtmToRtmLsP(2),  -- From ADN2816 IC
         dataN             => dtmToRtmLsM(2),  -- From ADN2816 IC
         data              => data,            -- Copy of the ADN2816 IC's data
         lostLink          => cdr_locked,      -- From ADN2816 IC
         -- RMB Status Signals
         busyIn            => busy,
         busyP             => busyOutP,        -- RMB's busy LEMO interface
         busyN             => busyOutM,        -- RMB's busy LEMO interface
         locked            => calibdone,      -- RMB's LED Display
         -- I2C to RMB
         ttc_sda           => ttc_sda,
         ttc_scl           => ttc_scl,
         -- AXI-Lite Register and Status Bus Interface (axiClk domain)
         axiClk            => axiClk,
         axiRst            => axiRst,
         axiReadMaster     => mAxiReadMasters(TRIG_AXI_INDEX_C),
         axiReadSlave      => mAxiReadSlaves(TRIG_AXI_INDEX_C),
         axiWriteMaster    => mAxiWriteMasters(TRIG_AXI_INDEX_C),
         axiWriteSlave     => mAxiWriteSlaves(TRIG_AXI_INDEX_C),
         statusWords       => open,
         statusSend        => open,
         -- Emulation Trigger Signals
         emuClk            => emuClk,
         emuData           => emuData,
         emuBusy           => emuBusy,
         emuVersionSelect  => emuVersionSelect,
         -- Reference 200 MHz clock
         refClk200MHz      => sysClk200,
         refClkLocked      => sysClkLocked,
         -- Atlas Clocks and trigger interface  (atlasClk160MHz domain)
         atlasTtcRxOut     => open,
         atlasClk40MHz     => open,
         atlasClk160MHz    => atlasClk160MHz,
         atlasClk160MHzEn  => open,
         atlasClk160MHzRst => atlasClk160MHzRst);   

   -- Create a reference locked signals from the reset
   sysClkLocked <= not(sysClk200Rst);
   dtm_locked <= calibdone;

   -- Select the data source
   emuData <= emuDataV1 when(emuVersionSelect = '0') else emuDataV2;

   -- Emulator Version 1
   U_AtlasTtcTxEmuV1 : entity work.AtlasTtcTxEmu
      generic map (
         TPD_G              => TPD_G,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         STATUS_CNT_WIDTH_G => 32)
      port map (
         -- AXI-Lite Register and Status Bus Interface (axiClk domain)
         axiClk         => axiClk,
         axiRst         => axiRst,
         axiReadMaster  => mAxiReadMasters(EMU_V1_AXI_INDEX_C),
         axiReadSlave   => mAxiReadSlaves(EMU_V1_AXI_INDEX_C),
         axiWriteMaster => mAxiWriteMasters(EMU_V1_AXI_INDEX_C),
         axiWriteSlave  => mAxiWriteSlaves(EMU_V1_AXI_INDEX_C),
         statusWords    => open,
         statusSend     => open,
         -- Emulation Trigger Signals
         emuClk         => atlasClk160MHz,  -- output of clock MUX
         emuRst         => atlasClk160MHzRst,
         emuData        => emuDataV1,
         emuBusy        => emuBusy);

   -- Emulator Version 2         
   U_AtlasTtcTxEmuV2 : entity work.AtlasTtcTxEmuV2
      generic map (
         TPD_G              => TPD_G,
         CLK_SELECT_G       => true,
         CASCADE_SIZE_G     => 75,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         STATUS_CNT_WIDTH_G => 32)
      port map (
         -- AXI-Lite Register and Status Bus Interface (axiClk domain)
         axiClk         => axiClk,
         axiRst         => axiRst,
         axiReadMaster  => mAxiReadMasters(EMU_V2_AXI_INDEX_C),
         axiReadSlave   => mAxiReadSlaves(EMU_V2_AXI_INDEX_C),
         axiWriteMaster => mAxiWriteMasters(EMU_V2_AXI_INDEX_C),
         axiWriteSlave  => mAxiWriteSlaves(EMU_V2_AXI_INDEX_C),
         statusWords    => open,
         statusSend     => open,
         -- Emulation Trigger Signals
         emuClk         => atlasClk160MHz,  -- output of clock MUX
         emuRst         => atlasClk160MHzRst,
         emuData        => emuDataV2,
         emuBusy        => emuBusy);         

   -- DPM Feedback Signals
   U_DpmFbGen : for i in 0 to 7 generate
      U_DpmFbIn : IBUFDS
         generic map (
            DIFF_TERM => true)
         port map(
            I  => dpmFbP(i),
            IB => dpmFbM(i),
            O  => busy(i));
   end generate;

   IBUFDS_2 : IBUFGDS
     generic map (
       DIFF_TERM => true)
     port map (
       I  => dtmToRtmLsP(0),
       IB => dtmToRtmLsM(0),
       O  => gbtclock120nb); 
   GBT_CLK_MR : BUFMR
     port map(
       I => gbtclock120nb,
       O => gbtout);
   GBT_CLK : BUFR
     port map(
       I => gbtout,
       CE => '1',
       CLR => '0',
       O => gbtclock120);
   GBT_CLK_IO : BUFIO
     port map(
       I => gbtout,
       O => gbtclock120out);
   -- DPM's MGT Clock
   DPM_MGT_CLK : entity work.ClkOutBufDiff
      port map(
         clkIn   => gbtclock120out,
         clkOutP => dpmClkP(0),         --DPM_CLK0_P
         clkOutN => dpmClkM(0));        --DPM_CLK0_M
--   clk_wiz_inst: entity work.clk_wiz_0
--     port map(
--       clk_in1 => gbtclock120,
--       clk_out1 => gbtout,
--       clkfb_in => clkfb,
--       clkfb_out => clkfb,
--       reset => not calibdone,
--       locked => open);
   -- DPM's FPGA Clock
   DPM_FPGA_CLK : entity work.ClkOutBufDiff
      port map(
         clkIn   => clk,
         clkOutP => dpmClkP(1),         --DPM_CLK1_P
         clkOutN => dpmClkM(1));        --DPM_CLK1_M 

   -- DPM's FPGA Data
   DPM_FPGA_DATA : OBUFDS
      port map (
         I  => data,
         O  => dpmClkP(2),              -- DPM_CLK2_P
         OB => dpmClkM(2));             --DPM_CLK2_M        

   -- Debug
   led(1) <= cdrHeartbeat;
   led(0) <= toggleL1Trig;

   U_Heartbeat : entity work.Heartbeat
      generic map (
         TPD_G        => TPD_G,
         USE_DSP48_G  => "no",
         PERIOD_IN_G  => getRealDiv(1, ATLAS_TTC_RX_CDR_CLK_FREQ_C),  --units of seconds
         PERIOD_OUT_G => 1.0E+0)                                      --units of seconds   
      port map(
         clk => atlasClk160MHz,
         o   => cdrHeartbeat);

   process(atlasClk160MHz)
   begin
      if rising_edge(atlasClk160MHz) then
         if atlasTtcRxOut.trigL1 = '1' then
            -- Toggle the LED every Level-1 Trigger
            if toggleL1Trig = '1' then
               toggleL1Trig <= '0' after TPD_G;
            else
               toggleL1Trig <= '1' after TPD_G;
            end if;
         end if;
      end if;
   end process;


   -- Generate local 160 MHz clock
   AtlasTtcTxEmuPll_Inst : AtlasTtcTxEmuPll
      port map (
         -- Clock in ports
         sysClk200MHz => sysClk200,
         -- Clock out ports  
         emuClk160MHz => locPllRef,
         -- Status and control signals                
         reset        => sysClk200Rst,
         locked       => open);   
         
   -- Backplane Clock Output
   BP_CLK_OUT : entity work.ClkOutBufSingle
      port map(
         clkIn  => locPllRef,
         clkOut => bpClkOut(0)); 
   bpClkOut(5 downto 1) <= (others => '0');         

   -- Backplane Clock Input
   emuClk <= bpClkIn(0);

end architecture rtl;
