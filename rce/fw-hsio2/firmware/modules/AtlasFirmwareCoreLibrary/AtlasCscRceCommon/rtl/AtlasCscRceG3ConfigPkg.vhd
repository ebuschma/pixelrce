-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscDpmTtcRxTest.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-06-03
-- Last update: 2014-06-05
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.RceG3Pkg.all;

package AtlasCscRceG3ConfigPkg is

   constant ATLAS_CSC_ETH_10G_EN_C   : boolean        := false;
   constant ATLAS_CSC_RCE_DMA_MODE_C : RceDmaModeType := RCE_DMA_AXIS_C;
   constant ATLAS_CSC_OLD_BSI_MODE_C : boolean        := true;
   
   constant ATLAS_CSC_AXIL_BASEADDR_C : slv(31 downto 0) := X"A0000000";   
   
end AtlasCscRceG3ConfigPkg;

