-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscDpmAsmPackEmuCore.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-06-03
-- Last update: 2014-08-15
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:  
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
use work.AtlasTtcRxPkg.all;

library unisim;
use unisim.vcomponents.all;

entity AtlasCscDpmAsmPackEmuCore is
   generic (
      TPD_G              : time             := 1 ns;
      AXIL_BASEADDR_G    : slv(31 downto 0) := X"A0000000";
      AXI_ERROR_RESP_G   : slv(1 downto 0) := AXI_RESP_OK_C;
      CASCADE_SIZE_G     : positive        := 1); 
   port (
      -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
      axiClk         : in  sl;
      axiRst         : in  sl;
      axiReadMaster  : in  AxiLiteReadMasterType;
      axiReadSlave   : out AxiLiteReadSlaveType;
      axiWriteMaster : in  AxiLiteWriteMasterType;
      axiWriteSlave  : out AxiLiteWriteSlaveType;
      statusSend     : out sl;
      statusWords    : out Slv64Array(0 to 1);
      -- Streaming RX Data Interface (sAxisClk domain) 
      sAxisClk       : in  sl;
      sAxisRst       : in  sl;
      sAxisMaster    : in  AxiStreamMasterType;
      sAxisSlave     : out AxiStreamSlaveType;
      -- Streaming TX Data Interface (sAxisClk domain) 
      mAxisClk       : in  sl;
      mAxisRst       : in  sl;
      mAxisMaster    : out AxiStreamMasterType;
      mAxisSlave     : in  AxiStreamSlaveType;      
      -- RTM High Speed
      dpmToRtmHsP    : out slv(11 downto 0);
      dpmToRtmHsM    : out slv(11 downto 0);
      rtmToDpmHsP    : in  slv(11 downto 0);
      rtmToDpmHsM    : in  slv(11 downto 0);
      -- DTM Signals
      dtmRefClkP     : in  sl;
      dtmRefClkM     : in  sl;
      dtmClkP        : in  slv(1 downto 0);
      dtmClkM        : in  slv(1 downto 0);
      dtmFbP         : out sl;
      dtmFbM         : out sl;
      -- Reference clocks
      sysClk200      : in  sl;
      sysClk200Rst   : in  sl;
      sysClk125      : in  sl;
      sysClk125Rst   : in  sl;
      -- Debug
      led            : out slv(1 downto 0));      
end AtlasCscDpmAsmPackEmuCore;

architecture rtl of AtlasCscDpmAsmPackEmuCore is

   constant NUM_AXI_MASTERS_C : natural := 2;

   constant TRIG_AXI_INDEX_C : natural := 0;
   constant EMU_AXI_INDEX_C  : natural := 1;

   constant TRIG_BASE_ADDR_C : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00000000");
   constant EMU_BASE_ADDR_C  : slv(31 downto 0) := (AXIL_BASEADDR_G + X"00001000");

   constant AXI_CROSSBAR_MASTERS_CONFIG_C : AxiLiteCrossbarMasterConfigArray(NUM_AXI_MASTERS_C-1 downto 0) := (
      TRIG_AXI_INDEX_C => (
         baseAddr      => TRIG_BASE_ADDR_C,
         addrBits      => 12,
         connectivity  => X"0001"),
      EMU_AXI_INDEX_C  => (
         baseAddr      => EMU_BASE_ADDR_C,
         addrBits      => 12,
         connectivity  => X"0001"));         

   signal mAxiWriteMasters : AxiLiteWriteMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiWriteSlaves  : AxiLiteWriteSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadMasters  : AxiLiteReadMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadSlaves   : AxiLiteReadSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);

   signal sysClkLocked,
      atlasClk40MHz,
      atlasClk160MHz,
      atlasClk160MHzEn,
      atlasClk160MHzRst,
      cdrHeartbeat,
      toggleL1Trig : sl;
   signal send : slv(1 downto 0);

   signal axisMaster : AxiStreamMasterType;
   signal axisSlave  : AxiStreamSlaveType;

   signal ttcRxIn : AtlasTTCRxOutType;

   attribute KEEP_HIERARCHY : string;
   attribute KEEP_HIERARCHY of
      U_AtlasTtcRx,
      U_AtlasAsmPackEmu : label is "TRUE";
   
begin

   statusSend <= uOr(send);

   -- AXI-Lite Crossbar
   AxiLiteCrossbar_Inst : entity work.AxiLiteCrossbar
      generic map (
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => NUM_AXI_MASTERS_C,
         DEC_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         MASTERS_CONFIG_G   => AXI_CROSSBAR_MASTERS_CONFIG_C)
      port map (
         axiClk              => axiClk,
         axiClkRst           => axiRst,
         sAxiWriteMasters(0) => axiWriteMaster,
         sAxiWriteSlaves(0)  => axiWriteSlave,
         sAxiReadMasters(0)  => axiReadMaster,
         sAxiReadSlaves(0)   => axiReadSlave,
         mAxiWriteMasters    => mAxiWriteMasters,
         mAxiWriteSlaves     => mAxiWriteSlaves,
         mAxiReadMasters     => mAxiReadMasters,
         mAxiReadSlaves      => mAxiReadSlaves);             

   -- TTC-RX Module
   U_AtlasTtcRx : entity work.AtlasTtcRx
      generic map (
         TPD_G              => TPD_G,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         IDELAY_VALUE_G     => toSlv(0, 5),
         STATUS_CNT_WIDTH_G => 32,
         IODELAY_GROUP_G    => "atlas_ttc_rx_delay_group",
         CASCADE_SIZE_G     => 1)     
      port map (
         -- External CDR Ports
         clkP              => dtmClkP(0),
         clkN              => dtmClkM(0),
         dataP             => dtmClkP(1),
         dataN             => dtmClkM(1),
         -- RMB Status Signals
         busyIn            => '0',
         busyP             => dtmFbP,
         busyN             => dtmFbM,
         -- AXI-Lite Register and Status Bus Interface (axiClk domain)
         axiClk            => axiClk,
         axiRst            => axiRst,
         axiReadMaster     => mAxiReadMasters(TRIG_AXI_INDEX_C),
         axiReadSlave      => mAxiReadSlaves(TRIG_AXI_INDEX_C),
         axiWriteMaster    => mAxiWriteMasters(TRIG_AXI_INDEX_C),
         axiWriteSlave     => mAxiWriteSlaves(TRIG_AXI_INDEX_C),
         statusWords(0)    => statusWords(0),
         statusSend        => send(0),
         -- Reference 200 MHz clock
         refClk200MHz      => sysClk200,
         refClkLocked      => sysClkLocked,
         -- Atlas Clocks and trigger interface  (atlasClk160MHz domain)
         atlasTtcRxOut     => ttcRxIn,
         atlasClk40MHz     => atlasClk40MHz,
         atlasClk160MHz    => atlasClk160MHz,
         atlasClk160MHzEn  => atlasClk160MHzEn,
         atlasClk160MHzRst => atlasClk160MHzRst);            

   -- Create the locked status signal
   sysClkLocked <= not(sysClk200Rst);

   -- SCA Emulation Module
   U_AtlasAsmPackEmu : entity work.AtlasAsmPackEmu
      generic map (
         TPD_G              => TPD_G,
         CASCADE_SIZE_G     => CASCADE_SIZE_G,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         STATUS_CNT_WIDTH_G => 32)
      port map (
         -- Streaming RX Data Interface (sAxisClk domain) 
         sAxisClk          => sAxisClk,
         sAxisRst          => sAxisRst,
         sAxisMaster       => sAxisMaster,
         sAxisSlave        => sAxisSlave,
         -- Streaming TX Data Interface (sAxisClk domain) 
         mAxisClk          => mAxisClk,
         mAxisRst          => mAxisRst,
         mAxisMaster       => mAxisMaster,
         mAxisSlave        => mAxisSlave,         
         -- AXI-Lite Register and Status Bus Interface (axiClk domain)
         axiClk            => axiClk,
         axiRst            => axiRst,
         axiReadMaster     => mAxiReadMasters(EMU_AXI_INDEX_C),
         axiReadSlave      => mAxiReadSlaves(EMU_AXI_INDEX_C),
         axiWriteMaster    => mAxiWriteMasters(EMU_AXI_INDEX_C),
         axiWriteSlave     => mAxiWriteSlaves(EMU_AXI_INDEX_C),
         statusWords(0)    => statusWords(1),
         statusSend        => send(1),
         -- GTX7 MGT I/O
         gtTxP             => dpmToRtmHsP,
         gtTxN             => dpmToRtmHsM,
         gtRxP             => rtmToDpmHsP,
         gtRxN             => rtmToDpmHsM,
         gtClkP            => dtmRefClkP,
         gtClkN            => dtmRefClkM,
         -- Global Signals
         stableClk         => sysClk125,
         atlasClk40MHz     => atlasClk40MHz,
         atlasClk160MHz    => atlasClk160MHz,
         atlasClk160MHzEn  => atlasClk160MHzEn,
         atlasClk160MHzRst => atlasClk160MHzRst);  

   -- Debug
   led(1) <= cdrHeartbeat;
   led(0) <= toggleL1Trig;

   U_Heartbeat : entity work.Heartbeat
      generic map (
         TPD_G        => TPD_G,
         USE_DSP48_G  => "no",
         PERIOD_IN_G  => getRealDiv(1, ATLAS_TTC_RX_CDR_CLK_FREQ_C),  --units of seconds
         PERIOD_OUT_G => 1.0E+0)                                      --units of seconds   
      port map(
         clk => atlasClk160MHz,
         o   => cdrHeartbeat);

   process(atlasClk160MHz)
   begin
      if rising_edge(atlasClk160MHz) then
         if ttcRxIn.trigL1 = '1' then
            -- Toggle the LED every Level-1 Trigger
            if toggleL1Trig = '1' then
               toggleL1Trig <= '0' after TPD_G;
            else
               toggleL1Trig <= '1' after TPD_G;
            end if;
         end if;
      end if;
   end process;

end architecture rtl;
