-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscBusyCnt.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-06-30
-- Last update: 2016-11-25
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;

library unisim;
use unisim.vcomponents.all;

entity AtlasCscBusyCnt is
   generic (
      TPD_G : time := 1 ns);
   port (
      -- Trigger Signals
      bc              : in  AtlasTTCRxBcType;
      invertOutput    : in  sl;
      forceBusy       : in  sl;
      ignoreExtBusyIn : in  slv(7 downto 0);
      busyIn          : in  slv(7 downto 0);
      busyOut         : out sl;
      dataIn          : in  sl; -- test
      busyP           : out sl;         -- RMB's busy LEMO interface
      busyN           : out sl;         -- RMB's busy LEMO interface
      busyRateRst     : in  sl;
      busyRateCnt     : out slv(31 downto 0);
      busyRate        : out slv(31 downto 0);
      -- Global Signals
      locClk          : in  sl;
      locClkEn        : in  sl;
      locRst          : in  sl);   
end AtlasCscBusyCnt;

architecture rtl of AtlasCscBusyCnt is

   constant MAX_CNT_C : slv(31 downto 0) := (others => '1');

   type RegType is record
      cnt      : slv(31 downto 0);
      busyRate : slv(31 downto 0);
   end record;
   
   constant REG_INIT_C : RegType := (
      (others => '0'),
      (others => '0')); 

   signal r   : RegType := REG_INIT_C;
   signal rin : RegType;

   signal busy      : sl;
   signal busyXor   : sl;
   signal busyInVec : slv(7 downto 0);
   
begin

   comb : process (bc, busy, busyRateRst, locClkEn, locRst, r) is
      variable v : RegType;
   begin

      -- Latch the current value
      v := r;

      -- Check if busy is active
      if (busy = '1') and (locClkEn = '1') then
         if r.cnt /= MAX_CNT_C then
            v.cnt := r.cnt + 1;
         end if;
      end if;

      -- Check if we need to reset the busyRate integrator
      if (bc.valid = '1') and (bc.cmdData(1) = '1') then
         -- Latch the counter value
         v.busyRate := r.cnt;
         -- Reset the counter
         v.cnt      := (others => '0');
      end if;

      -- Check for register reset of accumulator 
      if busyRateRst = '1' then
         -- Reset the counter
         v.cnt := (others => '0');
      end if;

      -- Synchronous Reset
      if locRst = '1' then
         v := REG_INIT_C;
      end if;

      -- Register the variable for next clock cycle
      rin <= v;

      -- Outputs
      busyRateCnt <= r.cnt;
      busyRate    <= r.busyRate;
      
   end process comb;

   seq : process (locClk) is
   begin
      if rising_edge(locClk) then
         r <= rin after TPD_G;
      end if;
   end process seq;

   GEN_BUSY_IN :
   for i in 0 to 7 generate
      busyInVec(i) <= busyIn(i) and not(ignoreExtBusyIn(i));
   end generate GEN_BUSY_IN;

   busy    <= uOr(busyInVec) or forceBusy;
   busyOut <= busy;
   busyXor <= busy xor invertOutput;

   OBUFDS_Inst : OBUFDS
      port map (
   --      I  => busyXor,
         I  => dataIn,
         O  => busyP,
         OB => busyN);

end rtl;
