-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasCscBusy.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-06-30
-- Last update: 2017-08-01
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AtlasTtcRxPkg.all;
use work.AtlasCscBusyPkg.all;

entity AtlasCscBusy is
   generic (
      TPD_G              : time                  := 1 ns;
      STATUS_CNT_WIDTH_G : natural range 1 to 32 := 32;
      AXI_ERROR_RESP_G   : slv(1 downto 0)       := AXI_RESP_SLVERR_C;
      IDELAY_VALUE_G     : slv(4 downto 0)       := toSlv(0, 5);
      IODELAY_GROUP_G    : string                := "atlas_csc_busy_delay_group");      
   port (
      -- External CDR Ports
      clkP              : in  sl;       -- From ADN2816 IC
      clkN              : in  sl;       -- From ADN2816 IC
      clk               : out sl;       -- Copy of the ADN2816 IC's clock
      gbtclock120       : in  sl;
      dataP             : in  sl;       -- From ADN2816 IC
      dataN             : in  sl;       -- From ADN2816 IC
      data              : out sl;       -- Copy of the ADN2816 IC's clock
      lostLink          : in  sl := '0';  -- From ADN2816 IC, differential copy of LOL (Optional External Ports) 
      sigDetP           : in  sl := '0';  -- From Fiber Optic Module (Optional External Ports)
      sigDetN           : in  sl := '1';  -- From Fiber Optic Module (Optional External Ports)
      -- I2C to RMB
      ttc_sda           : inout sl;
      ttc_scl           : inout sl;
      -- RMB Status Signals
      busyIn            : in  slv(7 downto 0);  -- FPGA fabric input busy signal
      busyP             : out sl;       -- RMB's busy LEMO interface
      busyN             : out sl;       -- RMB's busy LEMO interface
      locked            : out sl;       -- RMB's LED Display
      -- AXI-Lite Register and Status Bus Interface (axiClk domain)
      axiClk            : in  sl;
      axiRst            : in  sl;
      axiReadMaster     : in  AxiLiteReadMasterType;
      axiReadSlave      : out AxiLiteReadSlaveType;
      axiWriteMaster    : in  AxiLiteWriteMasterType;
      axiWriteSlave     : out AxiLiteWriteSlaveType;
      statusWords       : out Slv64Array(0 to 0);
      statusSend        : out sl;
      -- Emulation Trigger Signals
      emuClk            : in  sl;
      emuData           : in  sl;
      emuBusy           : out sl;
      emuVersionSelect  : out sl;
      -- Reference 200 MHz clock
      refClk200MHz      : in  sl;
      refClkLocked      : in  sl;
      -- Atlas Clocks and trigger interface  (atlasClk160MHz domain)
      atlasTtcRxOut     : out AtlasTTCRxOutType;
      atlasClk40MHz     : out sl;
      atlasClk80MHz     : out sl;
      atlasClk160MHz    : out sl;
      atlasClk160MHzEn  : out sl;       -- phased up with time mux'd CHA
      atlasClk160MHzRst : out sl);
end AtlasCscBusy;

architecture mapping of AtlasCscBusy is
   
   signal locClk,
      locClkEn,
      locRst,
      clkSync,
      bcValid,
      serDataRising,
      locRstReg,
      resetclockbuf,
      clockbufdone,
      resetdoneclockbuf,
      serDataFalling : sl;
   signal initclockbuf : sl:='0';
   signal oldAxiRst: sl:='0';
   signal bcCheck : slv(4 downto 0);
   signal bcData  : slv(7 downto 0);
   signal config  : AtlasCscBusyConfigType := ATLAS_CSC_BUSY_CONFIG_INIT_C;
   signal status  : AtlasCscBusyStatusType := ATLAS_CSC_BUSY_STATUS_INIT_C;

   attribute KEEP_HIERARCHY                             : string;
   attribute KEEP_HIERARCHY of AtlasTtcRxCdrInputs_Inst : label is "TRUE";
   
begin

   ----------
   -- Outputs 
   ----------
   atlasTtcRxOut     <= status.ttcRx;
   atlasClk160MHz    <= locClk;
   atlasClk160MHzEn  <= locClkEn;
   atlasClk160MHzRst <= locRst;
   emuBusy           <= status.busyOut;
   emuVersionSelect  <= config.emuVersionSelect;

   -------------------------------------
   -- Start RMB zero delay clock buffer.
   -------------------------------------
   process 
   begin
     wait until rising_edge(axiClk);
     oldAxiRst<=axiRst;
     if(axiRst='0' and oldAxiRst='1')then
       initclockbuf<='1';
     else
       initclockbuf<='0';
     end if;
   end process;
   ttcclockbuf: entity work.clockbuf
    generic map( PRESCALE_G => 120,
                 CONFIG_G => "XTL120_160_COB")
    port map(
      clk => axiClk,
      xtalon => initclockbuf,
      fail => open,
      done => clockbufdone,
      reset => resetclockbuf,
      resetdone => resetdoneclockbuf,
      scl => ttc_scl,
      sda => ttc_sda
    );
     calibClockbuf: entity work.calibrateClockbufPhase
       generic map (PHASE_G => "10")
       port map(
         sysClk40 => axiClk,
         clock160 => locClk,
         clock120 => gbtclock120,
         doneclockbuf => clockbufdone,
         bpmlocked => status.bpmLocked,
         resetclockbuf => resetclockbuf,
         resetdoneclockbuf => resetdoneclockbuf,
         clken => locClkEn,
         done => locked,
         counters_dbg => open);
   ---------
   -- Sync 
   ---------
   SyncIn_Busy : entity work.SynchronizerVector
      generic map (
         TPD_G   => TPD_G,
         WIDTH_G => 8)
      port map (
         clk     => locClk,
         dataIn  => busyIn,
         dataOut => status.busyIn);  

   -- Sync refClkLocked in the AtlasTtcRxReg's SyncStatusVector module
   status.refClkLocked <= refClkLocked;

   ------------------------------------------------------------
   -- Configuration/Status Register and FIFO for Trigger Events
   ------------------------------------------------------------
   AtlasCscBusyReg_Inst : entity work.AtlasCscBusyReg
      generic map (
         TPD_G              => TPD_G,
         STATUS_CNT_WIDTH_G => STATUS_CNT_WIDTH_G,
         AXI_ERROR_RESP_G   => AXI_ERROR_RESP_G,
         IDELAY_VALUE_G     => IDELAY_VALUE_G)      
      port map (
         -- Status Bus (axiClk domain)
         statusWords    => statusWords,
         statusSend     => statusSend,
         -- AXI-Lite Register Interface (axiClk domain)
         axiReadMaster  => axiReadMaster,
         axiReadSlave   => axiReadSlave,
         axiWriteMaster => axiWriteMaster,
         axiWriteSlave  => axiWriteSlave,
         -- Configuration and Status Interface (Mixed domains - refer to AtlasCscBusyPkg)
         config         => config,
         status         => status,
         -- Global Signals
         refClk200MHz   => refClk200MHz,
         axiClk         => axiClk,
         axiRst         => axiRst,
         locClk         => locClk,
         locRst         => locRstReg);   
   locRst<=locRstReg or resetclockbuf;
   ----------------------------------
   -- Clock/Data Recovery (CDR)Inputs 
   ---------------------------------- 
   AtlasTtcRxCdrInputs_Inst : entity work.AtlasTtcRxCdrInputs
      generic map (
         TPD_G           => TPD_G,
         IODELAY_GROUP_G => IODELAY_GROUP_G,
         XIL_DEVICE_G    => "7SERIES")   
      port map (
         -- CDR Signals
         clkP           => clkP,
         clkN           => clkN,
         clk            => clk,
         dataP          => dataP,
         dataN          => dataN,
         data           => data,
         -- Emulation Trigger Signals
         emuSel         => config.emuSel,
         clkSel         => config.clkSel,
         emuClk         => emuClk,
         emuData        => emuData,
         -- Serial Data Signals
         serDataRising  => serDataRising,
         serDataFalling => serDataFalling,
         -- Delay CTRL (refClk200MHz domain)
         delayIn        => config.delayIn,
         delayOut       => status.delayOut,
         -- Clock Signals
         refClk200MHz   => refClk200MHz,
         clkSync        => clkSync,
         locClk40MHz    => atlasClk40MHz,
         locClk80MHz    => atlasClk80MHz,
         locClk160MHz   => locClk);    

   -------------------
   -- Clock Monitoring
   -------------------
   AtlasTtcRxClkMon_Inst : entity work.AtlasTtcRxClkMon
      generic map (
         TPD_G             => TPD_G,
         EN_LOL_PORT_G     => false,
         EN_SIG_DET_PORT_G => false)    
      port map (
         -- Status Monitoring
         clkLocked       => status.clkLocked,
         freqLocked      => status.freqLocked,
         cdrLocked       => status.cdrLocked,
         sigLocked       => status.sigLocked,
         freqMeasured    => status.freqMeasured,
         ignoreSigLocked => config.ignoreSigLocked,
         ignoreCdrLocked => config.ignoreCdrLocked,
         lockedOut       => open,
         -- Optional External Ports
         lostLink        => lostLink,
         sigDetP         => sigDetP,
         sigDetN         => sigDetN,
         -- Global Signals
         refClk200MHz    => refClk200MHz,
         locClk          => locClk,
         locRst          => locRst);    
   ------------------------------------------------
   -- Time Demultiplexer and Deserialization Module
   ------------------------------------------------
   AtlasTtcRxDeSer_Inst : entity work.AtlasTtcRxDeSer
      generic map (
         TPD_G => TPD_G)    
      port map (
         -- Serial Data Signals
         serDataEdgeSel => config.serDataEdgeSel,
         serDataRising  => serDataRising,
         serDataFalling => serDataFalling,
         -- Level-1 Trigger
         trigL1         => status.ttcRx.trigL1,
         -- BC Encoded Message
         bcValid        => bcValid,
         bcData         => bcData,
         bcCheck        => bcCheck,
         -- IAC Encoded Message
         iacValid       => open,
         iacData        => open,
         iacCheck       => open,
         -- Status Monitoring
         clkLocked      => status.clkLocked,
         bpmLocked      => status.bpmLocked,
         bpmErr         => status.bpmErr,
         deSerErr       => status.deSerErr,
         -- Clock Signals
         clkSync        => clkSync,
         locClkEn       => locClkEn,
         locClk         => locClk,
         locRst         => locRst);       

   -----------------------     
   -- BC's Hamming Decoder
   -----------------------     
   AtlasTtcRxDecodeBc_Inst : entity work.AtlasTtcRxDecodeBc
      generic map (
         TPD_G                => TPD_G,
         BYPASS_ERROR_CHECK_G => false)   
      port map (
         -- Encoded Message Input      
         validIn   => bcValid,
         dataIn    => bcData,
         checkIn   => bcCheck,
         -- Decoded Message Output
         bc        => status.ttcRx.bc,
         sBitErrBc => status.sBitErrBc,
         dBitErrBc => status.dBitErrBc,
         -- Global Signals
         locClk    => locClk,
         locRst    => locRst);          

   ----------------
   -- OR-ing module
   ----------------
   AtlasCscBusyCnt_Inst : entity work.AtlasCscBusyCnt
      generic map (
         TPD_G => TPD_G)   
      port map (
         -- Trigger Signals
         bc              => status.ttcRx.bc,
         invertOutput    => config.invertOutput,
         forceBusy       => config.forceBusy,
         ignoreExtBusyIn => config.ignoreExtBusyIn,
         busyIn          => status.busyIn,
         busyOut         => status.busyOut,
         dataIn          => serDataRising,
         busyP           => busyP,
         busyN           => busyN,
         busyRateRst     => config.busyRateRst,
         busyRateCnt     => status.busyRateCnt,
         busyRate        => status.busyRate,
         -- Global Signals
         locClkEn        => locClkEn,
         locClk          => locClk,
         locRst          => locRst);      

end mapping;
