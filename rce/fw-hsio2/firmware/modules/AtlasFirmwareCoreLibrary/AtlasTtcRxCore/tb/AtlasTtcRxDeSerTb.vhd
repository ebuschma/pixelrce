-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2015-01-07
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity AtlasTtcRxDeserTb is end AtlasTtcRxDeserTb;

architecture testbed of AtlasTtcRxDeserTb is
   signal clk  : sl;
   signal rst  : sl;
   signal trigL1, bcValid, bcData, bcCheck, iacValid, iacData, iacCheck, bpmLocked, bpmErr, deSerErr, clkSync, locClkEn : sl;
   signal counter: integer range 0 to 15 := 0;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 50 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);


   AtlasTtcRxDeSer_Inst : entity work.AtlasTtcRxDeSer
      generic map (
         TPD_G => 1 ns)    
      port map (
         -- Serial Data Signals
         serDataEdgeSel => '0',
         serDataRising  => '0',
         serDataFalling => '0',
         -- Level-1 Trigger
         trigL1         => trigL1,
         -- BC Encoded Message
         bcValid        => bcValid,
         bcData         => open,
         bcCheck        => open,
         -- IAC Encoded Message
         iacValid       => iacValid,
         iacData        => open,
         iacCheck       => open,
         -- Status Monitoring
         clkLocked      => '1',
         bpmLocked      => bpmLocked,
         bpmErr         => bpmErr,
         deSerErr       => deSerErr,
         -- Clock Signals
         clkSync        => clkSync,
         locClkEn       => locClkEn,
         locClk         => clk,
         locRst         => rst);       

end testbed;
