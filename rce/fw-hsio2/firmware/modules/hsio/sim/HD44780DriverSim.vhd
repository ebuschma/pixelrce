library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.i2cpkg.all;
use work.all;
use work.I2cRegPCA9535DriverPkg.all;
use work.HD44780DriverPkg.all;

entity HD44780DriverSim is
end HD44780DriverSim;

architecture HD44780DriverSim of HD44780DriverSim is
  signal displayI2cIn:  i2c_in_type;
  signal displayI2cOut: i2c_out_type;

  signal displayI2cRegMasterIn: I2cRegMasterInType;
  signal displayI2cRegMasterOut: I2cRegMasterOutType;
    
  signal I2cRegPCA9535DriverOut: I2cRegPCA9535DriverOutType;
  signal I2cRegPCA9535DriverIn:  I2cRegPCA9535DriverInType;

  signal bv:   std_logic := '0';
  signal init: std_logic := '0';
  signal en:   std_logic := '0';

  signal HD44780GpioIn:  HD44780GpioInType := HD44780DRIVER_GPIO_IN_INIT;
  signal HD44780GpioOut: HD44780GpioOutType;
  signal HD44780En: std_logic := '0';
  
  signal  clk:           std_logic := '0';
  signal  rst:           std_logic := '0';
  signal  displayI2cSda: std_logic;
  signal  displayI2cScl: std_logic;

begin
  displayI2cSda    <= displayI2cOut.sda when displayI2cOut.sdaoen = '0' else 'Z';
  displayI2cIn.sda <= to_x01z(displayI2cSda);
  displayI2cScl    <= displayI2cOut.scl when displayI2cOut.scloen = '0' else 'Z';
  displayI2cIn.scl <= to_x01z(displayI2cScl);

  process begin
    wait for 2 ns;
    clk <= not clk;
  end process;

  --process begin
  --  wait until rising_edge(clk);
  --  HD44780GpioIn.ack  <= '0';
  --  HD44780GpioIn.fail <= '0';
  --  I2cRegPCA9535DriverOut.ack <= '0';
  --  if (init = '0') then
  --    if (I2cRegPCA9535DriverIn.configure = '0') then
  --      I2cRegPCA9535DriverIn.configure <= '1';
  --    else
  --      I2cRegPCA9535DriverIn.configure <= '0';
  --      init <= '1';
  --    end if;
  --  else
  --    if (I2cRegPCA9535DriverIn.write = '1') then
  --      I2cRegPCA9535DriverIn.write <= '0';
  --      I2cRegPCA9535DriverOut.ack <= '1';
  --      I2cRegPCA9535DriverOut.fail <= '0';
  --    elsif (I2cRegPCA9535DriverIn.configure = '1') then
  --      I2cRegPCA9535DriverIn.configure <= '0';
  --    else
  --      HD44780En <= '1';
  --      HD44780GpioIn.ack <= I2cRegPCA9535DriverOut.ack;
  --      if HD44780GpioOut.update = '1' then
  --        I2cRegPCA9535DriverIn.write <= '1';
  --        I2cRegPCA9535DriverIn.writeValue <= HD44780GpioOut.db & "00000" 
  --          & HD44780GpioOut.e & HD44780GpioOut.rw & HD44780GpioOut.rs;
  --      end if;
  --    end if;
  --  end if;
  --end process;
  process begin
    wait until rising_edge(clk);
      I2cRegPCA9535DriverOut.fail <= '0';
    if I2cRegPCA9535DriverIn.write = '1' then
      I2cRegPCA9535DriverOut.busy <= '1';
    elsif I2cRegPCA9535DriverIn.configure = '1' then
      I2cRegPCA9535DriverOut.busy <= '1';
    end if;
    if I2cRegPCA9535DriverOut.busy = '1' then
      I2cRegPCA9535DriverOut.busy <= '0';
      I2cRegPCA9535DriverOut.ack <= '1';
    else
      I2cRegPCA9535DriverOut.ack <= '0';
    end if;
  end process;
  process begin
    wait until rising_edge(clk);
    HD44780GpioIn.ack  <= '0';
    HD44780GpioIn.fail <= '0';
    if (init = '0') then
      if (I2cRegPCA9535DriverIn.configure = '0') then
        I2cRegPCA9535DriverIn.configure <= '1';
      else
        I2cRegPCA9535DriverIn.configure <= '0';
        init <= '1';
      end if;
    else
      if (I2cRegPCA9535DriverIn.write = '1') then
        I2cRegPCA9535DriverIn.write <= '0';
      elsif (I2cRegPCA9535DriverOut.busy = '1') then
        I2cRegPCA9535DriverIn.write <= '0';
      else
        HD44780En <= '1';
        HD44780GpioIn.ack <= I2cRegPCA9535DriverOut.ack;
        HD44780GpioIn.fail <= I2cRegPCA9535DriverOut.fail;
        if HD44780GpioOut.update = '1' then
          I2cRegPCA9535DriverIn.write <= '1';
          I2cRegPCA9535DriverIn.writeValue <= HD44780GpioOut.db & "00000" 
            & HD44780GpioOut.e & HD44780GpioOut.rw & HD44780GpioOut.rs;
        end if;
      end if;
    end if;
  end process;

  HD44780Driver_inst: entity work.HD44780Driver
  generic map
  (
    init_reset_count0 => 25,
    init_reset_count1 => 10,
    init_reset_count2 => 10
  )
  port map
  (
    clk => clk,
    rst => '0',
    en => HD44780En,

    GpioIn => HD44780GpioIn,
    GpioOut => HD44780GpioOut

    --DisplayBuffer: in HD44780DriverDisplayBufferType := (others => x"41");
  );

end HD44780DriverSim;

