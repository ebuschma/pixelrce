library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.all;
use work.HD44780DriverPkg.all;

entity HD44780Driver is
  generic
  (
    init_reset_count0: natural;
    init_reset_count1: natural;
    init_reset_count2: natural;
    display_width: natural := 20
  );
  port 
  (
    clk: in std_logic;
    rst: in std_logic := '0';
    en:  in std_logic := '1';

    GpioIn:  in  HD44780GpioInType;
    GpioOut: out HD44780GpioOutType := HD44780DRIVER_GPIO_OUT_INIT;

    DisplayBuffer: in HD44780DriverDisplayBufferType := (others => x"41")
  );
end HD44780Driver;

architecture HD44780Driver of HD44780Driver is
  subtype reset_counter_t is natural range 0 to init_reset_count0;
  signal reset_counter: reset_counter_t := reset_counter_t'high;

  signal wait_for_counter: std_logic := '1';
  signal wait_for_cycle: std_logic := '0';
  
  type cycle_fsm_state_t is (
    data,
    clock_high,
    clock_low
  );

  signal cycle_fsm_state: cycle_fsm_state_t := data;

  type fsm_state_t is (
    reset, 
    init_r1, init_r2, init_r3,
    init_display_on, init_display_entry_mode, init_display_clear,
    set_display_line, write_char
  );
  signal fsm_state: fsm_state_t := reset;
  signal current_line: std_logic := '1';
  constant DISPLAY_COUNTER_WIDTH: natural := 5;
  signal current_row: unsigned(DISPLAY_COUNTER_WIDTH - 1 downto 0) := to_unsigned(0, DISPLAY_COUNTER_WIDTH);
begin

  process begin
    wait until rising_edge(clk) and en = '1';
    GpioOut.update <= '0';
  
    if (rst = '1') then
      fsm_state <= reset;
      cycle_fsm_state <= data;
      current_line <= '1';
      current_row <= to_unsigned(0, DISPLAY_COUNTER_WIDTH);
      wait_for_counter <= '1';
      wait_for_cycle <= '0';
    elsif (wait_for_cycle = '1') then
      if (GpioIn.ack = '1') then
        case cycle_fsm_state is
          when data =>
            GpioOut.e <= '1';
            GpioOut.update <= '1';
            cycle_fsm_state <= clock_high;
          when clock_high =>
            GpioOut.e <= '0';
            GpioOut.update <= '1';
            cycle_fsm_state <= clock_low;
          when clock_low =>
            wait_for_cycle <= '0';
            cycle_fsm_state <= data;
        end case;
      elsif (GpioIn.fail = '1') then
        wait_for_cycle <= '0';
        fsm_state <= reset;
        cycle_fsm_state <= data;
      end if;
    elsif (wait_for_counter = '1') then
      if (reset_counter = 0) then
        wait_for_counter <= '0';
      else
        reset_counter <= reset_counter - 1;
      end if;
    else
      case fsm_state is 
        when reset =>
          reset_counter <= init_reset_count0;
          wait_for_counter <= '1';
          fsm_state <= init_r1;
        when init_r1 =>
          GpioOut.db <= "00111000";
          GpioOut.rs <= '0';
          GpioOut.rw <= '0';
          GpioOut.update <= '1';
          wait_for_cycle <= '1';
          wait_for_counter <= '1';
          reset_counter <= init_reset_count1;
          fsm_state <= init_r2;
        when init_r2 =>
          GpioOut.update <= '1';
          wait_for_cycle <= '1';
          wait_for_counter <= '1';
          reset_counter <= init_reset_count2;
          fsm_state <= init_r3;
        when init_r3 =>
          GpioOut.update <= '1';
          wait_for_cycle <= '1';
          fsm_state <= init_display_on;
        when init_display_on =>
          GpioOut.db <= "00001100";
          GpioOut.update <= '1';
          wait_for_cycle <= '1';
          fsm_state <= init_display_clear;
        when init_display_clear =>
          GpioOut.db <= "00000001";
          GpioOut.update <= '1';
          wait_for_cycle <= '1';
          fsm_state <= init_display_entry_mode;
        when init_display_entry_mode =>
          GpioOut.db <= "00000110";
          GpioOut.update <= '1';
          wait_for_cycle <= '1';
          fsm_state <= set_display_line;
        when set_display_line =>
          current_line <= not current_line;
          GpioOut.rs <= '0';
          GpioOut.rw <= '0';
          GpioOut.db <= "1" & (not current_line) & "000000";
          GpioOut.update <= '1';
          wait_for_cycle <= '1';
          current_row <= to_unsigned(0, DISPLAY_COUNTER_WIDTH);
          fsm_state <= write_char;
        when write_char =>
          GpioOut.rs <= '1';
          GpioOut.rw <= '0';
          if (current_line = '1') then
            GpioOut.db <= DisplayBuffer(to_integer(current_row) + display_width);
          else
            GpioOut.db <= DisplayBuffer(to_integer(current_row));
          end if;
          GpioOut.update <= '1';
          wait_for_cycle <= '1';
          if (current_row = display_width - 1) then
            fsm_state <= set_display_line;
          end if;
          current_row <= current_row + 1;
      end case;
    end if;
  end process;
end HD44780Driver;
