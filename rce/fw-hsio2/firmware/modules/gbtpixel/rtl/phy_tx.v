
// Modified by Pedro Leitao to sample PHY_TX with txclk40 and PHY_RX with rxclk40


// HDL file - FPGA_GBTX, PHY_HDLC_tx_FPGA, functional.

//Verilog HDL for "FPGA_GBTX", "PHY_HDLC_tx_FPGA" "functional"


module PHY_HDLC_tx_FPGA (tx_data, 
				tx_dvalid,
				 
				tx_dstrobe,
				 
				tx_sd, 
				rx_clk, 
				resetb,
				 
				forceSCFSM_reset 
				);

	input [7:0] tx_data;
	input resetb;
			
	input rx_clk;
	input tx_dvalid;
	
	output [1:0] tx_sd;
	
	output tx_dstrobe;
	
	input forceSCFSM_reset;
	
	//////////////////////////////////////////////////////////////////////
	reg [8:0] tx_reg_o;
	wire [8:0] tx_reg_i;
	
	reg tx_sdn_o;
	wire tx_sdn_i;
	
	reg tx_sdp_o;
	wire tx_sdp_i;
	
	reg [2:0] ones_count_o;
	wire [2:0] ones_count_i;
	
	reg [3:0] bit_count_o;
	wire [3:0] bit_count_i;
	
	reg [1:0] state_o;
	wire [1:0] state_i;
	
	reg tx_dstrobe_o;
	wire tx_dstrobe_i;
	
	assign tx_sd[1] = forceSCFSM_reset ? 1'b1 : tx_sdp_i;
	assign tx_sd[0] = forceSCFSM_reset ? 1'b1 : tx_sdn_i;
	
	parameter [1:0] IDLE = 2'd0,
			START= 2'd1,
			TX   = 2'd2,
			END  = 2'd3;
	//////////////////////////////////////////////////////////////////////
	// 
	assign #1 tx_reg_i = 	tx_reg_o;
	
	assign #1 tx_sdp_i = 	tx_sdp_o;
				
	assign #1 tx_sdn_i = 	tx_sdn_o;
				
	assign #1 ones_count_i = ones_count_o;
	
	assign #1 bit_count_i =	bit_count_o;
	
	assign #1 state_i =	state_o;
				
	assign #1 tx_dstrobe_i = tx_dstrobe_o;
				
	assign #1 tx_dstrobe = tx_dstrobe_i;
	
	always @(posedge rx_clk or negedge resetb) begin
		if (~resetb) begin
			tx_reg_o<=#1 0;
			tx_sdp_o <=#1 0;
			tx_sdn_o <=#1 0;
			ones_count_o <=#1 0;
			bit_count_o <=#1 0;
			state_o <=#1 0;
			tx_dstrobe_o <=#1 0;
		end
		else begin
			state_o <=#1 state_i;
			bit_count_o <=#1 bit_count_i;
			ones_count_o <=#1 ones_count_i;
			tx_sdn_o <=#1 tx_sdn_i;
			tx_sdp_o <=#1 tx_sdp_i;
			tx_reg_o <=#1 tx_reg_i;

			tx_dstrobe_o <=#1 0;
		
			if (tx_reg_i[0] && tx_reg_i[1]) begin
				if (ones_count_i<6) ones_count_o <=#1 ones_count_i + 2;
				if (ones_count_i==6) ones_count_o <=#1 7;
			end
			else begin
				if (~tx_reg_i[1]) ones_count_o <=#1 0;
				else ones_count_o <=#1 1;
			end

			case (state_i)
				IDLE: begin
					tx_sdn_o<=#1 1;
					tx_sdp_o<=#1 1;
					tx_reg_o[7:0] <=#1 'h7e;
					bit_count_o <=#1 8;
					if (tx_dvalid) state_o <=#1 START;
				end
				
				START: begin
					bit_count_o <=#1 bit_count_i - 2;
					tx_reg_o <=#1 tx_reg_i>>2;
					tx_sdp_o <=#1 tx_reg_i[1];
					tx_sdn_o <=#1 tx_reg_i[0];
					if (bit_count_i==2) begin
						state_o <=#1 TX;
						tx_reg_o[7:0] <=#1 tx_data;
						tx_dstrobe_o <=#1 1;
						bit_count_o<=#1 8;
					end
				end
			
				TX: begin				
					tx_reg_o <=#1 tx_reg_i>>2;
					tx_sdp_o <=#1 tx_reg_i[1];
					tx_sdn_o <=#1 tx_reg_i[0];
					bit_count_o <=#1 bit_count_i - 2;
					
					if ((ones_count_i!=5) && ((ones_count_o!=4) || (~tx_reg_o[0]))) begin
						if (bit_count_o==3) begin
							tx_dstrobe_o <=#1 1;
							if (tx_dvalid) begin
								bit_count_o <=#1 9;
								tx_reg_o[8:1] <=#1 tx_data;
							end
							else begin
								bit_count_o <=#1 1;
								tx_reg_o[8:1] <=#1 'h7e;
							end
						end
						if (bit_count_i==2) begin
							bit_count_o <=#1 8;
							tx_dstrobe_o <=#1 1;
							if (tx_dvalid) begin
								tx_reg_o[7:0] <=#1 tx_data;
							end
							else begin
								tx_reg_o[8:0] <=#1 'b101111110;
								state_o <=#1 END;
							end
						end
					end

					
					if ((ones_count_i==4) && tx_reg_o[0]) begin
						tx_sdp_o <=#1 0;
						tx_sdn_o <=#1 1;
						tx_reg_o <=#1 tx_reg_i>>1;
						ones_count_o <=#1 0;
						bit_count_o <=#1 bit_count_i - 1;
						if (bit_count_i==2) begin
							bit_count_o <=#1 9;
							tx_dstrobe_o <=#1 1;
							if (tx_dvalid) begin
								tx_reg_o[8:1] <=#1 tx_data;
							end
							else begin
								tx_reg_o[8:1] <=#1 'h7e;
								state_o <=#1 END;
							end
						end
					end
					
					if (ones_count_i==5) begin
						tx_sdp_o <=#1 tx_reg_i[0];
						tx_sdn_o <=#1 0;
						tx_reg_o <=#1 tx_reg_i>>1;
						ones_count_o <=#1 tx_reg_i[0];
						bit_count_o <=#1 bit_count_i - 1;
						if (bit_count_i==2) begin
							bit_count_o <=#1 9;
							tx_dstrobe_o <=#1 1;
							if (tx_dvalid) begin
								tx_reg_o[8:1] <=#1 tx_data;
							end
							else begin
								tx_reg_o[8:1] <=#1 'h7e;
								state_o <=#1 END;
							end
						end
					end
					
					if (bit_count_i==1) begin
						state_o <=#1 END;
						bit_count_o <=#1 7;
					end
				end
				
				END: begin
					tx_reg_o <=#1 tx_reg_i>>2;
					tx_sdp_o <=#1 tx_reg_i[1];
					tx_sdn_o <=#1 tx_reg_i[0];
					bit_count_o <=#1 bit_count_i - 2;
					
					if ((ones_count_i==5) && (tx_reg_o[0])) begin
						tx_sdp_o<=#1 0;
						if (tx_dvalid) begin
							state_o <=#1 TX;
							tx_reg_o[7:0] <=#1 tx_data;
							tx_dstrobe_o <=#1 1;
							bit_count_o<=#1 8;
						end
						else state_o <=#1 IDLE;
					end

					if (ones_count_i==6) begin
						tx_sdn_o<=#1 0;
						if (tx_dvalid) begin
							state_o <=#1 TX;
							tx_reg_o[6:0] <=#1 tx_data[7:1];
							tx_sdp_o <=#1 tx_data[0];
							tx_dstrobe_o <=#1 1;
							bit_count_o<=#1 7;
							ones_count_o <=#1 tx_data[0];
						end
						else begin
							tx_sdp_o<=#1 1;
							state_o <=#1 IDLE;
						end
					end
				end

			endcase

		end
	end

endmodule
