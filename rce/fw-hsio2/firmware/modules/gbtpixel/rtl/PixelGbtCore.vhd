-- set trigAdc to 0 to avoid temp readout
-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : DtmPixelCore.vhd
-- Author     : Martin Kocian  <kocian@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-10-21
-- Last update: 2017-06-05
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

LIBRARY ieee;
Library Unisim;
use unisim.vcomponents.all;
use work.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.arraytype.all;
use work.Version.all;
use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
use work.AtlasTtcRxPkg.all;
use work.SsiPkg.all;
use work.SsiCmdMasterPkg.all;
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;

entity PixelGbtCore is 
   generic (
     framedFirstChannel   : integer := 0; --First framed channel
     framedLastChannel    : integer := 7; --Last framed channel
     rawFirstChannel      : integer := 11; --First raw channel
     rawLastChannel       : integer := 14; -- Last raw channel
     hptdcChannel1        : integer := 12;  -- use hptdc formatter
     hptdcChannel2        : integer := 13;  -- use hptdc formatter
     buffersizefe         : integer := 13; --FIFO size
     buffersizetdc        : integer := 13; --FIFO size
     encodingDefault      : std_logic_vector(1 downto 0) := "00"; --BPM or Manchester or nothing 
     hitbusreadout        : std_logic := '0'; -- hitbus configuration
     atlasafp             : std_logic := '0';  -- testbeam or ATLAS event data
     buffersizettcefb     : integer := 11; -- FIFO size
     buffersizefeefb      : integer := 11; -- FIFO size
     TX_OPTIMIZATION_G	  : integer range 0 to 1 := STANDARD;
     -- MGT Configurations
     CLK_DIV_G        : integer;
     CLK25_DIV_G      : integer;
     RX_OS_CFG_G      : bit_vector;
     RXCDR_CFG_G      : bit_vector;
     RXLPM_INCM_CFG_G : bit;
     RXLPM_IPCM_CFG_G : bit    
   );
   port ( 

      -- Master system clocks
      sysClk160        : in std_logic;
      sysClk80        : in  std_logic;
      sysClk40         : in  std_logic;
      sysRst40         : in  std_logic;
      sysClk160Unbuf    : in std_logic;
      sysClk160Unbuf90 : in std_logic;
      sysClk40Unbuf    : in std_logic;
      sysClk40Unbuf90  : in std_logic;
      sysClk120        : in std_logic;
      stableClk125     : in std_logic;
      sysClkLock       : in std_logic;
      clk_in_sel       : out std_logic := '1';
      ttcClkOk         : in std_logic;
      clk_in_sel_in    : in std_logic;
      ttc_out          : in AtlasTTCRxOutType;
      rxClk160     : in std_logic;
      rxClk160En   : in std_logic;
      sysClk100    : in std_logic;
      sysRst100    : in std_logic;
      axiClk40     : in std_logic;
      axiRst40     : in std_logic;
      -- PGP Clocks
      pgpClk       : in  std_logic;
      pgpRst     : in  std_logic;

      -- QPLL clocks
      gtQPllOutRefClk  : in  slv(1 downto 0);
      gtQPllOutClk     : in  slv(1 downto 0);
      gtQPllLock       : in  slv(1 downto 0);
      gtQPllRefClkLost : in  slv(1 downto 0);
      gtQPllReset      : out slv(1 downto 0);

      -- reload firmware
      reload       : out std_logic;

      -- MGT Serial Pins
      mgtRxN       : in  std_logic;
      mgtRxP       : in  std_logic;
      mgtTxN       : out std_logic;
      mgtTxP       : out std_logic;
      -- SLink MGT pins
      SLinkRxN     : in std_logic;
      SLinkRxP     : in std_logic;
      SLinkTxN     : out std_logic;
      SLinkTxP     : out std_logic;

      -- Axi i/o for DMA interface
      txMaster     : in AxiStreamMasterType:=AXI_STREAM_MASTER_INIT_C;
      txSlave      : out AxiStreamSlaveType;
      rxMaster     : out AxiStreamMasterType;
      rxSlave      : in AxiStreamSlaveType:=AXI_STREAM_SLAVE_INIT_C;
      -- ATLAS Pixel module pins
      serialin     : in std_logic_vector(31 downto 0);
      serialout    : out std_logic_vector(31 downto 0):=(others =>'0');

      -- Input from trigger logic
      l1a           : in std_logic;
      latchtriggerword: in std_logic_vector(7 downto 0);
      busy          : in std_logic;
      eudaqdone     : in std_logic;
      eudaqtrgword  : in std_logic_vector(14 downto 0);
 
      -- Output to trigger logic
      present       : out std_logic;
      calibmodeout  : out std_logic_vector(1 downto 0);
      pausedout     : out std_logic;
      trgenabledout : out std_logic;
      vetodisout    : out std_logic;
      rstFromCore   : out std_logic;
      fifothresh    : out std_logic;
      triggermask   : out std_logic_vector(15 downto 0);
      discop        : out std_logic_vector(15 downto 0);
      period        : out std_logic_vector(31 downto 0);
      telescopeop   : out std_logic_vector(2 downto 0);

      resetdelay    : out std_logic;
      incrementdelay: out std_logic_vector(4 downto 0);
      sbusy         : out std_logic; --serbusy
      tdcbusy       : out std_logic;
      hitbus        : out std_logic;

      cdtenabled    : out std_logic;
      numbuffers    : out std_logic_vector(4 downto 0);
      window        : out std_logic_vector(15 downto 0);

      axiReadMasterTtc:  out AxiLiteReadMasterType;
      axiReadSlaveTtc:   in AxiLiteReadSlaveType;
      axiWriteMasterTtc: out AxiLiteWriteMasterType;
      axiWriteSlaveTtc:  in AxiLiteWriteSlaveType;
      -- Debug
      debug         : out std_logic_vector(7 downto 0):=(others=>'0');
      exttriggero   : out std_logic;
      extrsto       : out std_logic;

      dispDigitA    : out std_logic_vector(7 downto 0):=x"20";
      dispDigitB    : out std_logic_vector(7 downto 0):=x"20";
      dispDigitC    : out std_logic_vector(7 downto 0):=x"20";
      dispDigitD    : out std_logic_vector(7 downto 0):=x"20";
      dispDigitE    : out std_logic_vector(7 downto 0):=x"20";
      dispDigitF    : out std_logic_vector(7 downto 0):=x"20";
      dispDigitG    : out std_logic_vector(7 downto 0):=x"20";
      dispDigitH    : out std_logic_vector(7 downto 0):=x"20";
      txReady       : out sl;
      rxReady       : out sl;
      trigAdc       : out sl:='0';
      adcSel        : out sl:='0';
      sendAdcData   : in sl;
      adcData       : in Slv16Array(11 downto 0)
   );
end PixelGbtCore;


-- Define architecture
architecture PixelGbtCore of PixelGbtCore is

   function nibbleToAscii(hexnum: slv(3 downto 0)) return slv(7 downto 0) is
   begin
     return toSlv(conv_integer(unsigned(hexnum(3 downto 0)))+ite(unsigned(hexnum(3 downto 0))>9, 55, 48), 8); 
   end function nibbleToAscii;
   function linkUp(isup: sl) return slv(7 downto 0) is
   begin
     if(isup='1')then
       return x"4c";
     else
       return x"50";
     end if;
   end function linkUp;
     
   constant maxchannel : integer := maximum(framedLastChannel, rawLastChannel);
   constant ackchannel : integer:=31;
   constant tdcchannel: integer:=30;
   constant adcchannel: integer:=29;
   constant gbtaddr: slv(6 downto 0):="0000001";
   constant NUM_AXI_MASTERS_C : natural := 4;

   constant VERSION_INDEX_C : natural := 0;
   constant REG_INDEX_C     : natural := 1;
   constant SLINK_INDEX_C   : natural := 2;
   constant TTCRX_INDEX_C   : natural := 3;

   constant TTCRX_BASE_ADDR_C : slv(31 downto 0) := X"00003000";
   constant SLINK_BASE_ADDR_C : slv(31 downto 0) := X"00002000";
   constant VERSION_BASE_ADDR_C : slv(31 downto 0) := X"00001000";
   constant REG_ADDR_C          : slv(31 downto 0) := X"00000000";
   constant TX_AXI_CONFIG_C : AxiStreamConfigType := ssiAxiStreamConfig(2);
   
   constant AXI_CROSSBAR_MASTERS_CONFIG_C : AxiLiteCrossbarMasterConfigArray(NUM_AXI_MASTERS_C-1 downto 0) := (
      TTCRX_INDEX_C   => (
         baseAddr     => TTCRX_BASE_ADDR_C,
         addrBits     => 12,
         connectivity => X"0001"),
      SLINK_INDEX_C   => (
         baseAddr     => SLINK_BASE_ADDR_C,
         addrBits     => 12,
         connectivity => X"0001"),
      VERSION_INDEX_C => (
         baseAddr     => VERSION_BASE_ADDR_C,
         addrBits     => 12,
         connectivity => X"0001"),
      REG_INDEX_C     => (
         baseAddr     => REG_ADDR_C,
         addrBits     => 12,
         connectivity => X"0001")); 

   signal sAxiReadMaster  : AxiLiteReadMasterType;
   signal sAxiReadSlave   : AxiLiteReadSlaveType;
   signal sAxiWriteMaster : AxiLiteWriteMasterType;
   signal sAxiWriteSlave  : AxiLiteWriteSlaveType;

   signal mAxiWriteMasters : AxiLiteWriteMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiWriteSlaves  : AxiLiteWriteSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadMasters  : AxiLiteReadMasterArray(NUM_AXI_MASTERS_C-1 downto 0);
   signal mAxiReadSlaves   : AxiLiteReadSlaveArray(NUM_AXI_MASTERS_C-1 downto 0);

   signal txAxisMaster : AxiStreamMasterType;
   signal txAxisSlave  : AxiStreamSlaveType;
   signal efbAxisMaster : AxiStreamMasterType:=AXI_STREAM_MASTER_INIT_C;
   signal efbAxisSlave  : AxiStreamSlaveType;
   signal intAxisMaster : AxiStreamMasterType;
   signal intAxisSlave  : AxiStreamSlaveType;
   signal rxAxisMaster : AxiStreamMasterType;
   signal rxAxisSlave  : AxiStreamSlaveType;
   signal rxAxisMasterSLink : AxiStreamMasterType;
   signal rxAxisCtrlSLink  : AxiStreamCtrlType;
   signal pgpCmd : SsiCmdMasterType;
   signal gbtStatus: slv(5 downto 0);

      

   -- PGP Front End Wrapper

   component BUFGMUX port (O: out std_logic; I0: in std_logic; I1: in std_logic; S: in std_logic); end component;

  component ODDR port ( 
         Q  : out std_logic;
         CE : in std_logic;
         C  : in std_logic;
         D1 : in std_logic;
         D2 : in std_logic;
         R  : in std_logic;
         S  : in std_logic
      );
  end component;

   -- Local Signals
   signal pgpDispA        : std_logic_vector(7 downto 0);
   signal pgpDispB        : std_logic_vector(7 downto 0);
   signal blockdata       : std_logic_vector(31 downto 0);
   signal configdataout   : std_logic_vector(31 downto 0);
   signal replynow        : std_logic;
   signal o01             : std_logic;
   signal o23             : std_logic;
   signal clockselect     : std_logic_vector(1 downto 0);
   signal starttdcreadout : std_logic;
   signal counterout1     : std_logic_vector(31 downto 0);
   signal counterout2     : std_logic_vector(31 downto 0);
   signal oldl1a          : std_logic;
   signal l1amod          : std_logic;
   signal ecrmod          : std_logic;
   signal ecrin           : std_logic:='0';
   signal bcrmod          : std_logic;
   signal bcrin           : std_logic:='0';
   signal stop            : std_logic;
   signal go              : std_logic;
   signal writevalid      : std_logic;
   signal configprogfull  : std_logic;
   signal tdcdata         : std_logic_vector(17 downto 0);
   signal tdcld           : std_logic;
   signal datawaiting     : std_logic_vector(31 downto 0);
   signal moredatawaiting : std_logic_vector(31 downto 0);
   signal indatavalid     : std_logic_vector(31 downto 0);
   signal chanld          : std_logic_vector(31 downto 0);
   signal datain          : dataarray;
   signal channeldata     : dataarray;
   signal reqdata         : std_logic_vector(31 downto 0);
   signal dfifothresh     : std_logic_vector(31 downto 0);
   signal dfifothresh40   : std_logic_vector(31 downto 0);
   signal andr            : std_logic_vector(31 downto 0);
   signal empty           : std_logic;
   signal full            : std_logic;
   signal configfull      : std_logic;
   signal bufoverflow     : std_logic_vector(31 downto 0);
   signal overflow        : std_logic_vector(31 downto 0);
   signal underflow       : std_logic_vector(31 downto 0);
   signal prog_full       : std_logic;
   signal emptyv          : std_logic_vector(31 downto 0);
   signal fullv           : std_logic_vector(31 downto 0);
   signal overflowv       : std_logic_vector(31 downto 0);
   signal underflowv      : std_logic_vector(31 downto 0);
   signal prog_fullv      : std_logic_vector(31 downto 0);
   signal isrunning       : std_logic_vector(31 downto 0);
   signal rst             : std_logic;
   signal rst160          : std_logic;
   signal rstRC           : std_logic;
   signal softrst         : std_logic;
   signal trigenabled     : std_logic;
   signal exttrgclkeu     : std_logic;
   signal marker          : std_logic;
   signal ldser           : std_logic;
   signal d_out           : std_logic;
   signal serbusy         : std_logic;
   signal trgbusy         : std_logic;
   signal calibmode       : std_logic_vector(1 downto 0);
   signal startrun        : std_logic;
   signal paused          : std_logic;
   signal reg             : std_logic_vector(31 downto 0);
   signal channelmask     : std_logic_vector(31 downto 0);
   signal channeloutmask  : std_logic_vector(31 downto 0);
   signal disablemask     : std_logic_vector(15 downto 0);
   signal efbMaskedChannels: std_logic_vector(15 downto 0);
   signal efbMaskedInRun  : std_logic_vector(15 downto 0);
   signal efbMaskedPerm   : std_logic_vector(15 downto 0);
   signal enablereadout   : std_logic_vector(31 downto 0);
   signal status          : std_logic_vector(31 downto 0);
   signal statusd         : std_logic_vector(31 downto 0);
   signal trgtime         : std_logic_vector(63 downto 0);
   signal trgtimel        : std_logic_vector(63 downto 0);
   signal deadtime        : std_logic_vector(63 downto 0);
   signal deadtimel       : std_logic_vector(63 downto 0);
   signal l1count         : std_logic_vector(3 downto 0);
   signal l1countlong     : std_logic_vector(31 downto 0);
   signal l1countl        : std_logic_vector(3 downto 0);
   signal rstl1count      : std_logic;
   signal trgdelay        : std_logic_vector(7 downto 0);
   signal markercounter   : std_logic_vector(9 downto 0);
   signal conftrg         : std_logic;
   signal ld8b10b         : std_logic_vector(31 downto 0);
   signal ldout8b10b      : std_logic_vector(31 downto 0);
   signal data8b10b       : array10b;
   signal dout8b10b       : array10b;
   signal decodingerrors  : Slv32Array(15 downto 0):=(others=>(others=>'0'));
   signal busycounter     : Slv32Array(15 downto 0):=(others=>(others=>'0'));
   signal ldfei4          : std_logic_vector(31 downto 0);
   signal ldfei4reg       : std_logic_vector(31 downto 0);
   signal ldfei4efb       : std_logic_vector(31 downto 0);
   signal ldfifo          : std_logic_vector(31 downto 0);
   signal dnextvalid      : std_logic_vector(31 downto 0);
   signal dvalid          : std_logic_vector(31 downto 0);
   signal frameoverflow   : std_logic_vector(31 downto 0);
   signal fei4data        : fei4array;
   signal fifodata        : fei4array;
   signal dnext           : fei4array;
   signal aligned         : std_logic_vector(31 downto 0);
   signal alignout        : std_logic_vector(31 downto 0);
   signal receiveclock    : std_logic;
   signal receiveclock90  : std_logic;
   signal recdata         : std_logic_vector(31 downto 0);
   signal recdatab        : std_logic_vector(31 downto 0);
   signal read8b10bfifo   : std_logic_vector(31 downto 0);
   signal fifo8b10bempty  : std_logic_vector(31 downto 0);
   signal valid8b10b      : std_logic_vector(31 downto 0);
   signal pgpencdatain    : array10b;
   signal selfei4clk      : std_logic_vector(1 downto 0);
   signal trgcount        : std_logic_vector(15 downto 0);
   signal trgcountdown    : std_logic_vector(15 downto 0);

   signal encoding        : std_logic_vector(1 downto 0);
   signal trgin           : std_logic;
   signal setdeadtime     : std_logic_vector(15 downto 0);
   signal serialoutb      : std_ulogic_vector(31 downto 0);
   signal setfifothresh   : std_logic_vector(15 downto 0);

   signal phaseConfig     : std_logic;

   signal hitbusa           : std_logic;
   signal hitbusb           : std_logic;
   signal hitbusop          : std_logic_vector(15 downto 0);
   signal hitbusin          : std_logic_vector(5 downto 0);
   signal hitbusword         : hitbusoutput;
   signal hitbusdepth : std_logic_vector(4 downto 0);
   signal tdcreadoutdelay:   std_logic_vector(4 downto 0);
    
   signal ofprotection      : std_logic;

   signal writemem: std_logic;
   signal maxmem: std_logic_vector(9 downto 0);
   signal memval: std_logic_vector(31 downto 0);
   signal ld32: std_logic;
   signal oldld32: std_logic;
   signal serdata32: std_logic_vector(31 downto 0);
   signal readpointer: std_logic_vector(9 downto 0);
   signal memout : std_logic;
   signal stop32: std_logic;
   signal go32 : std_logic;
   signal going32: std_logic;
   signal l1route: std_logic;
   signal l1modin: std_logic;
   signal l1memin: std_logic;
   signal readmem: std_logic;
   signal multiplicity: std_logic_vector(63 downto 0);

   signal adccounter: slv(31 downto 0):=x"00000000";
   signal adcperiod : slv(31 downto 0):=x"02625a00";
   signal oldadcperiod : slv(31 downto 0):=x"02625a00";
   signal enableAdcReadout: sl;
   signal maxlength: natural range 0 to 16383:=100;
   signal badparity: sl:='0';
   signal sendoverflow: sl;
   signal locoverflow: sl;
   signal txErr: sl;
   signal newtrig:sl;
   signal goodcounter: slv(15 downto 0);
   signal badcounter: slv(15 downto 0);
   signal sofc: slv(15 downto 0);
   signal eofc: slv(15 downto 0);
   signal busy40: sl;
   signal busy160: sl;
   signal slinkStatusWords: Slv64Array(0 to 0);
   signal slinkStatusSend: sl;
   signal outputdelay: Slv8Array(31 downto 0); 
   signal encoderout: Slv8Array(31 downto 0); 
   signal coarse_s1_dout: slv(31 downto 0); 
   signal coarse_s2_sr: Slv15Array(31 downto 0); 
   signal coarse_s2_delay: IntegerArray(0 to 31);
   signal D1: slv(31 downto 0);
   signal D2: slv(31 downto 0);
   signal D3: slv(31 downto 0);
   signal D4: slv(31 downto 0);
   signal D5: slv(31 downto 0);
   signal D6: slv(31 downto 0);
   signal D7: slv(31 downto 0);
   signal D8: slv(31 downto 0);
   signal ttcefbdata: slv(52 downto 0);
   signal ttcefbdatarx: slv(52 downto 0);
   signal ttcefbdatasim: slv(52 downto 0);
   signal efbdataout: slv(52 downto 0);
   signal efbdatanext: slv(52 downto 0);
   signal ldttcefb: sl;
   signal ldttcefbrx: sl;
   signal ldttcefbsim: sl;
   signal ttcsim: sl;
   signal efbreadenable: sl;
   signal efbnextvalid: sl;
   signal efbvalid: sl;
   signal hitdiscconfig: Slv2Array(15 downto 0):=(others => (others=>'0'));
   signal fei4dataformatted: Slv33Array(15 downto 0);
   signal fei4dataefb: Slv33Array(15 downto 0);
   signal dnextefb: Slv33Array(15 downto 0);
   signal ldformatted: slv(15 downto 0);
   signal ldefb: slv(15 downto 0);
   signal dnextvalidefb: slv(15 downto 0);
   signal dfifothreshn: slv(31 downto 0):=(others =>'0');
   signal dfifothreshefb: slv(31 downto 0):=(others =>'0');
   signal dvalidefb: slv(15 downto 0);
   signal datapath: sl:='0';
   signal ttcroenabled: sl:='0';
   signal efbenabled: sl:='0';
   signal enabledmask: slv(15 downto 0);
   signal efbcounters: CounterType;
   signal nExp: slv(4 downto 0):= (others => '0');
   signal bcidShift: slv(11 downto 0):= (others => '0');
   signal efbtimeout: slv(15 downto 0);
   signal efbtimeoutfirst: slv(15 downto 0);
   signal efb_missing_header_timeout: slv(7 downto 0);
   signal runnumber: slv(31 downto 0);
   signal rodid: slv(31 downto 0);
   signal detevtype: slv(31 downto 0);
   signal doecrreset: slv(15 downto 0);
   signal nummon: slv(31 downto 0);
   signal monenabled: sl:='0';
   signal nEvt: slv(31 downto 0);
   signal nEvtGood: slv(31 downto 0);
   signal nEvtNonMon: slv(31 downto 0);
   signal enableEcrReset: sl;
   signal resetFifo: slv(15 downto 0);
   signal bcrl1acounter: slv(31 downto 0):=(others =>'0');
   signal l1abcrcounter: slv(31 downto 0):=(others =>'0');
   signal l1al1acounter: slv(31 downto 0):=(others =>'0');
   signal bcrveto: sl;
   signal vetoFirstBcid, vetoNumBcid: slv(11 downto 0);
   signal eventlimit: slv(11 downto 0);
   signal recdata40: slv(83 downto 0);
   signal recdata160: slv(20 downto 0);
   signal gbtDataReordered: slv(83 downto 0);
   signal clockcounters: slv(31 downto 0);
   signal icword: slv(31 downto 0);
   signal icdone, useic: sl;
   signal icin, icout, icrst: slv(1 downto 0);
   signal hptdctemps1: Slv10Array(7 downto 0);
   signal hptdctemps2: Slv10Array(7 downto 0);
   signal ttcbusycounter: slv(31 downto 0):=(others =>'0');
   function vectorize(s: std_logic) return std_logic_vector is
     variable v: std_logic_vector(0 downto 0);
   begin
     v(0) := s;
     return v;
   end;

   function vectorize(v: std_logic_vector) return std_logic_vector is
   begin
     return v;
   end;

   signal probe0: slv(99 downto 0);
   signal hb: sl := '0';

   signal txReady_int: std_logic;
   signal rxReady_int: std_logic;
   signal neout: std_logic;
   signal blockEcrBcr: slv(1 downto 0);

   -- GBT
   signal to_gbtBank_clks    : gbtBankClks_i_R;
   signal from_gbtBank_clks  : gbtBankClks_o_R;
   --------------------------------------------------------        
   signal to_gbtBank_gbtTx   : gbtTx_i_R_A(1 to 1);
   signal from_gbtBank_gbtTx : gbtTx_o_R_A(1 to 1);
   --------------------------------------------------------        
   signal to_gbtBank_mgt     : mgt_i_R;
   signal from_gbtBank_mgt   : mgt_o_R;
   --------------------------------------------------------        
   signal to_gbtBank_gbtRx   : gbtRx_i_R_A(1 to 1);
   signal from_gbtBank_gbtRx : gbtRx_o_R_A(1 to 1);
   signal txData_to_gbtBank             : gbtframe_A(1 to 1):=(others => (others =>'0'));

   attribute KEEP_HIERARCHY : string;
   attribute KEEP_HIERARCHY of PgpFrontEnd_Inst : label is "TRUE";      

begin

   ---------------------
   -- PGP Front End Core
   ---------------------
       PgpFrontEnd_Inst : entity work.AxiFrontEnd
         generic map(
           SLAVE_AXI_CONFIG_G  => TX_AXI_CONFIG_C,
           MASTER_AXI_CONFIG_G => ssiAxiStreamConfig(4),
           atlasafp            => atlasafp)
         port map (
           txMaster => txMaster,
           txSlave  => txSlave,
           rxMaster => rxMaster,
           rxSlave  => rxSlave,
           -- Axi Master Interface - Registers (sysClk domain)      
           mAxiLiteReadMaster  => sAxiReadMaster,
           mAxiLiteReadSlave   => sAxiReadSlave,
           mAxiLiteWriteMaster => sAxiWriteMaster,
           mAxiLiteWriteSlave  => sAxiWriteSlave,
           -- Streaming Links (sysClk domain)      
           sAxisMaster         => txAxisMaster,
           sAxisSlave          => txAxisSlave,
           mAxisMaster         => rxAxisMaster,
           mAxisSlave          => rxAxisSlave,
           pgpCmd              => pgpCmd,
           -- Efb
           sAxisMasterEfb      => efbAxisMaster,
           sAxisSlaveEfb       => efbAxisSlave,
           -- Clock, Resets, and Status Signals
           sysReset            => sysRst40,
           sysClk              => sysClk40,
           pgpClk              => pgpClk,
           pgpReset            => pgpRst,
           sysClk100           => sysClk100,
           sysRst100           => sysRst100,
           axiClk40            => axiClk40,
           axiRst40            => axiRst40,
           receiveclock        => sysClk80,
           receiverst          => rst,
           txReady             => txReady_int,
           rxReady             => rxReady_int,
           sendoverflow        => sendoverflow,
           locoverflow         => locoverflow,
           txErr               => txErr);

   SLINKGEN: if (atlasafp='1') generate
   SLink_Inst: entity work.AtlasSLinkLsc
     generic map(
       SLAVE_AXI_CONFIG_G => ssiAxiStreamConfig(4),
       TRANSCEIVER_G => "GTX")
     port map(
       axiClk => axiClk40,
       axiRst => axiRst40,
       axiReadMaster  => mAxiReadMasters(SLINK_INDEX_C),
       axiReadSlave => mAxiReadSlaves(SLINK_INDEX_C),
       axiWriteMaster => mAxiWriteMasters(SLINK_INDEX_C),
       axiWriteSlave => mAxiWriteSlaves(SLINK_INDEX_C),
       statusWords => slinkStatusWords,
       statusSend => slinkStatusSend,

       sAxisClk => sysClk80,
       sAxisRst => sysRst40,
       sAxisMaster => rxAxisMasterSLink,
       sAxisCtrl => rxAxisCtrlSLink,

       sysClk => sysClk100,
       sysRst => sysRst100,

       testLed => debug(3),
       ldErrLed => debug(2),
       flowCtrlLed => debug(1),
       activityLed => debug(0),

       gtTxP => SLinkTxP,
       gtTxN => SLinkTxN,
       gtRxP => SLinkRxP,
       gtRxN => SLinkRxN);
   end generate SLINKGEN;

   with datapath select
   dfifothresh <= dfifothreshn when '0',
                  dfifothreshefb and x"ffff"&enabledmask when '1',
                  (others =>'0') when others;
   SyncOut_dfifothresh : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 32)    
      port map (
         clk        => sysClk40,
         dataIn => dfifothresh,
         dataOut => dfifothresh40);  
    process (dfifothresh40) begin
     for I in 0 to 15 loop
       if(rstRC='1')then
         busycounter(I)<=(others =>'0');
       elsif(rising_edge(sysClk40))then
         if(dfifothresh40(I)='1')then
           busycounter(I)<=unsigned(busycounter(I))+1;
         end if;
       end if;
     end loop;
    end process;
    process (dfifothresh40) begin
       if(rstRC='1')then
         ttcbusycounter<=(others =>'0');
       elsif(rising_edge(sysClk40))then
         if(dfifothresh40(tdcchannel)='1')then
           ttcbusycounter<=unsigned(ttcbusycounter)+1;
         end if;
       end if;
    end process;

   debug(4)<=slinkStatusWords(0)(2);

   pausedout<=paused;
   trgenabledout<=trigenabled;
   calibmodeout<=calibmode;
   andr<= channeloutmask and dfifothresh40;
   fifothresh<=uOr(andr);

  txReady <= txReady_int;
  rxReady <= rxReady_int;
  process begin
    wait until rising_edge(sysClk40);
    case rxReady_int is
      when '1' =>
        case txReady_int is
          when '1' =>
            dispDigitA <= x"4C";
          when '0' =>
            dispDigitA <= x"54";
        end case;
      when '0' =>
        dispDigitA <= x"50";
    end case;
    dispDigitG<=nibbleToAscii(FPGA_VERSION_C(7 downto 4));
    dispDigitH<=nibbleToAscii(FPGA_VERSION_C(3 downto 0));
  end process;

   dispDigitB<=linkUp(slinkStatusWords(0)(2));
   with atlasafp select
     dispDigitC <= x"41" when '1',
                   x"32" when others;
   with hitbusreadout select
     dispDigitD <= x"41" when '1',
                   x"32" when others;
   with encoding select
     dispDigitF <= x"42" when "01",
                   x"4d" when "10",
                   x"4e" when others;
   -------------------------
   -------------------------
   -- AXI-Lite Crossbar Core
   -------------------------         
   AxiLiteCrossbar_Inst : entity work.AxiLiteCrossbar
      generic map (
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => NUM_AXI_MASTERS_C,
         MASTERS_CONFIG_G   => AXI_CROSSBAR_MASTERS_CONFIG_C)
      port map (
         sAxiWriteMasters(0) => sAxiWriteMaster,
         sAxiWriteSlaves(0)  => sAxiWriteSlave,
         sAxiReadMasters(0)  => sAxiReadMaster,
         sAxiReadSlaves(0)   => sAxiReadSlave,
         mAxiWriteMasters    => mAxiWriteMasters,
         mAxiWriteSlaves     => mAxiWriteSlaves,
         mAxiReadMasters     => mAxiReadMasters,
         mAxiReadSlaves      => mAxiReadSlaves,
         axiClk              => axiClk40,
         axiClkRst           => axiRst40);        

   axiReadMasterTtc <= mAxiReadMasters(TTCRX_INDEX_C);
   axiWriteMasterTtc <= mAxiWriteMasters(TTCRX_INDEX_C);
   mAxiReadSlaves(TTCRX_INDEX_C) <= axiReadSlaveTtc;
   mAxiWriteSlaves(TTCRX_INDEX_C) <= axiWriteSlaveTtc;
   --------------------------
   -- AXI-Lite Version Module
   --------------------------          
   --AxiVersion_Inst : entity work.AxiVersion
   --   generic map (
   --      EN_DEVICE_DNA_G => true)   
   --   port map (
   --      axiReadMaster  => mAxiReadMasters(VERSION_INDEX_C),
   --      axiReadSlave   => mAxiReadSlaves(VERSION_INDEX_C),
   --      axiWriteMaster => mAxiWriteMasters(VERSION_INDEX_C),
   --      axiWriteSlave  => mAxiWriteSlaves(VERSION_INDEX_C),
   --      axiClk         => axiClk40,
   --      axiRst         => axiRst40);            
   mAxiReadSlaves(VERSION_INDEX_C)<=AXI_LITE_READ_SLAVE_INIT_C;
   mAxiWriteSlaves(VERSION_INDEX_C)<=AXI_LITE_WRITE_SLAVE_INIT_C;

   monitorclock_inst: entity work.monitorClockbufPhase
     port map(
       clock160=>rxClk160,
       clock120=>sysClk120,
       clken=>rxClk160En,
       result=>clockcounters);
   registers_inst: entity work.registers
     generic map(
       encodingdefault => encodingdefault,
       adcchannel => adcchannel,
       ackchannel => ackchannel,
       tdcchannel => tdcchannel)
     port map(
      sysClk40 => sysClk40,
      sysRst40 => sysRst40,
      axiClk => axiClk40,
      axiRst => axiRst40,
      axiReadMaster => mAxiReadMasters(REG_INDEX_C),
      axiReadSlave => mAxiReadSlaves(REG_INDEX_C),
      axiWriteMaster => mAxiWriteMasters(REG_INDEX_C),
      axiWriteSlave => mAxiWriteSlaves(REG_INDEX_C),
      clockselect => clockselect,
      trgcount => trgcount,
      calibmode => calibmode,
      resetdelay => resetdelay,
      incrementdelay => incrementdelay,
      conftrg => conftrg,
      trgdelay => trgdelay,
      triggermask => triggermask,
      vetodis => vetodisout,
      enableEcrReset => enableEcrReset,
      blockEcrBcr => blockEcrBcr,
      period => period,
      setdeadtime => setdeadtime,
      channelmask => channelmask,
      channeloutmask => channeloutmask,
      disablemask => disablemask,
      encoding => encoding,
      eventlimit => eventlimit,
      hitbusop => hitbusop,
      discop => discop,
      telescopeop => telescopeop,
      setfifothresh => setfifothresh,
      ofprotection => ofprotection,
      writemem => writemem,
      maxmem => maxmem,
      memval => memval,
      l1route => l1route,
      hitbusdepth => hitbusdepth,
      tdcreadoutdelay => tdcreadoutdelay,
      multiplicity => multiplicity,
      maxlength => maxlength,
      outputdelay => outputdelay,
      clk_in_sel => clk_in_sel,
      selfei4clk => selfei4clk,
      clk_in_sel_in => clk_in_sel_in,
      ttcClkOk => ttcClkOk,
      clockcounters => clockcounters,
      gbtstatus => gbtStatus,
      icword => icword,
      hitdiscconfig => hitdiscconfig,
      nExp => nExp,
      bcidShift => bcidShift,
      efbtimeout => efbtimeout,
      efbtimeoutfirst => efbtimeoutfirst,
      efb_missing_header_timeout => efb_missing_header_timeout,
      runnumber => runnumber,
      rodid => rodid,
      detevtype => detevtype,
      doecrreset => doecrreset,
      nummon => nummon,
      monenabled => monenabled,
      datapath => datapath,
      ttcsim => ttcsim,
      cdtenabled => cdtenabled,
      numbuffers => numbuffers,
      window => window,
      nEvt => nEvt,
      nEvtGood => nEvtGood,
      nEvtNonMon => nEvtNonMon,
      disabledMask => efbMaskedChannels,
      maskedInRun => efbMaskedInRun,
      maskedPerm  => efbMaskedPerm,
      bcrl1acounter => bcrl1acounter,
      l1abcrcounter => l1abcrcounter,
      l1al1acounter => l1al1acounter,
      bcrveto => bcrveto,
      vetoFirstBcid => vetoFirstBcid,
      vetoNumBcid => vetoNumBcid,
      decodingerrors => decodingerrors,
      busycounter => busycounter,
      ttcbusycounter => ttcbusycounter,
      hptdctemps1 => hptdctemps1,
      hptdctemps2 => hptdctemps2,
      efbcounters => efbcounters
      );

   -- CMD interface
  process (sysRst40, sysClk40) begin
   if(sysRst40='1')then
     startrun<='0';
     softrst<='0';
     present<='0';
     trigenabled<='0';
     rstl1count<='0';
     marker<='0';
     reload <='1';
     markercounter<="0000000000";
     phaseConfig <='0';
   elsif rising_edge(sysClk40) then
     if(pgpCmd.valid='1')then
      if(pgpCmd.opCode=x"03")then -- start run
        softrst<='1';
        startrun<='1';
        trgcountdown<=trgcount; -- for runs with a finite number of events.
      elsif(pgpCmd.opCode=x"04")then -- pause run
        trigenabled<='0';
        paused<='1';
      elsif(pgpCmd.opCode=x"05")then -- stop run
        trigenabled<='0';
        paused<='0';
        present<='0';
      elsif(pgpCmd.opCode=x"06")then -- resume run
        trigenabled<='1';
        paused<='0';
      elsif(pgpCmd.opCode=x"07")then -- resume run and set marker after delay
        markercounter<="1111111111";
      elsif(pgpCmd.opCode=x"08")then -- Reboot
        reload<='0';          
      elsif(pgpCmd.opCode=x"09")then -- soft reset
        softrst<='1';
      elsif(pgpCmd.opCode=x"10")then -- FS: Write new phase configuration from phase recognition
        phaseConfig <= '1';
      elsif(pgpCmd.opCode=x"11")then -- Tell HSIO that this core is active.
        present <= '1';
      elsif(pgpCmd.opCode=x"12")then -- Tell HSIO that this core is not active.
        present <= '0';
      end if;
     elsif(startrun='1')then
        softrst<='0';
        startrun<='0';
        trigenabled<='1';
        paused<='0';
     elsif(softrst='1')then
        softrst<='0';
     elsif(trgcountdown=x"0001")then -- stop run
       trigenabled<='0';
       paused<='0';
     elsif(markercounter="0000000001")then
       marker<='1';
       trigenabled<='1';
       paused<='0';
       rstl1count<='1';
     else
       marker<='0';
       rstl1count<='0';
       phaseConfig<='0';
     end if;
     if(markercounter/="0000000000")then
       markercounter<=unsigned(markercounter)-1;
     end if;
     if(newtrig='1' and trgcountdown/=x"0000")then
       trgcountdown<=unsigned(trgcountdown)-1;
     end if;
   end if;
  end process;
  rst<= sysRst40 or softrst; 
  rstFromCore<= rst;
  SysRst_Inst160 : entity work.Synchronizer
      port map(
        clk    => sysClk160,
        dataIn => rst,
        dataOut=> rst160);  
  SysRst_InstRC : entity work.Synchronizer
      port map(
        clk    => receiveclock,
        dataIn => rst,
        dataOut=> rstRC);  

  process begin
    wait until rising_edge(sysClk40);
    oldadcperiod<=adcperiod;
    if(adccounter=adcperiod)then
      adccounter<=(others => '0');
      trigAdc <= '1';
      adcSel<='0';
    elsif(adccounter(30 downto 0)=adcperiod(31 downto 1))then
      adccounter<=unsigned(adccounter)+1;
      trigAdc <= '1';
      adcSel<='1';
    elsif (adcperiod/=oldadcperiod)then
      adccounter<=(others => '0');
      trigAdc <= '0';
    else
      adccounter<=unsigned(adccounter)+1;
      trigAdc <='0';
    end if;
  end process;
   
  synch_0: entity work.Synchronizer
   port map(
     clk => sysClk40,
     dataIn => busy,
     dataOut => busy40);
  synch_1: entity work.Synchronizer
   port map(
     clk => rxClk160,
     dataIn => busy,
     dataOut => busy160);
  process (rst, sysClk40) begin
   if(rst='1')then
     status<=x"00000000";
     statusd<=x"00000000";
     l1count<=x"1";
     l1countlong<=x"00000000";
     l1countl<=x"1";
     trgtime<=x"0000000000000000";
     trgtimel<=x"0000000000000000";
     deadtime<=x"0000000000000000";
     deadtimel<=x"0000000000000000";
   elsif (rising_edge(sysClk40)) then
     oldl1a<=l1a;
     if(trigenabled='1' or paused='1')then
       trgtime<=unsigned(trgtime)+1;
     end if;
     if(busy40='1' or paused='1')then
       deadtime<=unsigned(deadtime)+1;
     end if;
     if(rstl1count='1')then
       l1count<=x"1";
     elsif(l1a='1')then
       l1countl<=l1count;
       l1count<=unsigned(l1count)+1;
       if(oldl1a='0')then
         l1countlong<=unsigned(l1countlong)+1;
       end if;
       trgtimel<=trgtime;
       deadtimel<=deadtime;
     end if;
     status(10 downto 0)<= dfifothresh(10 downto 0);
     status(11)<= paused;
     status(12)<= '0';
     
     --statusd(10 downto 0)<= statusd(10 downto 0) or underflow;
     statusd(3 downto 0) <= l1count;
     statusd(10 downto 9) <=(others=>'0');
     statusd(21 downto 11)<= statusd(21 downto 11) or overflow(10 downto 0);
     --statusd(24)<=busy;
     --statusd(26)<=extbusy;
     statusd(26)<='0';
   end if;
   end process;

    sbusy<=trgbusy or going32;
    l1modin<=l1a and not l1route;
    l1memin<=l1a and l1route;
    trgpipeline: entity work.triggerpipeline
      port map(
        rst=> sysRst40,
        clk=> sysClk40,
        L1Ain=> l1modin, -- was l1a
        L1Aout=> l1amod,
        ECRin => ecrin,
        ECRout => ecrmod,
        BCRin => bcrin,
        BCRout => bcrmod,
        configure=> conftrg,
        delay => trgdelay,
        busy => trgbusy,
        deadtime => setdeadtime
        );
   
   with hitbusop(3) select 
   hitbusa<= (hitbusin(0) and hitbusop(0)) or (hitbusin(1) and hitbusop(1)) or (hitbusin(2) and hitbusop(2)) when '0',
             (hitbusin(0) or not hitbusop(0)) and (hitbusin(1) or not hitbusop(1)) and (hitbusin(2) or not hitbusop(2)) 
             and (hitbusop(0) or hitbusop(1) or hitbusop(2)) when '1',
             '0' when others;
   with hitbusop(7) select 
   hitbusb<= (hitbusin(3) and hitbusop(4)) or (hitbusin(4) and hitbusop(5)) or (hitbusin(5) and hitbusop(6)) when '0',
             (hitbusin(3) or not hitbusop(4)) and (hitbusin(4) or not hitbusop(5)) and (hitbusin(5) or not hitbusop(6)) 
             and  (hitbusop(4) or hitbusop(5) or hitbusop(6)) when '1',
             '0' when others;
   with hitbusop(8) select
   hitbus <= hitbusa or hitbusb when '0',
             hitbusa and hitbusb when '1',
             '0' when others;

      hr1: if(hitbusreadout='1') generate
        counterout1<=hitbusword(0);
        counterout2<=hitbusword(1);
      end generate hr1;
      hr2: if(hitbusreadout='0') generate
        counterout1<=(others => '0');
        counterout2<=(others => '0');
      end generate hr2;
    theconfigreceiver: entity work.ConfigurationStreamReceiver 
      port map(
        sysClk => sysClk40,
        sysRst => sysRst40,
        sAxisMaster => rxAxisMaster,
        sAxisSlave => rxAxisSlave,
        go => go,
        replynow => replynow,
        empty => stop,
        progfull => configprogfull,
        blockdata => blockdata,
        writevalid => writevalid);
      
    theconfigfifo: entity work.FifoSync
      generic map(
        DATA_WIDTH_G => 32,
        ADDR_WIDTH_G => 14,
        FULL_THRES_G => 16000)
      port map (
        rst => sysRst40,
        clk => sysClk40,
        wr_en => writevalid,
        rd_en => ldser,
        din => blockdata,
        dout => configdataout,
        empty => stop,
        full => configfull,
        prog_full => configprogfull);
   theser: entity work.ser
     port map(
       clk=>sysClk40,
       ld=>ldser,
--       l1a=>l1a,
       l1a=>l1amod,
       ecr=>ecrmod,
       bcr=>bcrmod,
       go=>go,
       busy=>serbusy,
       stop=>stop,
       rst=>rst,
       blockEcrBcr => blockEcrBcr,
       d_in=>configdataout,
       d_out=>d_out
       );

   process (sysClk40, sysRst40) begin
     if(sysRst40='1')then
       oldld32<='0';
       stop32<='1';
       readpointer<="0000000000";
     elsif rising_edge(sysClk40)then
       oldld32<=ld32 or l1memin;
       if(l1memin='1' and going32 ='0' and maxmem/="0000000000")then -- serialize trigger sequence
         stop32<='0';
       end if;
       if(going32='1')then
         if(oldld32='1' and ld32='0' )then
           if(readpointer=unsigned(maxmem)-1)then
             stop32<='1';
             readpointer<="0000000000";      
           else
             readpointer<=unsigned(readpointer)+1;
           end if;
         end if;
       end if;
     end if;
   end process;

   readmem<=ld32 or l1memin;
   configmem : entity work.SimpleDualPortRam
     generic map(
       DATA_WIDTH_G => 32,
       ADDR_WIDTH_G => 10)
     port map (
       clka => axiClk40,
       dina => memval,
       addra => maxmem,
       ena => writemem,
       wea => '1',
       clkb => sysClk40,
       rstb => sysRst40,
       addrb => readpointer,
       enb => readmem,
       doutb => serdata32);
  
   theser32: entity work.ser32
     port map(
       clk => sysClk40,
       ld => ld32,
       go => l1memin,
       busy => going32,
       stop => stop32,
       rst => sysRst40,
       d_in => serdata32,
       d_out => memout);
   
   
   fanout: for I in 0 to framedLastChannel generate
     serialoutb(I)<= (d_out or memout) when channelmask(I)='1' else '0';   
    -- first stage coarse delay
     coarse_s1: SRLC32E
       port map (
         Q => coarse_s1_dout(I),
         Q31 => open,
         D => serialoutb(I),
         CLK => sysClk40,
         CE => '1',
         A => outputdelay(I)(7 downto 3)
         );
     bpmenc: entity work.outputencoderphase
       port map(
         clock=>sysClk40,
         rst=>sysRst40,
         datain=>coarse_s1_dout(I),
         encode=>encoding,
         dataout=>encoderout(I));
     -- second stage coarse delay
     process begin
       wait until rising_edge(sysClk40);
       coarse_s2_sr(I) <= coarse_s2_sr(I)(6 downto 0) & encoderout(I);
     end process;
     coarse_s2_delay(I) <= conv_integer(unsigned(outputdelay(I)(2 downto 0)));
    OUT_EVEN: if (I mod 2 = 0) generate
      DELAY_OUT: for J in 0 to 7 generate
         txData_to_gbtBank(1)(I*4+J) <= coarse_s2_sr(I)(coarse_s2_delay(I)+J);
      end generate DELAY_OUT;
    end generate OUT_EVEN;
   end generate fanout;

  pgpack: entity work.deser
    generic map( CHANNEL=>toSlv(ackchannel, 8))
    port map (
      clk       => sysClk40,
      rst       => sysRst40,
      d_in      => '0',
      enabled   => channeloutmask(ackchannel),
      replynow  => replynow,
      marker =>'0',
      d_out     => channeldata(ackchannel)(15 downto 0),
      ld        => chanld(ackchannel),
      sof       => channeldata(ackchannel)(16),
      eof       => channeldata(ackchannel)(17)
      );
  pgpackfifo: entity work.FifoSync
      generic map(
        DATA_WIDTH_G => 18,
        ADDR_WIDTH_G => 10,
        FULL_THRES_G => 800)
    port map (
      clk => sysClk40,
      rst => rst,
      din => channeldata(ackchannel),
      rd_en => reqdata(ackchannel),
      wr_en => chanld(ackchannel),
      dout => datain(ackchannel),
      empty => open,
      full => open,
      overflow => overflow(ackchannel),
      prog_full => dfifothreshn(ackchannel),
      valid => indatavalid(ackchannel),
      underflow => underflow(ackchannel));
  pgpackdataflag: entity work.dataflagnew
    port map(
      eofin=>channeldata(ackchannel)(17),
      eofout=>datain(ackchannel)(17),
      datawaiting=> datawaiting(ackchannel),
      clkin=>sysClk40,
      clkout=>sysClk40,
      rst=>rst
      );
      moredatawaiting(ackchannel)<='0';
  enableAdcReadout<=not dfifothreshn(adcchannel) and channeloutmask(adcchannel) and trigenabled;
  adcreadout_inst: entity work.adcreadout
    generic map( CHANNEL=>toSlv(adcchannel, 8))
    port map (
      clk       => sysClk40,
      rst       => rst,
      d_in      => AdcData,
      enabled   => enableAdcReadout,
      go        => sendAdcData,
      d_out     => channeldata(adcchannel)(15 downto 0),
      ld        => chanld(adcchannel),
      sof       => channeldata(adcchannel)(16),
      eof       => channeldata(adcchannel)(17)
      );
  adcfifo: entity work.FifoSync
      generic map(
        DATA_WIDTH_G => 18,
        ADDR_WIDTH_G => 10,
        FULL_THRES_G => 800)
    port map (
      clk => sysClk40,
      rst => rst,
      din => channeldata(adcchannel),
      rd_en => reqdata(adcchannel),
      wr_en => chanld(adcchannel),
      dout => datain(adcchannel),
      empty => open,
      full => open,
      overflow => overflow(adcchannel),
      prog_full => dfifothreshn(adcchannel),
      valid => indatavalid(adcchannel),
      underflow => underflow(adcchannel));
  adcdataflag: entity work.dataflagnew
    port map(
      eofin=>channeldata(adcchannel)(17),
      eofout=>datain(adcchannel)(17),
      datawaiting=> datawaiting(adcchannel),
      clkin=>sysClk40,
      clkout=>sysClk40,
      rst=>rst
      );

   CHANNELREADOUT:
   for I in rawFirstChannel to rawLastChannel generate
     enablereadout(I)<=channeloutmask(I) and ((trigenabled and not ofprotection) or not dfifothreshn(I)); 
     channelreadout: entity work.deser
       generic map( CHANNEL=> std_logic_vector(conv_unsigned(I,8)))
       port map (
        clk       => sysClk40,
	rst       => sysRst40,
	d_in      => serialin(I),
        enabled   => enablereadout(I),
        replynow => '0',
        marker    => marker,
	d_out     => channeldata(I)(15 downto 0),
        ld        => chanld(I),
        sof       => channeldata(I)(16),
        eof       => channeldata(I)(17)
        );
      channelfifo : entity work.FifoSync 
        generic map(
          DATA_WIDTH_G => 18,
          ADDR_WIDTH_G => 13,
          FULL_THRES_G => 6166)
         port map (
           din => channeldata(I),
           clk => sysClk40,
           rd_en => reqdata(I),
           rst => rst,
           wr_en => chanld(I),
           dout => datain(I),
           empty => open,
           full => open,
           overflow => overflow(I),
           prog_full => dfifothreshn(I),
           valid => indatavalid(I),
           underflow => underflow(I));
     channeldataflag: entity work.dataflagnew
       port map(
         eofin=>channeldata(I)(17),
         eofout=>datain(I)(17),
         datawaiting=> datawaiting(I),
         moredatawaiting=> moredatawaiting(I),
         clkin=>sysClk40,
         clkout=>sysClk40,
         rst=>rst
       );
   end generate CHANNELREADOUT;

   starttdcreadout<=l1a and channeloutmask(tdcchannel);

   TESTBEAMEVDATA: if (atlasafp='0') generate
     thereadout: entity work.tdcreadout
       generic map(CHANNEL=>toSlv(tdcchannel, 8))
       port map(
         clk=>sysClk160,
         slowclock=>sysClk40,
         rst=>rst160,
         go=>starttdcreadout,
         delay=>tdcreadoutdelay,
         counter1=>counterout1,
         counter2=>counterout2,
         trgtime=>trgtimel,
         deadtime=>deadtimel,
         status=>status(14 downto 0),
         marker=>marker,
         l1count=>l1countl,
         bxid=>trgtimel(7 downto 0),
         d_out=>tdcdata(15 downto 0),
         ld=>tdcld,
         busy=>tdcbusy,
         sof=>tdcdata(16),
         eof=>tdcdata(17),
         runmode => calibmode,
         eudaqdone => eudaqdone,
         eudaqtrgword => eudaqtrgword,
         hitbus => "00",
         triggerword => latchtriggerword
       );
     tdcfifo : entity work.FifoAsync
       generic map(
         DATA_WIDTH_G => 18,
         ADDR_WIDTH_G => buffersizetdc,
         FULL_THRES_G => (2**buffersizetdc)*4/5)
       port map (
         din => tdcdata,
         rd_clk => sysClk40,
         rd_en => reqdata(tdcchannel),
         rst => rst,
         wr_clk => sysClk160,
         wr_en => tdcld,
         dout => datain(tdcchannel),
         empty => empty,
         full => full,
         overflow => overflow(tdcchannel),
         prog_full => dfifothreshn(tdcchannel),
         valid => indatavalid(tdcchannel),
         underflow => underflow(tdcchannel));
     tdcdataflag: entity work.dataflagnew
       port map(
         eofin=>tdcdata(17),
         eofout=>datain(tdcchannel)(17),
         datawaiting=>datawaiting(tdcchannel),
         moredatawaiting=>moredatawaiting(tdcchannel),
         clkin=>sysClk160,
         clkout=>sysClk40,
         rst=>rst);
   end generate TESTBEAMEVDATA;
   AFPEVDATA: if (atlasafp='1') generate
     with ttcsim select 
       ttcefbdata<= ttcefbdatarx when '0',
                    ttcefbdatasim when others;
     with ttcsim select 
       ldttcefb<= ldttcefbrx when '0',
                    ldttcefbsim when others;
     ttcroenabled<= trigenabled and datapath and not ttcsim;
     ttcreadout_inst: entity work.ttcreadoutefb
       port map(
         clk => rxClk160,
         clken => rxClk160En,
         clk40 => sysClk40,
         rst40 => sysRst40,
         regclk => axiClk40,
         rstCounters => softrst,
         bcrl1acounter => bcrl1acounter,
         l1abcrcounter => l1abcrcounter,
         l1al1acounter => l1al1acounter,
         bcrveto => bcrveto,
         vetoout => tdcbusy,
         vetoFirstBcid => vetoFirstBcid,
         vetoNumBcid => vetoNumBcid,
         ttc_in => ttc_out,
         enabled => ttcroenabled, 
         ecr => ecrin,
         bcr => bcrin,
         d_out=>ttcefbdatarx,
         ld=>ldttcefbrx);
     ttcreadoutsim_inst: entity work.ttcreadoutefbsim
       port map(
         clk => sysClk40,
         enabled => trigenabled,
         l1a => l1a,
         trgtime => trgtime,
         l1count => l1countlong,
         d_out => ttcefbdatasim,
         ld => ldttcefbsim);
      ttcefbfifo : entity work.FifoAsync
        generic map(
          DATA_WIDTH_G => 53,
          ADDR_WIDTH_G => buffersizettcefb,
          FWFT_EN_G => true,
          FULL_THRES_G => (2**buffersizettcefb)*4/5)  
        port map (
          din => ttcefbdata,
          rd_clk => sysClk80,
          rd_en => efbreadenable,
          rst => rstRC,
          wr_clk => sysClk40,
          wr_en => ldttcefb,
          dout => efbdataout,
          prog_full => dfifothreshefb(tdcchannel),
          underflow => underflowv(tdcchannel),
          valid => efbvalid);
     indatavalid(tdcchannel)<='0';
     datawaiting(tdcchannel)<='0';

     efbenabled<=trigenabled and datapath;
     efb_inst: entity work.eventfragmentbuilder
       generic map(
         AXI_CONFIG_C  => ssiAxiStreamConfig(4), 
         hptdcChannel1 => hptdcChannel1,
         hptdcChannel2 => hptdcChannel2)
       port map(
         clk=>sysClk80,
         regClk => axiClk40,
         rst=>rst,
         enabled => efbenabled,
         enableEcrReset => enableEcrReset,
         enabledmask => enabledmask,
         mAxisMaster=> rxAxisMasterSLink,
         mAxisCtrl=> rxAxisCtrlSLink,
         mAxisMasterMon=> efbAxisMaster,
         mAxisSlaveMon=> efbAxisSlave,
         channelmask=>channeloutmask(15 downto 0),
         disablemask => disablemask,
         masked=>efbMaskedChannels,
         maskedInRun=>efbMaskedInRun,
         maskedPerm=>efbMaskedPerm,
         resetFifo=>resetFifo,
         counters => efbcounters,
         resetRun => softrst,
         nExp => nExp,
         bcidShift => bcidShift,
         timeout => efbtimeout,
         timeoutfirst => efbtimeoutfirst,
         missing_header_timeout => efb_missing_header_timeout,
         runnumber => runnumber,
         rodid => rodid,
         detevtype => detevtype,
         doecrreset => doecrreset,
         nummon => nummon,
         monenabled => monenabled,
         nEvt => nEvt,
         nEvtGood => nEvtGood,
         nEvtNonMon => nEvtNonMon,
         datavalid => dvalidefb,
         nextdatavalid => dnextvalidefb,
         datain=>fei4dataefb,
         nextdatain=> dnextefb,
         ldfei4 => ldefb,
         ttcvalid => efbvalid,
         ttcdata => efbdataout,
         ttcld => efbreadenable
         );
   end generate AFPEVDATA;
   --ila0_inst: entity work.ila_0
   --  port map(
   --    clk => rxClk160,
   --    probe0(0) => ttc_out.trigL1,
   --    probe1(0) => ttc_out.bc.valid,
   --    probe2(0) => ttc_out.bc.cmdData(0),
   --    probe3(0) => ttc_out.bc.cmdData(1),
   --    probe4 => ttc_out.eventCnt,
   --    probe5 => ttc_out.eventRstCnt,
   --    probe6 => ttc_out.bunchCnt,
   --    probe7 => ttc_out.bunchRstCnt,
   --    probe8(0) => busy160);
       
              
   multiplexer: entity work.multiplexdata
     generic map(
       AXI_CONFIG_C  => TX_AXI_CONFIG_C ,
       maxchannel => maxchannel
     )
     port map(
       clk=>sysClk40,
       rst=>rst,
       pausedout=>neout,
       --l1counter=>l1countlong,
       badparity=>badparity,
       --triggerin=>l1a,
       --newtrig => newtrig,
       --maxlength=>maxlength,
       --goodcounter=>goodcounter,
       --badcounter=>badcounter,
       --eofcounter=>eofc,
       --sofcounter=>sofc,
       channelmask=>channeloutmask,
       datawaiting=>datawaiting,
       moredatawaiting=>moredatawaiting,
       indatavalid => indatavalid,
       datain=>datain,
       mAxisMaster=> txAxisMaster,
       mAxisSlave=> txAxisSlave,
       reqdata=>reqdata,
       multiplicity => multiplicity
       );
--   aximux: entity work.AxiStreamMux
--     generic map(NUM_SLAVES_G=>1)
--     port map(axisClk=>sysClk40,
--              axisRst=>sysRst40,
--              sAxisMasters(0)=>intAxisMaster,
--              sAxisSlaves(0)=>intAxisSlave,
--              mAxisMaster=>txAxisMaster,
--              mAxisSlave=>txAxisSlave);

    CLKFEI4: BUFGMUX
      port map(
        O=> receiveclock,
        I0 => sysClk40Unbuf,
        I1 => sysClk160Unbuf,
        S => selfei4clk(1)
        );
    --CLKFEI490: BUFGMUX
    --  port map(
    --    O=> receiveclock90,
    --    I0 => sysClk40Unbuf90,
    --    I1 => sysClk160Unbuf90,
    --    S => selfei4clk(1)
    --    );

    FRAMEDCHANNELREADOUT:
    for I in framedFirstChannel to framedLastChannel generate
    --receivedata: entity work.syncdatac
    --  port map(
    --    phaseConfig => phaseConfig,
    --    clk => receiveclock,
    --    clk90 => receiveclock90,
    --    rdatain => serialin(I),
    --    rst => sysRst40,
    --    useaout => open,
    --    usebout => open,
    --    usecout => open,
    --    usedout => open,
    --    sdataout => recdata(I));
    FE_CHANNEL: if ((I/=3 and I/=7) or hitbusreadout='0') generate
    alignframe: entity work.framealign
      port map(
        clk => receiveclock,
        rst => rstRC,
        d_in => recdata(I),
        d_out => alignout(I),
        aligned => aligned(I)
        );
    deser8b10b: entity work.deser10b
      port map(
        clk => receiveclock,
        rst => rstRC,
        d_in => alignout(I),
        align => aligned(I),
        d_out => data8b10b(I),
        ld => ld8b10b(I)
        );
   decode8b10b: entity work.Decoder8b10bND
     generic map(
       NUM_BYTES_G => 1)
     port map(
       clk => receiveclock,
       dataIn => data8b10b(I),
       dataOut => dout8b10b(I)(7 downto 0),
       dataKOut(0) => dout8b10b(I)(8),
       clkEn => ld8b10b(I),
       rst => rstRC,
       codeErr(0) =>dout8b10b(I)(9),
       newData =>ldout8b10b(I)
       );
   AFP_DECODINGERRORS: if (atlasafp='1') generate
   process (receiveclock, rstRC) begin
     if(rstRC='1')then
       decodingerrors(I)<=(others=>'0');
     elsif rising_edge(receiveclock)then
       if(ldout8b10b(I)='1' and dout8b10b(I)(9)='1' and channeloutmask(I)='1')then
	decodingerrors(I)<=unsigned(decodingerrors(I))+1;
       end if;
     end if;
   end process;
   end generate AFP_DECODINGERRORS;
   enablereadout(I)<=channeloutmask(I) and ((trigenabled and not ofprotection) or not dfifothresh(I));
   decode: entity work.decodefei4record
    port map(clk => receiveclock,
             rst => rstRC,
             enabled => enablereadout(I),
             eventlimit => eventlimit,
             isrunning => isrunning(I),
             d_in => dout8b10b(I)(7 downto 0),
             k_in => dout8b10b(I)(8),
             err_in => dout8b10b(I)(9),
             d_out => fei4data(I),
             ldin => ldout8b10b(I),
             ldout => ldfei4(I),
             overflow => bufoverflow(I)
             );
   fei4fifo : entity work.lookaheadfifo
     generic map(
       DATA_WIDTH_G => 25,
       ADDR_WIDTH_G => buffersizefe,
       FULL_THRES_G => (2**buffersizefe)*4/5) 
     port map (
       din => fei4data(I),
       rd_clk => sysClk40,
       rd_en => ldfifo(I), 
       rst => rstRC,
       wr_clk => receiveclock,
       wr_en => ldfei4reg(I),
       dout => fifodata(I),
       dnext => dnext(I),
       dnextvalid => dnextvalid(I),
       empty => emptyv(I),
       full => fullv(I),
       overflow => overflowv(I),
       prog_full => dfifothreshn(I),
       valid => dvalid(I),
       underflow => underflowv(I));
    fei4dataflag: entity work.dataflagff
      port map(
        eofin=>fei4data(I)(24),
        ldin => ldfei4reg(I),
        eofout=>dnext(I)(24),
        ldout => ldfifo(I),
        datawaiting=>datawaiting(I),
        moredatawaiting=>moredatawaiting(I),
        clkin=>receiveclock,
        clkinrate=>selfei4clk(1),
        clkout=>sysClk40,
        rst=>rstRC);
   encode: entity work.encodepgp24bit
    generic map( CHANNEL=> std_logic_vector(conv_unsigned(I,8)))
    port map(clk => sysClk40,
         rst => rstRC,
         enabled =>channeloutmask(I),
         maxlength => maxlength,
         isrunning => open,
         d_in => fifodata(I),
         d_next => dnext(I),
         marker => marker,
         d_out => datain(I),
         ldin => reqdata(I), 
         ldout => ldfifo(I),
         dnextvalid => dnextvalid(I),
         dvalid => dvalid(I),
         datawaiting=> datawaiting(I),
         moredatawaiting=> moredatawaiting(I),
         overflow => frameoverflow(I),
         valid => indatavalid(I)
    );
   with datapath select
     ldfei4reg(I) <= ldfei4(I) when '0',
                     '0' when others;
    end generate FE_CHANNEL;
    AFP_FEFORMATTER: if (atlasafp='1') generate
      with datapath select
        ldfei4efb(I) <= ldfei4(I) when '1',
                        '0' when others;
    AFP_FEFORMATTER_FEI4: if (I/=hptdcChannel1 and I/=hptdcChannel2) generate
      fei4bformatter_inst: entity work.fei4bformatter
        generic map( CHANNEL=> std_logic_vector(conv_unsigned(I,4)))
        port map(
          clk => receiveclock,
          rst => rstRC,
          hitdiscconfig => hitdiscconfig(I),
          d_in => fei4data(I),
          d_out => fei4dataformatted(I),
          ldin => ldfei4efb(I),
          ldout => ldformatted(I));
      end generate AFP_FEFORMATTER_FEI4;
    AFP_FEFORMATTER_HPTDC1: if (I=hptdcChannel1) generate
      hptdcformatter_inst: entity work.hptdcformatter
        generic map( CHANNEL=> std_logic_vector(conv_unsigned(I,4)))
        port map(
          clk => receiveclock,
          rst => rstRC,
          d_in => fei4data(I),
          d_out => fei4dataformatted(I),
          hptdctemps => hptdctemps1,
          ldin => ldfei4efb(I),
          ldout => ldformatted(I));
      end generate AFP_FEFORMATTER_HPTDC1;
    AFP_FEFORMATTER_HPTDC2: if (I=hptdcChannel2) generate
      hptdcformatter_inst: entity work.hptdcformatter
        generic map( CHANNEL=> std_logic_vector(conv_unsigned(I,4)))
        port map(
          clk => receiveclock,
          rst => rstRC,
          d_in => fei4data(I),
          d_out => fei4dataformatted(I),
          hptdctemps => hptdctemps2,
          ldin => ldfei4efb(I),
          ldout => ldformatted(I));
      end generate AFP_FEFORMATTER_HPTDC2;
      fei4fifo : entity work.lookaheadfifo
        generic map(
          DATA_WIDTH_G => 33,
          ADDR_WIDTH_G => buffersizefeefb,
          FULL_THRES_G => (2**buffersizefeefb)*4/5) 
        port map (
          din => fei4dataformatted(I),
          rd_clk => sysClk80,
          rd_en => ldefb(I), 
          rst => resetFifo(I),
          wr_en => ldformatted(I),
          wr_clk => receiveclock,
          dout => fei4dataefb(I),
          dnext => dnextefb(I),
          dnextvalid => dnextvalidefb(I),
          empty => open,
          full => open,
          overflow => open,
          prog_full => dfifothreshefb(I),
          valid => dvalidefb(I),
          underflow => open);
    end generate AFP_FEFORMATTER;
    HITBUS_CHANNEL: if (hitbusreadout='1' and (I=3 or I=7)) generate
    alignframe: entity work.framealignhitbus
      port map(
        clk => receiveclock,
        rst => rstRC,
        d_in => recdata(I),
        d_out => alignout(I),
        aligned => aligned(I)
        );
    deserhitbus: entity work.deser4b
      port map(
        clk => receiveclock,
        rst => rstRC,
        d_in => alignout(I),
        align => aligned(I),
        d_out => data8b10b(I)(3 downto 0),
        ld => ld8b10b(I)
        );
    hitbusin(I/4*3)<=data8b10b(I)(2);
    hitbusin(I/4*3+1)<=data8b10b(I)(1);
    hitbusin(I/4*3+2)<=data8b10b(I)(0);
    thehitbuspipeline: entity work.hitbuspipeline
     port map(
     rst  => rstRC,
     clk => receiveclock,
     ld => ld8b10b(I),
     depth => hitbusdepth,
     wordin => data8b10b(I)(2 downto 0),
     wordout => hitbusword(I/4)
     );
    end generate HITBUS_CHANNEL;
   end generate FRAMEDCHANNELREADOUT;
   hbin: if framedfirstchannel>7 or framedlastchannel<7 or hitbusreadout='0' generate
      hitbusin(3)<='0';  
      hitbusin(4)<='0';  
      hitbusin(5)<='0';  
   end generate hbin;
   hbin2: if framedfirstchannel>3 or framedlastchannel<3 or hitbusreadout='0' generate
      hitbusin(0)<='0';  
      hitbusin(1)<='0';  
      hitbusin(2)<='0';  
   end generate hbin2;

      to_gbtBank_clks.tx_frameClk(1) <= sysClk40;
      to_gbtBank_clks.rx_frameClk(1) <= sysClk40;
      to_gbtBank_clks.mgt_clks.mgtRefClk        <= sysClk120;
      to_gbtBank_clks.mgt_clks.mgtRstCtrlRefClk <= sysClk40;
      to_gbtBank_clks.mgt_clks.cpllLockDetClk   <= stableClk125;
      to_gbtBank_clks.mgt_clks.drpClk           <= stableClk125;
      to_gbtBank_gbtTx(1).reset             <= sysRst40;
      to_gbtBank_gbtTx(1).isDataSel         <= '0';
      to_gbtBank_gbtTx(1).data              <= txData_to_gbtBank(1);
      to_gbtBank_gbtTx(1).extraData_wideBus <= (others => '0');

      to_gbtBank_gbtRx(1).reset           <= sysRst40;
      to_gbtBank_gbtRx(1).rxFrameClkReady <= sysClkLock;

      to_gbtBank_mgt.mgtLink(1).drp_addr <= "000000000";
      to_gbtBank_mgt.mgtLink(1).drp_en   <= '0';
      to_gbtBank_mgt.mgtLink(1).drp_di   <= x"0000";
      to_gbtBank_mgt.mgtLink(1).drp_we   <= '0';

      to_gbtBank_mgt.mgtLink(1).prbs_txSel      <= "000";
      to_gbtBank_mgt.mgtLink(1).prbs_rxSel      <= "000";
      to_gbtBank_mgt.mgtLink(1).prbs_txForceErr <= '0';
      to_gbtBank_mgt.mgtLink(1).prbs_rxCntReset <= '0';

      to_gbtBank_mgt.mgtLink(1).conf_diffCtrl   <= "1000";             -- Comment: 807 mVppd
      to_gbtBank_mgt.mgtLink(1).conf_postCursor <= "00000";            -- Comment: 0.00 dB (default)
      to_gbtBank_mgt.mgtLink(1).conf_preCursor  <= "00000";            -- Comment: 0.00 dB (default)
      -- to_gbtBank_mgt.mgtLink(1).conf_txPol      <= '0';  -- Comment: Not inverted
      to_gbtBank_mgt.mgtLink(1).conf_txPol      <= '1';  -- Comment: inverted
      to_gbtBank_mgt.mgtLink(1).conf_rxPol      <= '0';  -- Comment: Not inverted     

      to_gbtBank_mgt.mgtLink(1).rxBitSlip_enable   <= '1';
      to_gbtBank_mgt.mgtLink(1).rxBitSlip_ctrl     <= '0';
      to_gbtBank_mgt.mgtLink(1).rxBitSlip_nbr      <= "000000";
      to_gbtBank_mgt.mgtLink(1).rxBitSlip_run      <= '0';
      to_gbtBank_mgt.mgtLink(1).rxBitSlip_oddRstEn <= '0';  -- Comment: If '1' resets the MGT RX when the the number of bitslips 
      to_gbtBank_mgt.mgtLink(1).loopBack <= "00";

      to_gbtBank_mgt.mgtLink(1).rx_p <= mgtRxP;
      to_gbtBank_mgt.mgtLink(1).rx_n <= mgtRxN;

       mgtTxN <= from_gbtBank_mgt.mgtLink(1).tx_n;
       mgtTxP <= from_gbtBank_mgt.mgtLink(1).tx_p;

      to_gbtBank_mgt.mgtLink(1).tx_reset <= sysRst40;
      to_gbtBank_mgt.mgtLink(1).rx_reset <= sysRst40;

   SyncOut_data40 : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 84)    
      port map (
         clk        => receiveclock,
         dataIn => from_gbtBank_gbtRx(1).data,
         dataOut => recdata40);  

   bitgen4: for I in 0 to 83 generate
        gbtDataReordered((I mod 4)*21+I/4)<= from_gbtBank_gbtRx(1).data(I) ;
   end generate bitgen4;
   inst_gbtDataIn : entity work.gbtDataIn160
     PORT MAP (
       rst => sysRst40,
       wr_clk => sysClk40,
       rd_clk => receiveclock,
       din => gbtDataReordered,
       wr_en => '1',
       rd_en => '1',
       dout => recdata160,
       full => open,
       empty => open
       );
     
   recdatagen: for I in framedFirstChannel to framedLastChannel generate
     with selfei4clk(1) select
      recdata(I) <= recdata40(I*4) when '0',
                    recdata160(I) when '1',
                    '0' when others;
   end generate recdatagen;
      --gbtIla: entity work.ila_0
      --      port map (
      --         CLK => sysclk40,
      --         PROBE0(0) => icword(31),
      --         PROBE1(0) => icdone,
      --         PROBE2 => icin,
      --         PROBE3 => from_gbtBank_gbtRx(1).data(83 downto 82));
      --gbtIla2: entity work.ila_1
      --      port map (
      --         CLK => receiveclock,
      --         PROBE0 => recdata(19 downto 0));

      gbtStatus(0)<=from_gbtBank_gbtTx(1).latOptGbtBank_tx;
      gbtStatus(1)<=from_gbtBank_gbtTx(1).txGearboxAligned_o;
      gbtStatus(2)<=from_gbtBank_gbtTx(1).txGearboxAligned_done;
      gbtStatus(3)<=from_gbtBank_gbtRx(1).ready;
      gbtStatus(4)<=from_gbtBank_mgt.mgtLink(1).ready;
      gbtStatus(5)<=sysClklock;

      ic_channel_inst: entity work.icwritephytx
        port map(
          sysClk40 => sysClk40,
          sysRst40 => sysRst40,
          gbtaddr => icword(30 downto 24),
          regaddress => icword(15 downto 0),
          value => icword(23 downto 16),
          ic => icin,
          done => icdone,
          go => icword(31));
      reseteport_inst: entity work.eportphasereset
        port map(sysClk40 => sysClk40,
                 sysRst40 => sysRst40,
                 gbtaddr => gbtaddr,
                 ic => icrst,
                 useic => useic,
                 done => open,
                 go => phaseconfig);
      with useic select
         txData_to_gbtBank(1)(83 downto 82)<= icin when '0',
                                             icrst when '1',
                                              icin when others;
      gbtBank : entity work.gbt_bank
      generic map (
         GBT_BANK_ID     => 0,
         NUM_LINKS       => 1,
         TX_OPTIMIZATION => TX_OPTIMIZATION_G,
         RX_OPTIMIZATION => STANDARD,
         TX_ENCODING     => GBT_FRAME,
         RX_ENCODING     => GBT_FRAME)
      port map (
         CLKS_I   => to_gbtBank_clks,
         CLKS_O   => from_gbtBank_clks,
         --------------------------------------------------               
         GBT_TX_I => to_gbtBank_gbtTx,
         GBT_TX_O => from_gbtBank_gbtTx,
         --------------------------------------------------               
         MGT_I    => to_gbtBank_mgt,
         MGT_O    => from_gbtBank_mgt,
         --------------------------------------------------               
         GBT_RX_I => to_gbtBank_gbtRx,
         GBT_RX_O => from_gbtBank_gbtRx
         );



end PixelGbtCore;
