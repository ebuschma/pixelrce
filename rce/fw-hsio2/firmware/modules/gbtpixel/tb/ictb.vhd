-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2017-03-02
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity ictb is end ictb;

architecture testbed of ictb is
   signal clk  : sl;
   signal rst  : sl;
   signal go   : sl:='0';
   signal done : sl;
   signal gbtaddr : slv(6 downto 0):="0000010";
   signal regaddress : slv(15 downto 0):=x"010d";
   signal ic: slv(1 downto 0);
   signal value: slv(7 downto 0):=x"04";
   signal counter: integer range 0 to 255:=0;
   signal useic: sl;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 50 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);


   process begin
   wait until rising_edge(clk);
   counter<=counter+1;
   if(counter=5)then
     go<='1';
   else
     go<='0';
   end if;
   end process;

   writereg: entity work.eportphasereset
     port map(sysClk40 => clk,
              sysRst40 => rst,
              gbtaddr => gbtaddr,
              ic => ic,
              useic => useic,
              done => done,
              go => go);
   --writereg: entity work.icwritephytx
   -- port map(sysClk40 => clk,
   --      sysRst40 => rst,
   --      gbtaddr => gbtaddr,
   --      regaddress => regaddress,
   --      value => value,
   --      ic => ic,
   --      done => done,
   --      go => go
   -- );

end testbed;
