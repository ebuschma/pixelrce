.PHONY: program
program:
	$(call ACTION_HEADER, "Program bit file")
	@cd $(OUT_DIR); vivado -mode batch -source $(PROJ_DIR)/vivado/program.tcl
