-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2015-01-07
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;

entity ttcreadouttb is end ttcreadouttb;

architecture testbed of ttcreadouttb is
   signal clk, clk1  : sl;
   signal rst  : sl;
   signal ttcin : AtlasTTCRxOutType:=ATLAS_TTC_RX_OUT_INIT_C ;
   signal d_out: slv(15 downto 0);
   signal go: sl:='0';
   signal busy, ttcbusy, serbusy: sl;
   signal l1a, l1mod: sl;
   signal conftrg: sl:='0';
   signal serialout: sl;
   signal trgdelay: slv(7 downto 0) := x"05";
   signal deadtime: slv(15 downto 0):= x"0001";
   signal counter: integer range 0 to 511 := 0;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 0 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => open,
         rstL => open);
   CLK_1 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 6.25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 25 ns)  -- Hold reset for this long)
      port map (
         clkP => clk1,
         clkN => open,
         rst  => rst,
         rstL => open);

   process begin
     wait until rising_edge(clk1);
     if(counter<512)then
       counter <= counter+1;
       if(counter=100 or counter=120)then
         ttcin.trigL1<='1';
           ttcin.bunchCnt<=x"abc";
         if(counter=100)then
           ttcin.eventCnt<=x"123456";
         else
           ttcin.eventCnt<=x"654321";
         end if;
         ttcin.bunchRstCnt<=x"de";
         ttcin.eventRstCnt<=x"78";
       elsif(counter>=6 and counter<=9) then
         conftrg<='1';
       else
         conftrg<='0';
         ttcin.trigL1<='0';
       end if;
     end if;
   end process;

   coincidence_inst: entity work.coincidence
     port map(
       clk => clk,
       rst => rst,
       enabled => '1',
       fifothresh => '0',
       trgin => ttcin.trigL1,
       serbusy => serbusy,
       tdcreadoutbusy => '0',
       extbusy => '0',
       l1a => l1a,
       busy => busy,
       coinc => open,
       coincd => open);
   ttcreadout_Inst : entity work.ttcreadout
     generic map( CHANNEL => x"1c")
      port map (
        clk => clk1,
        rst => rst,
        ttc_in => ttcin,
        enabled => '1',
        go => l1a,
        d_out => d_out,
        busy => ttcbusy,
        ld => open,
        sof => open,
        eof => open);
   triggerpipeline_inst: entity work.triggerpipeline
     port map(
       rst => rst,
       clk => clk,
       L1Ain => l1a,
       L1Aout => l1mod,
       configure => conftrg,
       delay => trgdelay,
       busy => serbusy,
       deadtime => deadtime);
   ser_inst: entity work.ser
     port map(
       clk=> clk,
       ld => open,
       l1a => l1mod,
       go => '0',
       busy => open,
       stop => '0',
       rst => rst,
       d_in => (others => '0'),
       d_out => serialout);

end testbed;
