-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2013-10-02
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;

entity adcreadouttb is end adcreadouttb;

architecture testbed of adcreadouttb is
   signal clk  : sl;
   signal rst  : sl;
   signal adcs : Slv16Array(7 downto 0) := (0=>x"0001", 1 => x"0002", 2=>x"0003", 3=>x"0004",
                                            4=>x"0005", 5=>x"0006", 6=>x"0007", 7=>x"0008");
   signal runonce: sl:='1';
   signal eof, sof, ld, go: sl;
   signal d_out: slv(15 downto 0);
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 50 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);


   process begin
   wait until rising_edge(clk);
   if(rst='0' and runonce='1')then
     go<='1';
     runonce<='0';
   else
     go<='0';
   end if;
   end process;
   adcreadout_1 : entity work.adcreadout
      generic map(
         CHANNEL => "1001")
      port map (
         clk => clk,
         rst => rst,
         d_in => adcs,
         enabled=>'1',
         go => go,
         d_out => d_out,
         ld => ld,
         sof => sof,
         eof => eof);

end testbed;
