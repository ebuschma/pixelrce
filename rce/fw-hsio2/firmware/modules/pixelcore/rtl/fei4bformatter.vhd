--------------------------------------------------------------
-- FEI4B formatter
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;

--------------------------------------------------------------

entity fei4bformatter is
generic( CHANNEL: std_logic_vector(3 downto 0):=x"f");
port(	clk: 	       in std_logic;
	rst:	       in std_logic;
        hitdiscconfig: in std_logic_vector(1 downto 0);
	d_in:	       in std_logic_vector(24 downto 0);
	d_out:	       out std_logic_vector(32 downto 0);
        ldin:          in std_logic;
        ldout:         out std_logic
);
end fei4bformatter;

--------------------------------------------------------------

architecture FEI4BFORMATTER of fei4bformatter is


begin
    process(rst, clk)
    begin
        if(rst='1') then
          d_out<=(others => '0');
          ldout<='0';
        elsif (clk'event and clk='1') then
          if(ldin='1')then 
            ldout<='1';
            d_out(32)<=d_in(24);  -- forward eof
            d_out(27 downto 24)<= CHANNEL;
            if(d_in(23 downto 8)=x"0000")then --empty record
              d_out(31 downto 28)<=x"0"; --svc record
              d_out(23)<='1'; -- internal svc record
              d_out(22 downto 18)<="00000";
              d_out(17 downto 16)<=d_in(1 downto 0);
              d_out(15 downto 0)<=(others => '0');
            elsif(d_in(23 downto 16)=x"e9")then --header
              d_out(31 downto 28)<=x"3"; --header
              d_out(23 downto 16)<=(others => '0');
              d_out(15 downto 0)<=d_in(15 downto 0); --flag/l1id/bcid
            elsif(d_in(23 downto 16)=x"ef")then --service record
              d_out(31 downto 28)<=x"0"; --service record
              d_out(23 downto 16)<=(others => '0');
              d_out(15 downto 0)<=d_in(15 downto 0); --count/type
            elsif(d_in(23 downto 22)/="11" )then --data record
              d_out(31 downto 30)<="11"; --data record
              d_out(29 downto 28)<=hitdiscconfig;
              d_out(23 downto 0)<=d_in(23 downto 0);
            else
              d_out(31 downto 28)<=x"0"; --svc record
              d_out(23)<='1'; -- internal svc record
              d_out(18)<='1'; --other record
              d_out(22 downto 19)<=(others => '0');
              d_out(17 downto 0)<=(others => '0');
            end if;     
          else
            ldout<='0';
          end if;
 	end if;
    end process;		

end FEI4BFORMATTER;

--------------------------------------------------------------
