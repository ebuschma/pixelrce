-------------------------------------------------------------------------------
-- Title      : Configuration Stream Receiver
-- Project    : HSIO Pixel code
-------------------------------------------------------------------------------
-- File       : ConfigurationStreamReceiver.vhd
-- Author     : Martin Kocian, kocian@slac.stanford.edu
-- Created    : 2014-10-28
-- Last update: 2014-10-30
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Copyright (c) 2014 by Martin Kocian. All rights reserved.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;

entity ConfigurationStreamReceiver is
   port (

      -- Streaming Data Interface
      sysClk     : in  sl;
      sysRst     : in  sl := '0';
      sAxisMaster : in  AxiStreamMasterType;
      sAxisSlave  : out AxiStreamSlaveType;

      -- Config signals
      go        : out sl;
      replynow  : out sl;
      empty     : in  sl;
      progfull  : in  sl;
      blockdata : out slv(31 downto 0);
      writevalid: out sl
      );
end ConfigurationStreamReceiver;

architecture rtl of ConfigurationStreamReceiver is

   type RegType is record
      txnNumber : slv(1 downto 0);
      go        : sl;
      handshake : sl;
      blockdata : slv(31 downto 0);
      writevalid: sl;
      eof       : sl;
   end record RegType;

   constant REG_INIT_C : RegType := (
      txnNumber => (others => '0'),
      go => '0',
      handshake => '0',
      blockdata => (others => '0'),
      writevalid => '0',
      eof => '0'
      );

   signal r   : RegType := REG_INIT_C;
   signal rin : RegType;

   constant INT_CONFIG_C : AxiStreamConfigType := ssiAxiStreamConfig(4);

   signal oldempty: sl;
begin

   ----------------------------------

   comb : process (sysRst, sAxisMaster, r) is
      variable v : RegType;
   begin
      v := r;

      -- Init, always read
      v.writevalid := '0';
      v.eof := '0';

      if (sAxisMaster.tValid = '1') then
        if(ssiGetUserSof(INT_CONFIG_C, sAxisMaster)='1') then
          v.txnNumber := "11";
        elsif (r.txnNumber /= "00")then
          v.txnNumber := unsigned(r.txnNumber)-1;
         case r.txnNumber is
            when "11" =>
               v.handshake := sAxisMaster.tData(0);
            when others => null;
         end case;
        else
          v.blockdata := sAxisMaster.tData;
          v.eof := sAxisMaster.tLast;
          v.writevalid := '1';
        end if;
      end if;

      if (sysRst = '1') then
         v := REG_INIT_C;
      end if;

      rin <= v;

   end process comb;

   blockdata <= r.blockdata;
   writevalid <= r.writevalid;

   seq : process (sysClk) is
   begin
      if (rising_edge(sysClk)) then
         r <= rin;
      end if;
   end process seq;

   process (sysRst, sysClk) is
   begin
     if(sysRst='1')then
       go <= '0';
       oldempty <= '0';
     elsif(rising_edge(sysClk)) then
       go <= r.eof;
       oldempty <= empty;
       if(empty='1' and oldempty='0') then
         replynow<=r.handshake;
       else
         replynow <= '0';
       end if;
     end if;
   end process;
       
   sAxisSlave.tReady <= not progfull;

end architecture rtl;

