--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;
use work.all;

--------------------------------------------------------------

entity ttcreadout is
generic( CHANNEL: std_logic_vector(7 downto 0):=x"ff");
port(	clk: 	    in std_logic;
	rst:	    in std_logic;
        ttc_in:     in AtlasTTCRxOutType;
        enabled:    in std_logic;
        go:         in std_logic;
	d_out:	    out std_logic_vector(15 downto 0);
        busy:       out std_logic;
        ld:         out std_logic;
        sof:        out std_logic;
        eof:        out std_logic
);
end ttcreadout;

--------------------------------------------------------------

architecture TTCREADOUT of ttcreadout is
  signal bunchCnt: slv(11 downto 0);
  signal bunchRstCnt: slv(7 downto 0);
  signal eventCnt: slv(23 downto 0);
  signal eventRstCnt: slv(7 downto 0);

begin

   SyncOut_bunchCnt : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 12)    
      port map (
         clk    => clk,
         dataIn => ttc_in.bunchCnt,
         dataOut => bunchCnt);  
   SyncOut_bunchRstCnt : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 8)    
      port map (
         clk    => clk,
         dataIn => ttc_in.bunchRstCnt,
         dataOut => bunchRstCnt);  
   SyncOut_eventCnt : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 24)    
      port map (
         clk    => clk,
         dataIn => ttc_in.eventCnt,
         dataOut => eventCnt);  
   SyncOut_eventRstCnt : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 8)    
      port map (
         clk    => clk,
         dataIn => ttc_in.eventRstCnt,
         dataOut => eventRstCnt);  

    process(rst, clk)

    variable headercounter: natural range 0 to 9 := 0;
    begin
        if(rst='1') then
          d_out<=x"0000";
          eof<='0';
          sof<='0';
          headercounter:=0;
          ld<='0';
          busy<='0';
        elsif (clk'event and clk='1') then
          if(headercounter=0 and enabled='1' and go='1')then 
            headercounter:=9;
            busy<='1';
          end if;
          if (headercounter/=0)then  
            if(headercounter=9)then
              ld<='1';                
              sof<='1';
              d_out<=(others=>'0'); 
            elsif(headercounter=8)then
              sof<='0';
              d_out(7 downto 0)<=(others=>'0'); 
              d_out(15 downto 8)<=CHANNEL;
            elsif(headercounter=7)then
              d_out<= eventRstCnt & eventCnt(23 downto 16);
            elsif(headercounter=6)then
              d_out<=eventCnt(15 downto 0);
            elsif(headercounter=5)then
              d_out<=x"00"& ttc_in.bunchRstCnt;
            elsif(headercounter=4)then
              d_out<=x"0" & bunchCnt;
            elsif(headercounter=3)then
              d_out <= (others => '0');
            elsif(headercounter=2)then
              d_out <= (others => '0');
              eof<='1';
            elsif(headercounter=1)then
              eof<='0';
              ld<='0';
              busy<='0';
            end if;
            headercounter:=headercounter-1;
          end if;
 	end if;
    
    end process;		

end TTCREADOUT;

--------------------------------------------------------------
