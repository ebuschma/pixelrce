
LIBRARY ieee;
use work.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.Version.all;
use work.arraytype.all;
use work.StdRtlPkg.all;
use work.AxiLitePkg.all;

entity registers is 
  generic(
    encodingDefault: slv(1 downto 0) := "00";
    atlasafp: sl:='0';
    adcchannel: integer := 29;
    ackchannel: integer := 31;
    tdcchannel: integer := 30);
   port (
     sysClk40      : in std_logic;
     sysRst40      : in std_logic;
     axiClk        : in std_logic;
     axiRst        : in std_logic;
     axiReadMaster : in AxiLiteReadMasterType;
     axiReadSlave  : out AxiLiteReadSlaveType;
     axiWriteMaster : in AxiLiteWriteMasterType;
     axiWriteSlave : out AxiLiteWriteSlaveType;
     clockselect: out slv(1 downto 0);
     trgcount: out slv(15 downto 0);
     calibmode: out slv(1 downto 0);
     resetdelay: out sl;
     incrementdelay: out slv(4 downto 0);
     conftrg: out sl;
     trgdelay: out slv(7 downto 0);
     triggermask: out slv(15 downto 0);
     vetodis: out sl;
     enableEcrReset: out sl;
     blockEcrBcr: out slv(1 downto 0);
     period: out slv(31 downto 0);
     setdeadtime: out slv(15 downto 0);
     channelmask: out slv(31 downto 0);
     channeloutmask: out slv(31 downto 0);
     disablemask: out slv(15 downto 0);
     encoding: out slv(1 downto 0);
     eventlimit: out slv(11 downto 0);
     hitbusop: out slv(15 downto 0);
     discop: out slv(15 downto 0);
     telescopeop: out slv(2 downto 0);
     setfifothresh: out slv(15 downto 0);
     ofprotection: out sl;
     writemem: out sl;
     maxmem: out slv(9 downto 0);
     memval: out slv(31 downto 0);
     l1route: out sl;
     hitbusdepth: out slv(4 downto 0);
     tdcreadoutdelay: out slv(4 downto 0);
     multiplicity: out slv(63 downto 0);
     maxlength: out natural range 0 to 16383:=100;
     outputdelay: out Slv8Array(31 downto 0);
     clk_in_sel: out sl;
     selfei4clk: out slv(1 downto 0);
     adcperiod: out slv(31 downto 0);
     clk_in_sel_in: in sl;
     ttcClkOk: in sl;
     clockcounters: in slv(31 downto 0):=x"00000000";
     gbtstatus: in slv(5 downto 0):="000000";
     icword: out slv(31 downto 0);
     hitdiscconfig: out Slv2Array(15 downto 0);
     nExp: out slv(4 downto 0);
     bcidShift: out slv(11 downto 0);
     efbtimeout: out slv(15 downto 0);
     efbtimeoutfirst: out slv(15 downto 0);
     efb_missing_header_timeout: out slv(7 downto 0);
     runnumber: out slv(31 downto 0);
     rodid: out slv(31 downto 0);
     detevtype: out slv(31 downto 0);
     doecrreset: out slv(15 downto 0);
     nummon: out slv(31 downto 0);
     monenabled: out sl;
     datapath: out sl;
     ttcsim: out sl;
     cdtenabled: out sl;
     numbuffers: out slv(4 downto 0);
     window: out slv(15 downto 0);
     nEvt: in slv(31 downto 0);
     nEvtGood: in slv(31 downto 0);
     nEvtNonMon: in slv(31 downto 0);
     disabledMask: in slv(15 downto 0);
     maskedInRun: in slv(15 downto 0);
     maskedPerm: in slv(15 downto 0);
     bcrl1acounter: in slv(31 downto 0);
     l1abcrcounter: in slv(31 downto 0);
     l1al1acounter: in slv(31 downto 0);
     bcrveto: out sl;
     vetoFirstBcid: out slv(11 downto 0);
     vetoNumBcid: out slv(11 downto 0);
     decodingerrors: in Slv32Array(15 downto 0):=(others => (others =>'0'));
     busycounter: in Slv32Array(15 downto 0):=(others => (others =>'0'));
     ttcbusycounter: in slv(31 downto 0):=(others => '0');
     hptdctemps1: in Slv10Array(7 downto 0):=(others=>(others=>'0'));
     hptdctemps2: in Slv10Array(7 downto 0):=(others=>(others=>'0'));
     efbcounters: in CounterType
     );
end registers;

architecture REGISTERS of registers is

  signal clockselect_a: slv(1 downto 0);
  signal trgcount_a: slv(15 downto 0);
  signal calibmode_a: slv(1 downto 0);
  signal conftrg_a: sl;
  signal trgdelay_a: slv(7 downto 0);
  signal triggermask_a: slv(15 downto 0);
  signal vetodis_a: sl;
  signal blockEcrBcr_a: slv(1 downto 0);
  signal period_a: slv(31 downto 0);
  signal setdeadtime_a: slv(15 downto 0);
  signal channelmask_a: slv(31 downto 0);
  signal channeloutmask_a: slv(31 downto 0);
  signal encoding_a: slv(1 downto 0);
  signal hitbusop_a: slv(15 downto 0);
  signal discop_a: slv(15 downto 0);
  signal telescopeop_a: slv(2 downto 0);
  signal setfifothresh_a: slv(15 downto 0);
  signal ofprotection_a: sl;
  signal writemem_a: sl;
  signal l1route_a: sl;
  signal hitbusdepth_a: slv(4 downto 0);
  signal tdcreadoutdelay_a: slv(4 downto 0);
  signal multiplicity_a: slv(63 downto 0);
  signal maxlength_a: slv(13 downto 0);
  signal maxlength_s: slv(13 downto 0);
  signal outputdelay_a: Slv8Array(31 downto 0); 
  signal clk_in_sel_a: sl:='1';
  signal selfei4clk_a: slv(1 downto 0);
  signal adcperiod_a: slv(31 downto 0);
  signal maxmem_s: slv(9 downto 0);
  signal writemem_s: sl;
  signal nummon_s: slv(31 downto 0);
  signal delayatlasclock: sl;
  signal setatlasclock: sl;
  signal clockcounters_a: slv(31 downto 0);
  signal gbtstatus_a: slv(5 downto 0);
  signal icword_a: slv(31 downto 0);
  signal hptdctemps_s1: Slv10Array(7 downto 0);
  signal hptdctemps_s2: Slv10Array(7 downto 0);
  signal busycounter_s: Slv32Array(15 downto 0);
  signal ttcbusycounter_s: slv(31 downto 0);
  signal decodingerrors_s: Slv32Array(15 downto 0);
  signal disablemask_r: slv(15 downto 0);
  type RegType is record
     writeReg     : sl;
     regDataOut   : slv(31 downto 0);
     address      : slv(7 downto 0);
      
     axiReadSlave  : AxiLiteReadSlaveType;
     axiWriteSlave : AxiLiteWriteSlaveType;
  end record RegType;

  constant REG_INIT_C : RegType := (
     writeReg         => '0',
     regDataOut       => (others => '0'),
     address          => (others => '0'),
     axiReadSlave     => AXI_LITE_READ_SLAVE_INIT_C,
     axiWriteSlave    => AXI_LITE_WRITE_SLAVE_INIT_C);


  signal r   : RegType := REG_INIT_C;
  signal rin : RegType;


begin
   comb : process (axiRst, axiReadMaster, axiWriteMaster, r, channeloutmask_a, channelmask_a) is
      variable v            : RegType;
      variable axiStatus    : AxiLiteStatusType;
      variable axiWriteResp : slv(1 downto 0);
      variable axiReadResp  : slv(1 downto 0);
      variable feindex: integer;
   begin
      -- Latch the current value
      v := r;

      v.writeReg := '0';

      -- Determine the transaction type
      axiSlaveWaitTxn(axiWriteMaster, axiReadMaster, v.axiWriteSlave, v.axiReadSlave, axiStatus);

      if (axiStatus.writeEnable = '1') then
         -- Check for an out of 32 bit aligned address
         axiWriteResp := ite(axiWriteMaster.awaddr(1 downto 0) = "00", AXI_RESP_OK_C, AXI_RESP_SLVERR_C);
         -- Decode address and perform write
         v.writeReg := '1';
         v.address := axiWriteMaster.awaddr(9 downto 2);
         v.regDataOut := axiWriteMaster.wdata;
         axiSlaveWriteResponse(v.axiWriteSlave, axiWriteResp);
      end if;

      if (axiStatus.readEnable = '1') then
         -- Check for an out of 32 bit aligned address
         axiReadResp          := ite(axiReadMaster.araddr(1 downto 0) = "00", AXI_RESP_OK_C, AXI_RESP_SLVERR_C);
         -- Decode address and assign read data
         v.axiReadSlave.rdata := (others => '0');
         if(axiReadMaster.araddr(9 downto 8)/="00")then  --EFB counters
           feindex:=conv_integer(unsigned(axiReadMaster.araddr(5 downto 2)));
           case(axiReadMaster.araddr(9 downto 6)) is
             when "0100" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.timeoutcounter(feindex);
             when "0101" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.toomanyheadercounter(feindex);
             when "0110" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.skippedtriggercounter(feindex);
             when "0111" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.badheadercounter(feindex);
             when "1000" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.missingtriggercounter(feindex);
             when "1001" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.datanoheadercounter(feindex);
             when "1010" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.desynchcounter(feindex);
             when "1011" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.ecrresetcounter(feindex);
             when "1100" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.occounter(feindex);
             when "1101" =>
               v.axiReadSlave.rdata(31 downto 0) := decodingerrors_s(feindex);
             when "1110" =>
               v.axiReadSlave.rdata(31 downto 0) := busycounter_s(feindex);
             when "1111" =>
               v.axiReadSlave.rdata(31 downto 0) := efbcounters.goodeventcounter(feindex);
             when others =>
           end case; 
         else
           case (axiReadMaster.araddr(9 downto 2)) is
             when X"0d" =>
               v.axiReadSlave.rdata := FPGA_VERSION_C;
             when X"0f" =>
               v.axiReadSlave.rdata := channeloutmask_a;
             when X"14" =>
               v.axiReadSlave.rdata(1 downto 0) := ttcClkOk & clk_in_sel_a;
             when X"15" =>
               v.axiReadSlave.rdata := nEvt;
             when X"16" =>
               v.axiReadSlave.rdata := nEvtNonMon;
             when X"17" =>
               v.axiReadSlave.rdata(15 downto 0) := disabledMask;
             when X"18" =>
               v.axiReadSlave.rdata(15 downto 0) := maskedInRun;
             when X"19" =>
               v.axiReadSlave.rdata(31 downto 0) := bcrl1acounter;
             when X"1a" =>
               v.axiReadSlave.rdata(31 downto 0) := l1abcrcounter;
             when X"1b" =>
               v.axiReadSlave.rdata(31 downto 0) := l1al1acounter;
             when X"1c" =>
               v.axiReadSlave.rdata(15 downto 0) := maskedPerm;
             when X"2a" =>
               v.axiReadSlave.rdata := nummon_s;
             when X"2b" =>
               v.axiReadSlave.rdata := clockcounters_a;
             when X"2c" =>
               v.axiReadSlave.rdata(5 downto 0) := gbtstatus_a;
             when X"2d" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s1(0);
             when X"2e" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s1(1);
             when X"2f" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s1(2);
             when X"30" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s1(3);
             when X"31" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s1(4);
             when X"32" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s1(5);
             when X"33" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s1(6);
             when X"34" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s1(7);
             when X"35" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s2(0);
             when X"36" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s2(1);
             when X"37" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s2(2);
             when X"38" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s2(3);
             when X"39" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s2(4);
             when X"3a" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s2(5);
             when X"3b" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s2(6);
             when X"3c" =>
               v.axiReadSlave.rdata(9 downto 0) := hptdctemps_s2(7);
             when X"3d" =>
               v.axiReadSlave.rdata := ttcbusycounter_s;
             when X"3e" =>
               v.axiReadSlave.rdata(15 downto 0) := disablemask_r;
             when X"3f" =>
               v.axiReadSlave.rdata := nEvtGood;
             when others =>
               axiReadResp := AXI_RESP_SLVERR_C;
           end case;
         end if;
         -- Send AXI Response
         axiSlaveReadResponse(v.axiReadSlave, axiReadResp);
      end if;

      ----------------------------------------------------------------------------------------------
      -- Reset
      ----------------------------------------------------------------------------------------------
      if (axiRst = '1') then
         v             := REG_INIT_C;
      end if;

      rin <= v;
      axiReadSlave <= r.axiReadSlave;
      axiWriteSlave <= r.axiWriteSlave;
      
   end process comb;

   seq : process (axiClk) is
   begin
      if (rising_edge(axiClk)) then
         r <= rin;
      end if;
   end process seq;

    process(axiClk,axiRst) 
    begin
      if(axiRst='1')then
        clockselect_a<="00";
        trgcount_a<=x"0000";
        calibmode_a<="00";
        resetdelay<='0';
        incrementdelay<="00000";
        conftrg_a<='0';
        trgdelay_a<=x"02";
        triggermask_a<=x"0000";
        vetodis_a<='0';
        enableEcrReset<='0';
        blockEcrBcr_a<="00";
        period_a<=x"00000000";
        setdeadtime_a<=x"0007";
        channelmask_a<=(others =>'0');
        channeloutmask_a <= (ackchannel => '1', others => '0');       
        disablemask_r <= (others => '0');
        encoding_a<=encodingDefault;
        hitbusop_a<=x"0000";
        discop_a<=x"0000";
        telescopeop_a<="000";
        setfifothresh_a<=x"efff";
        ofprotection_a<='0';
        writemem_s<='0';
        maxmem_s<=(others => '0');
        l1route_a<='0';
        hitbusdepth_a<="00000";
        tdcreadoutdelay_a<="00000";
        multiplicity_a<=x"0000000000000000";
        maxlength_a<="00"&x"064";
        outputdelay_a <= (others => (others => '0'));
        clk_in_sel_a<='1';
        selfei4clk_a<="00";
        adcperiod_a<=(others => '0');
        efbtimeout <= (others => '0');
        efbtimeoutfirst <= (others => '0');
        efb_missing_header_timeout <= (others => '0');
        nExp <= (others => '0');
        bcidShift <= (others => '0');
        hitdiscconfig <= (others => (others => '0'));
        runnumber <= (others => '0');
        rodid <= x"00850001";
        detevtype <= (others => '0');
        doecrreset <= (others => '0');
        nummon_s <= x"000003e8";
        monenabled <= '0';
        datapath <= '0';
        ttcsim <= '1';
        bcrveto<='0';
        vetoFirstBcid<=(others=>'0');
        vetoNumBcid<=(others=>'0');
        cdtenabled<='0';
        numbuffers<=(others=>'0');
        window<=(others=>'0');
        eventlimit<=x"800";
        icword_a<=(others=>'0');
      elsif(rising_edge(axiClk))then
        if(r.writeReg='1')then  -- Write register
          if(r.address(7 downto 5)="010") then  -- output delays
            outputdelay_a(conv_integer(unsigned(r.address(4 downto 0))))<=r.regDataOut(7 downto 0);
          elsif(r.address(7 downto 4)="0110") then  -- HitDiscConfig
            hitdiscconfig(conv_integer(unsigned(r.address(3 downto 0))))<=r.regDataOut(1 downto 0);
          else
            case r.address is
              when x"00" => -- Set channelmask
                channelmask_a<=r.regDataOut;
              when x"03" => -- Select calib mode
                calibmode_a<= r.regDataOut(1 downto 0);
                -- 0 is normal
                -- 1 is tdc calib
                -- 2 is eudaq
              when x"04" => -- Switch TDC/Trigger data on and off
                channeloutmask_a(tdcchannel)<=r.regDataOut(0);
              when x"05" => -- Increment disc delays
                incrementdelay<=r.regDataOut(4 downto 0);
              when x"06" =>
                discop_a<=r.regDataOut(15 downto 0);
              when x"07" => -- reset input delays
                resetdelay<='1';
              when x"08" => -- Set trigger delay
                conftrg_a<='1';
                if(unsigned(r.regDataOut(7 downto 0))<2)then
                  trgdelay_a<=x"02";
                else
                  trgdelay_a<=r.regDataOut(7 downto 0);
                end if;
              when x"09" => -- Set cyclic trigger period
                period_a<=r.regDataOut;
              when x"0a" => -- Clock select for receive clock
                selfei4clk_a<=r.regDataOut(1 downto 0);
              when x"0b" => -- Set trigger mask -1 is scintillator
                -- 2 is cyclic
                -- 4 is external
                -- 8 is external (HSIO)
                -- 16 is hitbus
                triggermask_a<=r.regDataOut(15 downto 0);
              when x"0d" => -- enable data output
                channeloutmask_a(28 downto 0)<=r.regDataOut(28 downto 0);
                channeloutmask_a(ackchannel)<='1';
              when x"0e" => 
                trgcount_a<=r.regDataOut(15 downto 0);
              when x"0f" =>
                setdeadtime_a<=r.regDataOut(15 downto 0);
              when x"12" =>  -- write a word into the command stream buffer
                writemem_s<='1';
                memval<=r.regDataOut;
              when x"13" => -- clear the command stream buffer
                maxmem_s<=(others => '0');
              when x"14" =>
                encoding_a<=r.regDataOut(1 downto 0);
              when x"15" =>
                hitbusop_a<=r.regDataOut(15 downto 0);
              when x"16" =>
                multiplicity_a(31 downto 0)<=r.regDataOut;
              when x"17" =>
                multiplicity_a(63 downto 32)<=r.regDataOut;
              when x"19" =>
                setfifothresh_a<=r.regDataOut(15 downto 0);
              when x"1a" =>
                ofprotection_a<=r.regDataOut(0);
              when x"1b" =>
                l1route_a<=r.regDataOut(0);
              when x"1c" =>
                hitbusdepth_a <= r.regDataOut(4 downto 0);
              when x"1d" =>
                tdcreadoutdelay_a <= r.regDataOut(4 downto 0);
              when x"1e" =>
                telescopeop_a<=r.regDataOut(2 downto 0);
              when x"1f" =>
                adcperiod_a<=r.regDataOut;
              when x"20" =>
                channeloutmask_a(adcchannel)<=r.regDataOut(0);
              when x"21" =>
                maxlength_a<=r.regDataOut(13 downto 0);
              when x"23" =>             --don't use
              when x"24" =>
                if(r.regDataOut(0)='0' and ttcClkOk='1')then
                  clk_in_sel_a<='0';
                else
                  clk_in_sel_a<='1';
                end if;
              when x"25" =>
                nExp<=r.regDataOut(4 downto 0);
              when x"26" =>
                efbtimeout<=r.regDataOut(15 downto 0);
              when x"27" =>
                efbtimeoutfirst<=r.regDataOut(15 downto 0);
              when x"28" =>
                efb_missing_header_timeout<=r.regDataOut(7 downto 0);
              when x"29" =>
                runnumber<=r.regDataOut;
              when x"2a" =>
                nummon_s<=r.regDataOut;
              when x"2b" =>
                monenabled<=r.regDataOut(0);
              when x"2c" =>
                datapath<=r.regDataOut(0);
              when x"2d" =>
                ttcsim<=r.regDataOut(0);
              when x"2e" =>
                vetodis_a<=r.regDataOut(0);
              when x"2f" =>
                blockEcrBcr_a<=r.regDataOut(1 downto 0);
              when x"30" =>
                enableEcrReset<=r.regDataOut(0);
              when x"31" =>
                bcrveto<=r.regDataOut(0);
              when x"32" =>
                vetoFirstBcid<=r.regDataOut(11 downto 0);
              when x"33" =>
                vetoNumBcid<=r.regDataOut(11 downto 0);
              when x"34" =>
                cdtenabled<=r.regDataOut(0);
              when x"35" =>
                numbuffers<=r.regDataOut(4 downto 0);
              when x"36" =>
                window<=r.regDataOut(15 downto 0);
              when x"37" =>
                bcidShift<=r.regDataOut(11 downto 0);
              when x"38" =>
                eventlimit<=r.regDataOut(11 downto 0);
              when x"39" =>
                icword_a<='1'&r.regDataOut(30 downto 0);
              when x"3a" =>
                detevtype<=r.regDataOut;
              when x"3b" =>
                doecrreset<=r.regDataOut(15 downto 0);
              when x"3c" =>
                disablemask_r(conv_integer(unsigned(r.regDataOut(3 downto 0))))<=r.regDataOut(4);
                if(r.regDataOut(4)='0')then
                  doecrreset(conv_integer(unsigned(r.regDataOut(3 downto 0))))<='1';
                end if;
              when x"3d" =>
                rodid<=r.regDataOut;
              when x"3e" =>
                disablemask_r<=r.regDataOut(15 downto 0);
              when others =>
            end case;
          end if;
        else
          icword_a(31)<='0';
          if(clk_in_sel_in='1')then
            clk_in_sel_a<='1';
          elsif(setatlasclock='1' and ttcClkOk='1' and atlasafp='1')then
            clk_in_sel_a<='0';
          end if;
          if(writemem_s='1')then
            maxmem_s<=unsigned(maxmem_s)+1;
            writemem_s<='0';
          end if;
          incrementdelay<="00000";
          resetdelay<='0';
          conftrg_a<='0';
          doecrreset<=(others =>'0');
        end if;
      end if;
   end process;
   nummon<=nummon_s;
   maxmem<=maxmem_s;
   writemem<=writemem_s;
   clk_in_sel<=clk_in_sel_a;
   disablemask<=disablemask_r;
   PwrUpRst_Inst : entity work.PwrUpRst
      generic map(
         OUT_POLARITY_G => '0',
         DURATION_G => 800000000) --20 seconds
      port map (
         clk    => axiClk,
         rstOut => delayatlasclock);
   oneshot_atlasclock: entity work.oneshot
     port map(
       clk => axiClk,
       rst => axiRst,
       datain => delayatlasclock,
       dataout => setatlasclock);
   oneshot_conftrg: entity work.oneshot
     port map(
       clk => sysClk40,
       rst => sysRst40,
       datain => conftrg_a,
       dataout => conftrg);
   SyncOut_combined : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 26)    
      port map (
         clk        => sysClk40,
         dataIn(1 downto 0) => clockselect_a,
         dataIn(3 downto 2) => calibmode_a,
         dataIn(5 downto 4) => encoding_a,
         dataIn(8 downto 6) => telescopeop_a,
         dataIn(9) => ofprotection_a,
         dataIn(10) => l1route_a,
         dataIn(20 downto 16) => tdcreadoutdelay_a,
         dataIn(15 downto 11) => hitbusdepth_a,
         dataIn(22 downto 21) => selfei4clk_a,
         dataIn(23) => vetodis_a,
         dataIn(25 downto 24) => blockEcrBcr_a,
         dataOut(1 downto 0) => clockselect,
         dataOut(3 downto 2) => calibmode,
         dataOut(5 downto 4) => encoding,
         dataOut(8 downto 6) => telescopeop,
         dataOut(9) => ofprotection,
         dataOut(10) => l1route,
         dataOut(15 downto 11) => hitbusdepth,
         dataOut(20 downto 16) => tdcreadoutdelay,
         dataOut(22 downto 21) => selfei4clk,
         dataOut(23) => vetodis,
         dataOut(25 downto 24) => blockEcrBcr);
         

   SyncOut_trgdelay : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 8)    
      port map (
         clk        => sysClk40,
         dataIn => trgdelay_a,
         dataOut => trgdelay);  
   SyncOut_trgcount : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 16)    
      port map (
         clk        => sysClk40,
         dataIn => trgcount_a,
         dataOut => trgcount);  
   SyncOut_triggermask : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 16)    
      port map (
         clk        => sysClk40,
         dataIn => triggermask_a,
         dataOut => triggermask);  
   SyncOut_period : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 32)    
      port map (
         clk        => sysClk40,
         dataIn => period_a,
         dataOut => period);  
   SyncOut_setdeadtime : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 16)    
      port map (
         clk        => sysClk40,
         dataIn => setdeadtime_a,
         dataOut => setdeadtime);  
   SyncOut_channelmask : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 32)    
      port map (
         clk        => sysClk40,
         dataIn => channelmask_a,
         dataOut => channelmask);  
   SyncOut_adcperiod : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 32)    
      port map (
         clk        => sysClk40,
         dataIn => adcperiod_a,
         dataOut => adcperiod);  
   SyncIn_clockcounters : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 32)    
      port map (
         clk        => axiClk,
         dataIn => clockcounters,
         dataOut => clockcounters_a);  
   SyncIn_gbttxaligned : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 6)    
      port map (
         clk        => axiClk,
         dataIn => gbtstatus,
         dataOut => gbtstatus_a);  
   SyncOut_channeloutmask : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 32)    
      port map (
         clk        => sysClk40,
         dataIn => channeloutmask_a,
         dataOut => channeloutmask);  
   SyncOut_icword : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 32)    
      port map (
         clk        => sysClk40,
         dataIn => icword_a,
         dataOut => icword);  
   SyncOut_hitbusop : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 16)    
      port map (
         clk        => sysClk40,
         dataIn => hitbusop_a,
         dataOut => hitbusop);  
   SyncOut_discop : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 16)    
      port map (
         clk        => sysClk40,
         dataIn => discop_a,
         dataOut => discop);  
   SyncOut_setfifothresh : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 16)    
      port map (
         clk        => sysClk40,
         dataIn => setfifothresh_a,
         dataOut => setfifothresh);  
   SyncOut_multiplicity : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 64)    
      port map (
         clk        => sysClk40,
         dataIn => multiplicity_a,
         dataOut => multiplicity);  
   SyncOut_maxlength : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 14)    
      port map (
         clk        => sysClk40,
         dataIn => maxlength_a,
         dataOut => maxlength_s);  
   maxlength<=conv_integer(unsigned(maxlength_s));
   outputgen: for I in 0 to 31 generate
     SyncOut_outputdelay : entity work.SynchronizerVector
        generic map (
         WIDTH_G => 8)    
        port map (
         clk        => sysClk40,
         dataIn => outputdelay_a(I),
         dataOut => outputdelay(I));  
   end generate outputgen;
   hptdccounters_synch_loop: for I in 0 to 7 generate
     SyncIn_hptdccounters1 : entity work.SynchronizerVector
        generic map (
         WIDTH_G => 10)    
        port map (
         clk        => axiClk,
         dataIn => hptdctemps1(I),
         dataOut => hptdctemps_s1(I));  
     SyncIn_hptdccounters2 : entity work.SynchronizerVector
        generic map (
         WIDTH_G => 10)    
        port map (
         clk        => axiClk,
         dataIn => hptdctemps2(I),
         dataOut => hptdctemps_s2(I));  
   end generate hptdccounters_synch_loop;
   decodingerrors_synch_loop: for I in 0 to 15 generate
     SyncIn_decodingerrors : entity work.SynchronizerVector
        generic map (
         WIDTH_G => 32)    
        port map (
         clk        => axiClk,
         dataIn => decodingerrors(I),
         dataOut => decodingerrors_s(I));  
   end generate decodingerrors_synch_loop;
   busycounter_synch_loop: for I in 0 to 15 generate
     SyncIn_busycounters : entity work.SynchronizerVector
        generic map (
         WIDTH_G => 32)    
        port map (
         clk        => axiClk,
         dataIn => busycounter(I),
         dataOut => busycounter_s(I));  
   end generate busycounter_synch_loop;
     SyncIn_ttcbusycounter : entity work.SynchronizerVector
        generic map (
         WIDTH_G => 32)    
        port map (
         clk        => axiClk,
         dataIn => ttcbusycounter,
         dataOut => ttcbusycounter_s);  
end REGISTERS;
