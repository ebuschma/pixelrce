-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasTtcRxCdrInputs.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-03-19
-- Last update: 2015-02-24
-- Platform   : Vivado 2013.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This module monitors the errors detections.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AtlasTtcRxPkg.all;

library unisim;
use unisim.vcomponents.all;

entity AtlasTtcRxCdrInputs is
   generic (
      TPD_G           : time   := 1 ns;
      IODELAY_GROUP_G : string := "atlas_ttc_rx_delay_group";
      XIL_DEVICE_G    : string := "7SERIES");      
   port (
      -- CDR Signals
      clock          : in  sl;          -- From ADN2816 IC
      clk            : out sl;
      ttcRxData      : in  sl;          -- From ADN2816 IC
      data           : out sl;
      -- Emulation Trigger Signals
      emuSel         : in  sl;
      emuClk         : in  sl;
      emuData        : in  sl;
      -- Serial Data Signals
      serDataRising  : out sl;
      serDataFalling : out sl;
      -- Clock Signals
      clkSync        : in  sl;          -- Sync strobe
      locClk40MHz    : out sl;
      locClk80MHz    : out sl;
      locClk160MHz   : out sl);
end AtlasTtcRxCdrInputs;

architecture rtl of AtlasTtcRxCdrInputs is

   signal locClock,
      serRising,
      serFalling,
      ttcRxDataDly : sl;
   
   signal divClk : slv(1 downto 0);

begin

   locClk160MHz <= locClock;
   clk          <= locClock;

   data           <= ttcRxData  when(emuSel = '0') else emuData;
   serDataRising  <= serRising  when(emuSel = '0') else emuData;
   serDataFalling <= serFalling when(emuSel = '0') else emuData;


   BUFG_160MHz : BUFGMUX
      port map (
         O  => locClock,                -- 1-bit output: Clock output
         I0 => clock,                   -- 1-bit input: Clock input (S=0)
         I1 => emuClk,                  -- 1-bit input: Clock input (S=1)
         S  => emuSel);                 -- 1-bit input: Clock select           

   BUFR_0 : BUFR
      generic map (
         BUFR_DIVIDE => "4",
         SIM_DEVICE  => XIL_DEVICE_G)
      port map (
         I   => locClock,  -- 1-bit input: Clock buffer input driven by an IBUFG, MMCM or local interconnect
         CE  => '1',                    -- 1-bit input: Active high, clock enable input
         CLR => clkSync,                -- 1-bit input: ACtive high reset input
         O   => divClk(0));             -- 1-bit output: Clock output port

   BUFG_40MHz : BUFG
      port map (
         I => divClk(0),
         O => locClk40MHz); 

   BUFR_1 : BUFR
      generic map (
         BUFR_DIVIDE => "2",
         SIM_DEVICE  => XIL_DEVICE_G)
      port map (
         I   => locClock,  -- 1-bit input: Clock buffer input driven by an IBUFG, MMCM or local interconnect
         CE  => '1',                    -- 1-bit input: Active high, clock enable input
         CLR => clkSync,                -- 1-bit input: ACtive high reset input
         O   => divClk(1));             -- 1-bit output: Clock output port

   BUFG_80MHz : BUFG
      port map (
         I => divClk(1),
         O => locClk80MHz);          

   IDDR_Inst : IDDR
      generic map (
         DDR_CLK_EDGE => "SAME_EDGE_PIPELINED",  -- "OPPOSITE_EDGE", "SAME_EDGE", or "SAME_EDGE_PIPELINED"
         INIT_Q1      => '0',           -- Initial value of Q1: '0' or '1'
         INIT_Q2      => '0',           -- Initial value of Q2: '0' or '1'
         SRTYPE       => "SYNC")        -- Set/Reset type: "SYNC" or "ASYNC" 
      port map (
         D  => ttcRxData,            -- 1-bit DDR data input
         C  => locClock,                -- 1-bit clock input
         CE => '1',                     -- 1-bit clock enable input
         R  => '0',                     -- 1-bit reset
         S  => '0',                     -- 1-bit set
         Q1 => serRising,               -- 1-bit output for positive edge of clock 
         Q2 => serFalling);             -- 1-bit output for negative edge of clock

end rtl;
