--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity adcreadout is
generic( CHANNEL: std_logic_vector(7 downto 0):=x"ff");
port(	clk: 	    in std_logic;
	rst:	    in std_logic;
	d_in:	    in Slv16Array(11 downto 0);
        enabled:    in std_logic;
        go:         in std_logic;
	d_out:	    out std_logic_vector(15 downto 0);
        ld:         out std_logic;
        sof:        out std_logic;
        eof:        out std_logic
);
end adcreadout;

--------------------------------------------------------------

architecture ADCREADOUT of adcreadout is

begin


    process(rst, clk)

    variable headercounter: natural range 0 to 29 := 0;
    begin
        if(rst='1') then
          d_out<=x"0000";
          eof<='0';
          sof<='0';
          headercounter:=0;
          ld<='0';
        elsif (clk'event and clk='1') then
          if(headercounter=0 and enabled='1' and go='1')then 
            headercounter:=29;
          end if;
          if (headercounter/=0)then  
            if(headercounter=29)then
              ld<='1';                
              sof<='1';
              d_out<=(others=>'0'); 
            elsif(headercounter=28)then
              sof<='0';
              d_out(7 downto 0)<=(others=>'0'); 
              d_out(15 downto 8)<=CHANNEL;
            elsif(headercounter<14 and headercounter>1)then
              d_out<=d_in(13-headercounter);
            else
               d_out<=(others=>'0');
            end if;
            if(headercounter=2)then
              eof<='1';
            elsif(headercounter=1)then
              eof<='0';
              ld<='0';
            end if;
            headercounter:=headercounter-1;
          end if;
 	end if;
    
    end process;		

end ADCREADOUT;

--------------------------------------------------------------
