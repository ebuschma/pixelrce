--------------------------------------------------------------
-- Complex dead time with sliding window algorithm
-- Martin Kocian 04/2016
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity complexdeadtime is
generic( maxbuffers: integer:=16);
port(	clk: 	    in std_logic;
	rst:	    in std_logic;
        enabled:    in std_logic;
        l1a:        in std_logic;
        busy:       out std_logic;
        numbuffers: in std_logic_vector(4 downto 0);
        window:     in std_logic_vector(15 downto 0)
);
end complexdeadtime;

--------------------------------------------------------------

architecture COMPLEXDEADTIME of complexdeadtime is
  signal counter: Slv16Array(maxbuffers downto 0):=(others => (others=> '0')); 
  signal numbufs: slv(4 downto 0):=(others => '0');
  signal windows: slv(15 downto 0);
  signal inuse: slv(maxbuffers downto 0):=(others =>'0');
  signal enableds: sl;
  signal busys: sl;


  function lowestFree(iu: slv) return integer is
    variable retvar: integer range 0 to maxbuffers;
  begin
    for i in 0 to maxbuffers loop
      retvar:=i;
      exit when iu(i)='0';
    end loop;
    return retvar;
  end;
      
begin
   SyncOut_window : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 16)    
      port map (
         clk    => clk,
         dataIn => window,
         dataOut => windows);  
   SyncOut_numbuf : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 5)    
      port map (
         clk    => clk,
         dataIn => numbuffers,
         dataOut => numbufs);  
   SyncOut_enabled : entity work.Synchronizer
      port map (
         clk    => clk,
         rst    => rst,
         dataIn => enabled,
         dataOut => enableds);  

    IN_USE: for I in 0 to maxbuffers generate
      with counter(I) select
        inuse(I)<= '0' when x"0000",
                   '1' when others;
    end generate IN_USE;
    process(inuse, numbufs) begin
      if(lowestFree(inuse)=conv_integer(unsigned(numbufs)))then
        busys<='1';
      else
        busys<='0';
      end if;
    end process;
    busy<=busys and enableds;
        
    process(rst, clk)
    begin
        if(rst='1') then
          counter<=(others => (others=> '0'));
        elsif (clk'event and clk='1') then
          if(l1a='1' and enableds='1' and busys='0')then
            counter(lowestFree(inuse))<=unsigned(windows)-1;
          end if;
          for I in 0 to maxbuffers loop
            if(counter(I)/=x"0000")then
              counter(I)<=unsigned(counter(I))-1;
            end if;
          end loop;
        end if;
    end process;		
end COMPLEXDEADTIME;

--------------------------------------------------------------
