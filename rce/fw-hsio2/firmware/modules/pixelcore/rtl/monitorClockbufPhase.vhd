--------------------------------------------------------------
-- Reset Si5338 until the phases between 40 MHz, 160 MHz, and 120 MHz match.
-- Martin Kocian 04/2016
--------------------------------------------------------------

library ieee;
Library Unisim;
use unisim.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity monitorClockbufPhase is
port(   clock160:	    in std_logic;
        clock120:           in std_logic;
        clken:              in std_logic;
        result:             out slv(31 downto 0):=(others =>'0')
);
end monitorClockbufPhase;

--------------------------------------------------------------

architecture CBP of monitorClockbufPhase is

   signal counters: Slv8Array(3 downto 0);
   signal phasecount: slv(1 downto 0):="00";
   

begin
  process begin
    wait until falling_edge(clock160);
    if(clken='1')then
      phasecount<="00";
    else
      phasecount<=unsigned(phasecount)+1;
    end if;
    if(counters(0)=x"ff" or counters(1)=x"ff" or counters(2)=x"ff" or counters(3)=x"ff")then
      result(31 downto 24)<=counters(3);
      result(23 downto 16)<=counters(2);
      result(15 downto 8)<=counters(1);
      result(7 downto 0)<=counters(0);
      counters<=(others=>(others=>'0'));
    else
      if(phasecount="00")then
        if(clock120='1')then
          counters(0)<=unsigned(counters(0))+1;
         end if;
       elsif(phasecount="01")then
         if(clock120='1')then
           counters(1)<=unsigned(counters(1))+1;
         end if;
       elsif(phasecount="10")then
         if(clock120='1')then
           counters(2)<=unsigned(counters(2))+1;
         end if;
       elsif(phasecount="11")then
         if(clock120='1')then
           counters(3)<=unsigned(counters(3))+1;
         end if;
       end if;
    end if;
  end process;

end CBP;

--------------------------------------------------------------
