-------------------------------------------------------------------------------
-- file:///afs/slac/g/reseng/svn/repos/AtlasCsc/truck/firmware/targets/AtlasCscDtm/Version.vhd
-------------------------------------------------------------------------------
-- Project    : ATLAS CSC
-- File       : Version.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-05-05
-- Last update: 2015-07-13
-- Platform   : Vivado 2014.4
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-- Version Constant Module
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package Version is

   constant FPGA_VERSION_C : std_logic_vector(31 downto 0) := x"DC00030B";  -- MAKE_VERSION

   constant BUILD_STAMP_C : string := "AtlasAfpDtm: Vivado v2014.4 (x86_64) Built Thu Dec  7 12:49:24 CET 2017 by mkocian";

end Version;

-------------------------------------------------------------------------------
-- Revision History:
-- 05/14/2014 (0xDC000300): New Structure
-- 07/14/2014 (0xDC000301): M5 release
-- 09/08/2014 (0xDC000302): Change default config.ignoreExtBusyIn to 0xC0
-- 09/10/2014 (0xDC000303): Added config.invertOutput
-- 09/17/2014 (0xDC000304): Added trigL1Rate register
-- 10/31/2014 (0xDC000305): Fixed the broken trigL1Rate register.  
--                          It was missing the L1-Trig driving the status signal
-- 12/11/2014 (0xDC000306): Upgrade the TTC-TX emulator to version 2
-- 01/06/2015 (0xDC000307): Added TTC-TX emulator to version 1 back into the firmware
--                          to make it backward compatible with firmware version 0xDC000305
--                          Moved the AXI-Lite base address of TTC-TX emulator to version 2
--                          to 0xA0002000
--                          In the Busy module, added emuVersionSelect configuration register
-- 02/20/2015 (0xDC000308):
--    In the TTC-RX module, Added the frequency measurement of the ATLAS clock back into the register space
--
-- 02/20/2015 (0xDC000309): Sourcing external oscillator for 200 MHz clock reference
--
-- 02/27/2015 (0xDC00030A): 
--    In the BUSY module,   Added clockSelect register
--    Note: This version (or later) is required for running the EMU_DPM Version 0xBEEF030C (or later)
--          with the busy.config.emuSel = '1' (local TTC data) and busy.config.clkSel = '0' (external clock)
--
-- 07/23/2015 (0xDC00030B): 
--    In the TOP level,    Transmitting the locally generated 160 MHz reference on bpClkOut[0] and using 
--                         bpClkIn[0] as the local 160 MHz clock for emulation mode.  This give the ATCA crate 
--                         the ability to have a common clock on all COB within the crate for emulation playback
--                         testing.
--                         Note: To use the bpClkIn[0], the IPMC "bpClkOe" (A.K.A. "clock output enable") 
--                         command needs to be sent ONLY one COB within the crate.
--
-------------------------------------------------------------------------------
