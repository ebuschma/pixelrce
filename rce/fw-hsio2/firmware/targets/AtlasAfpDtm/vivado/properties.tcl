##############################################################################
## This file is part of 'ATLAS NRC CSC DEV'.
## It is subject to the license terms in the LICENSE.txt file found in the 
## top-level directory of this distribution and at: 
##    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
## No part of 'ATLAS NRC CSC DEV', including this file, 
## may be copied, modified, propagated, or distributed except according to 
## the terms contained in the LICENSE.txt file.
##############################################################################

## Check for version 2014.4 of Vivado
if { [version -short] != 2014.4 } {
   close_project
   exit -1
}
