-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : AtlasAfpDtm.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-05-16
-- Last update: 2017-05-31
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- This file is part of 'ATLAS NRC CSC DEV'.
-- It is subject to the license terms in the LICENSE.txt file found in the 
-- top-level directory of this distribution and at: 
--    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
-- No part of 'ATLAS NRC CSC DEV', including this file, 
-- may be copied, modified, propagated, or distributed except according to 
-- the terms contained in the LICENSE.txt file.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library Unisim;
use unisim.vcomponents.all;

use work.AtlasCscRceG3ConfigPkg.all;
use work.RceG3Pkg.all;
use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
USE work.ALL;

entity AtlasAfpDtm is
   port (
      -- Debug
      led         : out   slv(1 downto 0);
      -- I2C
      i2cSda      : inout sl;
      i2cScl      : inout sl;
      -- PCI Express
      pciRefClkP  : in    sl;
      pciRefClkM  : in    sl;
      pciRxP      : in    sl;
      pciRxM      : in    sl;
      pciTxP      : out   sl;
      pciTxM      : out   sl;
      pciResetL   : out   sl;
      -- COB Ethernet
      ethRxP      : in    sl;
      ethRxM      : in    sl;
      ethTxP      : out   sl;
      ethTxM      : out   sl;
      -- Reference Clock
      locRefClkP  : in    sl;
      locRefClkM  : in    sl;
      -- Clock Select
      clkSelA     : out   sl;
      clkSelB     : out   sl;
      -- Base Ethernet
      ethRxCtrl   : in    slv(1 downto 0);
      ethRxClk    : in    slv(1 downto 0);
      ethRxDataA  : in    Slv(1 downto 0);
      ethRxDataB  : in    Slv(1 downto 0);
      ethRxDataC  : in    Slv(1 downto 0);
      ethRxDataD  : in    Slv(1 downto 0);
      ethTxCtrl   : out   slv(1 downto 0);
      ethTxClk    : out   slv(1 downto 0);
      ethTxDataA  : out   Slv(1 downto 0);
      ethTxDataB  : out   Slv(1 downto 0);
      ethTxDataC  : out   Slv(1 downto 0);
      ethTxDataD  : out   Slv(1 downto 0);
      ethMdc      : out   Slv(1 downto 0);
      ethMio      : inout Slv(1 downto 0);
      ethResetL   : out   Slv(1 downto 0);
      -- -- RTM High Speed
      --dtmToRtmHsP : out   sl;
      --dtmToRtmHsM : out   sl;
      --rtmToDtmHsP : in    sl;
      --rtmToDtmHsM : in    sl;
      -- RTM Low Speed
      dtmToRtmLsP : in slv(2 downto 0);
      dtmToRtmLsM : in slv(2 downto 0);
      busyOutP: out sl;
      busyOutM: out sl;
      --Single ended TTC
      cdr_locked  : in sl;
      dtm_locked  : out sl;
      ttc_scl     : inout sl;
      ttc_sda     : inout sl;
      -- DPM Signals
      dpmClkP     : out   slv(2 downto 0);
      dpmClkM     : out   slv(2 downto 0);
      dpmFbP      : in    slv(7 downto 0);
      dpmFbM      : in    slv(7 downto 0);
      -- Backplane Clocks
      bpClkIn     : in    slv(5 downto 0);
      bpClkOut    : out   slv(5 downto 0);
      -- -- Spare Signals
      --plSpareP     : inout slv(4 downto 0);
      --plSpareM     : inout slv(4 downto 0);
      -- IPMI
      dtmToIpmiP  : out   slv(1 downto 0);
      dtmToIpmiM  : out   slv(1 downto 0));
end AtlasAfpDtm;

architecture TOP_LEVEL of AtlasAfpDtm is

   constant TPD_C : time := 1 ns;

   signal axiClk             : sl;
   signal axiClkRst          : sl;
   signal sysClk125          : sl;
   signal sysClk125Rst       : sl;
   signal sysClk200          : sl;
   signal sysClk200Rst       : sl;
   signal extAxilReadMaster  : AxiLiteReadMasterType;
   signal extAxilReadSlave   : AxiLiteReadSlaveType;
   signal extAxilWriteMaster : AxiLiteWriteMasterType;
   signal extAxilWriteSlave  : AxiLiteWriteSlaveType;
   signal dmaClk             : slv(2 downto 0);
   signal dmaClkRst          : slv(2 downto 0);
   signal dmaState           : RceDmaStateArray(2 downto 0);
   signal dmaObMaster        : AxiStreamMasterArray(2 downto 0);
   signal dmaObSlave         : AxiStreamSlaveArray(2 downto 0);
   signal dmaIbMaster        : AxiStreamMasterArray(2 downto 0);
   signal dmaIbSlave         : AxiStreamSlaveArray(2 downto 0);
   signal stableClk125       : sl;

   attribute KEEP_HIERARCHY : string;
   attribute KEEP_HIERARCHY of
      U_DtmCore,
      U_AtlasCscDtmCore : label is "TRUE";
   
begin

   --------------------------------------------------
   -- Core
   --------------------------------------------------
   U_DtmCore : entity work.DtmCore
      generic map (
         TPD_G          => TPD_C,
         RCE_DMA_MODE_G => ATLAS_CSC_RCE_DMA_MODE_C,
         OLD_BSI_MODE_G => ATLAS_CSC_OLD_BSI_MODE_C) 
      port map (
         i2cSda             => i2cSda,
         i2cScl             => i2cScl,
         pciRefClkP         => pciRefClkP,
         pciRefClkM         => pciRefClkM,
         pciRxP             => pciRxP,
         pciRxM             => pciRxM,
         pciTxP             => pciTxP,
         pciTxM             => pciTxM,
         pciResetL          => pciResetL,
         ethRxP             => ethRxP,
         ethRxM             => ethRxM,
         ethTxP             => ethTxP,
         ethTxM             => ethTxM,
         clkSelA            => clkSelA,
         clkSelB            => clkSelB,
         ethRxCtrl          => ethRxCtrl,
         ethRxClk           => ethRxClk,
         ethRxDataA         => ethRxDataA,
         ethRxDataB         => ethRxDataB,
         ethRxDataC         => ethRxDataC,
         ethRxDataD         => ethRxDataD,
         ethTxCtrl          => ethTxCtrl,
         ethTxClk           => ethTxClk,
         ethTxDataA         => ethTxDataA,
         ethTxDataB         => ethTxDataB,
         ethTxDataC         => ethTxDataC,
         ethTxDataD         => ethTxDataD,
         ethMdc             => ethMdc,
         ethMio             => ethMio,
         ethResetL          => ethResetL,
         dtmToIpmiP         => dtmToIpmiP,
         dtmToIpmiM         => dtmToIpmiM,
         sysClk125          => sysClk125,
         sysClk125Rst       => sysClk125Rst,
         sysClk200          => open,
         sysClk200Rst       => open,
         axiClk             => axiClk,
         axiClkRst          => axiClkRst,
         extAxilReadMaster  => extAxilReadMaster,
         extAxilReadSlave   => extAxilReadSlave,
         extAxilWriteMaster => extAxilWriteMaster,
         extAxilWriteSlave  => extAxilWriteSlave,
         dmaClk             => dmaClk,
         dmaClkRst          => dmaClkRst,
         dmaState           => dmaState,
         dmaObMaster        => dmaObMaster,
         dmaObSlave         => dmaObSlave,
         dmaIbMaster        => dmaIbMaster,
         dmaIbSlave         => dmaIbSlave,
         userInterrupt      => (others => '0'));

   --------------------------------------------------
   -- PPI Loopback
   --------------------------------------------------
   dmaClk      <= (others => sysClk125);
   dmaClkRst   <= (others => sysClk125Rst);
   dmaIbMaster <= dmaObMaster;
   dmaObSlave  <= dmaIbSlave;

   ----------------------------
   -- Precision Clock reference
   ----------------------------      
   U_AtlasCscDpmClk : entity work.AtlasCscDpmClk
      port map (
         -- 250 Reference Clock
         gtClkP       => locRefClkP,
         gtClkN       => locRefClkM,
         -- Output Clocks and Resets
         stableClk125 => stableClk125,
         sysClk200Clk => sysClk200,
         sysClk200Rst => sysClk200Rst,
         sysClk100Clk => open,
         sysClk100Rst => open,
         sysClk50Clk  => open,
         sysClk50Rst  => open);    

   --------------------------------------------------
   -- Application Core Module
   --------------------------------------------------
   U_AtlasCscDtmCore : entity work.AtlasCscDtmCore
      generic map (
         TPD_G              => TPD_C,
         CASCADE_SIZE_G     => 100,     -- 0.5 second buffer @ 100 kHz L1Trig rate
         FIFO_FIXED_THES_G  => false,
         FIFO_AFULL_THRES_G => 256)
      port map (
         -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
         axiClk         => axiClk,
         axiRst         => axiClkRst,
         axiReadMaster  => extAxilReadMaster,
         axiReadSlave   => extAxilReadSlave,
         axiWriteMaster => extAxilWriteMaster,
         axiWriteSlave  => extAxilWriteSlave,
         -- RTM Low Speed
         dtmToRtmLsP     => dtmToRtmLsP,
         dtmToRtmLsM     => dtmToRtmLsM,
         busyOutP       => busyOutP,
         busyOutM       => busyOutM,
         cdr_Locked      => cdr_locked,
         dtm_Locked      => dtm_locked,
         -- I2C to RMB
         ttc_sda        => ttc_sda,
         ttc_scl        => ttc_scl,
         -- DPM Signals
         dpmClkP        => dpmClkP,
         dpmClkM        => dpmClkM,
         dpmFbP         => dpmFbP,
         dpmFbM         => dpmFbM,
         -- Reference 200 MHz clock
         sysClk200      => sysClk200,
         sysClk200Rst   => sysClk200Rst,
         sysClk125      => sysClk125,
         sysClk125Rst   => sysClk125Rst,
         stableClk125   => stableClk125,
         -- Backplane Clocks
         bpClkIn        => bpClkIn,
         bpClkOut       => bpClkOut,
         -- Debug
         led            => led);       

end architecture TOP_LEVEL;
