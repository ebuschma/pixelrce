##############################################################################
## This file is part of 'ATLAS NRC CSC DEV'.
## It is subject to the license terms in the LICENSE.txt file found in the 
## top-level directory of this distribution and at: 
##    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
## No part of 'ATLAS NRC CSC DEV', including this file, 
## may be copied, modified, propagated, or distributed except according to 
## the terms contained in the LICENSE.txt file.
##############################################################################
set_property PACKAGE_PIN U10 [get_ports cdr_locked]
set_property PACKAGE_PIN V10 [get_ports dtm_locked]
set_property PACKAGE_PIN T20 [get_ports ttc_scl]
set_property PACKAGE_PIN U20 [get_ports ttc_sda]
set_property IOSTANDARD LVCMOS25   [get_ports ttc_scl]
set_property IOSTANDARD LVCMOS25   [get_ports ttc_sda]
set_property IOSTANDARD LVCMOS25   [get_ports cdr_locked]
set_property IOSTANDARD LVCMOS25   [get_ports dtm_locked]

set_property PACKAGE_PIN V8  [get_ports busyOutP]
set_property PACKAGE_PIN W8  [get_ports busyOutM]
set_property PACKAGE_PIN R17 [get_ports dtmToRtmLsP[2]]
set_property PACKAGE_PIN R18 [get_ports dtmToRtmLsM[2]]
set_property PACKAGE_PIN R19 [get_ports dtmToRtmLsP[1]]
set_property PACKAGE_PIN T19 [get_ports dtmToRtmLsM[1]]
set_property PACKAGE_PIN T17 [get_ports dtmToRtmLsP[0]]
set_property PACKAGE_PIN U17 [get_ports dtmToRtmLsM[0]]
set_property PULLUP TRUE [get_ports dtmToRtmLsM[0]]  
set_property PULLDOWN TRUE [get_ports dtmToRtmLsP[0]]  
set_property PULLUP TRUE [get_ports dtmToRtmLsM[2]]  
set_property PULLDOWN TRUE [get_ports dtmToRtmLsP[2]]  
# IO Types
set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsP]
set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsM]
set_property IOSTANDARD LVDS_25  [get_ports busyOutP]
set_property IOSTANDARD LVDS_25  [get_ports busyOutM]

set_property DIFF_TERM true [get_ports {dtmToRtmLsP[0]}]
set_property DIFF_TERM true [get_ports {dtmToRtmLsM[0]}]

#set_property DIFF_TERM true [get_ports {dtmToRtmLsP[1]}]
#set_property DIFF_TERM true [get_ports {dtmToRtmLsM[1]}]

set_property DIFF_TERM true [get_ports {dtmToRtmLsP[2]}]
set_property DIFF_TERM true [get_ports {dtmToRtmLsM[2]}]

#set_property IOSTANDARD LVCMOS25 [get_ports plSpareP]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareM]

create_clock -period  6.237 -name locClk     [get_pins {U_AtlasCscDtmCore/U_AtlasCscBusy/AtlasTtcRxCdrInputs_Inst/BUFG_160MHz/O}]
create_clock -period  6.237 -name locPllRef  [get_ports {bpClkIn[0]}]
create_clock -period  4.000 -name locRefClkP [get_ports {locRefClkP}]
create_clock -period  5.000 -name locRef200  [get_pins  {U_AtlasCscDpmClk/Clk200_BUFG/O}]

set_clock_groups -asynchronous -group [get_clocks {locClk}] \
                               -group [get_clocks {emuClk160MHz_AtlasTtcTxEmuPll}] \
                               -group [get_clocks {locRef200}] \
                               -group [get_clocks {sysClk125}] 

set_clock_groups -asynchronous -group [get_clocks {locPllRef}] -group [get_clocks {sysClk125}]                                

set_property LOC BUFGCTRL_X0Y1 [get_cells {U_AtlasCscDtmCore/U_AtlasCscBusy/AtlasTtcRxCdrInputs_Inst/BUFG_160MHz}]
