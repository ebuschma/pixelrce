-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : ClkGen.vhd
-- Author     : Martin Kocian  <kocian@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-10-23
-- Last update: 2016-07-22
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Clock module for Hsio Pixel Core.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;

library unisim;
use unisim.vcomponents.all;

entity ClkGenDtm is
   port (
      -- Clock, Resets
      extRstL            : in  sl;
      stableClk250       : out sl;
      sysClk40           : out sl;
      sysRst40           : out sl;
      sysClk80          : out sl;
      sysClk160          : out sl;
      sysClk40Unbuf      : out sl;
      sysClk40Unbuf90    : out sl;
      sysClk160Unbuf     : out sl;
      sysClk160Unbuf90   : out sl;
      sysClkLock         : out sl;
      sysClkRst          : in sl;
      gtClk2P             : in  sl;
      gtClk2N             : in  sl);

end ClkGenDtm;

-- Define architecture
architecture mapping of ClkGenDtm is



   --Misc.
   signal stableClock2,
      stableReset2,
      sysClk,
      sysClk100int,
      locked100,
      gtClkout,
      gtClkout2,
      gtClkDiv2,
      sysClkRstint,
      axiClk40int,
      locked : sl;
   
begin

   sysClkLock<=locked;
   sysClk40<=sysClk;
   stableClk250 <= stableClock2;

   -- GT Reference Clock

   IBUFDS_GTE2_Inst2 : IBUFDS_GTE2
      port map (
         I     => gtClk2P,
         IB    => gtClk2N,
         CEB   => '0',
         ODIV2 => open,
         O     => gtClkout2);

   BUFG_Inst2 : BUFG
      port map (
         I => gtClkOut2,
         O => stableClock2);

   -- Power Up Reset      

   PwrUpRst_Inst2 : entity work.PwrUpRst
      generic map(
         IN_POLARITY_G => '0')
      port map (
         arst   => extRstL,
         clk    => stableClock2,
         rstOut => stableReset2);

   SysClkGen_Inst : entity work.SysClkGenDtm
     port map ( 
       
       -- Clock in ports
       clk_in1 => stableClock2,
       -- Clock out ports  
       sysClk40 => sysClk,
       sysClk80 => sysClk80,
       sysClk160 => sysClk160,
       sysClk40Unbuf => sysClk40Unbuf,
       sysClk40Unbuf90 => sysClk40Unbuf90,
       sysClk160Unbuf => sysClk160Unbuf,
       sysClk160Unbuf90 => sysClk160Unbuf90,
       -- Status and control signals                
       reset => sysClkRst,
       locked => locked            
       );

end mapping;
