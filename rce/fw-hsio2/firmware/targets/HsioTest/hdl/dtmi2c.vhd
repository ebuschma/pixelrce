--------------------------------------------------------------
-- DTM Power readout
-- Martin Kocian 12/2014
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.StdRtlPkg.all;
use work.I2cPkg.all;
use work.all;

--------------------------------------------------------------

entity dtmi2c is
generic( PRESCALE_G: integer range 0 to 65535 := 79);
port(	clk: 	    in std_logic;
	d_out:	    out slv(7 downto 0):= (others => '0');
        trig:       in std_logic;
        rw:         in std_logic;
        scl:        inout std_logic;
        sda:        inout std_logic
);
end dtmi2c;

--------------------------------------------------------------

architecture DTMI2C of dtmi2c is

  type state_type is (idle, req, ack);
  signal state: state_type:=idle;
  signal adcI2cIn   : i2c_in_type;
  signal adcI2cOut  : i2c_out_type;
  signal i2cregmasterin: I2cRegMasterInType := I2C_REG_MASTER_IN_INIT_C;
  signal i2cregmasterout: I2cRegMasterOutType;
  signal oldtrig: sl:='0';
  signal secondwrite: sl:='0';
begin
   sda    <= adcI2cOut.sda when adcI2cOut.sdaoen = '0' else 'Z';
   adcI2cIn.sda <= to_x01z(sda);
   scl    <= adcI2cOut.scl when adcI2cOut.scloen = '0' else 'Z';
   adcI2cIn.scl <= to_x01z(scl);

   i2cregmasterin.regDataSize<="00";
   i2cregmasterin.i2cAddr<="0000100000";

   I2cRegMaster_Inst: entity work.I2cRegMaster
     generic map(
       PRESCALE_G => PRESCALE_G)
     port map(
       clk => clk,
       regIn => i2cregmasterin,
       regOut => i2cregmasterout,
       i2ci => adcI2cIn,
       i2co => adcI2cOut);

   process begin
     wait until (rising_edge(clk));
     oldtrig<=trig;
     if(state=idle)then
       if(trig='1' and oldtrig='0') then
         secondwrite<='0';
         state<=req;
       end if;
     elsif (state=req)then
       if(rw='0')then
         i2cregmasterin.regAddr(7 downto 0) <= x"04";
         i2cregmasterin.regOp<='0';
       else
         i2cregmasterin.regOp<='1';
         if(secondwrite='0')then
           i2cregmasterin.regAddr(7 downto 0) <= x"18";
           i2cregmasterin.regWrData(7 downto 0) <= x"ef";
         else
           i2cregmasterin.regAddr(7 downto 0) <= x"08";
           i2cregmasterin.regWrData(7 downto 0) <= x"10";
         end if;
       end if;
       i2cregmasterin.regReq<='1';
       state<=ack;
     elsif(state=ack)then
       i2cregmasterin.regReq<='0';
       if(i2cregmasterout.regAck='1')then
         secondwrite<='1';
         if(rw='0' or secondwrite='1') then
           d_out<= i2cregmasterout.regRdData(7 downto 0); 
           state<=idle;
         else
           state<=req;
         end if;
       end if;
     end if;
   end process;
       
end DTMI2C;

--------------------------------------------------------------
