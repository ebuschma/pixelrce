--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;

--------------------------------------------------------------

entity deser32 is
port(	clk: 	    in std_logic;
	rst:	    in std_logic;
	d_in:	    in std_logic;
        enabled:    in std_logic;
        stop:       in std_logic;
	d_out:	    out std_logic_vector(31 downto 0);
        ld:         out std_logic
);
end deser32;

--------------------------------------------------------------

architecture DESER32 of deser32 is

signal reg : std_logic_vector(31 downto 0);

begin


    process(rst, clk)

    variable counter: std_logic_vector(4 downto 0);
    variable going: std_logic;
    begin
        if(rst='1') then
          reg<=(others => '0');
          d_out<=(others => '0');
          going:='0';
          ld<='0';
        elsif (clk'event and clk='1') then
          if(going='0' and enabled='1' and d_in='1')then 
            going:='1';
            counter:="11111";
          elsif (going='1' and counter="11111") then
            d_out<=reg;
            ld<='1';
            if(stop='1')then
              going:='0';
            end if;
          elsif(counter="11110")then
            ld<='0';
          end if;
          reg(31 downto 1)<=reg(30 downto 0);
	  reg(0)<=d_in;
          counter:=unsigned(counter)-1;
 	end if;
    
    end process;		

end DESER32;

--------------------------------------------------------------
