library ieee;
use ieee.std_logic_1164.all;
use work.stdrtlpkg.all;
use work.HsioTestPkg.all;
use work.all;

library unisim;
use unisim.vcomponents.all;

entity TestClkGen is
  port (
    iRefClk0B113P: in   sl;
    iRefClk0B113M: in   sl;

    iRefClk1B113P: in   sl;
    iRefClk1B113M: in   sl;

    iRefClk0B116P: in   sl;
    iRefClk0B116M: in   sl;

    iRefClk1B116P: in   sl;
    iRefClk1B116M: in   sl;
    
    iRefClk1B213P: in   sl;
    iRefClk1B213M: in   sl;

    iRefClk1B216P: in   sl;
    iRefClk1B216M: in   sl;
  
    sys_clk_out:   out sl;
    sys_rst_out:   out sl;
    clk_out:       out slv(5 downto 0);
    
    QPllRefClk    : out Slv2Array(3 downto 0);
    QPllClk       : out Slv2Array(3 downto 0);
    QPllLock      : out Slv2Array(3 downto 0);
    QPllRefClkLost: out Slv2Array(3 downto 0);
    QPllReset     : in  Slv2Array(3 downto 0);
    QPllResetReg  : in  slv(7 downto 0)
  );
end TestClkGen;

architecture TestClkGen of TestClkGen is
  signal sys_clk: sl;
  signal sys_rst: sl;
  signal refclks_in: std_logic_vector(5 downto 0);

  signal clk_f: std_logic_vector(5 downto 0);
  signal QPllLockDetClk: Slv2Array(3 downto 0);
   
  signal PllReset: Slv2Array(3 downto 0);
begin
   -- Power Up Reset      
  PwrUpRst_Inst : entity work.PwrUpRst
    port map (
      clk    => sys_clk,
      rstOut => sys_rst);
  sys_rst_out<=sys_rst;
  IBUFDS_GTE2_inst0 : IBUFDS_GTE2
  port map (
    I     => iRefClk0B113P,
    IB    => iRefClk0B113M,
    CEB   => '0',
    ODIV2 => open,
    O     => refclks_in(CLK0B113)
  );
  
  IBUFDS_GTE2_inst1 : IBUFDS_GTE2
  port map (
    I     => iRefClk1B113P,
    IB    => iRefClk1B113M,
    CEB   => '0',
    ODIV2 => open,
    O     => refclks_in(CLK1B113)
  );
  
  
  IBUFDS_GTE2_inst2 : IBUFDS_GTE2
  port map (
    I     => iRefClk0B116P,
    IB    => iRefClk0B116M,
    CEB   => '0',
    ODIV2 => open,
    O     => refclks_in(CLK0B116)
  );
  
  IBUFDS_GTE2_inst3 : IBUFDS_GTE2
  port map (
    I     => iRefClk1B116P,
    IB    => iRefClk1B116M,
    CEB   => '0',
    ODIV2 => open,
    O     => refclks_in(CLK1B116)
  );
  
  IBUFDS_GTE2_inst4 : IBUFDS_GTE2
  port map (
    I     => iRefClk1B213P,
    IB    => iRefClk1B213M,
    CEB   => '0',
    ODIV2 => open,
    O     => refclks_in(CLK1B213)
  );
  
  IBUFDS_GTE2_inst5 : IBUFDS_GTE2
  port map (
    I     => iRefClk1B216P,
    IB    => iRefClk1B216M,
    CEB   => '0',
    ODIV2 => open,
    O     => refclks_in(CLK1B216)
  );
  
  bufgen: for I in 0 to 5 generate
    BUFG_Inst : BUFG
      port map (
        I => refclks_in(I),
        O => clk_f(I)
        );
    clk_out(I)<=clk_f(I);
  end generate bufgen;
  clockDivider_inst: entity work.ClockDivider
  generic map (
    BUFR_DIVIDE_G => "2")
  port map(
    clkIn => clk_f(0),
    clkOut => sys_clk);
  sys_clk_out<=sys_clk;
  
  -- Lock det clock must always come from a different source than the pll clock
  QPllLockDetClk(BANK113) <= (others => clk_f(CLK0B116));
  QPllLockDetClk(BANK213) <= (others => clk_f(CLK0B116));
  QPllLockDetClk(BANK116) <= (others => clk_f(CLK0B113));
  QPllLockDetClk(BANK216) <= (others => clk_f(CLK0B113));

  ResetGen: for I in 0 to 7 generate
    PllReset(I/2)(I mod 2) <= QPllReset(I/2)(I mod 2) or QPllResetReg(I) or sys_rst;
  end generate ResetGen;
  Gtp7QuadPll113: entity work.Gtp7QuadPll  
  generic map 
  (
    PLL0_REFCLK_SEL_G    => "001",
    PLL0_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
    PLL0_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
    PLL0_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C,
    PLL1_REFCLK_SEL_G    => "010",
    PLL1_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
    PLL1_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
    PLL1_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C
  ) 
  port map 
  (
    qPllRefClk(0)  => refclks_in(CLK0B113),
    qPllRefClk(1)  => refclks_in(CLK1B113),
    qPllOutClk     => QPllClk(BANK113),
    qPllOutRefClk  => QPllRefClk(BANK113),
    qPllLock       => QPllLock(BANK113),
    qPllLockDetClk => QPllLockDetClk(BANK113),
    qPllRefClkLost => QPllRefClkLost(BANK113),
    qPllReset      => PllReset(BANK113)
  );
  
  Gtp7QuadPll116: entity work.Gtp7QuadPll  
  generic map 
  (
    PLL0_REFCLK_SEL_G    => "001",
    PLL0_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
    PLL0_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
    PLL0_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C,
    PLL1_REFCLK_SEL_G    => "010",
    PLL1_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
    PLL1_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
    PLL1_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C
  ) 
  port map 
  (
    qPllRefClk(0)  => refclks_in(CLK0B116),
    qPllRefClk(1)  => refclks_in(CLK1B116),
    qPllOutClk     => QPllClk(BANK116),
    qPllOutRefClk  => QPllRefClk(BANK116),
    qPllLock       => QPllLock(BANK116),
    qPllLockDetClk => QPllLockDetClk(BANK116),
    qPllRefClkLost => QPllRefClkLost(BANK116),
    qPllReset      => PllReset(BANK116)
  );
  
  Gtp7QuadPll213: entity work.Gtp7QuadPll  
  generic map 
  (
    PLL0_REFCLK_SEL_G    => "101",
    PLL0_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
    PLL0_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
    PLL0_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C,
    PLL1_REFCLK_SEL_G    => "110",
    PLL1_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
    PLL1_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
    PLL1_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C
  ) 
  port map 
  (
    qPllRefClk(0)  => refclks_in(CLK0B113),
    qPllRefClk(1)  => refclks_in(CLK1B113),
    qPllOutClk     => QPllClk(BANK213),
    qPllOutRefClk  => QPllRefClk(BANK213),
    qPllLock       => QPllLock(BANK213),
    qPllLockDetClk => QPllLockDetClk(BANK213),
    qPllRefClkLost => QPllRefClkLost(BANK213),
    qPllReset      => PllReset(BANK213)
  );
  
  Gtp7QuadPll216: entity work.Gtp7QuadPll  
  generic map 
  (
    PLL0_REFCLK_SEL_G    => "101",
    PLL0_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
    PLL0_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
    PLL0_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C,
    PLL1_REFCLK_SEL_G    => "110",
    PLL1_FBDIV_IN_G      => QPLL_FBDIV_IN_C,
    PLL1_FBDIV_45_IN_G   => QPLL_FBDIV_45_IN_C,
    PLL1_REFCLK_DIV_IN_G => QPLL_REFCLK_DIV_IN_C
  ) 
  port map 
  (
    qPllRefClk(0)  => refclks_in(CLK0B116),
    qPllRefClk(1)  => refclks_in(CLK1B116),
    qPllOutClk     => QPllClk(BANK216),
    qPllOutRefClk  => QPllRefClk(BANK216),
    qPllLock       => QPllLock(BANK216),
    qPllLockDetClk => QPllLockDetClk(BANK216),
    qPllRefClkLost => QPllRefClkLost(BANK216),
    qPllReset      => PllReset(BANK216)
  );
  
end TestClkGen;
