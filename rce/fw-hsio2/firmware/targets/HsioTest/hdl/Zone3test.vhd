library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.StdRtlPkg.all;

entity Zone3test is
  port (
    clk    : in sl;
    zone3co_p: out slv(19 downto 0);
    zone3co_n: out slv(19 downto 0);
    zone3ci_p: in slv(19 downto 0);
    zone3ci_n: in slv(19 downto 0);
    zone3c2o_p: out slv(19 downto 0);
    zone3c2o_n: out slv(19 downto 0);
    zone3c2i_p: in slv(19 downto 0);
    zone3c2i_n: in slv(19 downto 0);
    zone3gpo: out slv(28 downto 0);
    zone3gpi: in slv(28 downto 0);
    J25: out Slv8Array(9 downto 0) := (others => (others => '0'));
    J26: out Slv8Array(9 downto 0) := (others => (others => '0'));
    J27: out Slv8Array(9 downto 0) := (others => (others => '0'))
  );
end Zone3test;

architecture Zone3test of zone3test is
  signal J25int: Slv8Array(9 downto 0) := (others => (others => '0'));
  signal J26int: Slv8Array(9 downto 0) := (others => (others => '0'));
  signal J27int: Slv8Array(9 downto 0) := (others => (others => '0'));
  signal counter: slv(11 downto 0) := (others => '0');
  signal zone3c_p: slv(19 downto 0);
  signal zone3c_n: slv(19 downto 0);
  signal zone3c2_p: slv(19 downto 0);
  signal zone3c2_n: slv(19 downto 0);
  signal zone3gp: slv(28 downto 0);
begin
  zone3co_p<=zone3c_p;
  zone3co_n<=zone3c_n;
  zone3c2o_p<=zone3c2_p;
  zone3c2o_n<=zone3c2_n;
  zone3gpo<=zone3gp;
  process begin
  wait until rising_edge(clk);
  counter<=unsigned(counter)+1;
  if(counter=x"000")then
    for I in 0 to 9 loop
      J25int(I) <= (others => '0');
      J26int(I) <= (others => '0');
      J27int(I) <= (others => '0');
    end loop;
  elsif(counter=x"fff")then
    J25 <= J25int;
    J26 <= J26int;
    J27 <= J27int;
  elsif(counter(3 downto 0)=x"1")then
    zone3c_p<=counter(7 downto 4)&counter(7 downto 4)&counter(7 downto 4)&counter(7 downto 4)&counter(7 downto 4);
    zone3c_n<=counter(11 downto 8)&counter(11 downto 8)&counter(11 downto 8)&counter(11 downto 8)&counter(11 downto 8);
    zone3c2_p<=counter(7 downto 4)&counter(7 downto 4)&counter(7 downto 4)&counter(7 downto 4)&counter(7 downto 4);
    zone3c2_n<=counter(11 downto 8)&counter(11 downto 8)&counter(11 downto 8)&counter(11 downto 8)&counter(11 downto 8);
    zone3gp<=counter(11 downto 4) & counter(11 downto 4) & counter(11 downto 4) & counter(11 downto 7);
  elsif(counter(3 downto 0)=x"e")then
    for I in 0 to 19 loop
      J26int((I/4)*2)((I mod 4)*2)<=J26int((I/4)*2)((I mod 4)*2) or (zone3c_p(I) xor zone3ci_p(I));
      J26int((I/4)*2+1)((I mod 4)*2)<=J26int((I/4)*2+1)((I mod 4)*2) or (zone3c_p(I) xor zone3ci_p(I));
      J26int((I/4)*2)((I mod 4)*2+1)<=J26int((I/4)*2)((I mod 4)*2+1) or (zone3c_n(I) xor zone3ci_n(I));
      J26int((I/4)*2+1)((I mod 4)*2+1)<=J26int((I/4)*2+1)((I mod 4)*2+1) or (zone3c_n(I) xor zone3ci_n(I));
      J27int((I/4)*2)((I mod 4)*2)<= J27int((I/4)*2)((I mod 4)*2) or (zone3c2_p(I) xor zone3c2i_p(I));
      J27int((I/4)*2+1)((I mod 4)*2)<=J27int((I/4)*2+1)((I mod 4)*2) or (zone3c2_p(I) xor zone3c2i_p(I));
      J27int((I/4)*2)((I mod 4)*2+1)<=J27int((I/4)*2)((I mod 4)*2+1) or (zone3c2_n(I) xor zone3c2i_n(I));
      J27int((I/4)*2+1)((I mod 4)*2+1)<=J27int((I/4)*2+1)((I mod 4)*2+1) or (zone3c2_n(I) xor zone3c2i_n(I));
    end loop;
    for I in 0 to 24 loop
      J25int(2+I/4)((I mod 4)*2)<=J25int(2+I/4)((I mod 4)*2) or (zone3gp(I) xor zone3gpi(I));
      J25int(2+I/4)((I mod 4)*2+1)<=J25int(2+I/4)((I mod 4)*2+1) or (zone3gp(I) xor zone3gpi(I));
    end loop;
    for I in 0 to 3 loop
      J25int(9)((I mod 4)*2)<=J25int(9)((I mod 4)*2) or (zone3gp(I+25) xor zone3gpi(I+25));
      J25int(9)((I mod 4)*2+1)<=J25int(9)((I mod 4)*2+1) or (zone3gp(I+25) xor zone3gpi(I+25));
    end loop;
  end if;
end process;
end zone3test;
  
    
      
