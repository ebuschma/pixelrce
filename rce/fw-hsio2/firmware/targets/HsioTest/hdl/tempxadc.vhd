--------------------------------------------------------------
-- Temperature readout
-- Martin Kocian 2/2015
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.I2cPkg.all;
use work.all;

--------------------------------------------------------------

entity tempxadc is
generic( PRESCALE_G: integer range 0 to 65535 := 79);
port(	clk: 	    in std_logic;
        rst:        in std_logic;
	d_out:	    out slv(15 downto 0):= (others => '0');
        valid:      out sl:='0'
);
end tempxadc;

--------------------------------------------------------------

architecture TEMPXADC of tempxadc is

  type state_type is (idle, ack);
  signal state: state_type:=idle;
  signal req: sl:='0';
  signal rack: sl;
  signal dout: slv(15 downto 0);
  signal counter     : slv(26 downto 0):=(others => '0');
begin
   process(rst, clk) begin
     if(rising_edge(clk))then
       counter<=unsigned(counter)+1;
     end if;
   end process; 
   XadcCore_inst: entity work.XadcSimpleCore
   port map(
      -- Parallel interface
      locReq  => req,
      locRnW  => '1',                   -- read
      locAck  => rack, 
      locAddr => (others => '0'),
      locDin  => (others => '0'),
      locDout => dout,
      --XADC I/O ports
      vpIn    => '0',
      vnIn    => '0',
      --Global Signals
      locClk  => clk,
      locRst  => rst);            
   process begin
     wait until (rising_edge(clk));
     if(state=idle)then
       if(rst='0' and counter="000"&x"000000") then
         req<='1';
         state<=ack;
       end if;
     elsif(state=ack)then
       req<='0';
       if(rack='1')then
         d_out<=dout;
         valid<='1';
         state<=idle;
       end if;
     end if;
   end process;
       
end TEMPXADC;

--------------------------------------------------------------
