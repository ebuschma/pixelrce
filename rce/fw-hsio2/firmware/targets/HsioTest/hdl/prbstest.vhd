--------------------------------------------------------------
-- DTM Power readout
-- Martin Kocian 12/2014
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.all;

--------------------------------------------------------------

entity prbstest is
generic( PRBS_TAPS_G   : NaturalArray    := (0 => 31, 1 => 6, 2 => 2, 3 => 1));
port(	clk: 	    in std_logic;
        rst:        in std_logic;
	err:	    out slv(31 downto 0):= (others => '0');
        fail:       out std_logic:='0';
        trig:       in std_logic;
        done:       out std_logic:='0';
        len:        in std_logic_vector(31 downto 0);
        dataind:     out slv(31 downto 0);
        dataoutd:    out slv(31 downto 0);
        datainvalidd:out sl:='0';
        dataoutvalidd:out sl:='0';
        busy:       out sl;
        d_in:       in std_logic;
        d_out:      out std_logic
);
end prbstest;

--------------------------------------------------------------

architecture PRBSTEST of prbstest is

  type rx_state_type is (idle, ready, running);
  type tx_state_type is (idle, first, seed, running, last);
  signal rxstate: rx_state_type;
  signal txstate: tx_state_type;
  signal go: sl:='0';
  signal stop: sl:='0';
  signal stoprx: sl:='0';
  signal ld: sl:='0';
  signal oldtrig: sl:='1';
  signal datainvalid: sl:='0';
  signal errsig: slv(31 downto 0);
  signal dataout: slv(31 downto 0):=(others=>'0');
  signal datain: slv(31 downto 0):=(others=>'0');
  signal dataexp: slv(31 downto 0);
  signal txcounter: slv(31 downto 0);
  signal rxcounter: slv(31 downto 0);
begin
   err<=errsig;
   dataind<=datain;
   dataoutd<=dataout;
   datainvalidd<=datainvalid;
   dataoutvalidd<=ld;
   process begin
     wait until (rising_edge(clk));
     oldtrig<=trig;
     if(rst='1')then
       txstate<=idle;
     elsif(txstate=idle)then
       if(trig='1' and oldtrig='0') then
         go<='1';
         stop<='0';
         txstate<=first;
       end if;
     elsif (txstate=first)then
       go<='0';
       if(ld='1')then
         dataout<=x"80000000"; -- Header
         txstate<=seed;
       end if;
     elsif(txstate=seed)then
       if(ld='1')then
         dataout<=len;
         txcounter<=len;
         txstate<=running;
       end if;
     elsif(txstate=running)then
       if(ld='1')then
         dataout <= lfsrShift(dataout, PRBS_TAPS_G);
         txcounter<=unsigned(txcounter)-1;
         if(txcounter=x"00000001")then
           txstate<=last;
         end if;
       end if;
     elsif(txstate=last)then
       if(ld='1')then
         stop<='1';
         dataout<=(others =>'0');
         txstate<=idle;
       end if;
     end if;
   end process;

   process begin
     wait until (rising_edge(clk));
     if(rst='1')then
       rxstate<=idle;
     elsif(rxstate=idle)then
       if(trig='1' and oldtrig='0') then
         stoprx<='0';
         done<='0';
         fail<='0';
         errsig<=(others =>'0');
         rxstate<=ready;
       end if;
     elsif(rxstate=ready)then
       if(datainvalid='1')then
         if(datain/=x"80000000")then
           fail<='1';
           done<='1';
           stoprx<='1';
           rxstate<=idle;
         else
           dataexp<=len;
           rxstate<=running;
           rxcounter<=len;
         end if;
       end if;
     elsif(rxstate=running)then
       if(datainvalid='1')then
         if(datain/=dataexp)then
           errsig<=unsigned(errsig)+1;
         end if;
         dataexp <= lfsrShift(dataexp, PRBS_TAPS_G);
         rxcounter<=unsigned(rxcounter)-1;
         if(rxcounter=x"00000000")then
           stoprx<='1';
           done<='1';
           if(errsig/=x"00000000")then
             fail<='1';
           end if;
           rxstate<=idle;
         end if;
       end if;
     end if;
   end process;
   ser_inst: entity work.ser32
     port map(
       clk => clk,
       ld => ld,
       go => go,
       busy => busy,
       stop => stop,
       rst => rst,
       d_in => dataout,
       d_out => d_out
       );
   deser_inst: entity work.deser32
     port map(
       clk => clk,
       rst => rst,
       d_in => d_in,
       enabled => '1',
       stop => stoprx,
       d_out => datain,
       ld => datainvalid
       );
       
end PRBSTEST;

--------------------------------------------------------------
