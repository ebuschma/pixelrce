-------------------------------------------------------------------------------
-- Title         : HsioCosmic
-- Project       : ATLAS IBL
-------------------------------------------------------------------------------
-- File          : HsioTest.vhd
-- Author        : Martin Kocian, kocian@slac.stanford.edu
-- Author        : Johannes Agricola, johannes.agricola@cern.ch
-- Created       : 11/23/2014
-------------------------------------------------------------------------------
-- Description:
-- Test FGPA configuration for the HSIO 2 board
-------------------------------------------------------------------------------

library ieee;
library unisim;
use unisim.vcomponents.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.stdrtlpkg.all;
use work.HD44780DriverPkg.all;
use work.HsioTestPkg.all;
use work.all;


entity HsioTest is 
  port ( 
    iRefClk0B113P: in   sl;
    iRefClk0B113M: in   sl;

    iRefClk1B113P: in   sl;
    iRefClk1B113M: in   sl;

    iRefClk0B116P: in   sl;
    iRefClk0B116M: in   sl;

    iRefClk1B116P: in   sl;
    iRefClk1B116M: in   sl;

    iRefClk1B213P: in   sl;
    iRefClk1B213M: in   sl;

    iRefClk1B216P: in   sl;
    iRefClk1B216M: in   sl;
  
    oMgtB113TxP: out slv(2 downto 0);
    oMgtB113TxM: out slv(2 downto 0);
    iMgtB113RxP: in  slv(2 downto 0);
    iMgtB113RxM: in  slv(2 downto 0);

    oMgtB213TxP: out slv(3 downto 0);
    oMgtB213TxM: out slv(3 downto 0);
    iMgtB213RxP: in  slv(3 downto 0);
    iMgtB213RxM: in  slv(3 downto 0);

    oMgtB116TxP: out slv(2 downto 0);
    oMgtB116TxM: out slv(2 downto 0);
    iMgtB116RxP: in  slv(2 downto 0);
    iMgtB116RxM: in  slv(2 downto 0);

    oMgtB216TxP: out slv(3 downto 0);
    oMgtB216TxM: out slv(3 downto 0);
    iMgtB216RxP: in  slv(3 downto 0);
    iMgtB216RxM: in  slv(3 downto 0);

    led:           out  slv(7 downto 0) := (others => '0');
    dip_sw:        in   slv(7 downto 0);
    lemoout:       out sl := '1';
    lemoin:        in slv(6 downto 0);
    --lemoout:        out slv(7 downto 0);

    lvdso_p:       out slv(15 downto 0);
    lvdso_n:       out slv(15 downto 0);
    lvdsi_p:       in  slv(15 downto 0);
    lvdsi_n:       in  slv(15 downto 0);

    zone3co_p: out slv(19 downto 0);
    zone3co_n: out slv(19 downto 0);
    zone3ci_p: in slv(19 downto 0);
    zone3ci_n: in slv(19 downto 0);
    zone3c2o_p: out slv(19 downto 0);
    zone3c2o_n: out slv(19 downto 0);
    zone3c2i_p: in slv(19 downto 0);
    zone3c2i_n: in slv(19 downto 0);
    zone3gpo: out slv(28 downto 0);
    zone3gpi: in slv(28 downto 0);

    --DTM low speed lines
    idpmFbP: in slv(3 downto 0);
    idpmFbM: in slv(3 downto 0);
    odpmFbP: out slv(3 downto 0);
    odpmFbM: out slv(3 downto 0);

    --DTM clocks
    dpmClkP: in slv(2 downto 0);
    dpmClkM: in slv(2 downto 0);

    i232DceTx:        in   sl;
    o232DceRx:        out  sl := '0';
    i232DceRts:       in   sl;
    o232DceCts:       out  sl := '1';
    o232DceDsr:       out  sl := '1';
    i232DceDtr:       in   sl;
    o232DceCd:        out  sl := '1';
    i232DceRi:        out  sl := '0';
    
    displayI2cSda: inout std_logic;
    displayI2cScl: inout std_logic;

    sw_softreset: in std_logic;
    sw_dtmboot: in std_logic;

    ttc_scl: inout std_logic;
    ttc_sda: inout std_logic;

    ttc_data_p: in std_logic;
    ttc_data_n: in std_logic;

    dtm_ps0_l: in std_logic;
    dtm_en_l:  out std_logic := '1';
    dtmI2cScl: inout std_logic;
    dtmI2cSda: inout std_logic
  );
end HsioTest;


architecture HsioTest of HsioTest is 
  signal clk: std_logic;
  signal rst: std_logic;
  signal fabric_clk: slv(5 downto 0);
  signal fabric_rst: slv(5 downto 0);
  signal freqOut113n1: slv(31 downto 0);
  signal locked113n1: sl;
  signal freqOut116n0: slv(31 downto 0);
  signal locked116n0: sl;
  signal freqOut116n1: slv(31 downto 0);
  signal locked116n1: sl;
  signal freqOut216n1: slv(31 downto 0);
  signal locked216n1: sl;
  signal freqOut213n1: slv(31 downto 0);
  signal locked213n1: sl;
  signal freqOutDtm0: slv(31 downto 0);
  signal lockedDtm0: sl;
  signal freqOutDtm1: slv(31 downto 0);
  signal lockedDtm1: sl;
  signal freqOutDtm2: slv(31 downto 0);
  signal lockedDtm2: sl;
  signal header1: slv(39 downto 0);
  signal header2: slv(39 downto 0);
  signal J25: Slv8Array(9 downto 0);
  signal J26: Slv8Array(9 downto 0);
  signal J27: Slv8Array(9 downto 0);
  signal txReady_int: slv(15 downto 0);
  signal rxReady_int: slv(15 downto 0);
  
  signal displayBuffer : HD44780DriverDisplayBufferType := (others => x"20");
  
  signal FIT1_Toggle:     std_logic;
  signal PIT1_Toggle:     std_logic;
  signal PIT2_Toggle:     std_logic;
  signal PIT3_Toggle:     std_logic;
  signal PIT4_Toggle:     std_logic;
  signal GPO1:            std_logic_vector(31 downto 0);
  signal GPO2:            std_logic_vector(31 downto 0);
  signal GPO3:            std_logic_vector(31 downto 0);
  signal GPO4:            std_logic_vector(31 downto 0);
  signal GPI1:            std_logic_vector(31 downto 0) := (others => '0');
  signal GPI2:            std_logic_vector(31 downto 0) := (others => '0');
  signal GPI3:            std_logic_vector(31 downto 0) := (others => '0');
  signal GPI4:            std_logic_vector(31 downto 0) := (others => '0');
  signal GPI1_Interrupt:  std_logic;
  signal GPI2_Interrupt:  std_logic;
  signal GPI3_Interrupt:  std_logic;
  signal GPI4_Interrupt:  std_logic;
  signal INTC_Interrupt:  std_logic_vector(15 downto 0) := (others => '0');
  signal INTC_IRQ:        std_logic;
  signal IO_Addr_Strobe:  std_logic;
  signal IO_Read_Strobe:  std_logic;
  signal IO_Write_Strobe: std_logic;
  signal IO_Address:      std_logic_vector(31 downto 0);
  signal IO_Byte_Enable:  std_logic_vector(3 downto 0);
  signal IO_Write_Data:   std_logic_vector(31 downto 0);
  signal IO_Read_Data:    std_logic_vector(31 downto 0) := (others => '0');
  signal IO_Ready:        std_logic := '0';

  signal QPllRefClk:      Slv2Array(3 downto 0);
  signal QPllClk:         Slv2Array(3 downto 0);
  signal QPllLock:        Slv2Array(3 downto 0);
  signal QPllRefClkLost:  Slv2Array(3 downto 0);
  signal QPllResetReg:    slv(7 downto 0);
  signal QPllReset:       Slv2Array(3 downto 0):= (others=>(others => '0'));
  signal userReset:       sl;
  signal userResetv:      slv(15 downto 0);
  signal timercounter:    slv(30 downto 0):=(others =>'0');
  signal dtmdata:         slv(7 downto 0) := (others => '0');
  signal dtmtrig:         sl:='0';
  signal dtmrw:           sl:='0';
  signal fbin:            slv(3 downto 0);
  signal fbout:           slv(3 downto 0);
  signal fb:              slv(3 downto 0);
  signal dtmclk:          slv(2 downto 0);
  signal temperature:     slv(15 downto 0);
  signal tempvalid:       sl;
  signal serDataRising:   sl;
  signal serDataFalling:  sl;
  type na4 is array (natural range 0 to 3) of NaturalArray(0 to 3);
  constant taps:            na4:=
    ((0 => 31, 1 => 6, 2 => 2, 3 => 1), 
    (0 => 30, 1 => 7, 2 => 4, 3 => 2), 
    (0 => 29, 1 => 8, 2 => 3, 3 => 1), 
    (0 => 28, 1 => 9, 2 => 6, 3 => 3)); 
  signal prbsreset:        slv(3 downto 0):="0000";
  signal prbsfail:        slv(3 downto 0);
  signal prbstrig:        slv(3 downto 0):="0000";
  signal prbsdone:        slv(3 downto 0);
  signal prbslen:         slv(31 downto 0):=x"00000010";
  signal prbserr:         Slv32Array(3 downto 0);
  signal clkSync:         sl;
  signal locClkEn:        sl;
  signal locClk:          sl;
  signal locRst:          sl;
  signal clkLocked:       sl;
  signal trigL1:          sl;
  signal trigsynch:       sl;
  signal rsttrigcounter:  sl:='0';
  signal trigcounterloc:  slv(31 downto 0):=(others =>'0');
  signal trigcounter:     slv(31 downto 0):=(others =>'0');
  constant NUMBER_OF_MGTS: natural := 16;
  signal MgtTestState: HsioMgtTestStateArray(NUMBER_OF_MGTS - 1 downto 0) := 
    (
      others => (
        PrbsPacketLength   => x"00001000",
        PrbsRxPacketLength => x"00000000",
        PrbsErrWordCnt     => x"00000000",
        PrbsErrbitCnt      => x"00000000",
        PrbsPacketRate     => x"00000000",
        txPowerDown => "11",
        rxPowerDown => "11",
        others => '0'
      )
    );
  signal MgtTestQpll: HsioMgtTestQPllArray(NUMBER_OF_MGTS - 1 downto 0) := (others => (others => (others => '0')));
  signal MgtTestRxTx: HsioMgtTestRxTxArray(NUMBER_OF_MGTS - 1 downto 0) := (others => (others => '0'));
  constant MgtGenerateMask: std_logic_vector(NUMBER_OF_MGTS - 1 downto 0) := "1111001111110111";
  signal initclockbuf: std_logic:='0';
  signal oldrst: std_logic:='0';
  signal ttc_data: std_logic;
  signal ttcphase: std_logic;
  signal clk_out1: std_logic;

begin

  GEN_DPM_CLK:
  for I in 0 to 2 generate
    U_iclk_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
      port map ( I  => dpmClkP(I)  , IB=> dpmClkM(I)   , O => dtmclk(I)   );
  end generate GEN_DPM_CLK;
  GEN_DPM_LS:
  for I in 0 to 3 generate
    U_ols_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
      port map ( I  => fbout(I)   , O => odpmFbP(I) , OB => odpmFbM(I)  );
    
    U_ils_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
      port map ( I  => idpmFbP(I)  , IB=>idpmFbM(I)   , O => fbin(I)   );
  end generate GEN_DPM_LS;
  U_ttc_data_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
    port map ( I  => ttc_data_p  , IB=> ttc_data_n , O => ttc_data   );

  clkgen_inst: entity work.TestClkGen
  port map(
    iRefClk0B113P => iRefClk0B113P,
    iRefClk0B113M => iRefClk0B113M,
    iRefClk1B113P => iRefClk1B113P,
    iRefClk1B113M => iRefClk1B113M,
    iRefClk0B116P => iRefClk0B116P,
    iRefClk0B116M => iRefClk0B116M,
    iRefClk1B116P => iRefClk1B116P,
    iRefClk1B116M => iRefClk1B116M,
    iRefClk1B213P => iRefClk1B213P,
    iRefClk1B213M => iRefClk1B213M,
    iRefClk1B216P => iRefClk1B216P,
    iRefClk1B216M => iRefClk1B216M, 
  
    sys_clk_out => clk,
    sys_rst_out => rst,
    clk_out => fabric_clk,
    
    QPllRefClk     => QPllRefClk,
    QPllClk        => QPllClk,
    QPllLock       => QPllLock,
    QPllRefClkLost => QPllRefClkLost,
    QPllResetReg   => QPllResetReg,
    QPllReset      => QPllReset
  ); 
  syncClockFreq_inst0: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 312.4e6,
    CLK_UPPER_LIMIT_G => 312.6e6
  )
  port map
  (
    freqOut => freqOut113n1,
    locked => locked113n1,
    clkIn => fabric_clk(CLK1B113),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst1: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 249.9e6,
    CLK_UPPER_LIMIT_G => 250.1e6
  )
  port map
  (
    freqOut => freqOut116n0,
    locked => locked116n0,
    clkIn => fabric_clk(CLK0B116),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst2: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 312.4e6,
    CLK_UPPER_LIMIT_G => 312.6e6
  )
  port map
  (
    freqOut => freqOut116n1,
    locked => locked116n1,
    clkIn => fabric_clk(CLK1B116),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst3: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 119.9e6,
    CLK_UPPER_LIMIT_G => 120.1e6
  )
  port map
  (
    freqOut => freqOut216n1,
    locked => locked216n1,
    clkIn => fabric_clk(CLK1B216),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst4: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 119.9e6,
    CLK_UPPER_LIMIT_G => 120.1e6
  )
  port map
  (
    freqOut => freqOut213n1,
    locked => locked213n1,
    clkIn => fabric_clk(CLK1B213),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst_dtm0: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 249.9e6,
    CLK_UPPER_LIMIT_G => 250.1e6
  )
  port map
  (
    freqOut => freqOutDtm0,
    locked => lockedDtm0,
    clkIn => dtmclk(0),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst_dtm1: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 99.9e6,
    CLK_UPPER_LIMIT_G => 100.1e6
  )
  port map
  (
    freqOut => freqOutDtm1,
    locked => lockedDtm1,
    clkIn => dtmclk(1),
    locClk => clk,
    refClk => clk
  );
  syncClockFreq_inst_dtm2: entity SyncClockFreq
  generic map
  (
    REF_CLK_FREQ_G => 125.0e6,
    REFRESH_RATE_G => 10.0,
    CLK_LOWER_LIMIT_G => 159.9e6,
    CLK_UPPER_LIMIT_G => 160.1e6
  )
  port map
  (
    freqOut => freqOutDtm2,
    locked => lockedDtm2,
    clkIn => dtmclk(2),
    locClk => clk,
    refClk => clk
  );

  TestPgpFrontends: for mgtii in 0 to NUMBER_OF_MGTS - 1 generate
    TestPgpFrontendsCond: if (MgtGenerateMask(mgtii) = '1') generate
     TestPgpFrontend : entity work.TestPgpFrontend
        generic map(
           CLK_DIV_G           => CLK_DIV_C,
           CLK25_DIV_G         => CLK25_DIV_C,
           RX_OS_CFG_G         => RX_OS_CFG_C,
           RXCDR_CFG_G         => RXCDR_CFG_C,
           RXLPM_INCM_CFG_G    => RXLPM_INCM_CFG_C,
           RXLPM_IPCM_CFG_G    => RXLPM_IPCM_CFG_C)    
        port map (
           clk                 => clk,
           rst                 => MgtTestState(mgtii).feSysRst,
           -- Clock, Resets, and Status Signals
           pgpClk              => fabric_clk(mgtii/8*2),
           txReady             => txReady_int(mgtii),
           rxReady             => rxReady_int(mgtii),
           -- QPLL
           gtQPllOutRefClk     => MgtTestQpll(mgtii).QPllRefClk,
           gtQPllOutClk        => MgtTestQpll(mgtii).QPllClk,
           gtQPllLock          => MgtTestQpll(mgtii).QPllLock,
           gtQPllRefClkLost    => MgtTestQpll(mgtii).QPllRefClkLost,
           gtQPllReset         => MgtTestQpll(mgtii).QPllReset,
           -- GT Pins
           gtTxP               => MgtTestRxTx(mgtii).gtTxP,
           gtTxN               => MgtTestRxTx(mgtii).gtTxN,
           gtRxP               => MgtTestRxTx(mgtii).gtRxP,
           gtRxN               => MgtTestRxTx(mgtii).gtRxN,
         
           trigIn          => MgtTestState(mgtii).trig,
           txbusy          => MgtTestState(mgtii).PrbsTxBusy,
           updatedResults  => MgtTestState(mgtii).PrbsUpdatedResults,
           rxbusy          => MgtTestState(mgtii).PrbsRxBusy,
           errMissedPacket => MgtTestState(mgtii).PrbsErrMissedPacket,
           errLength       => MgtTestState(mgtii).PrbsErrLength,
           errDataBus      => MgtTestState(mgtii).PrbsErrDataBus,
           errEofe         => MgtTestState(mgtii).PrbsErrEofe,
           errWordCnt      => MgtTestState(mgtii).PrbsErrWordCnt,
           errbitCnt       => MgtTestState(mgtii).PrbsErrbitCnt,
           packetRate      => MgtTestState(mgtii).PrbsPacketRate,
           packetLength    => MgtTestState(mgtii).PrbsPacketLength,
           RxPacketLength  => MgtTestState(mgtii).PrbsRxPacketLength,

           -- Power down control
           txPowerDown      => MgtTestState(mgtii).txPowerDown,
           rxPoweRdown      => MgtTestState(mgtii).rxPowerDown
         ); 
    end generate;
  end generate;

  LedTestPatternGenerator_inst: entity work.LedTestPatternGenerator
  generic map 
  (
    divider => 250000000
  )
  port map 
  (
    clk    => clk,
    led    => led,
    --led(7 downto 4)    => led(7 downto 4),
    --led(3 downto 0) => open,
    dip_sw => (others => '0')
  );
  --led(0)<=rxReady_int(1);
  --led(1)<=rxReady_int(2);
  --led(2)<=rxReady_int(8);
  --led(3)<=rxReady_int(9);

  headertest_inst: entity work.Headertest
  port map(
    clk => clk,
    lvdso_p => lvdso_p,
    lvdso_n => lvdso_n,
    lvdsi_p => lvdsi_p,
    lvdsi_n => lvdsi_n,
    header1 => header1,
    header2 => header2);

  zone3test_9inst: entity work.Zone3test
  port map(
    clk => clk,
    zone3co_p => zone3co_p,
    zone3co_n => zone3co_n, 
    zone3ci_p => zone3ci_p, 
    zone3ci_n => zone3ci_n, 
    zone3c2o_p => zone3c2o_p, 
    zone3c2o_n => zone3c2o_n, 
    zone3c2i_p => zone3c2i_p, 
    zone3c2i_n => zone3c2i_n, 
    zone3gpo => zone3gpo,
    zone3gpi => zone3gpi,
    J25 => J25,
    J26 => J26,
    J27 => J27
  );

  prbstestgen: for i in 0 to 3 generate
    prbs_inst: entity work.prbstest 
      generic map( PRBS_TAPS_G => taps(I))
      port map(
        clk => clk,
        rst => prbsreset(I),
	err => prbserr(I),
        fail => prbsfail(I),
        trig => prbstrig(I),
        done => prbsdone(I),
        len => prbslen,
        d_in => fbin(I),
        d_out => fbout(I)
        );
    end generate prbstestgen;
  
  I2cHD44780Display_inst: entity work.I2cHD44780Display
  port map
  (
    clk => clk,
    rst => '0',
    displayI2cSda => displayI2cSda,
    displayI2cScl => displayI2cScl,
    dISPLayBuffer => displayBuffer
  );

  dtmI2c_inst: entity work.dtmi2c
    port map(
      clk => clk,
      d_out => dtmdata,
      trig => dtmtrig,
      rw => dtmrw,
      scl => dtmI2cScl,
      sda => dtmI2cSda
      );

  process 
  begin
    wait until rising_edge(clk);
    oldrst<=rst;
    if(rst='0' and oldrst='1')then
      initclockbuf<='1';
    else
      initclockbuf<='0';
    end if;
  end process;
  ttcclockbuf: entity work.clockbuf
    port map(
      clk => clk,
      trig => initclockbuf,
      fail => open,
      scl => ttc_scl,
      sda => ttc_sda
      );

  tempxadc_inst: entity work.tempxadc
    port map(
      clk => clk,
      rst => rst,
      d_out => temperature,
      valid => tempvalid
      );
--clk_wiz_inst : entity work.clk_wiz_0
--   port map ( 

--   -- Clock in ports
--   clk_in1 => dtmclk(2),
--  -- Clock out ports  
--   clk_out1 => clk_out1              
-- );
  -- AtlasTtcRxCdrInputs_Inst : entity work.AtlasTtcRxCdrInputs
  --    port map (
  --       -- CDR Signals
  --       clock          => clk_out1,
  --       --clock          => fabric_clk(CLK1B213),
  --       clk            => open,
  --       ttcRxData      => dtmclk(1),
  --       --ttcRxData      => ttc_data,
  --       data           => open,
  --       -- Emulation Trigger Signals
  --       emuSel         => '0',
  --       emuClk         => '0',
  --       emuData        => '0',
  --       -- Serial Data Signals
  --       serDataRising  => serDataRising,
  --       serDataFalling => serDataFalling,
  --       -- Clock Signals
  --       clkSync        => clkSync,
  --       locClk40MHz    => open,
  --       locClk80MHz    => open,
  --       locClk160MHz   => locClk);   
  -- AtlasTtcRxClkMon_Inst : entity work.AtlasTtcRxClkMon
  --    generic map (
  --       EN_LOL_PORT_G     => false,
  --       EN_SIG_DET_PORT_G => false)    
  --    port map (
  --       -- Status Monitoring
  --       clkLocked       => clkLocked,
  --       freqLocked      => open,
  --       cdrLocked       => open,
  --       sigLocked       => open,
  --       freqMeasured    => open,
  --       ignoreSigLocked => '1',
  --       ignoreCdrLocked => '1',
  --       lockedP         => open,
  --       lockedN         => open,
  --       -- Global Signals
  --       refClk125       => clk,
  --       locClk          => locClk,
  --       locRst          => rst);   

  -- AtlasTtcRxDeSer_Inst : entity work.AtlasTtcRxDeSer
  --    port map (
  --       -- Serial Data Signals
  --       serDataEdgeSel => ttcphase,
  --       serDataRising  => serDataRising,
  --       serDataFalling => serDataFalling,
  --       -- Level-1 Trigger
  --       trigL1         => trigL1,
  --       -- BC Encoded Message
  --       bcValid        => open,
  --       bcData         => open,
  --       bcCheck        => open,
  --       -- IAC Encoded Message
  --       iacValid       => open,
  --       iacData        => open,
  --       iacCheck       => open,
  --       -- Status Monitoring
  --       clkLocked      => clkLocked,
  --       bpmLocked      => open,
  --       bpmErr         => open,
  --       deSerErr       => open,
  --       -- Clock Signals
  --       clkSync        => clkSync,
  --       locClkEn       => locClkEn,
  --       locClk         => locClk,
  --       locRst         => rst);       
  --process(locclk, rsttrigcounter) 
  --begin
  --  if(rsttrigcounter='1') then
  --    trigcounterloc<=(others =>'0');
  --  elsif(rising_edge(locclk)) then
  --    if(trigL1='1')then
  --      trigcounterloc<=std_logic_vector(unsigned(trigcounterloc)+1);
  --    end if;
  --  end if;
  --end process;
  --syncvec: entity work.SynchronizerVector
  --  generic map(WIDTH_G => 32)
  --  port map(
  --    clk => clk,
  --    dataIn => trigcounterloc,
  --    dataOut => trigcounter);
  hsio_test_mb_inst : entity work.hsio_test_mb
  port map 
  (
    Clk => clk,
    Reset => rst,
    IO_Addr_Strobe => IO_Addr_Strobe,
    IO_Read_Strobe => IO_Read_Strobe,
    IO_Write_Strobe => IO_Write_Strobe,
    IO_Address => IO_Address,
    IO_Byte_Enable => IO_Byte_Enable,
    IO_Write_Data => IO_Write_Data,
    IO_Read_Data => IO_Read_Data,
    IO_Ready => IO_Ready,
    UART_Rx => i232DceTx,
    UART_Tx => o232DceRx,
    UART_Interrupt => open,
    FIT1_Toggle => FIT1_Toggle,
    PIT1_Toggle => PIT1_Toggle,
    PIT2_Toggle => PIT2_Toggle,
    PIT3_Toggle => PIT3_Toggle,
    PIT4_Toggle => PIT4_Toggle,
    GPO1 => GPO1,
    GPO2 => GPO2,
    GPO3 => GPO3,
    GPO4 => GPO4,
    GPI1 => GPI1,
    GPI2 => GPI2,
    GPI3 => GPI3,
    GPI4 => GPI4,
    GPI1_Interrupt => GPI1_Interrupt,
    GPI2_Interrupt => GPI2_Interrupt,
    GPI3_Interrupt => GPI3_Interrupt,
    GPI4_Interrupt => GPI4_Interrupt,
    INTC_Interrupt => INTC_Interrupt,
    INTC_IRQ => INTC_IRQ
  );

  process 
    variable addr: natural;
  begin
    wait until rising_edge(clk);
    IO_Read_Data <= (others => '0');
    IO_Ready <= IO_Addr_Strobe;
    case IO_Address(31 downto 16) is
      when x"C000" =>
        if (IO_Write_Strobe = '1') then
          addr := to_integer(unsigned(io_address(7 downto 2)));
          if (addr < 40) then
            displayBuffer(addr) <= IO_Write_Data(7 downto 0);
          end if;
        end if;
      when x"C001" =>
        if (IO_Read_Strobe = '1') then
          IO_Read_Data(7 downto 0) <= dip_sw;
          IO_Read_Data(8) <= sw_softreset;
          IO_Read_Data(9) <= sw_dtmboot;
        end if;
      when x"C002" => -- Headers
        if (IO_Read_Strobe = '1') then
          if(IO_Address(15 downto 0)=x"0000")then
            IO_Read_Data <= header1(31 downto 0);
          elsif(IO_Address(15 downto 0)=x"0004")then
            IO_Read_Data <= x"000000"&header1(39 downto 32);
          elsif(IO_Address(15 downto 0)=x"0008")then
            IO_Read_Data <= header2(31 downto 0);
          elsif(IO_Address(15 downto 0)=x"000c")then
            IO_Read_Data <= x"000000"&header2(39 downto 32);
          else
            IO_Read_Data <= (others => '1');
          end if;
        end if;
      when x"C003" => -- Zone 3 connector
        if (IO_Read_Strobe = '1') then
          if(IO_Address(15 downto 0)=x"0000")then
            IO_Read_Data <= J25(3) & J25(2) & J25(1) & J25(0);
          elsif(IO_Address(15 downto 0)=x"0004")then
            IO_Read_Data <= J25(7) & J25(6) & J25(5) & J25(4);
          elsif(IO_Address(15 downto 0)=x"0008")then
            IO_Read_Data <= x"0000" & J25(9) & J25(8);
          elsif(IO_Address(15 downto 0)=x"000c")then
            IO_Read_Data <= J26(3) & J26(2) & J26(1) & J26(0);
          elsif(IO_Address(15 downto 0)=x"0010")then
            IO_Read_Data <= J26(7) & J26(6) & J26(5) & J26(4);
          elsif(IO_Address(15 downto 0)=x"0014")then
            IO_Read_Data <= x"0000" & J26(9) & J26(8);
          elsif(IO_Address(15 downto 0)=x"0018")then
            IO_Read_Data <= J27(3) & J27(2) & J27(1) & J27(0);
          elsif(IO_Address(15 downto 0)=x"001c")then
            IO_Read_Data <= J27(7) & J27(6) & J27(5) & J27(4);
          elsif(IO_Address(15 downto 0)=x"0020")then
            IO_Read_Data <= x"0000" & J27(9) & J27(8);
          else
            IO_Read_Data <= (others => '1');
          end if;
        end if;
      when x"C004" =>
        if (IO_Read_Strobe = '1') then
          IO_Read_Data(6 downto 0) <= lemoin;
        end if;
      when x"C005" =>
        if (IO_Read_Strobe = '1') then
          if(IO_Address(15 downto 0)=x"0000")then
            IO_Read_Data <= locked113n1 & freqOut113n1(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0004")then
            IO_Read_Data <= locked116n0 & freqOut116n0(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0008")then
            IO_Read_Data <= locked116n1 & freqOut116n1(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"000c")then
            IO_Read_Data <= lockedDtm0 & freqOutDtm0(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0010")then
            IO_Read_Data <= lockedDtm1 & freqOutDtm1(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0014")then
            IO_Read_Data <= lockedDtm2 & freqOutDtm2(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"0018")then
            IO_Read_Data <= locked216n1 & freqOut216n1(30 downto 0);
          elsif(IO_Address(15 downto 0)=x"001c")then
            IO_Read_Data <= locked213n1 & freqOut213n1(30 downto 0);
          else
            IO_Read_Data <= (others => '0');
          end if;
        end if;
      when x"C006" =>
        if (IO_Read_Strobe = '1') then
          if(IO_Address(15 downto 0)=x"0000")then
            IO_Read_Data <= '0' & timercounter;
          end if;
        end if;
      when x"C007" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data <= (0=> dtm_ps0_l, others => '0');
            when x"0004" =>
              IO_Read_Data(7 downto 0) <= dtmdata;
            when others =>
          end case;
        elsif (IO_Write_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              dtm_en_l <= IO_Write_Data(0);
            when x"0004" =>
              dtmtrig <= IO_Write_Data(0);
              dtmrw <= IO_Write_Data(1);
            when others =>
          end case;
        end if;
      when x"C008" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data <= prbserr(0);
            when x"0004" =>
              IO_Read_Data <= prbserr(1);
            when x"0008" =>
              IO_Read_Data <= prbserr(2);
            when x"000c" =>
              IO_Read_Data <= prbserr(3);
            when x"0010" =>
              IO_Read_Data(3 downto 0) <= prbsdone;
            when x"0014" =>
              IO_Read_Data(3 downto 0) <= prbsfail;
            when others =>
              IO_Read_Data <= (others=>'0');
          end case;
        elsif (IO_Write_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              prbsreset <= IO_Write_Data(3 downto 0);
            when x"0004" =>
              prbstrig <= IO_Write_Data(3 downto 0);
            when x"0008" =>
              prbslen <= IO_Write_Data;
            when others =>
          end case;
        end if;
      when x"C009" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data(31) <= tempvalid;
              IO_Read_Data(15 downto 0) <= temperature;
            when others =>
          end case;
        end if;
      when x"C00A" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data <= trigcounter;
            when others =>
          end case;
        elsif (IO_Write_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              rsttrigcounter <= IO_Write_Data(0);
              ttcphase <= IO_Write_Data(1);
            when others =>
          end case;
        end if;
      when x"C100" =>
        if (IO_Read_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0000" =>
              IO_Read_Data(7 downto 0) <= QPllLock(3) & QPllLock(2) & QPllLock(1) & QPllLock(0);
            when x"0004" =>
              IO_Read_Data(7 downto 0) <= QPllRefClkLost(3) & QPllRefClkLost(2) & QPllRefClkLost(1) & QPllRefClkLost(0);
            when others =>
          end case;
        end if;
        if (IO_Write_Strobe = '1') then
          case IO_Address(15 downto 0) is
            when x"0008" =>
              QPllResetReg <= IO_Write_Data(7 downto 0);
            when others =>
          end case;
        end if;
      when x"C110" =>
        addr := to_integer(unsigned(io_address(15 downto 8)));
        if IO_Write_Strobe = '1' then
          case IO_Address(7 downto 0) is
            when x"00" =>
              MgtTestState(addr).PrbsPacketLength <= IO_Write_Data;
            when x"04" =>
              MgtTestState(addr).PrbsTrigIn <= IO_Write_Data(0);
            when x"08" =>
              MgtTestState(addr).PrbsSysReset <= IO_Write_Data(0);
            whEn x"24" =>
              MgtTestState(addr).rxPowerDown <= IO_Write_Data(1 downto 0);
              MgtTestState(addr).txPowerDown <= IO_Write_Data(3 downto 2);
            when others =>
          end case;
        end if;
        if IO_Read_Strobe = '1' then
          case IO_Address(7 downto 0) is
            when x"10" =>
              IO_Read_Data(0) <= MgtTestState(addr).PrbsRxBusy;
              IO_Read_Data(1) <= MgtTestState(addr).PrbsTxBusy;
              IO_Read_Data(2) <= MgtTestState(addr).updatedResults;
              IO_Read_Data(3) <= MgtTestState(addr).PrbsErrMissedPacket;
              IO_Read_Data(4) <= MgtTestState(addr).PrbsErrLength;
              IO_Read_Data(5) <= MgtTestState(addr).PrbsErrDataBus;
              IO_Read_Data(6) <= MgtTestState(addr).PrbsErrEofe;
            when x"14" =>
              IO_Read_Data <= MgtTestState(addr).PrbsErrWordCnt;
            when x"18" =>
              IO_Read_Data <= MgtTestState(addr).PrbsErrbitCnt;
            when x"1c" =>
              IO_Read_Data <= MgtTestState(addr).PrbsPacketRate;
            when x"20" =>
              IO_Read_Data <= MgtTestState(addr).PrbsRxPacketLength;
            when others =>
          end case;
        end if;
      when others =>
    end case;
  end process;

  process
  variable mgti: natural;
  begin
    wait until rising_edge(clk);
    for mgti in NUMBER_OF_MGTS - 1 downto 0 loop
      MgtTestState(mgti).trigIn_d1 <= MgtTestState(mgti).PrbsTrigIn;
      if (MgtTestState(mgti).trigIn_d1 = '0' and MgtTestState(mgti).PrbsTrigIn = '1') then
        MgtTestState(mgti).trig <= '1';
        MgtTestState(mgti).updatedResults <='0';
      elsif(MgtTestState(mgti).PrbsSysReset='1')then
        MgtTestState(mgti).trig<='0';
        MgtTestState(mgti).updatedResults<='0';
      elsif(MgtTestState(mgti).PrbsUpdatedResults='1')then
        MgtTestState(mgti).trig<='0';
        MgtTestState(mgti).updatedResults<='1';
      else
        MgtTestState(mgti).trig <= '0';
      end if;
    end loop;
  end process;
  
  uresetgen: for mgti in NUMBER_OF_MGTS - 1 downto 0 generate
    MgtTestState(mgti).feSysRst<=MgtTestState(mgti).PrbsSysReset or rst;
  end generate uresetgen;

  genpll: for mgti in 0 to NUMBER_OF_MGTS - 1 generate
    MgtTestQpll(mgti).QPllRefClk <= QPllRefClk(mgti/4);
    MgtTestQpll(mgti).QPllClk <= QPllClk(mgti/4);
    MgtTestQpll(mgti).QPllLock <= QPllLock(mgti/4);
    MgtTestQpll(mgti).QPllRefClkLost <= QPllRefClkLost(mgti/4);
  end generate genpll;
  genpllreset: for I in 0 to 3 generate
    QPllReset(I)<= MgtTestQpll(I*4).QPllReset or
                   MgtTestQpll(I*4+1).QPllReset or
                   MgtTestQpll(I*4+2).QPllReset or
                   MgtTestQpll(I*4+3).QPllReset;
  end generate genpllreset;
  
  process
    variable banki: natural;
    variable mgti: natural;
  begin
    for mgti in 0 to 2 loop
      banki := mgti;
      MgtTestRxTx(mgti).gtRxN <= iMgtB113RxM(banki);
      MgtTestRxTx(mgti).gtRxP <= iMgtB113RxP(banki);
      oMgtB113TxM(banki) <= MgtTestRxTx(mgti).gtTxN;
      oMgtB113TxP(banki) <= MgtTestRxTx(mgti).gtTxP;
    end loop;
    for mgti in 4 to 7 loop
      banki := mgti - 4;
      MgtTestRxTx(mgti).gtRxN <= iMgtB213RxM(banki);
      MgtTestRxTx(mgti).gtRxP <= iMgtB213RxP(banki);
      oMgtB213TxM(banki) <= MgtTestRxTx(mgti).gtTxN;
      oMgtB213TxP(banki) <= MgtTestRxTx(mgti).gtTxP;
    end loop;
    for mgti in 8 to 9 loop
      banki := mgti - 8;
      MgtTestRxTx(mgti).gtRxN <= iMgtB116RxM(banki);
      MgtTestRxTx(mgti).gtRxP <= iMgtB116RxP(banki);
      oMgtB116TxM(banki) <= MgtTestRxTx(mgti).gtTxN;
      oMgtB116TxP(banki) <= MgtTestRxTx(mgti).gtTxP;
    end loop;
    for mgti in 12 to 15 loop
      banki := mgti - 12;
      MgtTestRxTx(mgti).gtRxN <= iMgtB216RxM(banki);
      MgtTestRxTx(mgti).gtRxP <= iMgtB216RxP(banki);
      oMgtB216TxM(banki) <= MgtTestRxTx(mgti).gtTxN;
      oMgtB216TxP(banki) <= MgtTestRxTx(mgti).gtTxP;
    end loop;
  end process;

  process begin
    wait until rising_edge(clk);
    timercounter<=std_logic_vector(unsigned(timercounter)+1);
  end process;

end HsioTest;
