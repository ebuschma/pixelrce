library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.StdRtlPkg.all;

entity Clocktest is
  port (
    clk    : in sl;
    clkA: out sl;
    clkB: out sl;
    serialin: in slv(17 downto 0);
    result: out slv(17 downto 0) := (others => '0')
  );
end Clocktest;

architecture Clocktest of Clocktest is
  signal counter: slv(19 downto 0) := (others => '0');
  signal header1int: slv(17 downto 0) := (others => '0');
begin
  process begin
  wait until rising_edge(clk);
  counter<=unsigned(counter)+1;
  if(counter=x"00000")then
    header1int <= (others => '0');
  elsif(counter=x"fffff")then
    result<=header1int;
  elsif(counter(3 downto 0)=x"1")then
    clkA<=counter(4);
    clkB<=counter(4);
     
  elsif(counter(3 downto 0)=x"e")then
    for I in 0 to 17 loop
      header1int(I)<= header1int(I) or (serialin(I) xor counter(4));
    end loop;
  end if;
end process;
end Clocktest;
  
    
      
