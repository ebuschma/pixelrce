-------------------------------------------------------------------------------
-- Title         : Version File
-- Project       : PGP To PCI-E Bridge Card, 8x
-------------------------------------------------------------------------------
-- File          : PgpCard8xG2Version.vhd
-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
-- Created       : 04/27/2010
-------------------------------------------------------------------------------
-- Description:
-- Version Constant Module.
-------------------------------------------------------------------------------
-- Copyright (c) 2010 by SLAC National Accelerator Laboratory. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 04/27/2010: created.
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

package Version is

constant FPGA_VERSION_C : std_logic_vector(31 downto 0) := x"00000003"; -- MAKE_VERSION

constant BUILD_STAMP_C : string := "HsioTest: Vivado v2014.4 (x86_64) Built Sun Jan  3 19:33:09 CET 2016 by mkocian";


end Version;

-------------------------------------------------------------------------------
-- Revision History:
-- 11/26/2014 (0x00000001): HSIO test only
-- 01/22/2015 (0x00000002): HSIO plus DTM
-- 08/21/2015 (0x00000003): Prototype V 3
-------------------------------------------------------------------------------

