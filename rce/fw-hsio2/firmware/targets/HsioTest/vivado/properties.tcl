add_files -norecurse ../../../software/hsiotest/mb/hsio_test_mb_sw.elf
add_files -norecurse ../../../software/hsiotest/mb/hsio_test_mb_hw_platform_0/hsio_test_mb.bmm
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1
set_property SCOPED_TO_REF hsio_test_mb [get_files -all -of_objects [get_fileset sources_1] {hsio_test_mb_sw.elf}]
set_property SCOPED_TO_CELLS { U0/microblaze_I } [get_files -all -of_objects [get_fileset sources_1] {hsio_test_mb_sw.elf}]
