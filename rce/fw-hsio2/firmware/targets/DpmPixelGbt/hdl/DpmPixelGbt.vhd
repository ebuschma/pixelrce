-------------------------------------------------------------------------------
-- DtmHsio.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.numeric_std.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

use work.RceG3Pkg.all;
use work.StdRtlPkg.all;
use work.AxiLitePkg.all;
use work.AxiStreamPkg.all;
use work.Pgp3p125GbpsPkg.all;
use work.AtlasTtcRxPkg.all;
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;

entity DpmPixelGbt is
   port (
      -- Debug
      led        : out   slv(1 downto 0);
      -- I2C
      i2cSda     : inout sl;
      i2cScl     : inout sl;
      -- Ethernet
      ethRxP     : in    slv(0 downto 0);
      ethRxM     : in    slv(0 downto 0);
      ethTxP     : out   slv(0 downto 0);
      ethTxM     : out   slv(0 downto 0);
      ethRefClkP : in    sl;
      ethRefClkM : in    sl;
      -- -- RTM High Speed
      dpmToRtmHsP : out   slv(1 downto 0);
      dpmToRtmHsM : out   slv(1 downto 0);
      rtmToDpmHsP : in    slv(1 downto 0);
      rtmToDpmHsM : in    slv(1 downto 0);
      -- Reference Clocks
      locRefClkP : in    sl;
      locRefClkM : in    sl;
      -- DTM Signals
      dtmRefClkP : in    sl;
      dtmRefClkM : in    sl;
      dtmClkP    : in    slv(1 downto 0);
      dtmClkM    : in    slv(1 downto 0);
      dtmFbP     : out   sl;
      dtmFbM     : out   sl;
      -- Clock Select
      clkSelA    : out   slv(1 downto 0);
      clkSelB    : out   slv(1 downto 0);
      discin       : in    slv(3 downto 0)
   );
end DpmPixelGbt;

architecture STRUCTURE of DpmPixelGbt is

   constant TPD_C : time := 1 ns;

   -- Local Signals
   signal axiClk             : sl;
   signal axiClkRst          : sl;

   signal sysClk125          : sl;
   signal sysClk125Rst       : sl;

   signal sysClk200          : sl;
   signal sysClk200Rst       : sl;
   signal axiClk40          : std_logic;
   signal axiRst40          : std_logic;

   signal extAxilReadMaster  : AxiLiteReadMasterType;
   signal extAxilReadSlave   : AxiLiteReadSlaveType;
   signal extAxilWriteMaster : AxiLiteWriteMasterType;
   signal extAxilWriteSlave  : AxiLiteWriteSlaveType;

   signal locAxilReadMaster  : AxiLiteReadMasterArray(0 downto 0);
   signal locAxilReadSlave   : AxiLiteReadSlaveArray(0 downto 0);
   signal locAxilWriteMaster : AxiLiteWriteMasterArray(0 downto 0);
   signal locAxilWriteSlave  : AxiLiteWriteSlaveArray(0 downto 0);

   signal dmaClk             : slv(2 downto 0);
   signal dmaClkRst          : slv(2 downto 0);

   signal dmaObMaster        : AxiStreamMasterArray(2 downto 0);
   signal dmaObSlave         : AxiStreamSlaveArray(2 downto 0);
   signal dmaIbMaster        : AxiStreamMasterArray(2 downto 0);
   signal dmaIbSlave         : AxiStreamSlaveArray(2 downto 0);

   signal iethRxP : slv(3 downto 0);
   signal iethRxM : slv(3 downto 0);
   signal iethTxP : slv(3 downto 0);
   signal iethTxM : slv(3 downto 0);

   signal dtmClkIn           : slv(1 downto 0);
   signal disc           : std_logic_vector(3 downto 0);
   signal calibmode     : std_logic_vector(1 downto 0);
   signal eudaqdone     : std_logic;
   signal eudaqtrgword  : std_logic_vector(14 downto 0);
   signal trigenabled   : std_logic;
   signal paused        : std_logic;
   signal triggermask   : std_logic_vector(15 downto 0);
   signal resetdelay    : std_logic;
   signal incrementdelay: std_logic_vector(4 downto 0); 
   signal discop        : std_logic_vector(15 downto 0); 
   signal telescopeop   : std_logic_vector(2 downto 0);
   signal period        : std_logic_vector(31 downto 0);
   signal fifothresh    : std_logic;
   signal serbusy       : std_logic;
   signal tdcreadoutbusy: std_logic;
   signal l1a           : std_logic;
   signal triggerword   : std_logic_vector(7 downto 0);
   signal busy          : std_logic;
   signal rstFromCore   : std_logic;
   signal hitbus        : std_logic_vector(1 downto 0);
   signal serialoutb     : std_logic_vector(31 downto 0);
   signal serialinb      : std_logic_vector(31 downto 0):=(others => '0');
   signal sysClk40      : std_logic;
   signal sysRst40      : std_logic;
   signal sysClk80     : std_logic;
   signal sysClk160     : std_logic;
   signal sysClk160Unbuf    : std_logic;
   signal sysClk100         : std_logic;
   signal sysRst100         : std_logic;
   signal sysClk40Unbuf     : std_logic;
   signal sysClk40Unbuf90   : std_logic;
   signal stableClk125      : std_logic;
   signal sysClk120         : std_logic;
   signal busypause         : std_logic;
   signal iHSIOtriggerHB: slv(1 downto 0):="00";
   signal vetodis: sl;                
   signal cdtenabled : sl;
   signal numbuffers: slv(4 downto 0);
   signal window: slv(15 downto 0);
   signal debug          : std_logic_vector(7 downto 0);
   signal busyOut        : sl:='0';
   signal sd             : sl;
   signal sysClkLock     : sl;
   signal ttcClk160: sl;
   signal ttc_clk_40: sl;
   signal axiReadMasterTtc: AxiLiteReadMasterType;
   signal axiReadSlaveTtc: AxiLiteReadSlaveType;
   signal axiWriteMasterTtc: AxiLiteWriteMasterType;
   signal axiWriteSlaveTtc: AxiLiteWriteSlaveType;
   signal ttcStatusWord: Slv64Array(0 to 0);
   signal ttcStatusSend: sl; 
   signal atlasClk160MHz: sl;
   signal atlasClk160MHzEn: sl;
   signal ttc_rx_out: AtlasTTCRxOutType;
 

begin

   --------------------------------------------------
   -- Core
   --------------------------------------------------
   U_DpmCore : entity work.DpmCore
      generic map (
         TPD_G          => TPD_C,
         ETH_10G_EN_G   => false,
         RCE_DMA_MODE_G => RCE_DMA_AXIS_C,
         OLD_BSI_MODE_G => false)
      port map (
         -- I2C
         i2cSda             => i2cSda,
         i2cScl             => i2cScl,
         -- Ethernet
         ethRxP             => iethRxP,
         ethRxM             => iethRxM,
         ethTxP             => iethTxP,
         ethTxM             => iethTxM,
         ethRefClkP         => ethRefClkP,
         ethRefClkM         => ethRefClkM,
         -- Clock Select
         clkSelA            => clkSelA,
         clkSelB            => clkSelB,
         -- Clocks
         sysClk125          => sysClk125,
         sysClk125Rst       => sysClk125Rst,
         sysClk200          => sysClk200,
         sysClk200Rst       => sysClk200Rst,
         -- External Axi Bus, 0xA0000000 - 0xAFFFFFFF
         axiClk             => axiClk,
         axiClkRst          => axiClkRst,
         extAxilReadMaster  => extAxilReadMaster,
         extAxilReadSlave   => extAxilReadSlave,
         extAxilWriteMaster => extAxilWriteMaster,
         extAxilWriteSlave  => extAxilWriteSlave,
         -- DMA Interfaces
         dmaClk             => dmaClk,
         dmaClkRst          => dmaClkRst,
         dmaState           => open,
         dmaObMaster        => dmaObMaster,
         dmaObSlave         => dmaObSlave,
         dmaIbMaster        => dmaIbMaster,
         dmaIbSlave         => dmaIbSlave,
         -- User Interrupts
         userInterrupt      => (others => '0'));  

   -- 1 GigE Mapping
   ethTxP(0)           <= iethTxP(0);
   ethTxM(0)           <= iethTxM(0);
   iethRxP(0)          <= ethRxP(0);
   iethRxM(0)          <= ethRxM(0);
   iethRxP(3 downto 1) <= (others => '0');
   iethRxM(3 downto 1) <= (others => '0');


   -------------------------------------
   -- AXI Lite Crossbar
   -- Base: 0xA0000000 - 0xAFFFFFFF
   -------------------------------------
   U_AxiCrossbar : entity work.AxiLiteCrossbar 
      generic map (
         TPD_G              => TPD_C,
         NUM_SLAVE_SLOTS_G  => 1,
         NUM_MASTER_SLOTS_G => 1,
         DEC_ERROR_RESP_G   => AXI_RESP_OK_C,
         MASTERS_CONFIG_G   => (

            -- Channel 0 = 0xA0000000 - 0xA000FFFF : PGP
            0 => ( baseAddr     => x"A0000000",
                   addrBits     => 16,
                   connectivity => x"FFFF")
         )
      ) port map (
         axiClk              => axiClk,
         axiClkRst           => axiClkRst,
         sAxiWriteMasters(0) => extAxilWriteMaster,
         sAxiWriteSlaves(0)  => extAxilWriteSlave,
         sAxiReadMasters(0)  => extAxilReadMaster,
         sAxiReadSlaves(0)   => extAxilReadSlave,
         mAxiWriteMasters    => locAxilWriteMaster,
         mAxiWriteSlaves     => locAxilWriteSlave,
         mAxiReadMasters     => locAxilReadMaster,
         mAxiReadSlaves      => locAxilReadSlave
      );


   --------------------------------------------------
   -- PPI Loopback
   --------------------------------------------------
   DMA_STATIC: for i in 1 to 2 generate
      dmaIbMaster(i) <= AXI_STREAM_MASTER_INIT_C;
      dmaObSlave(i) <= AXI_STREAM_SLAVE_FORCE_C;
   end generate DMA_STATIC;
   dmaClk(0)      <= sysClk200;
   dmaClkRst(0)   <= sysClk200Rst;

   --U_ClkGen: entity work.ClkGen120
   --  port map(
   --    extRstL => '1', --iResetInL,
   --    stableClk125 => stableClk125,
   --    sysClk120 => sysClk120,
   --    sysClk40 => sysClk40,
   --    sysRst40 => sysRst40,
   --    sysClk80 => sysClk80,
   --    sysClk160 => sysClk160,
   --    sysClk40Unbuf => sysClk40Unbuf,
   --    sysClk40Unbuf90 => sysClk40Unbuf90,
   --    sysClk160Unbuf => sysClk160Unbuf,
   --    sysClk100 => sysClk100,
   --    sysRst100 => sysRst100,
   --    sysClkLock => sysClkLock,
   --    sysClkRst => '0',
   --    gtClk0P => dtmRefClkP,
   --    gtClk0N => dtmRefClkM,
   --    gtClk1P => locRefClkP,
   --    gtClk1N => locRefClkM);
   U_ClkGen: entity work.ClkGenGbt
     port map(
       ttc_clk_40 => ttc_clk_40,
       mgtClk120 => sysClk120,
       sysClk40 => sysClk40,
       sysRst40 => sysRst40,
       sysClk80 => sysClk80,
       sysClk160 => sysClk160,
       sysClk40Unbuf => sysClk40Unbuf,
       sysClk160Unbuf => sysClk160Unbuf,
       sysClk100 => sysClk100,
       sysRst100 => sysRst100,
       axiClk40 => axiClk40,
       axiRst40 => axiRst40,
       sysClkLock => sysClkLock,
       sysClkRst => not ttcStatusWord(0)(3), --bpm locked
       gtClkP => locRefClkP,
       gtClkN => locRefClkM,
       gtClk2P => dtmRefClkP,
       gtClk2N => dtmRefClkM);

  U_triggerlogic: entity work.triggerlogic 
    port map(
      clk => sysClk40,
      rst => rstFromCore,
      axiClk40 => axiClk40,
      axiRst40 => axiRst40,
      -- hardware inputs
      discin => discin,
      hitbusin => hitbus,
      -- HSIO trigger
      HSIObusy => open,
      HSIOtrigger => "00",
      --HSIOtrigger => iHSIOtriggerHB,
      --HSIObusyin => iHSIObusy,
      HSIObusyin => '0',
      hitbusout => open,
      
      -- eudet trigger
      exttrigger => '0',
      extrst => '0',
      extbusy => open,
      exttrgclk => open,
      
      calibmode => calibmode,
      eudaqdone => eudaqdone,
      eudaqtrgword => eudaqtrgword,
      
      ttctrig => '0',
      trigenabled => trigenabled,
      vetodis => vetodis,
      paused => paused,
      triggermask => triggermask,
      resetdelay => resetdelay,
      incrementdelay => incrementdelay,
      discop => discop,
      telescopeop => telescopeop,
      period => period,
      cdtenabled => cdtenabled,
      numbuffers => numbuffers,
      window => window,
      fifothresh => fifothresh,
      serbusy => serbusy,
      tdcreadoutbusy => tdcreadoutbusy,
      l1a => l1a,
      triggerword => triggerword,
      busy =>busy,
      coincd =>open
);

   --------------------------------------------------
   -- PGP Lane
   U_HsioCosmicCore1: entity work.PixelGbtCore
     generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 15,
                  rawFirstChannel => 21,
                  rawLastChannel => 20,
                  buffersizetdc => 14,
                  buffersizefe => 12,
                  encodingDefault => "00",
                  hitbusreadout => '0',
                  atlasafp => '1',
                  TX_OPTIMIZATION_G => LATENCY_OPTIMIZED,
                  CLK_DIV_G => CLK_DIV_C,
                  CLK25_DIV_G => CLK25_DIV_C,
                  RX_OS_CFG_G => RX_OS_CFG_C,
                  RXCDR_CFG_G => RXCDR_CFG_C,
                  RXLPM_INCM_CFG_G => RXLPM_INCM_CFG_C,
                  RXLPM_IPCM_CFG_G => RXLPM_IPCM_CFG_C)    
     port map (
      sysClk160 => sysClk160, sysClk80  => sysClk80,  
      sysClk40  => sysClk40,  sysRst40 => sysRst40,
      sysClk160Unbuf => sysClk160Unbuf, sysClk160Unbuf90 => '0',
      sysClk40Unbuf => sysClk40Unbuf, sysClk40Unbuf90 => sysClk40Unbuf90,
      sysClk120 => sysClk120, stableClk125 => sysClk125,
      sysClkLock => sysClkLock,
      clk_in_sel => open, ttcClkOk => '0',
      clk_in_sel_in => '1', ttc_out => ttc_rx_out,
      rxClk160 => atlasClk160MHz, rxClk160En => atlasClk160MHzEn,
      sysClk100 => sysClk100, sysRst100 => sysRst100,
      axiClk40 => axiClk40, axiRst40 => axiRst40,
      pgpClk    => dmaClk(0), pgpRst   => dmaClkRst(0),
      gtQPllOutRefClk => (others =>'0'), gtQPllOutClk => (others=>'0'),
      gtQPllLock => (others=>'0'), gtQPllRefClkLost => (others=>'0'),
      gtQPllReset => open,
      reload => open, mgtRxN    => rtmToDpmHsM(0),
      mgtRxP     => rtmToDpmHsP(0),     mgtTxN    => dpmToRtmHsM(0),
      mgtTxP     => dpmToRtmHsP(0),
      SLinkRxN => rtmToDpmHsM(1), SLinkRxP  => rtmToDpmHsP(1), 
      SLinkTxN => dpmToRtmHsM(1), SLinkTxP => dpmToRtmHsP(1),
      txMaster => dmaObMaster(0),
      txSlave => dmaObSlave(0),
      rxMaster => dmaIbMaster(0),
      rxSlave  => dmaIbSlave(0),
      serialin  => serialinb, serialout  => serialoutb, 
      l1a => l1a, 
      latchtriggerword => triggerword, busy =>busy,
      eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
      pausedout => paused, present =>open, 
      trgenabledout => trigenabled, vetodisout => vetodis,
      rstFromCore => rstFromCore,
      fifothresh => fifothresh,  triggermask => triggermask,
      discop => discop, period => period,
      telescopeop => telescopeop, calibmodeout => calibmode,
      resetdelay => resetdelay, incrementdelay => incrementdelay, 
      sbusy => serbusy, tdcbusy => tdcreadoutbusy,
      hitbus => hitbus(0), cdtenabled => cdtenabled,
      numbuffers => numbuffers, window => window,
      debug     => debug, 
      axiReadMasterTtc => axiReadMasterTtc, axiReadSlaveTtc => axiReadSlaveTtc,
      axiWriteMasterTtc => axiWriteMasterTtc, axiWriteSlaveTtc => axiWriteSlaveTtc,
      exttriggero => open, extrsto => open,
      dispDigitA => open, dispDigitB => open,
      dispDigitC => open, dispDigitD => open,
      dispDigitE => open, dispDigitF => open,
      dispDigitG => open, dispDigitH => open,
      txReady => open, rxReady => open,
      trigAdc => open, adcSel => open,
      sendAdcData => '0', adcData => (others => (others => '0'))
   );
   --------------------------------------------------
   busypause<= busy or paused;
   AtlasTtcRx_Inst: entity work.AtlasTtcRx
     port map(
       clkP => dtmClkP(0),
       clkN => dtmClkM(0),
       dataP => dtmClkP(1),
       dataN => dtmClkM(1),
       busyIn => busypause,
       busyP => dtmFbP,
       busyN => dtmFbM,
       emuSel => '0',
       emuClk => '0',
       emuData => '0',
       axiClk => axiClk40,
       axiRst => axiRst40,
       axiReadMaster => axiReadMasterTtc,
       axiReadSlave => axiReadSlaveTtc,
       axiWriteMaster => axiWriteMasterTtc,
       axiWriteSlave => axiWriteSlaveTtc,
       statusWords => ttcStatusWord,
       statusSend => ttcStatusSend,
       refClk200MHz => sysClk200,
       refClkLocked => not sysClk200Rst,
       atlasTtcRxOut => ttc_rx_out,
       atlasClk40MHz => ttc_clk_40,
       atlasClk80MHz => open,
       atlasClk160MHz => atlasClk160MHz,
       atlasClk160MHzEn => atlasClk160MHzEn,
       atlasClk160MHzRst => open);

   led<="00";

end architecture STRUCTURE;

