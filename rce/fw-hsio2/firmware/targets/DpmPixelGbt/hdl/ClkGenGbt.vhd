-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : ClkGen.vhd
-- Author     : Martin Kocian  <kocian@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-10-23
-- Last update: 2016-01-25
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Clock module for Hsio Pixel Core.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;

library unisim;
use unisim.vcomponents.all;

entity ClkGenGbt is
   port (
      -- Clock, Resets
      ttc_clk_40         : in sl;
      mgtClk120          : out sl;
      sysClk40           : out sl;
      sysRst40           : out sl;
      sysClk80          : out sl;
      sysClk160          : out sl;
      sysClk40Unbuf      : out sl;
      sysClk160Unbuf     : out sl;
      sysClkLock         : out sl;
      sysClkRst          : in sl;

      sysClk100           : out sl;
      sysRst100           : out sl;
      axiClk40            : out sl;
      axiRst40            : out sl;

      -- GT Pins
      gtClkP              : in  sl;
      gtClkN              : in  sl;
      gtClk2P             : in  sl;
      gtClk2N             : in  sl);
end ClkGenGbt;

-- Define architecture
architecture mapping of ClkGenGbt is



   --Misc.
   signal stableClock,
      stableClock2,
      stableReset,
      stableReset2,
      sysClk,
      sysClk100int,
      locked100,
      gtClkout,
      gtClkout2,
      gtClkDiv2,
      sysClkRstint,
      axiClk40int,
      locked : sl;
   
begin

   sysClkLock<=locked;
   sysClk40<=sysClk;
   mgtClk120<=gtClkout2;

   -- Internal crystal 250 MHz
   IBUFDS_GTE2_Inst : IBUFDS_GTE2
      port map (
         I     => gtClkP,
         IB    => gtClkN,
         CEB   => '0',
         O     => gtClkout);

   -- GBT clock 120 MHz
   IBUFDS_GTE2_Inst2 : IBUFDS_GTE2
      port map (
         I     => gtClk2P,
         IB    => gtClk2N,
         CEB   => '0',
         O     => gtClkout2);

   -- Power Up Reset      
   PwrUpRst_Inst : entity work.PwrUpRst
      port map (
         clk    => gtClkout,
         rstOut => stableReset);

   SysClkGen_Inst : entity work.SysClkGen
     port map ( 
       
       -- Clock in ports
       --clk_in1 => stableClock2,
       clk_in1 => ttc_clk_40,
       -- Clock out ports  
       sysClk40 => sysClk,
       sysClk320 => sysClk80,
       sysClk160 => sysClk160,
       sysClk40Unbuf => sysClk40Unbuf,
       sysClk40Unbuf90 => open,
       sysClk160Unbuf => sysClk160Unbuf,
       sysClk160Unbuf90 => open,
       -- Status and control signals                
       reset => sysClkRst,
       locked => locked            
       );
   SysRst_Inst : entity work.RstSync
      generic map(
         IN_POLARITY_G  => '0',
         OUT_POLARITY_G => '1')
      port map(
         clk      => sysClk,
         asyncRst => locked,
         syncRst  => sysRst40);  

   SysClk100_inst: entity work.SysClk100
     port map(
       clk_in250 => gtClkout,
       clk_out100 => sysClk100int,
       clk_out160 => open,
       clk_out40 => axiClk40int,
       reset => stableReset,
       locked => locked100);
   sysClk100 <= sysClk100int;
   axiClk40 <= axiClk40int;
   SysRst100_Inst : entity work.RstSync
      generic map(
         IN_POLARITY_G  => '0',
         OUT_POLARITY_G => '1')
      port map(
         clk      => sysClk100int,
         asyncRst => locked100,
         syncRst  => sysRst100);  
   SysRstaxi40_Inst : entity work.RstSync
      generic map(
         IN_POLARITY_G  => '0',
         OUT_POLARITY_G => '1')
      port map(
         clk      => axiClk40int,
         asyncRst => locked100,
         syncRst  => axiRst40);  

end mapping;
