-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : ClkGen.vhd
-- Author     : Martin Kocian  <kocian@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-10-23
-- Last update: 2016-10-31
-- Platform   : Vivado 2014.3
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Clock module for Hsio Pixel Core.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;

library unisim;
use unisim.vcomponents.all;

entity ClkGen120 is
   port (
      -- Clock, Resets
      extRstL            : in  sl;
      stableClk125       : out sl;
      sysClk120          : out sl;
      sysClk40           : out sl;
      sysRst40           : out sl;
      sysClk80          : out sl;
      sysClk160          : out sl;
      sysClk40Unbuf      : out sl;
      sysClk40Unbuf90    : out sl;
      sysClk160Unbuf     : out sl;
      sysClk100          : out sl;
      sysRst100          : out sl;
      sysClkLock         : out sl;
      sysClkRst          : in sl;
      gtClk0P             : in  sl;
      gtClk0N             : in  sl;
      gtClk1P             : in  sl;
      gtClk1N             : in  sl);

end ClkGen120;

-- Define architecture
architecture mapping of ClkGen120 is



   --Misc.
   signal stableClock2,
      stableReset2,
      sysClk,
      sysClk100int,
      locked100,
      gtClkout,
      gtClkout2,
      gtClkDiv2,
      sysClkRstint,
      axiClk40int,
      locked,
      gbtRefClkb,
      clk120locked,
      gbtRefClk: sl;
   
begin

   sysClkLock<=locked;
   sysClk40<=sysClk;
   stableClk125 <= stableClock2;
   sysClk120 <= gbtRefClk;

   -- GT Reference Clock

   IBUFDS_GTE2_Inst2 : IBUFDS_GTE2
      port map (
         I     => gtClk1P,
         IB    => gtClk1N,
         CEB   => '0',
         ODIV2 => gtClkout2,
         O     => open);

   BUFG_Inst2 : BUFG
      port map (
         I => gtClkOut2,
         O => stableClock2);

   Clk120_Inst: entity work.clk250_120
     port map(
       clk_in1 => stableClock2,
       clk_out1 => gbtRefClk,
       locked => clk120locked,
       reset => stableReset2);

   IBUFDS_GTE2_Inst3 : IBUFDS_GTE2
      port map (
         I     => gtClk0P,
         IB    => gtClk0N,
         CEB   => '0',
         ODIV2 => gbtRefClkb,
         O     => open);

   --U_bufg: BUFG port map (I => gbtRefClkb, O => gbtRefClk);
   -- Power Up Reset      

   PwrUpRst_Inst2 : entity work.PwrUpRst
      generic map(
         IN_POLARITY_G => '0')
      port map (
         arst   => extRstL,
         clk    => stableClock2,
         rstOut => stableReset2);
   PwrUpRst_Inst3 : entity work.PwrUpRst
      generic map(
         IN_POLARITY_G => '0')
      port map (
         arst   => extRstL,
         clk    => sysClk,
         rstOut => sysRst40);

   SysClkGen_Inst : entity work.sysClkGen120
     port map ( 
       
       -- Clock in ports
       clk_in1 => gbtRefClk,
       -- Clock out ports  
       sysClk40 => sysClk,
       sysClk80 => sysClk80,
       sysClk160 => sysClk160,
       sysClk40Unbuf => sysClk40Unbuf,
       sysClk40Unbuf90 => sysClk40Unbuf90,
       sysClk160Unbuf => sysClk160Unbuf,
       sysClk100 => sysClk100,
       -- Status and control signals                
       reset => not clk120locked,
       locked => locked            
       );
   sysRst100<=not locked;

end mapping;
