-------------------------------------------------------------------------------
-- Title         : DTMtest
-- Project       : ATLAS pixel
-------------------------------------------------------------------------------
-- File          : DTMtest.vhd
-- Author        : Martin Kocian, kocian@slac.stanford.edu
-- Created       : 01/16/2015
-------------------------------------------------------------------------------
-- Description:
-- DTM firmware test
-------------------------------------------------------------------------------
-- Copyright (c) 2015 by Martin Kocian. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 01/16/2015: created.
-------------------------------------------------------------------------------

LIBRARY ieee;
Library Unisim;
use unisim.vcomponents.all;
USE ieee. std_logic_1164.ALL;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.I2cPkg.all;
USE work.ALL;


entity DTMtest is 
   port ( 

      -- PGP Crystal Clock Input, 156.25Mhz
      mgtRefClkP   : in   slv(1 downto 0);
      mgtRefClkM   : in   slv(1 downto 0);

      -- PGP Rx/Tx Lines
      mgtRxP       : in    slv(3 downto 0);
      mgtRxM       : in    slv(3 downto 0);
      mgtTxP       : out   slv(3 downto 0);
      mgtTxM       : out   slv(3 downto 0);

      -- LEDs
      led           : out slv(1 downto 0);

      -- I2C
      i2cScl: in sl;
      i2cSda: in sl;

      --DTM output clocks
      dpmClkP: out slv(2 downto 0);
      dpmClkM: out slv(2 downto 0);

      --DTM low speed lines
      idpmFbP: in slv(3 downto 0);
      idpmFbM: in slv(3 downto 0);
      odpmFbP: out slv(3 downto 0);
      odpmFbM: out slv(3 downto 0);

      --BP clk (to header)
      bpClkIn: in  slv(3 downto 0);
      bpClkOut:out slv(3 downto 0);
      -- IPMI (to header)
      iDtmToIpmiP: in sl;
      iDtmToIpmiM: in sl;
      oDtmToIpmiP: out sl;
      oDtmToIpmiM: out sl;

      -- TTC board
      idtmToRtmLsP: in slv(3 downto 0);
      idtmToRtmLsM: in slv(3 downto 0);
      odtmToRtmLsP: out slv(1 downto 0);
      odtmToRtmLsM: out slv(1 downto 0);

      --Refclk 1 sel
      clkSelA: out sl;
      clkSelB: out sl
   );
end DTMtest;


-- Define architecture for top level module
architecture DTMtest of DTMtest is 

   signal clkout      : slv(2 downto 0);
   signal counter     : slv(26 downto 0):=(others => '0');
   signal clk100      : sl;
   signal pgpClk      : sl;
   signal sysClk      : sl;
   signal sysRst      : sl;
   signal refclk      : slv(1 downto 0);
   signal fb          : slv(3 downto 0);
   signal fbin        : slv(3 downto 0);
   signal fbout       : slv(3 downto 0);
   signal txready     : slv(3 downto 0);
   signal rxready     : slv(3 downto 0);
   signal ipmiOut     : sl;
   signal ipmiIn      : sl;
   signal ttcin       : slv(3 downto 0);
   signal ttcout      : slv(1 downto 0);

   -- Register delay for simulation
   constant tpd:time := 0.5 ns;

begin

  --hardcode clock frequency to 250 MHz
  clkSelA<='1';
  clkSelB<='1';
  
   -- Clocks
  U_ClkGen: entity work.TestClkGen 
    port map(
      mgtRefClkP => mgtRefClkP,
      mgtRefClkM => mgtRefClkM,
      sys_clk_out => sysClk,
      sys_rst_out => sysRst,
      refclk_out => refclk,
      pgpClk_out => pgpClk,
      clk100_out => clk100);
  
  ipmiOut<=sysClk;
  --clkout(0)<=ipmiIn;
  clkout(0)<=pgpClk;
  clkout(1)<=clk100;
  clkout(2)<=ttcin(0);
  GEN_DPM_CLK:
  for I in 0 to 2 generate
    U_clkout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25")
      port map ( I  => clkout(I)   , O => dpmClkP(I) , OB => dpmClkM(I)  );
  end generate GEN_DPM_CLK;

  GEN_DPM_LS:
  for I in 0 to 3 generate
    U_ols_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25")
      port map ( I  => fbout(I)   , O => odpmFbP(I) , OB => odpmFbM(I)  );
    U_ils_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
      port map ( I  => idpmFbP(I)  , IB=>idpmFbM(I)   , O => fbin(I)   );
  end generate GEN_DPM_LS;
  --GEN_FB:
  --for I in 0 to 3 generate
  --  fbout(I)<=bpClkIn(I); 
  --  U_bp : OBUF port map (I => fbin(I), O => bpClkOut(I));
  --  --bpClkOut(I)<=fbin(I); 
  --end generate GEN_FB;
  fbout<=fbin;

  U_ipmiout  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25")
    port map ( I  => ipmiOut   , O => oDtmToIpmiP , OB => oDtmToIpmiM  );
  
  U_ipmiin : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
    port map ( I  => iDtmToIpmiP  , IB=> iDtmToIpmiM  , O => ipmiIn   );

  GEN_TTCOUT:
  for I in 0 to 1 generate
    U_ttcout  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25")
      port map ( I  => ttcout(I)   , O => odtmToRtmLsP(I) , OB => odtmToRtmLsM(I)  );
  end generate GEN_TTCOUT;
  GEN_TTCIN:
  for I in 0 to 3 generate
    U_ttcin : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
      port map ( I  => idtmToRtmLsP(I)  , IB=> idtmToRtmLsM(I)  , O => ttcin(I)   );
  end generate GEN_TTCIN;

  GEN_LED:
  for I in 0 to 1 generate
    U_bp : OBUF port map (I => counter(I+24), O => led(I));
  end generate GEN_LED;
    
   ttcout<=counter(26 downto 25);
   process(sysClk) begin
     if(rising_edge(sysClk))then
       counter<=unsigned(counter)+1;
     end if;
   end process; 
  pgpgen: for I in 0 to 3 generate
    pgpfe: entity work.PgpFrontEnd
      port map(
        clk => sysClk,
        rst => sysRst,
        refclk => refclk(1),
        pgpclk => pgpClk,
        txReady => txready(I),
        rxReady => rxready(I),
        -- GT Pins
        gtTxP => mgtTxP(I),
        gtTxN => mgtTxM(I),
        gtRxP => mgtRxP(I),
        gtRxN => mgtRxM(I)
      ); 
  end generate pgpgen;
end DTMtest;
