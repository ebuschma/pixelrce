-------------------------------------------------------------------------------
-- Title         : Version File
-- Project       : PGP To PCI-E Bridge Card, 8x
-------------------------------------------------------------------------------
-- File          : PgpCard8xG2Version.vhd
-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
-- Created       : 04/27/2010
-------------------------------------------------------------------------------
-- Description:
-- Version Constant Module.
-------------------------------------------------------------------------------
-- Copyright (c) 2010 by SLAC National Accelerator Laboratory. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 04/27/2010: created.
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

package Version is

constant FPGA_VERSION_C : std_logic_vector(31 downto 0) := x"00000011"; -- MAKE_VERSION


constant BUILD_STAMP_C : string := "HsioCosmic: Vivado v2014.4 (x86_64) Built Fri Sep 22 14:33:37 CEST 2017 by mkocian";



end Version;

-------------------------------------------------------------------------------
-- Revision History:
-- 04/28/2016  0x00000004: Complex deadtime
-- 04/28/2016  0x00000011: Framesize 100.
-------------------------------------------------------------------------------

