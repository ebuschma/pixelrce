-------------------------------------------------------------------------------
-- Title         : HsioFe
-- Project       : ATLAS 
-------------------------------------------------------------------------------
-- File          : HsioFe.vhd
-- Author        : Martin Kocian, kocian@slac.stanford.edu
-- Created       : 07/01/2016
-------------------------------------------------------------------------------
-- Description:
-- Route signals through to adapter
-------------------------------------------------------------------------------
-- Copyright (c) 2016 by Martin Kocian. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 07/21/2008: created.
-------------------------------------------------------------------------------

LIBRARY ieee;
Library Unisim;
use unisim.vcomponents.all;
USE ieee. std_logic_1164.ALL;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;
use work.StdRtlPkg.all;
use work.HD44780DriverPkg.all;
USE work.ALL;


entity HsioFe is 
   port ( 

      -- PGP Crystal Clock Input, 156.25Mhz
      iMgtRefClkP   : in    std_logic;
      iMgtRefClkM   : in    std_logic;
      iMgtRefClk3P  : in    std_logic;
      iMgtRefClk3M  : in    std_logic;

     -- ATLAS Pixel module pins
      dtmin_p   : in std_logic_vector(3 downto 0);
      dtmin_n   : in std_logic_vector(3 downto 0);
      serialout_p  : out std_logic_vector(3 downto 0);
      serialout_n  : out std_logic_vector(3 downto 0);
      dtmout_p  : out std_logic_vector(3 downto 0);
      dtmout_n  : out std_logic_vector(3 downto 0);
      serialin_p   : in std_logic_vector(3 downto 0);
      serialin_n   : in std_logic_vector(3 downto 0);
      clkin_p      : in sl;
      clkin_n      : in sl;
      clkA_p       : out std_logic;
      clkA_n       : out std_logic;
      clkB_p       : out std_logic;
      clkB_n       : out std_logic;

      -- Reset button
      iResetInL     : in    std_logic;
      -- Reload firmware
      iExtreload    : in std_logic;
      -- LEDs
      led           : out slv(7 downto 0):=x"aa";
      fpga_status_led : out sl;
      usr_led       : out sl;
      -- Display
      displayI2cScl : inout sl;
      displayI2cSda : inout sl;

      sw_dtmboot: in std_logic;

      -- TTC
      ttc_scl : inout std_logic;
      ttc_sda : inout std_logic;
      clockout: out std_logic;

      --DTM
      dtm_ps0_l: in std_logic;
      dtm_en_l:  out std_logic := '1';
      dtmI2cScl: inout std_logic;
      dtmI2cSda: inout std_logic
      
   );
end HsioFe;


-- Define architecture for top level module
architecture HsioFe of HsioFe is 

  procedure printText (displayBuffer : inout HD44780DriverDisplayBufferType;
                      at: in integer; newpos: out integer; txt: in string) is
  begin
    for i in 1 to txt'high loop
      displayBuffer(at+i-1)(7 downto 0):= std_logic_vector(conv_unsigned(character'pos(txt(i)), 8));
    end loop;
    newpos:=at+txt'high;
  end procedure printText;

  procedure placeNextDigit(displayBuffer : inout HD44780DriverDisplayBufferType;
                           at: in integer; newpos: out integer; txt: in std_logic_vector(7 downto 0)) is
  begin
    displayBuffer(at):=txt;
    newpos:=at+1;
  end procedure placeNextDigit;
  
   -- Local signals
   signal oReload        : std_logic;
   signal sysClk40      : std_logic;
   signal sysRst40      : std_logic;
   signal gtClkDiv2      : std_logic;
   signal stableClock      : std_logic;
   signal serialinb      : std_logic_vector(3 downto 0);
   signal serialoutb      : std_logic_vector(3 downto 0);
   signal clk      : std_logic;
   signal initclockbuf: std_logic;
   signal oldRst40: std_logic:='0';
  signal halfclock, quarterclock: std_logic:='0';
   -- I2C Text Display
   signal displayBuffer : HD44780DriverDisplayBufferType := (others => x"20");
   -- Register delay for simulation
   constant tpd:time := 0.5 ns;

begin
     IBUFDS_GTE2_Inst : IBUFDS_GTE2
      port map (
        I => iMgtRefClkP,
        IB => iMgtRefClkM,
        CEB   => '0',
        ODIV2 => gtClkDiv2);
     IBUFDS_GTE2_Inst3 : IBUFDS_GTE2
      port map (
        I => iMgtRefClk3P,
        IB => iMgtRefClk3M,
        CEB   => '0',
        O     => clockout);
   BUFG_Inst : BUFG
      port map (
         I => gtClkDiv2,
         O => stableClock);
   PwrUpRst_Inst : entity work.PwrUpRst
      port map (
         clk    => sysClk40,
         rstOut => sysRst40);
   --BUFR_0 : BUFR
   --   generic map (
   --      BUFR_DIVIDE => "4")
   --   port map (
   --      I   => stableClock,  -- 1-bit input: Clock buffer input driven by an IBUFG, MMCM or local interconnect
   --      CE  => '1',                    -- 1-bit input: Active high, clock enable input
   --      CLR => '0',
   --      O   => sysClk40);          
   process begin
     wait until rising_edge(stableClock);
     halfclock<=not halfclock;
   end process;
   process begin
     wait until rising_edge(halfclock);
     quarterclock<=not quarterclock;
   end process;
   U_bufg : BUFG
     port map(
       I => quarterclock,
       O => sysClk40);
   U_DTMstart: entity work.startDtm
     port map(
       clk => sysClk40,
       rst => sysRst40,
       done => open, --led(5),
       fail => open, --led(6),
       detectL => dtm_ps0_l,
       powerupL => dtm_en_l,
       sw_dtmbootL => sw_dtmboot,
       scl => dtmI2cScl,
       sda => dtmI2cSda);

   process 
   begin
     wait until rising_edge(sysClk40);
     oldRst40<=sysRst40;
     if(sysRst40='0' and oldRst40='1')then
       initclockbuf<='1';
     else
       initclockbuf<='0';
     end if;
   end process;
   ttcclockbuf: entity work.clockbuf
    generic map( PRESCALE_G => 400,
                 CONFIG_G => "XTL120_120_120")
    port map(
      clk => sysClk40,
      trig => initclockbuf,
      fail => open,
      done => open,
      scl => ttc_scl,
      sda => ttc_sda
    );

   U_clkin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                         port map ( I  => clkin_p  , IB=>clkin_n   , O => clk   );

   U_clkA     : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => clk, O => clkA_p , OB => clkA_n  );
   U_clkB       : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => clk   , O => clkB_p , OB => clkB_n  );

    SERIAL_IO_DATA: for I in 0 to 3 generate
      U_serialout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                                 port map ( I  => serialoutb(I)   , O => serialout_p(I) , OB => serialout_n(I)  );
      U_dtmout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                                 port map ( I  => serialinb(I)   , O => dtmout_p(I) , OB => dtmout_n(I)  );
      U_serialin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                              port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialinb(I)   );
      U_dtmin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                              port map ( I  => dtmin_p(I)  , IB=>dtmin_n(I)   , O => serialoutb(I)   );
    end generate SERIAL_IO_DATA;
   oReload <= not iExtreload or not iResetInL;       -- active low

   reload_inst: entity work.Iprog7Series
     port map(
       clk => sysClk40,
       rst => sysRst40,
       start => oReload);
  
  I2cHD44780Display_inst: entity work.I2cHD44780Display
  port map
  (
    clk => sysClk40,
    rst => '0',
    displayI2cSda => displayI2cSda,
    displayI2cScl => displayI2cScl,
    dISPLayBuffer => displayBuffer
  );

  process
   variable displayBufferVar : HD44780DriverDisplayBufferType := (others => x"20");
   variable textpos: integer :=0;
   begin
    displayBufferVar:=(others => x"20");
    printText(displayBufferVar, 0, textpos, "Routing FE data. ");
    
    --displayBufferVar(21):= dispDigitB;
    --displayBufferVar(22):= dispDigitC;
    --displayBufferVar(23):= dispDigitD;
    --displayBufferVar(24):= dispDigitE;
    --displayBufferVar(25):= dispDigitF;
    --displayBufferVar(26):= dispDigitG;

    displayBuffer<=displayBufferVar;
   end process;

end HsioFe;
