# StdLib
set_property ASYNC_REG true [get_cells -hierarchical *crossDomainSyncReg_reg*]

# FPGA Port Definition
set_property PACKAGE_PIN W24 [get_ports iResetInL]
set_property IOSTANDARD LVCMOS25 [get_ports iResetInL]

# FPGA Hardware Configuration
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 2.5 [current_design]

### HSIO User I/O ###

#312.5 MHz clk 1 on Bank 116 (West)
set_property PACKAGE_PIN H14 [get_ports iMgtRefClkP]
set_property PACKAGE_PIN G14 [get_ports iMgtRefClkM]
set_property PACKAGE_PIN H20 [get_ports iMgtRefClk3P]
set_property PACKAGE_PIN G20 [get_ports iMgtRefClk3M]

# Green HSIO LEDs
set_property PACKAGE_PIN AM9  [get_ports {led[0]}]
set_property PACKAGE_PIN AM11 [get_ports {led[1]}]
set_property PACKAGE_PIN AN11 [get_ports {led[2]}]
set_property PACKAGE_PIN AJ11 [get_ports {led[3]}]
set_property PACKAGE_PIN AK11 [get_ports {led[4]}]
set_property PACKAGE_PIN AL10 [get_ports {led[5]}]
set_property PACKAGE_PIN AM10 [get_ports {led[6]}]
set_property PACKAGE_PIN AL8  [get_ports {led[7]}]

set_property IOSTANDARD LVCMOS25 [get_ports {led[*]}]

set_property PACKAGE_PIN AA32  [get_ports fpga_status_led]
set_property IOSTANDARD LVCMOS25  [get_ports fpga_status_led]
set_property PACKAGE_PIN AB26  [get_ports usr_led]
set_property IOSTANDARD LVCMOS25  [get_ports usr_led]

# Display I2C
set_property PACKAGE_PIN M26 [get_ports {displayI2cScl}]
set_property PACKAGE_PIN L34 [get_ports {displayI2cSda}]

set_property IOSTANDARD LVCMOS33 [get_ports {displayI2cScl}]
set_property IOSTANDARD LVCMOS33 [get_ports {displayI2cSda}]

# DTM
set_property PACKAGE_PIN R31 [get_ports {dtm_ps0_l}]
set_property IOSTANDARD LVCMOS25 [get_ports {dtm_ps0_l}]
set_property PULLUP TRUE [get_ports {dtm_ps0_l}]  
set_property PACKAGE_PIN P31 [get_ports {dtm_en_l}]
set_property IOSTANDARD LVCMOS25 [get_ports {dtm_en_l}]
set_property PACKAGE_PIN L29 [get_ports {dtmI2cScl}]
set_property PACKAGE_PIN L30 [get_ports {dtmI2cSda}]
set_property IOSTANDARD LVCMOS33 [get_ports {dtmI2cScl}]
set_property IOSTANDARD LVCMOS33 [get_ports {dtmI2cSda}]


set_property PACKAGE_PIN G34  [get_ports {sw_dtmboot}]
set_property IOSTANDARD LVCMOS33 [get_ports {sw_dtmboot}]

set_property PACKAGE_PIN Y31 [get_ports iExtreload]
set_property IOSTANDARD LVCMOS25 [get_ports iExtreload]

set_property PACKAGE_PIN AM31   [get_ports ttc_scl]
set_property PACKAGE_PIN AN32   [get_ports ttc_sda]
set_property IOSTANDARD LVCMOS33   [get_ports ttc_scl]
set_property IOSTANDARD LVCMOS33   [get_ports ttc_sda]
set_property PACKAGE_PIN AJ29 [get_ports {clockout}]
set_property IOSTANDARD LVTTL [get_ports {clockout}]
set_property SLEW FAST [get_ports {clockout}]
### Timing Constraints ###
# MGT Clocks
create_clock -period 3.200 -name mgtClkP [get_ports iMgtRefClkP]

#input clock
set_property PACKAGE_PIN P30 [get_ports clkin_n]
set_property IOSTANDARD LVDS_25 [get_ports clkin_p]
set_property IOSTANDARD LVDS_25 [get_ports clkin_n]

#output clocks
set_property PACKAGE_PIN L3 [get_ports clkA_n]
set_property PACKAGE_PIN G2 [get_ports clkB_n]

set_property IOSTANDARD LVDS_25 [get_ports clkA_p]
set_property IOSTANDARD LVDS_25 [get_ports clkA_n]
set_property IOSTANDARD LVDS_25 [get_ports clkB_p]
set_property IOSTANDARD LVDS_25 [get_ports clkB_n]

#Input from DTM
set_property PACKAGE_PIN P24  [get_ports dtmin_p[3]]
set_property PACKAGE_PIN N24  [get_ports dtmin_n[3]]
set_property PACKAGE_PIN U25  [get_ports dtmin_p[2]]
set_property PACKAGE_PIN T25  [get_ports dtmin_n[2]]
set_property PACKAGE_PIN N26  [get_ports dtmin_p[1]]
set_property PACKAGE_PIN M27  [get_ports dtmin_n[1]]
set_property PACKAGE_PIN R26  [get_ports dtmin_p[0]]
set_property PACKAGE_PIN P26  [get_ports dtmin_n[0]]
set_property PACKAGE_PIN N27  [get_ports dtmout_p[3]]
set_property PACKAGE_PIN N28  [get_ports dtmout_n[3]]
set_property PACKAGE_PIN T27 [get_ports dtmout_p[2]]
set_property PACKAGE_PIN R27 [get_ports dtmout_n[2]]
set_property PACKAGE_PIN R25  [get_ports dtmout_p[1]]
set_property PACKAGE_PIN P25 [get_ports dtmout_n[1]]
set_property PACKAGE_PIN U26 [get_ports dtmout_p[0]]
set_property PACKAGE_PIN U27 [get_ports dtmout_n[0]]
set_property IOSTANDARD LVDS_25         [get_ports dtmin_p]
set_property IOSTANDARD LVDS_25         [get_ports dtmin_n]
set_property IOSTANDARD LVDS_25         [get_ports dtmout_p]
set_property IOSTANDARD LVDS_25         [get_ports dtmout_n]
#Output to Frontends A
set_property PACKAGE_PIN AC6 [get_ports {serialout_n[0]}]
set_property PACKAGE_PIN AA7 [get_ports {serialout_n[1]}]
set_property PACKAGE_PIN AC3 [get_ports {serialout_n[2]}]
set_property PACKAGE_PIN AC1 [get_ports {serialout_n[3]}]

set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialout_n[0]}]

#Input from Frontends A
set_property PACKAGE_PIN W4 [get_ports {serialin_n[0]}]
set_property PACKAGE_PIN W3 [get_ports {serialin_n[1]}]
set_property PACKAGE_PIN Y2 [get_ports {serialin_n[2]}]
set_property PACKAGE_PIN V1 [get_ports {serialin_n[3]}]

set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_p[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {serialin_n[0]}]

