/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* XILINX CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "platform.h"
#include "xuartlite_l.h"
#include "xparameters.h"

char name[128];
void print(char *str);
#define printf xil_printf

#define IOBUS_BASE_DISPLAY 0xC0000000
#define IOBUS_BASE_DIP_SWITCHES 0xC0010000
#define IOBUS_BASE_HEADERS 0xC0020000
#define IOBUS_BASE_ZONE3 0xC0030000
#define IOBUS_BASE_LEMO 0xC0040000
#define IOBUS_BASE_CLOCKS 0xC0050000
#define IOBUS_BASE_TIMER 0xC0060000
#define IOBUS_BASE_DTM 0xC0070000
#define IOBUS_BASE_DTMLS 0xC0080000
#define IOBUS_BASE_TEMP 0xC0090000
#define IOBUS_BASE_TTC 0xC00A0000
#define IOBUS_BASE_ADCLK 0xC00B0000
#define IOBUS_BASE_MGT 0xC1000000
#define IOBUS_BASE_MGT_READBACK 0xC1100010
#define IOBUS_BASE_MGT_2 0xC1100000

void display_write(uint8_t line, uint8_t row, char v)
{
    uint32_t vv = v;

    if (row > 19) return;
    if (line > 1) return;

    volatile uint32_t * addr = (volatile uint32_t *) ((IOBUS_BASE_DISPLAY) + row * 4 + (line * 80));
    //xil_printf("Writing %c to 0x%x\n\r\r", v, addr);
    *addr = vv;
}

void display_puts(uint8_t line, uint8_t row, char *v)
{
    while (*v)
    {
        display_write(line, row, *v);
	row++;
	if (row == 20)
	{
	    line = (line + 1) % 2;
	    row -= 20;
	}
	v++;
    }
}
int inputHexNumber(){
  int retval=-1;
  char retstring[128];
  int pos=-1;
  do{
    retstring[++pos]=XUartLite_RecvByte(STDIN_BASEADDRESS);
    xil_printf("%c", retstring[pos]);
  }while(retstring[pos]!='\r');
  retstring[pos]=0;
  retval=strtol(retstring, 0, 16);
  return retval;
}

int inputDigit(){
  int retval;
  int stopasking=0;
  do{
    xil_printf("Input single-digit number or x to exit. ");
    uint8_t input=XUartLite_RecvByte(STDIN_BASEADDRESS);
    xil_printf("%c\n\r", input); 
    if(input=='x' || (input>='0' && input<='9'))stopasking=1;
    retval=input-48;
    if(retval==72)retval=255; //x
  } while(stopasking==0);
  return retval;
}
int askForConfirmation(const char* text){
  int retry=0;
  int stopasking;
  do{
    stopasking=1;
    xil_printf("%s",text);
    uint8_t input=XUartLite_RecvByte(STDIN_BASEADDRESS);
    xil_printf("%c\n\r", input); 
    if(input=='y' || input=='Y')retry=1;
    else if(input!='n' && input!='N')stopasking=0;
  } while(stopasking==0);
  return retry;
}
int chooseBoard(){
  int retry=0;
  int stopasking;
  do{
    stopasking=1;
    uint8_t input=XUartLite_RecvByte(STDIN_BASEADDRESS);
    xil_printf("%c\n\r", input); 
    if(input=='A' || input=='a')retry=1;
    else if(input!='H' && input!='h')stopasking=0;
  } while(stopasking==0);
  return retry;
}
void inputName(char* name){
  while(1){
    int index=0;
    xil_printf("Input board ID: ");
    while(1){
      uint8_t input=XUartLite_RecvByte(STDIN_BASEADDRESS);
      xil_printf("%c", input); 
      if(input=='\r' || input =='\n')break; 
      if((input==8 || input==127) && index>0){
	xil_printf("\b \b");
	index--;
      } else{
	name[index++]=input;
      } 
    }
    name[index]=0;
    xil_printf("\n\rBoard ID: %s\n\r", name);
    if(askForConfirmation("Is this correct (y/n)? "))break;
  } 
}

void usleep(uint32_t usec){
  if(usec>17000000){
    xil_printf("Timer works only up to 17 seconds\n\r");
    return;
  }
  volatile uint32_t* resultp=(uint32_t*)(IOBUS_BASE_TIMER);
  uint32_t start=*resultp;
  while(1){
    uint32_t stop=*resultp;
    if(stop<start)stop|=0x80000000;
    if(stop-start>=usec*125)break;
  }
}
 
int debug_MGTs(){
  int retval=0;
  volatile uint32_t* resultp;
  while(1){
    xil_printf("\n\r(t)rigger, (r)ead reg, (p)gp reset, (w)rite reg, (s)ys reset, (b)ert status. ");
    uint8_t input=XUartLite_RecvByte(STDIN_BASEADDRESS);
    switch(input){
    case 't': 
      resultp=(uint32_t*)(IOBUS_BASE_MGT+0x100004);
      *resultp=1;
      *resultp=0;
      break;
    case 'p':
      resultp=(uint32_t*)(IOBUS_BASE_MGT+0x10000c);
      *resultp=1;
      *resultp=0;
      break;
    case 's':
      resultp=(uint32_t*)(IOBUS_BASE_MGT+0x100008);
      *resultp=1;
      *resultp=0;
      break;
    case 'w':
      xil_printf("\n\rEnter Register Number ");
      int regnum=inputHexNumber();
      resultp=(uint32_t*)(IOBUS_BASE_MGT+regnum);
      xil_printf("\n\rEnter Register Value ");
      int regval =inputHexNumber();
      xil_printf("\n\r");
      *resultp=regval;
      break;
    case 'r':
      xil_printf("\n\rEnter Register Number ");
      int regnm=inputHexNumber();
      resultp=(uint32_t*)(IOBUS_BASE_MGT+regnm);
      uint32_t result=*resultp;
      xil_printf("\n\rRegister value: %x\r\n", result);
      break;
    case 'b':
      resultp=(uint32_t*)(IOBUS_BASE_MGT);
      xil_printf("\n\n\rBERT status:\n\r");
      xil_printf("============\n\r");
      xil_printf("QPLL Lock: \t\t%x\n\r", resultp[0]);
      xil_printf("QPLL Lock Lost: \t%x\n\r", resultp[1]);
      resultp=(uint32_t*)(IOBUS_BASE_MGT_READBACK);
      uint32_t status=*resultp;
      xil_printf("Rx Busy: \t\t%d\n\r", status&0x1);
      xil_printf("Tx Busy: \t\t%d\n\r", (status>>1)&0x1);
      xil_printf("Updated Results: \t%d\n\r", (status>>2)&0x1);
      xil_printf("ErrMissedPacket: \t%d\n\r", (status>>3)&0x1);
      xil_printf("ErrLength: \t\t%d\n\r", (status>>4)&0x1);
      xil_printf("ErrDataBus: \t\t%d\n\r", (status>>5)&0x1);
      xil_printf("ErrEofe: \t\t%d\n\r", (status>>6)&0x1);
      xil_printf("ErrWordCnt: \t\t%d\n\r", resultp[1]);
      xil_printf("ErrBitCnt: \t\t%d\n\r", resultp[2]);
      xil_printf("PacketRate: \t\t%d\n\r", resultp[3]);
      xil_printf("PacketLength: \t\t%d\n\n\n\r", resultp[4]);
      break;
    }
  }
  return retval;
}

int tindex;
int mgtindex[]={0, 1, 2, 4, 5, 6, 7, 12, 13, 14, 15, 8, 9};
int check_MGT(){
  int retval=0;
  const char* mgtname[]={"SMA", "SFP 0", "SFP 1", "N/C", "QSFP channel 0", "QSFP channel 1", "QSFP channel 2", "QSFP channel 3", "DTM 0", "DTM 1", "N/C", "N/C", "RTM 0", "RTM 1", "RTM 2", "RTM 3"};
  volatile uint32_t* resultp=(uint32_t*)(IOBUS_BASE_MGT);
  xil_printf("Now testing %s.\n\r", mgtname[mgtindex[tindex]]);
  if(*resultp!=0xff){
    xil_printf("PLLs are not locked (%x). Trying to reset... ", *resultp);
    resultp[2]=0xff;
    usleep(1000);
    resultp[2]=0;
    usleep(10000);
    if(*resultp!=0xff){
      xil_printf("BAD.\n\r");
      return 1;
    }
    xil_printf("OK. No errors.\n\r");
  }
  resultp=(uint32_t*)(IOBUS_BASE_MGT_2+(mgtindex[tindex]<<8));
  resultp[9]=0; // power up MGT
  usleep(10000); 
  uint32_t pl=450000000;// 32 bit words.
  resultp[0]=8; 
  resultp[2]=1; //reset. 
  usleep(1000);
  resultp[2]=0; //clear reset
  usleep(1000);
  resultp[0]=pl; 
  int gbits=pl/1000000*32/1000;
  int hmbits=pl/1000000*32/100-gbits*10;
  xil_printf("Sending %d.%d Gbits", gbits, hmbits);
  resultp[2]=1; //reset. 
  usleep(1000);
  resultp[2]=0; //clear reset
  int w;
  int updatedResults;
  for(w=1;w<10;w++){
    usleep(500000);
    updatedResults=(resultp[4]>>2)&0x1;
    if(updatedResults!=0)break;
    xil_printf(".");
  }
  int rxbusy=(resultp[4]>>0)&0x1;
  int txbusy=(resultp[4]>>1)&0x1;
  int errMissedPacket=(resultp[4]>>3)&0x1;
  int errLength=(resultp[4]>>4)&0x1;
  int errDataBus=(resultp[4]>>5)&0x1;
  int errEofe=(resultp[4]>>6)&0x1;
  uint32_t errWordCnt=resultp[5];
  uint32_t errBitCnt=resultp[6];
  uint32_t packetLength=resultp[8];
  if(rxbusy==0 && txbusy==0 && updatedResults==1 && errMissedPacket==0 && errLength==0 
     && errDataBus==0 && errEofe==0 && errWordCnt==0 && errBitCnt==0 && packetLength==pl){
    xil_printf(" OK.\n\r");
  }else{
    xil_printf(" BAD.\n\r");
    xil_printf("Rx Busy: \t\t%d\n\r", rxbusy);
    xil_printf("Tx Busy: \t\t%d\n\r", txbusy);
    xil_printf("Updated Results: \t%d\n\r", updatedResults);
    xil_printf("ErrMissedPacket: \t%d\n\r", errMissedPacket);
    xil_printf("ErrLength: \t\t%d\n\r", errLength);
    xil_printf("ErrDataBus: \t\t%d\n\r", errDataBus);
    xil_printf("ErrEofe: \t\t%d\n\r", errEofe);
    xil_printf("ErrWordCnt: \t\t%d\n\r", errWordCnt);
    xil_printf("ErrBitCnt: \t\t%d\n\r", errBitCnt);
    xil_printf("PacketLength: \t\t%d (expected %d)\n\n\n\r", packetLength, pl);
    retval=1;
  }
  resultp[9]=0xf; // power down MGT
  return retval;
}
    
  
  
  
int check_clk(){
  int i;
  int retval=0;
  volatile uint32_t* resultp=(uint32_t*)IOBUS_BASE_ADCLK;
  xil_printf("Testing clock lines...\n\r");
  if(resultp[0]==0){
      xil_printf("OK.\n\r");
  }else{
    xil_printf("Problems on the following connectors:\n\r");
    for(i=0;i<19;i++){
      if(resultp[0]&(1<<i)){
	if(i<9)xil_printf("A");
	else xil_printf("B");
	xil_printf("%d ", i%9);
      }
    }
    xil_printf("\n\r");
    retval=1;
  }
  return retval;
}
    
  

int check_headers(){
  int i, j;
  int retval=0;
  volatile uint32_t* resultp=(uint32_t*)IOBUS_BASE_HEADERS;
  xil_printf("Testing General Purpose Headers...\n\r");
  for(i=0;i<4;i+=2){
    if(i==0)xil_printf("Header P2: ");
    else xil_printf("Header P3: ");
    if(resultp[i]==0 && resultp[i+1]==0){
      xil_printf("OK.\n\r");
    }else{
      xil_printf("Problems on the following pins:\n\r");
      for(j=0;j<40;j++)if(resultp[i+j/32]&(1<<(j%32)))xil_printf("%d ", j);
      xil_printf("\n\r");
      retval=1;
    }
  }
  return retval;
}

int check_zone3(){
  int i,j,k;
  int retval=0;
  char labels[]={"ABCDEFGH"};
  volatile uint32_t* resultp=(uint32_t*)IOBUS_BASE_ZONE3;
  xil_printf("Testing Zone 3 Connector...\n\r");
  for(i=0;i<9;i+=3){
    xil_printf("J%d: ", 25+i/3);
    uint32_t bad=0;
    for(j=0;j<3;j++)bad|=resultp[i+j];
    if(bad==0){
      xil_printf("OK.\n\r");
      }else{
      retval=1;
      xil_printf("Problems on the following pins: \n\r");
      for(j=0;j<3;j++){
	for(k=0;k<32;k++){
	  if(resultp[i+j]&(1<<k))xil_printf("%c%d ", labels[k%8], (j*32+k)/8+1);
	}
      }
      xil_printf("\n\r");
    }
  }
  return retval;
}

void printSwitches(uint32_t sw){
  int i;
  xil_printf("Problems with switch(es) ");
  for(i=0;i<8;i++) if(sw^(1<<i))xil_printf("%d ",8-i);
  xil_printf("counting from the top.\n\r");
}
int check_pushswitches(){
  int retval=0;
  char* name[]={"Soft_reset", "DTM_reboot"};
  int i,j;
  for(i=0;i<2;i++){
    xil_printf("Push the button labeled %s: ", name[i]);
    int pushed=0;
    for(j=0;j<100;j++){
      unsigned int dipsw = *((volatile uint32_t *) IOBUS_BASE_DIP_SWITCHES);
      if(!(dipsw&(1<<(8+i)))){
	pushed=1;
        break;
      }
      usleep(100000);
    }
    if(pushed){
      xil_printf("OK.\r\n");
    }else{
      xil_printf("BAD.\r\n");
      retval=1;
    }
  }
  return retval;
}

int check_dipswitches(){
  int retval=0;
  int i;
  int patterns[]={0x55, 0xaa};
  for (i=0;i<2;i++){
    unsigned int dipsw = *((volatile uint32_t *) IOBUS_BASE_DIP_SWITCHES);
    xil_printf("Result: ");
    if((dipsw&0xff)!=patterns[i]){
      printSwitches(dipsw);
      retval=1;
      break;
    }else xil_printf("OK\n\r");
    if(i==0){
      xil_printf("Now do the opposite: Top switch right, 2nd left, 3rd right, and so on. Press any key when done\n\r");
      XUartLite_RecvByte(STDIN_BASEADDRESS);
    }
  }
  return retval;
}

int lindex;
int check_one_lemo(){
    unsigned int lemos = *((volatile uint32_t *) IOBUS_BASE_LEMO);
    if(lemos!=(1<<lindex)){
      xil_printf("Test for LEMO connector %d failed. ", lindex+1);
      return 1;
    }else{
      if(lindex!=6){
	xil_printf ("LEMO #%d OK. \n\rNow connect the bottom LEMO (8) to number %d from the top. When done press any key.\n\r", lindex+1, lindex+2);
	XUartLite_RecvByte(STDIN_BASEADDRESS);
      }
    }
    return 0;
}

int test_clocks(){
  int retval=0;
  int i;
  char* clockname[]={"CLK 1 on bank 113", "CLK 0 on bank 116", "CLK 1 on Bank 116", "CLK 1 on Bank 216", "CLK 1 on Bank 213"};
  int clockindex[]={0,1,2,6,7};
  volatile uint32_t* clock=(uint32_t*)IOBUS_BASE_CLOCKS;
  for(i=0;i<5;i++){
    uint32_t freq=clock[clockindex[i]]&0x7fffffff;
    uint32_t locked=clock[clockindex[i]]>>31;
    uint32_t freqmhz=freq/1e6;
    uint32_t freqkhz=freq/1000-freqmhz*1000;
    xil_printf("The frequency of %s is %d.%03d MHz.", clockname[i], freqmhz, freqkhz);
    if(locked)xil_printf(" - OK.\n\r");
    else {
      xil_printf(" - BAD.\n\r");
      if(i!=1) retval|=!askForConfirmation("If this crystal was not loaded on purpose press 'y' else press 'n': ");
      else retval|=1;
    }
  }
  xil_printf("\n\n Result: ");
  if(retval==0)xil_printf("OK\n\r");
  else xil_printf("Failed\n\r");
  return retval;
}
int test_dtm_clocks(){
  int retval=0;
  int i;
  char* clockname[]={"DTM CLK0", "DTM CLK1", "DTM CLK2"};
  volatile uint32_t* clock=(uint32_t*)IOBUS_BASE_CLOCKS;
  for(i=3;i<6;i++){
    uint32_t freq=clock[i]&0x7fffffff;
    uint32_t locked=clock[i]>>31;
    uint32_t freqmhz=freq/1e6;
    uint32_t freqkhz=freq/1000-freqmhz*1000;
    xil_printf("The frequency of %s is %d.%03d MHz.", clockname[i-3], freqmhz, freqkhz);
    if(locked)xil_printf(" - OK.\n\r");
    else {
      xil_printf(" - BAD.\n\r");
      retval|=1;
    }
  }
  xil_printf("\n\n Result: ");
  if(retval==0)xil_printf("OK\n\r");
  else xil_printf("Failed\n\r");
  return retval;
}
int test_dtm_ls(){
  const char* adchans[]={"A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "TLU(0)", "TLU(1)"};
  int retval=0;
  int i;
  volatile uint32_t* dtmls=(uint32_t*)IOBUS_BASE_DTMLS;
  dtmls[0]=0xfffff; //reset
  usleep(1000);
  dtmls[0]=0;
  unsigned nwords=15000000;
  unsigned Mbits=nwords*32/1000000;
  dtmls[2]=nwords; 
  xil_printf("Sending %d Mbits.\n\r", Mbits); 
  dtmls[1]=0xfffff; //trigger
  usleep(10);
  dtmls[1]=0;
  unsigned pause=(Mbits/125+1)*1000000;
  usleep(pause);
  if(dtmls[4]==0){
    xil_printf("OK.\n\r");
  }else{
    for(i=0;i<20;i++){
      if((dtmls[4]&(1<<i))!=0){
	if((dtmls[5]&(1<<i))!=0){
	  xil_printf("Channel %s failed.\n\r", adchans[i]);
	  retval|=1;
	}
      }else{
	xil_printf("Channel %s not receiving data. FAILED.\n\r", adchans[i]);
	retval|=1;
      }
    }
  }
  return retval;
}
int detect_dtm(){
  unsigned int dtm = *((volatile uint32_t *) IOBUS_BASE_DTM);
  return !(dtm&0x1);
}
int power_up_dtm(){
  volatile uint32_t* dtm=(uint32_t*)(IOBUS_BASE_DTM);
  dtm[0]=0;
  usleep(500000);
  return 0;
}
int read_dtm_power(){
  volatile uint32_t* dtm=(uint32_t*)(IOBUS_BASE_DTM);
  dtm[1]=1;
  usleep(100000);
  dtm[1]=0;
  uint32_t power=dtm[1];
  return power;
}
  
int init_dtm(){
  volatile uint32_t* dtm=(uint32_t*)(IOBUS_BASE_DTM);
  dtm[1]=3;
  usleep(100000);
  dtm[1]=0;
  return 0;
}
int power_MGTs(int num){
  volatile uint32_t* resultp;
  if(num>11){
    xil_printf("Only up to 9 MGTs can be powered up\r\n");
    return 1;
  }
  int l;
  for (l=0;l<11;l++){
    resultp=(uint32_t*)(IOBUS_BASE_MGT_2+(mgtindex[l]<<8));
    resultp[9]=0xf; // power down MGT
  }
  resultp=(uint32_t*)(IOBUS_BASE_MGT);
  if(*resultp!=0xff){
    xil_printf("PLLs are not locked (%x). Trying to reset... ", *resultp);
    resultp[2]=0xff;
    usleep(1000);
    resultp[2]=0;
    usleep(10000);
    if(*resultp!=0xff){
      xil_printf("BAD.\n\r");
      return 1;
    }
    xil_printf("OK. No errors.\n\r");
  }
  for (l=0;l<num;l++){
    resultp=(uint32_t*)(IOBUS_BASE_MGT_2+(mgtindex[l]<<8));
    resultp[9]=0; // power up MGT
  }
  return 0;
}

int temperature_test(){
  volatile uint32_t* temp=(uint32_t*)(IOBUS_BASE_TEMP);
  int retval=0;
  int input=0;
  int init=1;
  xil_printf("\nTemperature Test:\n\r");
  xil_printf("=================\n\r");
  while(1){
    int l;
    for(l=0;l<30;l++){
      if(init==1 && l==5){
	init=0;
	break;
      }
      int valid=temp[0]&0x80000000;
      if(!valid)xil_printf("Temperature not valid\n\r");
      else{
	unsigned adc=((temp[0]&0x7fffffff)>>4)*10;
	unsigned tc=adc*5040/40960-2732; //Celsius
	unsigned tf=adc*9072/40960-4597; //Fahrenheit
	unsigned degc=tc/10;
	unsigned decc=tc-degc*10;
	unsigned degf=tf/10;
	unsigned decf=tf-degf*10;
	xil_printf("Temperature: %d.%d C (%d.%d F)\n\r", degc, decc, degf, decf);
	if(degc>70){
	  xil_printf("Overheating. Turning off MGTs.\n\r");
	  retval=1;
	  power_MGTs(0);
	}
	usleep(1000000);
      }
    }
    xil_printf("How many MGTs do you want to power up?\n\r");
    input=inputDigit();
    if(input==255)break;
    xil_printf("Powering up %d MGTs.\n\r", input);
    power_MGTs(input);
  }
  return retval;
}
int retry(int (*func)()){
  int retry;
  int retval;
  do{
    retry=0;
    retval=func();
    if(retval!=0)retry=askForConfirmation("Retry (y/n)? ");
  }while(retry==1);
  return retval;
}
int testnr;
void writeTestHeader(const char* title, const char* instr){
  char titlestring[128];
  int i;
  sprintf(titlestring, "%d. %s:", ++testnr, title);
  xil_printf("\n\n\n%s\n\r", titlestring);
  for(i=0;i<strlen(titlestring);i++)xil_printf("=");
  xil_printf("\n\r%s. When ready press any key.\n\r", instr);
  XUartLite_RecvByte(STDIN_BASEADDRESS);
}
void print_result(const char* text, int result){
  char spaces[64];
  int i;
  for(i=0;i<25-strlen(text);i++)spaces[i]='.';
  spaces[i]=0;
  xil_printf("   %s:%s", text, spaces);
  if(result==0)xil_printf("OK\n\r");
  else xil_printf("BAD\n\r");
}

int TTC_test(){
  volatile uint32_t* ttc=(uint32_t*)(IOBUS_BASE_TTC);
  //reset counter
  ttc[0]=1;
  usleep(1000);
  ttc[0]=0;
  writeTestHeader("TTC Test", "Counting triggers");
  unsigned int ntrig=ttc[0];
  xil_printf("Found %d triggers.\n\r", ntrig);
  return 1;
}
int hsio_test_procedure(){
  // Print header
  int i;
  xil_printf("\r");
  for(i=0;i<10;i++)xil_printf("\n");
  xil_printf("                              ================\n\r");
  xil_printf("                              | HSIO II TEST |\n\r");
  xil_printf("                              ================\n\n\n\n\r");
  
  display_puts(0, 0, "       HSIO II         Functional Test");

  //Run tests
  testnr=0;
  int testresult=0;
  int retval;
  int ledresult=0;
  int clockresult=0;
  int displayresult=0;
  int dipswitchresult=0;
  int pushswitchresult=0;
  int lemoresult=0;
  int headerresult=0;
  int zone3result=0;
  int mgtresult=0;
  //LEDs
  writeTestHeader("LED Test", "Look at the LED flashing pattern");
  ledresult=!askForConfirmation("Are all LEDs flashing in a slow, regular pattern? (y/n) ");
  testresult+=ledresult;
  //Clocks
  writeTestHeader("Clock Test", "Clock frequency measurement. Assuming CLK 0 on bank 113 is loaded with a 250 Mhz oscillator");
  clockresult=test_clocks();
  testresult+=clockresult;
  //retry(TTC_test);
  //Display
  writeTestHeader("Display Test", "Look at the LCD Display");
  displayresult=!askForConfirmation("Does the display say \"HSIO II Functional Test\" (y/n)? ");
  testresult+=displayresult;
  //Push Switches
  writeTestHeader("Push Switch Test", "");
  pushswitchresult=retry(check_pushswitches);
  testresult+=pushswitchresult;
  //DIP Switches
  writeTestHeader("DIP Switch Test", "Set top switch left, the 2nd one right, the 3rd left again, and so on");
  dipswitchresult=retry(check_dipswitches);
  testresult+=dipswitchresult;
  //LEMO connectors
  writeTestHeader("LEMO Connector Test", "Connect the bottommost LEMO connector (8) to the topmost (1) one using a cable");
  for(lindex=0;lindex<7;lindex++){
    retval=retry(check_one_lemo);
    if(retval!=0)break;
  }
  if(retval==0)xil_printf("Result: OK\n\r");
  else xil_printf("Result: LEMO connector test failed.\n\r");
  lemoresult=retval;
  testresult+=lemoresult;
  //GPIO Headers
  writeTestHeader("GPIO Header Test", "Connect header test boards");
  headerresult=retry(check_headers);
  testresult+=headerresult;
  //Zone 3 connectors.
  writeTestHeader("Zone 3 Connector Test", "Install zone 3 test board");
  zone3result=retry(check_zone3);
  testresult+=zone3result;
  //MGTs
  writeTestHeader("MGT Test", "Install MGT loopback modules");
  for(tindex=0;tindex<11;tindex++){
    retval=retry(check_MGT);
    mgtresult|=retval;
    if(retval!=0){
      int abortmgt=askForConfirmation("Abort MGT test? (y/n) ");
      if(abortmgt)break;
    }
  }
  testresult+=mgtresult;
  //Summary
  xil_printf("\n\n\n\rDETAILED TEST SUMMARY for HSIO %s:\n\n\r", name);
  print_result("LEDs", ledresult);
  print_result("Clocks", clockresult);
  print_result("Display", displayresult);
  print_result("Push switches", pushswitchresult);
  print_result("DIP switches", dipswitchresult);
  print_result("LEMO connectors", lemoresult);
  print_result("GPIO Headers", headerresult);
  print_result("Zone 3 connectors", zone3result);
  print_result("MGTs", mgtresult);
  xil_printf("\n\n\n\r            FINAL TEST SUMMARY: ");
  if(testresult==0)xil_printf("OK\n\r");
  else xil_printf("%d tests out of %d failed.\n\r", testresult, testnr);
  xil_printf("\n\n\nThe HSIO Test has finished. ");
  return 1;
}
  
int cosmic_adapter_test_procedure(){
  // Print header
  int i;
  int testresult;
  int clocktestresult;
  int datatestresult;
  testresult=0;

  xil_printf("\r");
  for(i=0;i<10;i++)xil_printf("\n");
  xil_printf("                              =======================\n\r");
  xil_printf("                              | COSMIC ADAPTER TEST |\n\r");
  xil_printf("                              =======================\n\n\n\n\r");
  
  display_puts(0, 0, "   Cosmic Adapter      Functional Test");

  writeTestHeader("Clock test", "Connect the 18 blue ethernet connectors");
  clocktestresult=retry(check_clk);
  testresult+=clocktestresult;
  //data lines.
  writeTestHeader("Data line and TLU test", "Connect the 18 grey connectors plus one yellow connector");
  datatestresult=retry(test_dtm_ls);
  testresult+=datatestresult;
  //Summary
  xil_printf("\n\n\n\rDETAILED TEST SUMMARY for adapter %s:\n\n\r", name);
  print_result("Clocks", clocktestresult);
  print_result("Data lines", datatestresult);
  xil_printf("\n\n\n\r            FINAL TEST SUMMARY: ");
  if(testresult==0)xil_printf("OK\n\r");
  else xil_printf("%d tests out of %d failed.\n\r", testresult, testnr);
  xil_printf("\n\n\nThe adapter Test has finished. ");
  
  return 1;
}

int main()
{
    init_platform();
    int k;
    for (k=0;k<25;k++)xil_printf("\n");	
    print("\rHSIO Cosmic Adapter Test Firmware booting...\n\n\n\r");
    inputName(name);
    if(askForConfirmation("Run Cosmic Adapter Test? (y/n) ")==1)
      retry(cosmic_adapter_test_procedure);
    //Temperature test
    //temperature_test();
    print("Bye bye!\n\r");
    return 0;
}
