#ifndef AFPHPTDCMODULECONFIG_HH
#define AFPHPTDCMODULECONFIG_HH

#include <RCF/RCF.hpp>
#include <stdint.h>

namespace ipc{

  const long IPC_N_CALIBVALS = 1024;


  struct AFPHPTDCModuleConfig{ 
    //Module ID string
    char   idStr[16];
    //Fpga registers
    uint16_t test;
    uint8_t tdcControl;
    uint8_t run;
    uint8_t bypassLut;
    uint8_t localClockEn;
    uint8_t calClockEn;
    uint8_t refEn;
    uint8_t hitTestEn;
    uint8_t inputSel;
    uint8_t address;
    uint8_t fanspeed;
    uint16_t channelEn;
    //calibration constants. Inl=0, Dnl=1;
    float calib[2][12][IPC_N_CALIBVALS];
    // Tdc Chip setup
    uint8_t test_select[3];
    uint8_t enable_error_mark[3];
    uint8_t enable_error_bypass[3];
    uint16_t enable_error[3];
    uint8_t readout_single_cycle_speed[3];
    uint8_t serial_delay[3];
    uint8_t strobe_select[3];
    uint8_t readout_speed_select[3];
    uint8_t token_delay[3];
    uint8_t enable_local_trailer[3];
    uint8_t enable_local_header[3];
    uint8_t enable_global_trailer[3];
    uint8_t enable_global_header[3];
    uint8_t keep_token[3];
    uint8_t master[3];
    uint8_t enable_bytewise[3];
    uint8_t enable_serial[3];
    uint8_t enable_jtag_readout[3];
    uint8_t tdc_id[3];
    uint8_t select_bypass_inputs[3];
    uint8_t readout_fifo_size[3];
    uint16_t reject_count_offset[3];
    uint16_t search_window[3];
    uint16_t match_window[3];
    uint8_t leading_resolution[3];
    uint32_t fixed_pattern[3];
    uint8_t enable_fixed_pattern[3];
    uint8_t max_event_size[3];
    uint8_t reject_readout_fifo_full[3];
    uint8_t enable_readout_occupancy[3];
    uint8_t enable_readout_separator[3];
    uint8_t enable_overflow_detect[3];
    uint8_t enable_relative[3];
    uint8_t enable_automatic_reject[3];
    uint16_t event_count_offset[3];
    uint16_t trigger_count_offset[3];
    uint8_t enable_set_counters_on_bunch_reset[3];
    uint8_t enable_master_reset_code[3];
    uint8_t enable_master_reset_code_on_event_reset[3];
    uint8_t enable_reset_channel_buffer_when_separator[3];
    uint8_t enable_separator_on_event_reset[3];
    uint8_t enable_separator_on_bunch_reset[3];
    uint8_t enable_direct_event_reset[3];
    uint8_t enable_direct_bunch_reset[3];
    uint8_t enable_direct_trigger[3];
    uint16_t offset[32][3];
    uint16_t coarse_count_offset[3];
    uint16_t dll_tap_adjust3_0[3];
    uint16_t dll_tap_adjust7_4[3];
    uint16_t dll_tap_adjust11_8[3];
    uint16_t dll_tap_adjust15_12[3];
    uint16_t dll_tap_adjust19_16[3];
    uint16_t dll_tap_adjust23_20[3];
    uint16_t dll_tap_adjust27_24[3];
    uint16_t dll_tap_adjust31_28[3];
    uint16_t rc_adjust[3];
    uint8_t not_used[3];
    uint8_t low_power_mode[3];
    uint8_t width_select[3];
    uint8_t vernier_offset[3];
    uint8_t dll_control[3];
    uint8_t dead_time[3];
    uint8_t test_invert[3];
    uint8_t test_mode[3];
    uint8_t enable_trailing[3];
    uint8_t enable_leading[3];
    uint8_t mode_rc_compression[3];
    uint8_t mode_rc[3];
    uint8_t dll_mode[3];
    uint8_t pll_control[3];
    uint8_t serial_clock_delay[3];
    uint8_t io_clock_delay[3];
    uint8_t core_clock_delay[3];
    uint8_t dll_clock_delay[3];
    uint8_t serial_clock_source[3];
    uint8_t io_clock_source[3];
    uint8_t core_clock_source[3];
    uint8_t dll_clock_source[3];
    uint16_t roll_over[3];
    uint8_t enable_matching[3];
    uint8_t enable_pair[3];
    uint8_t enable_ttl_serial[3];
    uint8_t enable_ttl_control[3];
    uint8_t enable_ttl_reset[3];
    uint8_t enable_ttl_clock[3];
    uint8_t enable_ttl_hit[3];
    void serialize(SF::Archive &ar){
      for(int i=0;i<16;i++) ar & idStr[i];
      ar & test;
      ar & tdcControl;
      ar & run;
      ar & bypassLut;
      ar & localClockEn;
      ar & calClockEn;
      ar & refEn;
      ar & hitTestEn;
      ar & inputSel;
      ar & address;
      ar & fanspeed;
      ar & channelEn;
      for(int i=0;i<2;i++)
	for(int j=0;j<12;j++)
	  for (int k=0;k<IPC_N_CALIBVALS;k++)
	    ar & calib[i][j][k];
      for(int i=0;i<3;i++){
	ar & test_select[i];
	ar & enable_error_mark[i];
	ar & enable_error_bypass[i];
	ar & enable_error[i];
	ar & readout_single_cycle_speed[i];
	ar & serial_delay[i];
	ar & strobe_select[i];
	ar & readout_speed_select[i];
	ar & token_delay[i];
	ar & enable_local_trailer[i];
	ar & enable_local_header[i];
	ar & enable_global_trailer[i];
	ar & enable_global_header[i];
	ar & keep_token[i];
	ar & master[i];
	ar & enable_bytewise[i];
	ar & enable_serial[i];
	ar & enable_jtag_readout[i];
	ar & tdc_id[i];
	ar & select_bypass_inputs[i];
	ar & readout_fifo_size[i];
	ar & reject_count_offset[i];
	ar & search_window[i];
	ar & match_window[i];
	ar & leading_resolution[i];
	ar & fixed_pattern[i];
	ar & enable_fixed_pattern[i];
	ar & max_event_size[i];
	ar & reject_readout_fifo_full[i];
	ar & enable_readout_occupancy[i];
	ar & enable_readout_separator[i];
	ar & enable_overflow_detect[i];
	ar & enable_relative[i];
	ar & enable_automatic_reject[i];
	ar & event_count_offset[i];
	ar & trigger_count_offset[i];
	ar & enable_set_counters_on_bunch_reset[i];
	ar & enable_master_reset_code[i];
	ar & enable_master_reset_code_on_event_reset[i];
	ar & enable_reset_channel_buffer_when_separator[i];
	ar & enable_separator_on_event_reset[i];
	ar & enable_separator_on_bunch_reset[i];
	ar & enable_direct_event_reset[i];
	ar & enable_direct_bunch_reset[i];
	ar & enable_direct_trigger[i];
	for(int j=0;j<32;j++) ar & offset[j][i];
	ar & coarse_count_offset[i];
	ar & dll_tap_adjust3_0[i];
	ar & dll_tap_adjust7_4[i];
	ar & dll_tap_adjust11_8[i];
	ar & dll_tap_adjust15_12[i];
	ar & dll_tap_adjust19_16[i];
	ar & dll_tap_adjust23_20[i];
	ar & dll_tap_adjust27_24[i];
	ar & dll_tap_adjust31_28[i];
	ar & rc_adjust[i];
	ar & not_used[i];
	ar & low_power_mode[i];
	ar & width_select[i];
	ar & vernier_offset[i];
	ar & dll_control[i];
	ar & dead_time[i];
	ar & test_invert[i];
	ar & test_mode[i];
	ar & enable_trailing[i];
	ar & enable_leading[i];
	ar & mode_rc_compression[i];
	ar & mode_rc[i];
	ar & dll_mode[i];
	ar & pll_control[i];
	ar & serial_clock_delay[i];
	ar & io_clock_delay[i];
	ar & core_clock_delay[i];
	ar & dll_clock_delay[i];
	ar & serial_clock_source[i];
	ar & io_clock_source[i];
	ar & core_clock_source[i];
	ar & dll_clock_source[i];
	ar & roll_over[i];
	ar & enable_matching[i];
	ar & enable_pair[i];
	ar & enable_ttl_serial[i];
	ar & enable_ttl_control[i];
	ar & enable_ttl_reset[i];
	ar & enable_ttl_clock[i];
	ar & enable_ttl_hit[i];
      }
    }
  };

};

#endif   /* multiple inclusion protection */


