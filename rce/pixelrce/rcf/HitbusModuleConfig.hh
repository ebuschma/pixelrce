#ifndef HITBUSMODULECONFIG_HH
#define HITBUSMODULECONFIG_HH

#include <RCF/RCF.hpp>
#include <stdint.h>

namespace ipc{

  struct HitbusModuleConfig{ 
    uint8_t  bpm             ;                 
    uint8_t  delay_tam1      ;                    
    uint8_t  delay_tam2      ;                    
    uint8_t  delay_tam3      ;                    
    uint8_t  delay_tbm1      ;                    
    uint8_t  delay_tbm2      ;                    
    uint8_t  delay_tbm3      ;                    
    uint8_t  bypass_delay    ;                    
    uint8_t  clock           ;                      
    uint8_t  function_A      ;
    uint8_t  function_B      ;
    char   idStr[16]       ;//Module ID string
  void serialize(SF::Archive &ar){
    ar &  bpm             ;                 
    ar &  delay_tam1      ;                    
    ar &  delay_tam2      ;                    
    ar &  delay_tam3      ;                    
    ar &  delay_tbm1      ;                    
    ar &  delay_tbm2      ;                    
    ar &  delay_tbm3      ;                    
    ar &  bypass_delay    ;                    
    ar &  clock           ;                      
    ar &  function_A      ;
    ar &  function_B      ;
    for(int i=0;i<16;i++)ar & idStr[i];
  }
};

};

#endif   /* multiple inclusion protection */


