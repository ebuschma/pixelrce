#ifndef RCFAFPHPTDCADAPTER_IDL
#define RCFAFPHPTDCADAPTER_IDL

#include "rcf/AFPHPTDCModuleConfig.hh"

RCF_BEGIN(I_RCFAFPHPTDCAdapter, "I_RCAFPHPTDCAdapter")
RCF_METHOD_R1(int32_t, RCFdownloadConfig, ipc::AFPHPTDCModuleConfig)
RCF_END(I_RCFAFPHPTDCAdapter)

class RCFAFPHPTDCAdapter 
{
  virtual int32_t RCFdownloadConfig(ipc::AFPHPTDCModuleConfig config)=0;
};

#endif
