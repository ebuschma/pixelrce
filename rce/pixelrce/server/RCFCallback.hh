#ifndef RCFCALLBACK_HH
#define RCFCALLBACK_HH

#include "rcf/Callback.hh"
#include <mutex>
#include <condition_variable>

class CallbackInfo;

class RCFCallback : public Callback
{
public:
  RCFCallback(CallbackInfo* cbinfo);
  ~RCFCallback();
  void notify(ipc::CallbackParams msg);
  void stopServer();
  void addRce(int rce);
  void run();
    
protected:
  int m_rce;
  std::mutex m_mutex;
  std::condition_variable m_cond;
  CallbackInfo* m_cbinfo;
  RCF::RcfServer *m_callbackServer;
};

#endif
