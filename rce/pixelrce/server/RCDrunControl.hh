#ifndef RCDRUNCONTROL_HH
#define RCDRUNCONTROL_HH

#include "ipc/partition.h"
#include "config/ConfigBase.hh"
#include "server/GlobalConfigBase.hh"
#include "server/PixScan.hh"
#include <is/infoT.h>
#include <is/infodictionary.h>
#include <string>
#include <map>
#include <vector>

//class GlobalConfigBase;
class AbsController;
class Monitoring;

namespace RCE{
  class PixScan;
}

class RCDrunControl {

public:
  RCDrunControl(AbsController& acontroller, IPCPartition &p);
  virtual ~RCDrunControl(); 
  int Init();
  int Config();
  int UnConfig();
  int StartRun();
  int StopRun();
  void publish();
  bool isAtlasClockPresent();
  bool isExternalClockSet();
  bool isGbtStatusOK();
  void timingScan();
  void terminateScan();
  void quickStatus();
  void reconfigureModule(int rce, int outlink);
  void SetECRpreset(unsigned aECRpreset); 
  void SetGlobalConfDir(char * adir){       m_globalconfdir = adir;} 
  void SetGlobalConfName(char * aname){     m_globalconfname = aname; } 
  void SetLatency(int aLatency){            Latency = aLatency    ;}
  void SetSecLatency(int aSecLatency){      SecLatency = aSecLatency;}
  void SetConsecLvl1TrigA(int aNtrg){       ConsecLvl1TrigA = aNtrg; Ntrg = aNtrg;}
  void SetTriggerMask(int aTriggerMask){    TriggerMask = aTriggerMask;}
  void SetEventInterval(int aEventInterval){EventInterval = aEventInterval;}
  void SetStrobeLVL1Delay(int aStrobeLVL1Delay){StrobeLVL1Delay = aStrobeLVL1Delay;}
  void SetDeadTime(int aDeadTime){          DeadTime = aDeadTime  ;}
  void SetComplexDeadTimeWindow(int aDeadTimeW){          m_cdtwindow = aDeadTimeW  ;}
  void SetComplexDeadTimeBuffer(int aDeadTimeB){          m_cdtnbuf = aDeadTimeB  ;}
  void SetEnableComplexDeadTime(bool aEnableCDT){         EnableCDT = aEnableCDT  ;}
  void ResetL1id();
  void SetEventNormalization(int norm){ EventNormalization=norm;}
  void SetRunNumber(unsigned aRunNumber){RunNumber = aRunNumber;}
  void SetRodId(unsigned aRodId){RodId = aRodId;}
  void SetEnableMonitoring(bool aEnableMonitoring){EnableMonitoring=aEnableMonitoring;}
  void SetEfbTimeout(int aEfbTimeout){ EfbTimeout=aEfbTimeout;}
  void SetEfbTimeoutFirst(int aEfbTimeoutFirst){ EfbTimeoutFirst=aEfbTimeoutFirst;}
  void SetEfbMissingHeaderTimeout(int aEfbMissingHeaderTimeout){EfbMissingHeaderTimeout=aEfbMissingHeaderTimeout;}
  void SetEnableTtcSim(bool aEnableTtcSim){EnableTtcSim=aEnableTtcSim;}
  void SetBlockEcrBcr(int aBlockEcrBcr){BlockEcrBcr=aBlockEcrBcr;}
  void SetOutputBusy(bool aOutputBusy){ OutputBusy=aOutputBusy;}
  void SetDisableInternalBusy(bool aDisableInternalBusy){DisableInternalBusy=aDisableInternalBusy;}
  void SetEnableEcrReset(bool aEnableEcrReset){ EnableEcrReset=aEnableEcrReset;}
  void SetBcrBusyParams(int aBcrBusyParam1,int aBcrBusyParam2){ 
    BcrBusyParam1=aBcrBusyParam1; 
    BcrBusyParam2=aBcrBusyParam2;}
  void SetBcrVetoFirstBC(int aBcrVetoFirstBC){BcrBusyParam1=aBcrVetoFirstBC;}
  void SetBcrVetoWidth(int aBcrVetoWidth){BcrBusyParam2=aBcrVetoWidth;} 

  void SetEnableBcrBusy(bool aEnableBcrBusy){ EnableBcrBusy=aEnableBcrBusy;}
  void SetEnableSLinkBlowoff(bool aEnableSLinkBlowoff){EnableSLinkBlowoff=aEnableSLinkBlowoff;}
  void SetBcidOffset(int aBcidOffset){ BcidOffset=aBcidOffset;}
  void SetTimingScan(bool on){TimingScan=on;}
  void SetEventSizeLimit(int aEventSizeLimit){EventSizeLimit=aEventSizeLimit;}
private:

  std::map<int,int> rcemap;
  int Latency;
  int SecLatency;
  int ConsecLvl1TrigA;
  int TriggerMask;
  int EventInterval;
  int StrobeLVL1Delay;
  int DeadTime;
  int m_cdtnbuf; // if m_cdt* = 0, no complex deadtime is applied
  int m_cdtwindow;
  int Ntrg;
  int EfbTimeout;
  int EfbTimeoutFirst;
  int EfbMissingHeaderTimeout;
  int EventNormalization;
  int BlockEcrBcr;
  int BcrBusyParam1;
  int BcrBusyParam2;
  int BcidOffset;
  int EventSizeLimit;

  bool EnableCDT;
  bool EnableMonitoring;
  bool EnableTtcSim;
  bool OutputBusy;
  bool DisableInternalBusy;
  bool EnableEcrReset;
  bool EnableBcrBusy;
  bool EnableSLinkBlowoff;
  bool TimingScan;
  bool QuickStatus;
  bool QuickStatusAuto;
  
  unsigned RunNumber;
  unsigned RodId;
  bool m_timingScanRunning;
  ISInfoInt m_timingStart;
  ISInfoInt m_timingStep;
  ISInfoInt m_timingNumberOfSteps;
  ISInfoInt m_timingStepDuration;
  int m_currentStep;
  int m_durationCounter;
  std::map<int, int> m_origDelays;
  
  AbsController& m_controller;
  std::string m_globalconfdir;
  std::string m_globalconfname;
  GlobalConfigBase* m_globalconf;
  std::vector<Monitoring*> m_monitoring;
  IPCPartition &m_partition;
  std::map<int, std::vector<int> > m_outlink;
  float m_normalization;
  std::map<int, unsigned> m_permanentmask;
  std::map<int, int> m_blockReconfig;

};


#endif
