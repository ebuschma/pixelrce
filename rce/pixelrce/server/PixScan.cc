/////////////////////////////////////////////////////////////////////
// PixScan.cxx
/////////////////////////////////////////////////////////////////////
//
// 17/06/05  Version 1.0 (PM)
//
//
// Still missing:
//  setting up of the rod scan structure before sending reference
//  presets
//  histogram downloads
//  histogram staging
#include "json.hpp"
#include "server/PixScan.hh"

#include <sstream>
#include <iostream>


#include "server/RCFcompat.hh"
#define seqsize() size()
#define length(x) resize(x)


using namespace CORBA;

namespace RCE {
  PixScan::PixScan(ScanType presetName, FEflavour feFlavour) {
    //    initConfig();
    presetRCE(presetName, feFlavour);
    setupRCE(presetName, feFlavour);
    resetScan();
  }


  PixScan::~PixScan() {
  }


  bool PixScan::setupRCE(ScanType presetName, FEflavour feFlavour) {
    m_name="Scan";
    m_receiver="Pgp";
    m_dataHandler="RegularScan";
    m_dataProc="OCCUPANCY";
    m_scanType="RegularCfg";
    m_triggerType="default";
    m_formatterType["FEI3"]="JJ";
    m_formatterType["FEI4A"]="FEI4A";
    m_formatterType["FEI4B"]="FEI4B";
    m_formatterType["HPTDC"]="HPTDC";
    m_analysisType="NONE";
    m_hitbusConfig=0;
    m_callbackPriority=ipc::LOW;
    m_timeout_seconds=0;
    m_timeout_nanoseconds=300000000;
    m_firstStage = 0;
    m_stepStage = 1;
    m_oneByOne=false;
    m_LVL1Latency_Secondary=0;
    m_moduleTrgMask=0;
    m_deadtime=0;
    m_mixedTrigger=false;
    m_setupThreshold=false;
    m_triggerMask=0;
    m_triggerDataOn=false;
    m_threshold=100;
    m_fitfun="SCURVE";
    m_verifyConfig=false;
    m_eventInterval=0;
    m_hitbusConfig=0;
    m_clearMasks=false;
    m_protectFifo=false;
    m_runnumber=0;
    m_injectForTrigger=0;
    m_overrideLatency=false;
    m_allowedTimeouts=5;
    m_nTriggers=0;
    m_noiseThreshold=1e-6;
    m_useAbsoluteNoiseThreshold=false;
    m_useVcal=false;

    bool fei3=(feFlavour == PM_FE_I2);
    bool fei4=(feFlavour == PM_FE_I4A || feFlavour == PM_FE_I4B || feFlavour == PM_FE_I4);
    if (presetName == DIGITAL_TEST && fei3) {
      m_scanType="Regular";
      m_analysisType = "Fei3DigitalTest";
      addHistoName(".*FEI4_Errors");
      addHistoName("Mod_[0-9]+_Occupancy_Point_000");
    } else if (presetName == DIGITAL_TEST && fei4) {
      m_firstStage = 0;
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType = "Fei4DigitalTest";
      //    addHistoName("RCE[0-9]+_Mod_0_Occupancy_Point_000");
      addHistoName(".*");
    } else if (presetName == DIGITALTEST_SELFTRIGGER && fei4) {
      m_firstStage = 0;
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType = "Fei4DigitalTest";
      m_allowedTimeouts=1000;
      m_callbackPriority=ipc::HIGH;
      //    addHistoName("RCE[0-9]+_Mod_0_Occupancy_Point_000");
      addHistoName(".*");
    } else if (presetName == MULTISHOT && fei4) {    
      addHistoName("Mod_0_Occupancy_Point_000");
      m_triggerType="MultiShot";
      m_eventInterval=5;
    } else if (presetName == ANALOG_TEST && fei3) { 
      m_scanType="Regular";
      addHistoName(".*");
    } else if (presetName == TOT_TEST && fei3) {
      m_scanType="Regular";
      m_dataProc="TOT";
      m_analysisType="TOT";
      addHistoName(".*");
    } else if (presetName == ANALOG_TEST && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_analysisType = "Fei4DigitalTest";
      m_formatterType.clear();
      addHistoName(".*");
    } else if (presetName == TOT_TEST && fei4) {
      m_dataProc="TOT";
      m_analysisType="TOT";
      addHistoName(".*");
    } else if (presetName == TOT_CALIB && fei4) {
      m_dataProc="TOTCALIB";
      m_analysisType="TOTCALIB";
      addHistoName(".*");
    } else if (presetName == THRESHOLD_SCAN && fei3) {
      m_scanType="Regular";
      m_analysisType="Threshold";
      addHistoName(".*");
    } else if (presetName == THRESHOLD_SCAN && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType="Threshold";
      addHistoName(".*");
    } else if (presetName == TEMPERATURE_SCAN && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="Temperature";
      m_triggerType="Temperature";
      m_formatterType.clear();
      m_analysisType="Temperature";
      addHistoName(".*");
    } else if (presetName == MONLEAK_SCAN && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="Monleak";
      m_triggerType="Monleak";
      m_formatterType.clear();
      m_analysisType="Temperature";
      addHistoName(".*");
    } else if (presetName == SERIAL_NUMBER_SCAN && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="SerialNumber";
      m_triggerType="SerialNumber";
      m_formatterType.clear();
      m_analysisType="SerialNumber";
      addHistoName(".*");
    } else if (presetName == MODULE_CROSSTALK && fei4) {
      m_dataHandler="ModuleCrosstalkRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType="Threshold";
      m_LVL1Latency_Secondary=238;
      m_moduleTrgMask=1;
      // have to use m_oneByOne for this scan
      m_oneByOne=true;
      m_mixedTrigger=true;
      m_analysisType="ModuleCrosstalk";
      addHistoName(".*");
    } else if (presetName == OFFSET_SCAN && fei4) {
      m_stepStage = 5;
      //m_setupThreshold=true;
      //m_threshold=120;
      m_fitfun="SCURVE_NOCONV";
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType="Offset";
      addHistoName(".*");
    } else if (presetName == CROSSTALK_SCAN && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_fitfun="SCURVE_XTALK";
      m_formatterType.clear();
      m_analysisType="Crosstalk";
      m_callbackPriority=ipc::MEDIUM;
      addHistoName(".*");
    } else if (presetName == DIFFUSION && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_fitfun="SCAN_SCURVE_XTALK";      
      m_analysisType="Crosstalk";
      m_callbackPriority=ipc::MEDIUM;
      addHistoName(".*");
      std::vector<float> pixels; // = row + 336*col + 1 (row from 0-335, col from 0-79.  We add 1 because maskStage==0 has special 
      
      pixels.push_back(20 + 336*11 + 1);
      pixels.push_back(40 + 336*31 + 1);
      pixels.push_back(176 + 336*55 + 1);
      pixels.push_back(177 + 336*55 + 1);
      pixels.push_back(178 + 336*55 + 1);
      pixels.push_back(175 + 336*56 + 1); 
      pixels.push_back(176 + 336*56 + 1);
      
      setLoopVarValues(1, pixels);  
      
    } else if  (presetName == TWOTRIGGER_THRESHOLD && fei4) {
      m_eventInterval=245;
      m_dataHandler="RegularScan";
      m_injectForTrigger=3;  //bit mask.  3: inject on both triggers, 2: inject on 2nd trigger, 1: inject on 1st trigger, 0: no inject
      m_dataProc="MultiTrigOccupancy";
      m_triggerType="MultiTrigger";
      m_analysisType="MultiTrig";
      addHistoName(".*");
    } else if (presetName == TWOTRIGGER_NOISE && fei4) {
      m_eventInterval=245; // Overwritten by MULTITRIG_INTERVAL loop
      m_dataHandler="RegularScan";
      m_dataProc="MultiTrigNoise";
      m_triggerType="MultiTrigger";
      m_analysisType="MultiTrigNoise";
      addHistoName(".*");
      
    } else if (presetName == GDAC_SCAN && fei4) {
      if(feFlavour == PM_FE_I4A){ //FEI4A
	m_maskStageTotalSteps= FEI4_COL_ANL_40;
	m_maskStageSteps = 24; 
      }else{ //FEI4B
	m_maskStageTotalSteps= FEI4_COLPR1x6;
	m_maskStageSteps = 5; 
      }
      m_stepStage = 5;
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType="Gdac";
      addHistoName(".*");
    } else if ((presetName == GDAC_TUNE || presetName == GDAC_RETUNE) && fei4) {
      m_stepStage = 5;
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType="Gdac";
      addHistoName(".*");
    } else if ((presetName == GDAC_FAST_TUNE || presetName == GDAC_FAST_RETUNE) && fei4) {
      m_stepStage = 2;
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType="GdacFast";
      addHistoName(".*");
    } else if (presetName == GDAC_COARSE_FAST_TUNE && fei4) {
      m_stepStage = 2;
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType="GdacCoarseFast";
      addHistoName(".*");
    } else if (presetName == IF_TUNE && fei4) {
      m_scanType="IfScan";
      m_dataProc="TOT";
      m_analysisType = "Iffanalysis";
      m_callbackPriority=ipc::MEDIUM;
      addHistoName("^([^T]|T[^o]|To[^T]|ToT[^2])*.{0,3}$"); //anything except ToT2 histos
    } else if (presetName == FDAC_TUNE && fei4) {
      m_dataProc="TOT";
      m_analysisType = "Fei4Fdacanalysis";
      addHistoName(".*");
    } else if (presetName == VTHIN_SCAN && fei4) {
      addHistoName(".*");
    } else if (presetName == DELAY_SCAN && fei4) {
    } else if (presetName == MEASUREMENT_SCAN) {
      m_firstStage = 5;
      m_timeout_nanoseconds=1;
      m_allowedTimeouts=-1;
      m_name="172.21.6.43:1444"; //address of the computer that does the measurement
      m_receiver="Measurement";
      m_dataProc="Measurement";
      m_triggerType="Measurement";
      m_dataHandler="Simple";
      addHistoName(".*");
    } else if (presetName == SELFTRIGGER && fei4) {
      //m_name="data_RUNNUM.hit";
      m_scanType="Selftrigger";
      m_dataProc="Selftrigger";
      m_dataHandler="CosmicData";
      m_timeout_seconds=1000000;
      m_timeout_nanoseconds=0;
      m_allowedTimeouts=-1;
      m_protectFifo=true;
      addHistoName(".*");
    } else if (presetName == REGISTER_TEST && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="Fei4RegisterTest";
      m_triggerType="Fei4RegisterTest";
      m_formatterType.clear();
      m_analysisType="RegisterTest";
      m_allowedTimeouts=0;
      m_useVcal=true;
      addHistoName(".*");
    } else if (presetName == EXT_REGISTER_VERIFICATION && fei4) {
      m_scanType="CosmicData";
      m_dataProc="Selftrigger";
      m_dataHandler="CosmicData";
      m_timeout_seconds=0;
      m_timeout_nanoseconds=1000000;
      m_verifyConfig=true;
    } else if (presetName == NOISESCAN && fei4) {
      m_scanType="Noisescan";
      m_dataProc="Noisescan";
      m_dataHandler="CosmicData";
      m_receiver="PgpCosmic";
      m_analysisType="Fei4Noise";
      m_timeout_seconds=60;
      m_timeout_nanoseconds=0;
      m_allowedTimeouts=-1;
      m_triggerMask=0x2;  //Trigger mask 1=scint 2=cyclic 4=Eudet 8=HSIO
      m_eventInterval=20;
      //m_nTriggers=1001;
      m_protectFifo=true;
      m_noiseThreshold=1e-6;
      m_useAbsoluteNoiseThreshold=false;
      addHistoName(".*");
    } else if (presetName == NOISESCAN_SELFTRIGGER && fei4) {
      m_scanType="Selftrigger";
      m_dataProc="Noisescan";
      m_dataHandler="CosmicData";
      m_receiver="PgpCosmic";
      m_analysisType="Fei4Noise";
      m_timeout_seconds=60;
      m_timeout_nanoseconds=0;
      m_allowedTimeouts=-1;
      m_protectFifo=true;
      m_noiseThreshold=10;
      m_useAbsoluteNoiseThreshold=true;
      addHistoName(".*");
  } else if (presetName == STUCKPIXELS && fei4) {
      m_dataProc="Hitor";
      m_formatterType["FEI4A"]="FEI4Hitor";
      m_formatterType["FEI4B"]="FEI4Hitor";
      m_analysisType="Fei4StuckPixel";
      m_callbackPriority=ipc::HIGH;
      m_triggerType="Hitor";
      addHistoName(".*");
  } else if (presetName == LV1LATENCY_SCAN && fei3) {
      m_dataHandler="CosmicData";
      m_receiver="PgpCosmic";
      m_timeout_seconds=60;
      m_timeout_nanoseconds=0;
    } else if (presetName == SCINTDELAY_SCAN) {
      m_scanType="Delay";
      m_dataProc="Delay";
      m_dataHandler="DelayScan";
      m_receiver="PgpCosmic";
      m_timeout_seconds=5;
      m_timeout_nanoseconds=0;
      m_allowedTimeouts=-1;
      addHistoName(".*");
    } else if (presetName == COSMIC_DATA) {
      m_hitbusConfig=1;
      m_scanType="CosmicData";
      m_dataHandler="CosmicData";
      //m_dataProc="CosmicData";
      m_receiver="PgpCosmicNw";
      m_triggerDataOn=true;
      m_timeout_seconds=10000000;
      m_timeout_nanoseconds=0;
      m_allowedTimeouts=-1;
      m_overrideLatency=true;
    } else if (presetName == ATLAS_DATA) {
      m_hitbusConfig=1;
      m_scanType="AtlasData";
      m_dataHandler="AtlasData";
      m_dataProc="Atlas";
      m_triggerDataOn=true;
      m_timeout_seconds=10000000;
      m_timeout_nanoseconds=0;
      m_allowedTimeouts=-1;
      m_overrideLatency=true;
    } else if (presetName == COSMIC_RCE) {
      m_digitalInjection = false;
      m_scanType="CosmicData";
      m_dataHandler="CosmicData";
      m_dataProc="CosmicData";
      m_receiver="PgpCosmic";
      m_triggerDataOn=true;
      m_timeout_seconds=10000000;
      m_timeout_nanoseconds=0;
      m_allowedTimeouts=-1;
    } else if (presetName == EXTTRIGGER && fei4) {
      m_triggerDataOn=true;
      m_overrideLatency=true;
      m_LVL1Latency = 210;
      m_hitbusConfig=1;
      //m_eventInterval=750;
      m_triggerMask=0x8;  //Trigger mask 1=scint 2=cyclic 4=Eudet 8=HSIO
      m_scanType="CosmicData";
      m_dataProc="Selftrigger";
      m_dataHandler="CosmicData";
      m_receiver="PgpCosmic";
      //Setup below for multiple hardware triggers:
      //Trigger stream with  m_strobeLVL1Delay leading 0s, 
      //then m_consecutiveLvl1TrigA[1] triggers m_LVL1Latency_Secondary ticks apart
      //m_triggerType="MultiShot";
      //m_consecutiveLvl1TrigA[1]= 2; //n L1A
      //m_consecutiveLvl1TrigA[0]= 1; 
      //m_LVL1Latency_Secondary=10;
      //m_strobeLVL1Delay=32;
      //-------
      m_timeout_seconds=1000000;
      m_timeout_nanoseconds=0;
      m_allowedTimeouts=-1;
      addHistoName(".*");
    } else if ((presetName == TDAC_TUNE || presetName == GDAC_TUNE) && fei3) {
    
      if (presetName == TDAC_TUNE) {
	m_analysisType="Fei3Tdac_tune"; 
      } else {
	m_analysisType="Gdac";
      }
      addHistoName(".*");
    } else if ((presetName == TDAC_TUNE || presetName==TDAC_TUNE_ITERATED) && fei4) {
      m_thresholdTargetValue = 1600;
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_analysisType="Fei4Tdac_tune"; 
      m_formatterType.clear();
      addHistoName(".*");
    } else if ((presetName == TDAC_FAST_TUNE || presetName==TDAC_FAST_RETUNE) && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_analysisType="Fei4TdacFast_tune"; 
      m_formatterType.clear();
      addHistoName(".*");
    } else if (presetName == T0_SCAN && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_fitfun="SCURVE_NOCONV";
      m_formatterType.clear();
      m_analysisType="T0";
      addHistoName(".*");
    } else if (presetName == TIMEWALK_MEASURE && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_fitfun="SCURVE_NOCONV";
      m_formatterType.clear();
      m_analysisType="TimeWalk";
      addHistoName(".*");
    } else if (presetName == INTIME_THRESH_SCAN && fei4) {
      m_dataHandler="RegularScanRaw";
      m_dataProc="RawFei4Occupancy";
      m_formatterType.clear();
      m_analysisType="Threshold";
      addHistoName(".*");
    } else if ((presetName == FDAC_TUNE || presetName == IF_TUNE) && fei3) {
      addHistoName(".*");
      m_dataProc="TOT";

      if (presetName == FDAC_TUNE) {
	m_analysisType = "Fei3Fdacanalysis";
      } else {
	m_analysisType = "Iffanalysis";
      }
    } else if (presetName == TOT_CALIB && fei3) {
      m_scanType="Regular";
      m_dataProc="TOT";
    } else if (presetName == T0_SCAN && fei3) {
      m_scanType="Regular";
      addHistoName(".*");
    } else if (presetName == TIMEWALK_MEASURE && fei3) {
      m_scanType="Regular";
      addHistoName(".*");
    } else if (presetName == INTIME_THRESH_SCAN && fei3) {
      m_scanType="Regular";
    } else if (presetName == CROSSTALK_SCAN && fei3) {
      m_scanType="Regular";
      addHistoName(".*");
    } else if (presetName == INCREMENTAL_TDAC_SCAN && feFlavour == PM_FE_I2) {
      m_scanType="Regular";
      /* no RCE BOC scans at this point */
    } else if (presetName == BOC_RX_DELAY_SCAN) { return false;
    } else if (presetName == BOC_V0_RX_DELAY_SCAN) { return false;
    } else if (presetName == BOC_THR_RX_DELAY_SCAN) { return false;
    } else {
  
      std::cout<<"Scan not implemented (index= "<< presetName<<  ")"<<std::endl;
      return false;
    }
    return true;
  }
  
  bool PixScan::presetRCE(ScanType presetName, FEflavour feFlavour) {
  #include "PixScanBase_presetRCE.h"
  }
  /* void PixScan::preset(ScanType presetName) now in PixScanBase - 
     to sync with PixLib */





  bool PixScan::loop(int index) {
    if (index < MAX_LOOPS) {
      if (m_loopTerminating[index]) {
	m_loopTerminating[index] = false;
	m_loopEnded[index] = true;
	if (index == 0) {
	  m_FECounter = 0;
	  m_maskStageIndex = 0;
	  m_newMaskStage = true;
	  m_newScanStep = true;
	  m_newFE = m_FEbyFE;
	  if (m_dspProcessing[0]) m_newScanStep = false;
	  if (m_dspMaskStaging || m_maskStageMode == STATIC || m_runType != NORMAL_SCAN) m_newMaskStage = false;
	}
	m_loopIndex[index] = 0;
	return false;
      } else {
	m_loopEnded[index] = false;
	return true;
      }
    }
    assert(0);
    return false;
  }

  int PixScan::scanIndex(int index) {
    if (index < MAX_LOOPS) {
      return m_loopIndex[index];
    } 
    assert(0);
    return 0;
  }

  void PixScan::next(int index) {
    if (index == 0) {
      if ((m_dspProcessing[0] || !m_loopActive[0]) && 
	  (m_dspMaskStaging || m_maskStageMode == STATIC || m_runType != NORMAL_SCAN)) {
	m_loopIndex[index] = m_loopVarNSteps[index] - 1;
	m_maskStageIndex = m_maskStageSteps - 1;
	m_loopTerminating[0] = true;
	m_newScanStep = false;
	m_newMaskStage = false;
      } else if (m_dspProcessing[0] || !m_loopActive[0]) {
	if (m_maskStageIndex < m_maskStageSteps - 1) {
	  m_maskStageIndex++;
	  m_newScanStep = false;
	  m_newMaskStage = true;
	} else {
	  m_loopTerminating[0] = true;
	}
      } else if (m_dspMaskStaging || m_maskStageMode == STATIC || m_runType != NORMAL_SCAN) {
	if (m_loopIndex[0] < m_loopVarNSteps[0] - 1) {
	  m_loopIndex[0]++;
	  m_newScanStep = true;
	  m_newMaskStage = false;
	} else {
	  m_loopTerminating[0] = true;
	}
      } else {
	if ((m_loopIndex[0] == m_loopVarNSteps[0] - 1) && (m_maskStageIndex == m_maskStageSteps - 1)) {
	  m_loopTerminating[0] = true;
	  m_newScanStep = false;
	  m_newMaskStage = false;
	} else {
	  if (m_innerLoopSwap) {
	    if (m_loopIndex[0] < m_loopVarNSteps[0] - 1) {
	      m_loopIndex[0]++;
	      m_newScanStep = true;
	      m_newMaskStage = false;
	    } else {
	      m_loopIndex[0] = 0;
	      m_maskStageIndex++;
	      m_newScanStep = false;
	      m_newMaskStage = true;
	    }
	  } else {

	    if (m_maskStageIndex < m_maskStageSteps - 1) {
	      m_maskStageIndex++;
	      m_newScanStep = false;
	      m_newMaskStage = true;
	    } else {
	      m_maskStageIndex = 0;
	      m_loopIndex[0]++;
	      m_newScanStep = true;
	      m_newMaskStage = false;
	    }
	  }
	}
      }
    } else if (index < MAX_LOOPS) {  
      if (m_dspProcessing[index] || !m_loopActive[index]) {
	m_loopIndex[index] = m_loopVarNSteps[index] - 1;
	m_loopTerminating[index] = true;
      } else {
	if ((m_loopIndex[index] < m_loopVarNSteps[index] - 1) || m_loopVarValuesFree[index]) {
	  m_loopIndex[index]++;
	} else {
	  m_loopTerminating[index] = true;
	}
      }
    } else {
      assert(0);
    }
  }

  void PixScan::terminate(int index) {
    if (index > 0 && index < MAX_LOOPS) {
      if (m_loopVarValuesFree[index]) {
	m_loopTerminating[index] = true;
	m_loopVarNSteps[index] = m_loopIndex[index]+1;
      }
    } else {
      assert(0);
    }
  }

  bool PixScan::newMaskStep() {
    return m_newMaskStage;
  }

  bool PixScan::newScanStep() {
    return m_newScanStep;
  }
  
  void PixScan::resetScan() {
    m_FECounter=0;
    m_actualFE = -1;
    // Reset indexes
    for (int i=0; i<MAX_LOOPS; i++) {
      m_loopIndex[i] = 0;
      m_loopTerminating[i] = false;
      m_loopEnded[i] = false;
    }
    m_maskStageIndex = 0;
    m_newMaskStage = true;
    m_newScanStep = true;
    if (m_dspProcessing[0]) m_newScanStep = false;
    if (m_dspMaskStaging || m_maskStageMode == STATIC || m_runType != NORMAL_SCAN) m_newMaskStage = false;
    // Reset histograms
  }



  void PixScan::convertScanConfig(ipc::ScanOptions& scanPar){
    // Initialize ScanPar structure
    int nLoopTot = 0;
    int i;
    int m_nBinTot = 0; 
    for (i=0; i<MAX_LOOPS; i++) {
      if (getLoopActive(i) && getDspProcessing(i)) {
	nLoopTot++;
	m_nBinTot += getLoopVarNSteps(i);
      } else {
	break;
      }
    }

    PixLib::EnumMaskSteps mss;
    PixLib::EnumMaskStageMode msm;
    PixLib::EnumScanParam sp; 
    PixLib::EnumFitFunc ff;
    PixLib::EnumScanAct sa;
    PixLib::EnumTriggMode tm;
    PixLib::EnumTriggOptions to;

    // Load scan config
    scanPar.runNumber=m_runnumber;
    scanPar.name=string_dup(m_name.c_str());
    scanPar.scanType=string_dup(m_scanType.c_str());
    scanPar.receiver=string_dup(m_receiver.c_str());
    scanPar.dataHandler=string_dup(m_dataHandler.c_str());
    scanPar.dataProc=string_dup(m_dataProc.c_str());
    scanPar.timeout.seconds=m_timeout_seconds;
    scanPar.timeout.nanoseconds=m_timeout_nanoseconds;
    scanPar.timeout.allowedTimeouts=m_allowedTimeouts;
    if (getMaskStageMode() == PixScan::STATIC) {
      scanPar.stagingMode = string_dup(msm.lookup(STATIC).c_str());
      //scanPar.general.stageAdvanceFirst = 0;  //?
      scanPar.maskStages = string_dup(mss.lookup(STEPS_32).c_str()); 
      scanPar.nMaskStages = 32;
    } else {
      scanPar.stagingMode = string_dup(msm.lookup(getMaskStageMode()).c_str());
      if (getInnerLoopSwap()) {
	//scanPar.general.stageAdvanceFirst = 0; //?
      } else {
	//scanPar.general.stageAdvanceFirst = 1; //?
      }
      scanPar.maskStages = string_dup(mss.lookup(getMaskStageTotalSteps()).c_str());
      scanPar.nMaskStages = getMaskStageSteps();
    }
    scanPar.firstStage=m_firstStage;
    scanPar.stepStage=m_stepStage;
    scanPar.nLoops = 0;
    scanPar.scanLoop.length(nLoopTot);
    for (int i=0; i<nLoopTot; i++) {
      if (getLoopActive(i) && getDspProcessing(i)) {
	scanPar.nLoops++;
	scanPar.scanLoop[i].scanParameter = string_dup(sp.lookup(getLoopParam(i)).c_str());
	scanPar.scanLoop[i].nPoints = getLoopVarNSteps(i);
	//scanPar.scanLoop[i].dataPointsPtr = 0x0;
	scanPar.scanLoop[i].endofLoopAction.Action = string_dup(sa.lookup(PixLib::EnumScanAct::NO_ACTION).c_str());
	if (getDspLoopAction(i)) {
	  if (getLoopAction(i) == PixScan::SCURVE_FIT) {
	    //      scanPar.scanLoop[i].endofLoopAction.Action = SCAN_CALC_THRESH;
	    scanPar.scanLoop[i].endofLoopAction.Action = string_dup(sa.lookup(PixLib::EnumScanAct::FIT).c_str());
	    scanPar.scanLoop[i].endofLoopAction.fitFunction = string_dup(m_fitfun.c_str());
	  } else if (getLoopAction(i) == PixScan::TDAC_TUNING) {
	    scanPar.scanLoop[i].endofLoopAction.Action = string_dup(sa.lookup(PixLib::EnumScanAct::TUNE_THRESH).c_str());
	    scanPar.scanLoop[i].endofLoopAction.fitFunction = string_dup(ff.lookup(PixLib::EnumFitFunc::SCURVE).c_str());
	    scanPar.scanLoop[i].endofLoopAction.targetThreshold =  getThresholdTargetValue();
	  } else if (getLoopAction(i) == PixScan::NORMALIZE) {
	    scanPar.scanLoop[i].endofLoopAction.Action = string_dup(sa.lookup(PixLib::EnumScanAct::FIT).c_str());
	    scanPar.scanLoop[i].endofLoopAction.fitFunction = string_dup("NORMALIZE");
	  }
	}
	if (scanPar.scanLoop[i].nPoints > 0) {
	  //Add loop values at the very end
	  std::vector<float> loopvalues = getLoopVarValues(i);
	  scanPar.scanLoop[i].dataPoints.length(scanPar.scanLoop[i].nPoints);
	  for(unsigned int k=0;k<scanPar.scanLoop[i].nPoints;k++){
	    scanPar.scanLoop[i].dataPoints[k]= (int)loopvalues[k];
	  }
	}

      } else {
	scanPar.scanLoop[i].scanParameter = string_dup(sp.lookup(NO_PAR).c_str()); 
	scanPar.scanLoop[i].endofLoopAction.Action = string_dup(sa.lookup(PixLib::EnumScanAct::NO_ACTION).c_str());
	scanPar.scanLoop[i].endofLoopAction.fitFunction = string_dup("");
      }
    }
    //scanPar.general.configSet = getModConfig(); //?  
    scanPar.trigOpt.nEvents = getRepetitions();
    scanPar.trigOpt.moduleTrgMask = m_moduleTrgMask;
    scanPar.trigOpt.triggerMask = m_triggerMask;
    scanPar.trigOpt.deadtime = m_deadtime;
    scanPar.trigOpt.triggerDataOn = m_triggerDataOn;
    scanPar.trigOpt.nL1AperEvent = getConsecutiveLvl1TrigA(0);
    scanPar.trigOpt.nTriggersPerGroup = getConsecutiveLvl1TrigA(1);
    scanPar.trigOpt.nTriggers = m_nTriggers;
    scanPar.trigOpt.Lvl1_Latency = getLVL1Latency();
    scanPar.trigOpt.Lvl1_Latency_Secondary = m_LVL1Latency_Secondary;
    scanPar.trigOpt.strobeDuration = getStrobeDuration();
    scanPar.trigOpt.strobeMCCDelay = getStrobeMCCDelay();    
    scanPar.trigOpt.strobeMCCDelayRange = getStrobeMCCDelayRange();   
    scanPar.trigOpt.injectForTrigger = m_injectForTrigger;
    scanPar.trigOpt.hitbusConfig = m_hitbusConfig;
    //override
    scanPar.trigOpt.CalL1ADelay = getStrobeLVL1Delay();
    scanPar.trigOpt.eventInterval = m_eventInterval;
    scanPar.trigOpt.threshold=m_threshold;
    std::vector<std::string> optMask;
    if(m_totTargetCharge!=0 && m_useVcal==false){
      scanPar.trigOpt.vcal_charge = getTotTargetCharge();
      optMask.push_back(to.lookup(PixLib::EnumTriggOptions::SPECIFY_CHARGE_NOT_VCAL).c_str()); 
    } else {
      scanPar.trigOpt.vcal_charge = getFeVCal();
    }
    //getModScanConcurrent() //?
    //getFeHitbus() //?
    if (getDigitalInjection()) optMask.push_back(to.lookup(PixLib::EnumTriggOptions::DIGITAL_INJECT).c_str());
    else if (getChargeInjCapHigh()) {
      optMask.push_back(to.lookup(PixLib::EnumTriggOptions::USE_CHIGH).c_str());             
    } else {
      optMask.push_back(to.lookup(PixLib::EnumTriggOptions::USE_CLOW).c_str());             
    }
    if(m_oneByOne==true)optMask.push_back("ONE_BY_ONE");
    if(m_setupThreshold==true)optMask.push_back("SETUP_THRESHOLD");
    if(m_clearMasks==true)optMask.push_back("CLEAR_MASKS");
    if(m_protectFifo==true)optMask.push_back("PROTECT_FIFO");
    if(m_overrideLatency==true)optMask.push_back("OVERRIDE_LATENCY");
    scanPar.trigOpt.optionsMask.length(optMask.size());
    for(size_t i=0;i<optMask.size();i++){
      scanPar.trigOpt.optionsMask[i]=string_dup(optMask[i].c_str());
    }
  
    if (getSelfTrigger()) {
      scanPar.trigOpt.triggerMode = string_dup(tm.lookup(PixLib::EnumTriggMode::INTERNAL_SELF).c_str());
    } else if (m_mixedTrigger==true){
      scanPar.trigOpt.triggerMode = string_dup(tm.lookup(PixLib::EnumTriggMode::MIXED).c_str());
    } else {
      // More options needed here in PixScan
      scanPar.trigOpt.triggerMode = string_dup(tm.lookup(PixLib::EnumTriggMode::DSP).c_str());
    }

    scanPar.histos.length(m_histoNames.size());
    for(size_t i=0;i<m_histoNames.size();i++){
      scanPar.histos[i]=string_dup(m_histoNames[i].c_str());
    }
  }
  using json = nlohmann::json;
  void PixScan::toJSON(const ipc::ScanOptions& scanPar){
    json scan;
    scan["analysisType"]=m_analysisType;
    scan["totTargetValue"]=m_totTargetValue;
    scan["totTargetCharge"]=m_totTargetCharge;
    scan["thresholdTargetValue"]=m_thresholdTargetValue;
    json trigOpt;
    scan["name"]= scanPar.name ;
    scan["receiver"]= scanPar.receiver ; 
    scan["dataHandler"]= scanPar.dataHandler ; 
    scan["dataProc"]= scanPar.dataProc ; 
    scan["scanType"]= scanPar.scanType ; 
    scan["timeout.seconds"]= scanPar.timeout.seconds ; 
    scan["timeout.nanaseconds"]= scanPar.timeout.nanoseconds ; 
    scan["stagingMode"]= scanPar.stagingMode ;
    scan["nMaskStages"]= scanPar.nMaskStages ; 
    scan["maskStages"]= scanPar.maskStages ; 
    scan["firstStage"]= scanPar.firstStage ; 
    scan["stepStage"]= scanPar.firstStage ; 

    trigOpt["nEvents"]= scanPar.trigOpt.nEvents ;      
    trigOpt["moduleTrgMask"]= scanPar.trigOpt.moduleTrgMask ;      
    trigOpt["nL1AperEvent"]= (int) scanPar.trigOpt.nL1AperEvent ; 
    trigOpt["nTriggersPerGroup"]= (int) scanPar.trigOpt.nTriggersPerGroup ; 
    trigOpt["nTriggers"]= (int) scanPar.trigOpt.nTriggers ; 
    trigOpt["Lvl1_Latency"]= (int) scanPar.trigOpt.Lvl1_Latency ;
    trigOpt["Lvl1_Latency_Secondary"]= (int) scanPar.trigOpt.Lvl1_Latency_Secondary ;
    trigOpt["strobeDuration"]= scanPar.trigOpt.strobeDuration ;
    trigOpt["strobeMCCDelay"]= scanPar.trigOpt.strobeMCCDelay ;
    trigOpt["strobeMCCDelayRange"]= scanPar.trigOpt.strobeMCCDelayRange ;
    trigOpt["CalL1ADelay"]= scanPar.trigOpt.CalL1ADelay ;
    trigOpt["eventInterval"]= scanPar.trigOpt.eventInterval ;
    trigOpt["threshold"]=  scanPar.trigOpt.threshold ;
    trigOpt["triggerMode"]=  scanPar.trigOpt.triggerMode ;
    trigOpt["triggerMask"]=  scanPar.trigOpt.triggerMask ;
    trigOpt["triggerDataOn"]=  (int)scanPar.trigOpt.triggerDataOn ;
    trigOpt["injectForTrigger"]=  (int)scanPar.trigOpt.injectForTrigger ;
    trigOpt["hitbusConfig"]=  (int)scanPar.trigOpt.hitbusConfig ;
    trigOpt["deadtime"]=  scanPar.trigOpt.deadtime ;
    trigOpt["vcal_charge"]= scanPar.trigOpt.vcal_charge ;
    scan["trigOpt"]=trigOpt;
    scan["nLoops"]=scanPar.nLoops;
    json loops=json::array();
    for(int i=0;i<scanPar.nLoops;i++) {
      json scanLoop;
      scanLoop["scanParameter"]=scanPar.scanLoop[i].scanParameter;
      json ela;
      ela["Action"]=scanPar.scanLoop[i].endofLoopAction.Action;
      ela["fitFunction"]= scanPar.scanLoop[i].endofLoopAction.fitFunction;
      ela["targetThreshold"]= scanPar.scanLoop[i].endofLoopAction.targetThreshold;
      json npoints=json::array();
      scanLoop["nPoints"] = scanPar.scanLoop[i].nPoints ;
      
      for(unsigned int j=0;j<scanPar.scanLoop[i].nPoints;j++) {
	npoints.push_back(scanPar.scanLoop[i].dataPoints[j]);
      }
      scanLoop["dataPoints"]=npoints;
      loops.push_back(scanLoop);
    }
    json hist=json::array();
    for(unsigned i=0;i<scanPar.histos.seqsize();i++) {
      hist.push_back(scanPar.histos[i]);
    }
    scan["loop"]=loops;
    scan["histos"]=hist;
    std::cout <<scan.dump(2) << std::endl; 
  
  }

  void PixScan::dump(std::ostream &os, const ipc::ScanOptions& scanPar)
  {
    /* force decimal output */
    os <<std::dec<<"Run number = " << m_runnumber << std::endl;
    for (std::map<std::string, std::string>::iterator iter=m_formatterType.begin(); iter!=m_formatterType.end(); ++iter){
      os <<"Formatter["<<iter->first<<"] = " << iter->second << std::endl;
    }
    os << "Analysis = " << m_analysisType << std::endl;
    os << "ToT target value = " << m_totTargetValue << std::endl;
    os << "ToT target charge = " << m_totTargetCharge << std::endl;
    os << "Threshold target value = " << m_thresholdTargetValue << std::endl;
    os << "scanPar.name = " << scanPar.name << std::endl;
    os << "scanPar.receiver = " << scanPar.receiver << std::endl; 
    os << "scanPar.dataHandler = " << scanPar.dataHandler << std::endl; 
    os << "scanPar.dataProc = " << scanPar.dataProc << std::endl; 
    os << "scanPar.scanType = " << scanPar.scanType << std::endl; 
    os << "scanPar.timeout.seconds = " << scanPar.timeout.seconds << std::endl; 
    os << "scanPar.timeout.nanaseconds = " << scanPar.timeout.nanoseconds << std::endl; 
    os << "scanPar.stagingMode = " << scanPar.stagingMode << std::endl;
    os << "scanPar.nMaskStages = " << scanPar.nMaskStages << std::endl; 
    os << "scanPar.maskStages = " << scanPar.maskStages << std::endl; 
    os << "scanPar.firstStage = " << scanPar.firstStage << std::endl; 
    os << "scanPar.stepStage = " << scanPar.firstStage << std::endl; 
    os << "scanPar.trigOpt.nEvents = " << scanPar.trigOpt.nEvents << std::endl;      
    os << "scanPar.trigOpt.moduleTrgMask = " << scanPar.trigOpt.moduleTrgMask << std::endl;      
    os << "scanPar.trigOpt.nL1AperEvent = " << (int) scanPar.trigOpt.nL1AperEvent << std::endl; 
    os << "scanPar.trigOpt.nTriggersPerGroup = " << (int) scanPar.trigOpt.nTriggersPerGroup << std::endl; 
    os << "scanPar.trigOpt.nTriggers = " << (int) scanPar.trigOpt.nTriggers << std::endl; 
    os << "scanPar.trigOpt.Lvl1_Latency = " << (int) scanPar.trigOpt.Lvl1_Latency << std::endl;
    os << "scanPar.trigOpt.Lvl1_Latency_Secondary = " << (int) scanPar.trigOpt.Lvl1_Latency_Secondary << std::endl;
    os << "scanPar.trigOpt.strobeDuration = " << scanPar.trigOpt.strobeDuration << std::endl;
    os << "scanPar.trigOpt.strobeMCCDelay = " << scanPar.trigOpt.strobeMCCDelay << std::endl;
    os << "scanPar.trigOpt.strobeMCCDelayRange = " << scanPar.trigOpt.strobeMCCDelayRange << std::endl;
    os << "scanPar.trigOpt.CalL1ADelay = " << scanPar.trigOpt.CalL1ADelay << std::endl;
    os << "scanPar.trigOpt.eventInterval = " << scanPar.trigOpt.eventInterval << std::endl;
    os << "scanPar.trigOpt.threshold = " <<  scanPar.trigOpt.threshold << std::endl;
    os << "scanPar.trigOpt.triggerMode = " <<  scanPar.trigOpt.triggerMode << std::endl;
    os << "scanPar.trigOpt.triggerMask = " <<  scanPar.trigOpt.triggerMask << std::endl;
    os << "scanPar.trigOpt.triggerDataOn = " <<  (int)scanPar.trigOpt.triggerDataOn << std::endl;
    os << "scanPar.trigOpt.injectForTrigger = " <<  (int)scanPar.trigOpt.injectForTrigger << std::endl;
    os << "scanPar.trigOpt.hitbusConfig = " <<  (int)scanPar.trigOpt.hitbusConfig << std::endl;
    os << "scanPar.trigOpt.deadtime = " <<  scanPar.trigOpt.deadtime << std::endl;
    os << "scanPar.trigOpt.vcal_charge = " << scanPar.trigOpt.vcal_charge << std::endl;
    for(unsigned i=0;i<scanPar.trigOpt.optionsMask.seqsize();i++)
      os << "scanPar.trigOpt.optionsMask["<<i<<"] = " << scanPar.trigOpt.optionsMask[i] << std::endl;
 
    os << "scanPar.nLoops = " << (int)scanPar.nLoops << std::endl; 
    for(int i=0;i<scanPar.nLoops;i++) {
      os << "scanPar.scanLoop["<<i<<"].scanParameter = " << scanPar.scanLoop[i].scanParameter << std::endl;
      os << "scanPar.scanLoop["<<i<<"].endofLoopAction.Action = " << scanPar.scanLoop[i].endofLoopAction.Action << std::endl;
      os << "scanPar.scanLoop["<<i<<"].endofLoopAction.fitFunction = " << scanPar.scanLoop[i].endofLoopAction.fitFunction << std::endl;
      os << "scanPar.scanLoop["<<i<<"].endofLoopAction.targetThreshold = " << scanPar.scanLoop[i].endofLoopAction.targetThreshold << std::endl;
      os << "scanPar.scanLoop["<<i<<"].nPoints = " << scanPar.scanLoop[i].nPoints << std::endl;
      for(unsigned int j=0;j<scanPar.scanLoop[i].nPoints;j++) {
	os << "scanPar.scanLoop["<<i<<"].dataPoints["<<j<<"] = " << scanPar.scanLoop[i].dataPoints[j] <<std::endl;
      }
    }
    for(unsigned i=0;i<scanPar.histos.seqsize();i++)
      os << "scanPar.histos["<<i<<"] = " << scanPar.histos[i] << std::endl;
  }
}
