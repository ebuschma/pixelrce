#ifndef RCFCONTROLLER_HH
#define RCFCONTROLLER_HH

#include <boost/version.hpp>
#include <RCF/RCF.hpp>
#include "server/AbsController.hh"
#include "config/PixelConfig.hh"
#include "RCFScanRootAdapter.hh"
#include "RCFScanAdapter.hh"
#include "RCFConfigIFAdapter.hh"
#include <vector>
#include <map>

class RCFCallback;
class CallbackInfo;
namespace ipc{
  class ScanOptions;
}

class RCFController: public AbsController{
public:
  RCFController();
  void addRce(int rce);
  
  void downloadModuleConfig(int rce, int id, PixelConfig* config);
  void addModule(const char* name, const char* type, int id, int inLink, int outLink, int rce, const char* formatter);
  int setupTrigger(const char* type="default");
  void removeAllModules();
  void resetFE();
  void configureModulesHW();
  void configureModuleHW(int rce, int outlink);

  unsigned writeHWglobalRegister(const char* name, int reg, unsigned short val);
  unsigned readHWglobalRegister(const char* name, int reg, unsigned short& val);
  unsigned writeHWdoubleColumn(const char* name, unsigned bit, unsigned dcol, std::vector<unsigned> data, std::vector<unsigned> &retvec);
  unsigned readHWdoubleColumn(const char* name, unsigned bit, unsigned dcol, std::vector<unsigned> &retvec);

  unsigned writeHWregister(int rce, unsigned addr, unsigned val);
  unsigned readHWregister(int rce, unsigned addr, unsigned& val);
  unsigned sendHWcommand(int rce, unsigned char opcode);
  unsigned writeHWblockData(int rce, std::vector<unsigned> &data);
  unsigned readHWblockData(int rce, std::vector<unsigned> &data, std::vector<unsigned>& retvec);
  unsigned readHWbuffers(int rce, std::vector<unsigned char>& retvec);
  
  void downloadScanConfig(ipc::ScanOptions &scn);
  int verifyModuleConfigHW(int rce, int id);
  
  void waitForScanCompletion(ipc::Priority pr, CallbackInfo* callb);
  void runScan(ipc::Priority pr, CallbackInfo* callb);
  void startScan();
  void abortScan();
  void stopWaitingForData();
  void getEventInfo(unsigned* nevent);
  int getScanStatus();
  unsigned getNEventsProcessed();
  void resynch();
  void setupParameter(const char* par, int val);
  void setupMaskStage(int stage);

};

#endif
