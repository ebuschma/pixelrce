#include "server/ServerFWRegisters.hh"
#include "server/AbsController.hh"
#include "util/exceptions.hh"
#include <iostream>
void ServerFWRegisters::writeRegister(int rce, unsigned reg, unsigned val){
  int badHSIOconnection=m_controller->writeHWregister(rce, reg, val);
  if(badHSIOconnection){
    std::cout << "Serever writeRegister: reg= "<<reg<< " val= " << val << std::endl;
    rcecalib::Pgp_Problem err;
    throw err;
  }
}
unsigned ServerFWRegisters::readRegister(int rce, unsigned reg){
  unsigned val;
  int badHSIOconnection=m_controller->readHWregister(rce, reg, val);
  if(badHSIOconnection){
    std::cout << "Server readRegister: reg= "<<reg<< " val= " << val << std::endl;
    rcecalib::Pgp_Problem err;
    //throw err;
  }
  return val;
}
void ServerFWRegisters::sendCommand(int rce, unsigned opcode){
    m_controller->sendHWcommand(rce, opcode);
}
