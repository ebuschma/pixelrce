#include "rcecalib/server/CosmicMonitoringGui.hh"
#include "rcecalib/server/CosmicMonitoringGuiEmbedded.hh"
#include "TApplication.h"
#include "TGMsgBox.h"
#include "TH1.h"
#include "TGIcon.h"
#include "TGMenu.h"
#include "TCanvas.h"
#include "TGCanvas.h"
#include "TGListTree.h"
#include "TRootEmbeddedCanvas.h"
#include <TGFileDialog.h>
#include <TROOT.h>
#include <TStyle.h>
#include <assert.h>
#include <iostream>
#include <time.h>
#include <sys/stat.h> 
#include <fstream>
#include <list>
#include <pthread.h>
#include <vector>
#include "eudaq/Event.hh"
#include "config/FormattedRecord.hh"
#include "eudaq/DetectorEvent.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/FileSerializer.hh"
#include "eudaq/counted_ptr.hh"


  

CosmicMonitoringGui::~CosmicMonitoringGui(){
}

CosmicMonitoringGui::CosmicMonitoringGui(const char* filename, int nevt, const TGWindow *p,UInt_t w,UInt_t h)
  : TGMainFrame(p,w,h) {
  
  // connect x icon on window manager
  Connect("CloseWindow()","CosmicMonitoringGui",this,"quit()");

  TGMenuBar *menubar=new TGMenuBar(this,1,1,kHorizontalFrame | kRaisedFrame);
  TGLayoutHints *menubarlayout=new TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0);
  // menu "File"
  TGPopupMenu* filepopup=new TGPopupMenu(gClient->GetRoot());
  //filepopup->AddEntry("&Load Config",LOAD);
  //filepopup->AddEntry("&Save Config",SAVE);
  filepopup->AddSeparator();
  filepopup->AddEntry("&Quit",QUIT);
  menubar->AddPopup("&File",filepopup,menubarlayout);

  AddFrame(menubar, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0,0,0,2));
  
  filepopup->Connect("Activated(Int_t)","CosmicMonitoringGui",this,"handleFileMenu(Int_t)");

  embededGui = new CosmicMonitoringGuiEmbedded(filename, nevt, this,1,1,kChildFrame);
  AddFrame(embededGui,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
  
  SetWindowName("Monitoring GUI");
  Resize(w,h);
  Layout();
  MapSubwindows();
  MapWindow();
}
void CosmicMonitoringGui::quit(){
  gApplication->Terminate(0);
}


void CosmicMonitoringGui::handleFileMenu(int item){
  if(item==QUIT)quit();
}
  
//====================================================================
int main(int argc, char **argv){
  //
  // Initialize command line parameters with default values
//
// Declare command object and its argument-iterator
//       
//

  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);
  const char *filename=argv[1];
  std::cout<<"Filename "<<filename<<std::endl;
  int nevt=10000000;
  if(argc==3)nevt=atoi(argv[2]);
  std::cout<<"Nevt "<<nevt<<std::endl;
  TApplication theapp("app",&argc,argv);
  new CosmicMonitoringGui(filename, nevt, gClient->GetRoot(),800,600);
  theapp.Run();
  return 0;
}


