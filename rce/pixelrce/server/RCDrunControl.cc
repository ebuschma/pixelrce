
#include "PixelFEI4GenConfig.hh"
#include "config/FormatterFactory.hh"
#include "config/AbsFormatter.hh"
#include "config/ModuleInfo.hh"
#include "util/IsMonitoring.hh"
#include "server/GlobalConfigBase.hh"
#include "server/ServerFWRegisters.hh"
#include "server/AbsController.hh"
#include "server/RCDrunControl.hh"
#include "server/PixScan.hh"
#include <iostream>
#include <map>
//#include <pthread.h>
//#include <boost/property_tree/ptree.hpp>

using namespace RCE;

class GlobalConfigBase;
//class PixScan;

RCDrunControl::RCDrunControl(AbsController& acontroller, IPCPartition &p) : 
  m_controller(acontroller), m_globalconf(0), m_partition(p){
  //temporarily hardcoded directoriy and params as default
  //Will get them from a configuration file/OKS
  m_normalization=1;
  m_globalconfdir="/det/afp/configs/moduleconfigs/";
  m_globalconfname="top/sr1__1387.cfg";
  Latency=10;
  SecLatency=10;
  ConsecLvl1TrigA=8; //bunch crossings per L1A
  TriggerMask=32; //2: random triggers 16: hitbus 32: TTC
  EventInterval=4e7; 
  StrobeLVL1Delay=98;
  DeadTime=4;
  Ntrg=ConsecLvl1TrigA; //triggers per L1A
  m_cdtnbuf=15; // if m_cdt* = 0, no complex deadtime is applied
  m_cdtwindow=350;
  EnableCDT = true;
  EventNormalization=1000;
  RunNumber=0xabcdef;
  RodId=0x850001;
  EnableMonitoring=false;
  EfbTimeout=40;
  EfbTimeoutFirst=800;
  EfbMissingHeaderTimeout=100;
  EnableTtcSim=false;
  BlockEcrBcr=0;
  OutputBusy=true;
  DisableInternalBusy=true;
  EnableEcrReset=true;
  BcrBusyParam1=3543;
  BcrBusyParam2=20;
  EnableBcrBusy=true;
  EnableSLinkBlowoff=false;
  BcidOffset=18;
  TimingScan=false;
  EventSizeLimit=2048;
  QuickStatus=true;
  QuickStatusAuto=false;
  m_timingScanRunning=false;
  m_timingNumberOfSteps=0;
  m_currentStep=0;
}

RCDrunControl::~RCDrunControl(){
    std::cout << "Entering RCDrunControl destructor..." << std::endl;
    //  m_controller.removeAllRces();
    //  m_controller.removeAllModules();

    /*m_controller.stopWaitingForData();
  usleep(100000);
  m_controller.resetFE(); //also sweeps events stuck in the buffer                                              

  delete  m_globalconf  ;
  for(int i = 0; i< GlobalConfigBase::MAX_MODULES ;i ++)
    delete  m_config[i] ;
  */
}

//read all the configurations and get vaules from files.
// return number of configurations
int RCDrunControl::Init(){

  for(std::vector<Monitoring*>::iterator it=m_monitoring.begin(); it!=m_monitoring.end(); it++){
    delete *it;
  }
  m_monitoring.clear();
  m_outlink.clear();
  delete m_globalconf;
  m_globalconf = new GlobalConfigBase(m_globalconfdir.c_str(),m_globalconfname.c_str());

  ServerFWRegisters fw(&m_controller);
  rcemap.clear();
  m_controller.removeAllRces();
  for(configIterator it=m_globalconf->begin(); it!=m_globalconf->end(); it++){
   ConfigBase* config=(*it);
   unsigned rce=config->getRce();
   if(rcemap.find(rce)==rcemap.end()){ //new RCE
     m_controller.addRce(rce);
     //initialize IS monitoring
     m_monitoring.push_back(new IsMonitoring(m_partition, "Monitoring", rce));
     m_monitoring.back()->Reset(); //clear everything in IS
     m_monitoring.back()->Publish(); 
     rcemap[rce]=1; //"This rce is included"
   }
  }
  m_controller.removeAllModules();

  // Add all included Rces in m_controller
  for(configIterator it=m_globalconf->begin(); it!=m_globalconf->end(); it++){
    ConfigBase* config=(*it);
    unsigned rce=config->getRce();
    unsigned outlink=config->getOutlink();
    if(std::string(config->getModuleConfig()->getType())!="Hitbus")m_outlink[rce].push_back(outlink);
    unsigned inlink=config->getInlink();     
    int id=config->getModuleConfig()->getId();
    const char* modname=config->getModuleConfig()->getName();
    std::string fetype=config->getModuleConfig()->getType();
    std::string formatter=""; //TEST
    fw.setOutputDelay(rce, inlink, config->getPhase());

    printf("%s: addModule (%s, %d, %d, %d, %d, %s)\n",fetype.c_str(), modname, id, inlink, outlink, rce,formatter.c_str());

    m_controller.addModule(modname, fetype.c_str(),id, inlink, outlink, rce, formatter.c_str());
    PixelConfig *cfg=config->getModuleConfig();
    m_controller.downloadModuleConfig(rce, id ,cfg);
  }

  return m_globalconf->size();
}

//Configure all modules
// return 1 if all OK
int RCDrunControl::Config(){
  //  StopRun();

  ServerFWRegisters fw(&m_controller);
  unsigned goodHSIOconnection;
  m_permanentmask.clear();
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    m_permanentmask[rce]=0;
    fw.setTriggermask(rce, TriggerMask);
    fw.setMode(rce, FWRegisters::NORMAL);
    //fw.setL1Type(rce, FWRegisters::SINGLE);
    //fw.enableSLinkBlowoff(rce, false); //false unless runing with ROS

    //Discriminator Delays


    if(EnableCDT){
      fw.enableComplexDeadtime(rce, true);
      fw.setComplexDeadtimeParams(rce,m_cdtnbuf, m_cdtwindow);
    }else{
      fw.enableComplexDeadtime(rce, false);
    }

    fw.setEncoding(rce, FWRegisters::BIPHASEMARK); //BIPHASEMARK when using optical fibre
    fw.enableSLinkBlowoff(rce, EnableSLinkBlowoff);
    fw.enableMonitoring(rce, EnableMonitoring);
    fw.setNExp(rce,Ntrg);
    std::cout<<"Setting Timeout on RCE "<<rce<<" to "<<EfbTimeout<<std::endl;
    fw.setRodId(rce, RodId);
    fw.setDisableMask(rce, 0);
    fw.setEfbTimeout(rce,EfbTimeout);
    fw.setEfbTimeoutFirst(rce,EfbTimeoutFirst);
    fw.setEfbMissingHeaderTimeout(rce,EfbMissingHeaderTimeout);
    fw.enableTtcSim(rce,EnableTtcSim);
    fw.blockEcrBcr(rce,BlockEcrBcr);
    //    fw.resetL1id(rce);
    fw.outputBusy(rce, OutputBusy);
    fw.disableInternalBusy(rce, DisableInternalBusy);
    fw.setOccNormalization(rce, EventNormalization);
    if(EventNormalization!=0)
      m_normalization=ipc::IPC_N_I4_PIXEL_COLUMNS*ipc::IPC_N_I4_PIXEL_ROWS*EventNormalization;
    fw.enableEcrReset(rce,EnableEcrReset);
    fw.setBcrBusyParams(rce,BcrBusyParam1,BcrBusyParam2);
    fw.enableBcrBusy(rce, EnableBcrBusy); // Set false for ATLAS running
    fw.setBcidOffset(rce, BcidOffset);
    fw.setEventSizeLimit(rce, EventSizeLimit);
    //    fw.pauseECR(rce, false);

  }

  PixScan scn(PixScan::ATLAS_DATA, PixLib::EnumFEflavour::PM_FE_I2);
  
  ipc::ScanOptions options;
  
  //  SET UP SCAN  //
  
  // Override l1a latency
  scn.setLVL1Latency(Latency);
  scn.setSecondaryLatency(SecLatency);
  // Set number of triggers per L1A
  scn.setConsecutiveLvl1TrigA(0,ConsecLvl1TrigA);
  scn.setConsecutiveLvl1TrigA(1,ConsecLvl1TrigA);
  scn.setTriggerMask(TriggerMask);
  //scn.setHitbusConfig(hitbusconfig);
  scn.setEventInterval(EventInterval);
  scn.setStrobeLVL1Delay(StrobeLVL1Delay);
  scn.setDeadtime(DeadTime);
  scn.convertScanConfig(options);

  //////////////////////////////////////////////

  m_controller.setupTrigger(scn.getTriggerType());
  m_controller.downloadScanConfig(options);

  return 1;
}

int RCDrunControl::UnConfig(){
  ServerFWRegisters fw(&m_controller);
  m_controller.removeAllModules();
  m_controller.removeAllRces();
  return 1;
}

int RCDrunControl::StartRun(){
  ServerFWRegisters fw(&m_controller);
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    fw.setRunNumber(rce,RunNumber);
  }
  m_controller.startScan();
  usleep(1000000);
  return 1;
}
void RCDrunControl::SetECRpreset(unsigned aECRpreset){  
  ServerFWRegisters fw(&m_controller);
  std::cout << "Pre-setting ECR " << aECRpreset << std::endl;
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    fw.pauseECR(rce, true);
  }
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    fw.presetECR(rce, aECRpreset);
  }
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    fw.pauseECR(rce, false);
  }
}

int RCDrunControl::StopRun(){
  m_controller.stopWaitingForData();
  usleep(100000);
  m_controller.resetFE(); //also sweeps events stuck in the buffer                                              
  std::cout<<"StopRun"<< std::endl;
  return 1;
}

bool RCDrunControl::isAtlasClockPresent(){
  bool returnVal = true;
  ServerFWRegisters fw(&m_controller);
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    if(it->second==1 && !(fw.atlasClockPresent(rce)))
      returnVal=false;
  }
  return returnVal;
}

bool RCDrunControl::isGbtStatusOK(){
  bool returnVal = true;
  ServerFWRegisters fw(&m_controller);
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    if(it->second==1 && !(fw.getGbtStatus(rce)!=0x3F)){
      std::cout<<"GBT Status on RCE "<<rce<<" is "<<std::hex<<fw.getGbtStatus(rce)<<std::dec<<std::endl;
      returnVal=false;
    }
  }
  return returnVal;
}

bool RCDrunControl::isExternalClockSet(){
  bool returnVal = true;
  ServerFWRegisters fw(&m_controller);
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    if(it->second==1 && !(fw.externalClockSelected(rce))) //externalClockSelected
      returnVal=false;
  }
  return returnVal;
}

void RCDrunControl::ResetL1id(){
  ServerFWRegisters fw(&m_controller);
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    fw.resetL1id(rce);
  }
}

void RCDrunControl::publish(){
  const char* fenames[]={"NS-A-P0", "NS-A-P1", "NS-A-P2", "NS-A-P3", "", "", "", "",
                         "FS-A-P0", "FS-A-P1", "FS-A-P2", "FS-A-P3", "HPTDC-A-1", "HPTDC-A-2", "", "",
			 "NS-C-P0", "NS-C-P1", "NS-C-P2", "NS-C-P3", "", "", "", "",
                         "FS-C-P0", "FS-C-P1", "FS-C-P2", "FS-C-P3", "HPTDC-C-1", "HPTDC-C-2", "", ""};
  if(TimingScan==true)timingScan();
  if(QuickStatus==true)quickStatus();
  ServerFWRegisters fw(&m_controller);
  for(std::vector<Monitoring*>::iterator it=m_monitoring.begin(); it!=m_monitoring.end(); it++){
    Monitoring *mon=(*it);
    int rce=mon->GetRce();
    unsigned nhits=0;
    unsigned mask=fw.getDisabledMask(rce);
    unsigned maskperm=fw.getDisabledPermMask(rce);
    for(int i=0;i<16;i++){
      int bit=1<<i;
      if((maskperm&bit)!=0 && (m_permanentmask[rce]&bit)==0){
	char msg[128];
	sprintf(msg, "The frontend with outlink %d on RCE %d (%s) has been disabled.", i, rce, fenames[(rce-1)*16+i]);
	ERS_REPORT_IMPL( ers::warning, ers::Message, msg, );
	m_permanentmask[rce]|=bit;
      }
    }
    for(int i=0;i<m_outlink[rce].size();i++){
      for(int j=0;j<FWRegisters::RECONFIGCOUNTER;j++){
	if(j==FWRegisters::OCCUPANCY)continue; //Occupancy
	unsigned counter=fw.getEfbCounter(rce, m_outlink[rce][i], (FWRegisters::EFBCOUNTER)j);
	if((FWRegisters::EFBCOUNTER)j==FWRegisters::ECRRESET){
	  unsigned oldcounter=mon->GetErrorCounter(m_outlink[rce][i], FWRegisters::ECRRESET);
	  if(counter>oldcounter){
	    char msg[128];
	    sprintf(msg, "An ECR reset has occurred on outlink %d on RCE %d.", m_outlink[rce][i], rce);
	    ERS_REPORT_IMPL( ers::info, ers::Message, msg, );
	  }
	}
	mon->SetErrorCounter(m_outlink[rce][i], counter, (FWRegisters::EFBCOUNTER)j);
      }
      unsigned hitsmod=fw.getEfbCounter(rce,m_outlink[rce][i], FWRegisters::OCCUPANCY); 
      if((mask&(1<<m_outlink[rce][i]))==0)nhits+=hitsmod;
      mon->SetOccupancy(m_outlink[rce][i], (float)hitsmod/m_normalization);
      //std::cout<<m_outlink[rce][i]<<": "<<hitsmod<<std::endl;
    }
    for(int j=0;j<3;j++)mon->SetTtcClashCounter(fw.getTtcClashCounter(rce, (FWRegisters::CLASH)j), (FWRegisters::CLASH)j);
    for(int i=0;i<2;i++){ // 2 HPTDCs
      for(int j=0;j<8;j++){ // 8 monitoring quantities
	mon->SetHptdcInfo(i, j, fw.getHPTDCrecord(rce, i, j));
      }
    }
    mon->SetAverageOccupancy((float)nhits/m_normalization/(float)m_outlink[rce].size());
    //std::cout<<"All: "<<nhits<<std::endl;
    mon->SetNumberOfEvents(fw.getNumberOfEvents(rce));
    mon->SetNumberOfCleanEvents(fw.getNumberOfCleanEvents(rce));
    mon->SetNMissed(fw.getNumberOfMonMissed(rce));
    mon->SetDisabledMask(mask);
    mon->SetDisabledPermMask(maskperm);
    mon->SetDisabledInRunMask(fw.getDisabledInRunMask(rce));
    mon->SetTtcBusyCounter(fw.getTtcBusyCounter(rce));
    mon->SetTimingScanInfo(TimingScan, m_timingScanRunning, m_currentStep, m_timingNumberOfSteps);
    mon->Publish();
  }
}

void RCDrunControl::timingScan(){
  ServerFWRegisters fw(&m_controller);
  ISInfoDictionary dict(m_partition);
  ISInfoBool runscan;
  try{
    dict.getValue("Setup.AFPTimingScanRun", runscan);
  }catch(...){
    return;
  }
  if(runscan==0)m_currentStep=0;
  if(runscan==true && m_timingScanRunning==false && m_currentStep==0){ //scan is starting
    try{
      dict.getValue("Setup.AFPTimingScanStart", m_timingStart);
      dict.getValue("Setup.AFPTimingScanStep", m_timingStep);
      dict.getValue("Setup.AFPTimingScanNStep", m_timingNumberOfSteps);
      dict.getValue("Setup.AFPTimingScanDuration", m_timingStepDuration);
    }catch(...){
      std::cout<<"AFP timing scan parameters not correctly defined."<<std::endl;
      return;
    }
    std::cout<<"Starting timing scan with the following parameters:"<<std::endl;
    std::cout<<"Start (relative to current setting) = "<<m_timingStart<<std::endl;
    std::cout<<"Step = "<<m_timingStep<<std::endl;
    std::cout<<"Number of steps = "<<m_timingNumberOfSteps<<std::endl;
    std::cout<<"Duration per step = "<<m_timingStepDuration<<std::endl;
    m_timingScanRunning=true;
    m_currentStep=0;
    m_durationCounter=0;
    m_origDelays.clear();
    
    for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
      int rce=it->first;
      fw.forceBusy(rce, true);
    }
    usleep(1000);
    //save original phases and set new ones
    for(configIterator it=m_globalconf->begin(); it!=m_globalconf->end(); it++){
      ConfigBase* config=(*it);
      if(std::string(config->getModuleConfig()->getType())=="FEI4B"){
        m_origDelays[config->getRce()*1000+config->getInlink()]=config->getPhase();
        fw.setOutputDelay(config->getRce(), config->getInlink(), config->getPhase()+m_timingStart);
      }
    }
    for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
      int rce=it->first;
      m_controller.sendHWcommand(rce, 0x10); //recalibrate phase
      m_controller.sendHWcommand(rce, 9); //clear FIFOs
      fw.setDetEvType(rce, FWRegisters::TIMING);
      fw.forceBusy(rce, false);
    }
  }else if(runscan==false && m_timingScanRunning==true){ //scan was aborted
    std::cout<<"Timing scan was aborted."<<std::endl;
    terminateScan();
  }else if(runscan==true && m_timingScanRunning==true){ //scan is running
    m_durationCounter++;
    if(m_durationCounter>=m_timingStepDuration){//next step
      m_currentStep++;
      if(m_currentStep>=m_timingNumberOfSteps){//done
	std::cout<<"Timing scan has finished."<<std::endl;
	runscan=false;
	dict.checkin("Setup.AFPTimingScanRun", runscan);
	terminateScan();
      }else{ //next step
	for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
	  int rce=it->first;
	  fw.forceBusy(rce, true);
	}
	usleep(1000);
	for(configIterator it=m_globalconf->begin(); it!=m_globalconf->end(); it++){
	  ConfigBase* config=(*it);
          if(std::string(config->getModuleConfig()->getType())=="FEI4B"){
	    fw.setOutputDelay(config->getRce(), config->getInlink(), m_origDelays[config->getRce()*1000+config->getInlink()]+m_timingStart+m_currentStep*m_timingStep);
	    std::cout<<"Timing scan: Delay for FE with inlink "<<config->getInlink()<<" set to "<<m_origDelays[config->getRce()*1000+config->getInlink()]+m_timingStart+m_currentStep*m_timingStep<<" in step "<<m_currentStep+1<<" of "<<m_timingNumberOfSteps<<"."<<std::endl;
          }
	}
	for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
	  int rce=it->first;
	  m_controller.sendHWcommand(rce, 0x10); //recalibrate phase
	  m_controller.sendHWcommand(rce, 9); //clear FIFOs
	  fw.setDetEvType(rce, FWRegisters::TIMING+m_currentStep);
	  fw.forceBusy(rce, false);
	}
	m_durationCounter=0;
      }
    }
  }
}

void RCDrunControl::terminateScan(){
  ServerFWRegisters fw(&m_controller);
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    fw.forceBusy(rce, true);
  }
  usleep(1000);
  //restore original phases
  for(configIterator it=m_globalconf->begin(); it!=m_globalconf->end(); it++){
    ConfigBase* config=(*it);
    if(std::string(config->getModuleConfig()->getType())=="FEI4B"){
      fw.setOutputDelay(config->getRce(), config->getInlink(), m_origDelays[config->getRce()*1000+config->getInlink()]);
    }
  }
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    m_controller.sendHWcommand(rce, 0x10); //recalibrate phase
    m_controller.sendHWcommand(rce, 9); //clear FIFOs
    fw.setDetEvType(rce, FWRegisters::PHYSICS);
    fw.forceBusy(rce, false);
  }
  m_timingScanRunning=false;
}

void RCDrunControl::quickStatus(){
  if(EnableEcrReset==false){
    std::cout<<"Quick status does not work with EnableEcrReset=false. Skipping."<<std::endl;
    return;
  }
  ServerFWRegisters fw(&m_controller);
  if(QuickStatusAuto==false){
    ISInfoDictionary dict(m_partition);
    ISInfoBool reconfigure;
    ISInfoInt channel;
    try{
      dict.getValue("Setup.QSreconfigure", reconfigure);
      dict.getValue("Setup.QSchannel", channel);
    }catch(...){
      std::cout<<"Could not read QS values."<<std::endl;
      return;
    }
    if(reconfigure==true){
      std::cout<<"Reconfigure is true."<<std::endl;
      for(configIterator it=m_globalconf->begin(); it!=m_globalconf->end(); it++){
	ConfigBase* config=(*it);
	if(channel/1000==config->getRce() && channel%1000 == config->getOutlink()){
	  reconfigureModule(config->getRce(), config->getOutlink());
	  reconfigure=false;
	  dict.checkin("Setup.QSreconfigure", reconfigure);
	  break;
	}
      }
    }
  }else{ //do automatic check
  }
}

void RCDrunControl::reconfigureModule(int rce, int outlink){
  std::cout<<"Reconfiguring RCE "<<rce<<" channel "<<outlink<<std::endl;
  ServerFWRegisters fw(&m_controller);
  fw.forceBusy(rce, true);
  usleep(1000); //wait for 1 ms so the buffers are clear
  m_controller.configureModuleHW(rce, outlink);
  //fw.reenableFe(rce, outlink);
  fw.modifyDisableMask(rce, outlink, 0); //reenable frontend 
  fw.forceBusy(rce, false);
  for(std::vector<Monitoring*>::iterator it=m_monitoring.begin(); it!=m_monitoring.end(); it++){
    Monitoring *mon=(*it);
    if(mon->GetRce()==rce)mon->SetErrorCounter(outlink, mon->GetErrorCounter(outlink, FWRegisters::RECONFIGCOUNTER)+1, FWRegisters::RECONFIGCOUNTER);
  }
}
