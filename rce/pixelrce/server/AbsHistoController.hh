#ifndef ABSHISTOCONTROLLER_HH
#define ABSHISTOCONTROLLER_HH

#include <vector>

#include <TH1.h>


class AbsHistoController{
public:
  AbsHistoController() {};
  virtual void addRce(int rce)=0;
  virtual void removeAllRces()=0;
  
  virtual std::vector<TH1*> getHistos(const char* reg)=0; 
  virtual std::vector<std::string> getHistoNames(const char* reg)=0;
  virtual std::vector<std::string> getPublishedHistoNames()=0;

  virtual bool clear()=0;

  std::vector<int> m_rces;
};

#endif
