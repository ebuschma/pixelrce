#include <config/FEI4/FEI4BConfigFile.hh>
#include <util/json.hpp>
#include <string>
int main(int argc,char *argv[])
{
  if(argc!=2) {std::cerr << "usage: dumpModConfig <configFile>\n"; exit(-1);}
  ipc::PixelFEI4BConfig  config;
  std::string filename(argv[1]);
  bool is_json= (5 <= filename.size() && filename.find(".json", filename.size() - 5) != filename.npos);
  //  std::cout << is_json << std::endl;
  FEI4BConfigFile f;
  f.readModuleConfig(&config,filename);
  json j;
  if(!is_json) {
    f.toJSON(config,j);
    std::cout << j.dump(4) << std::endl;
  } else {
    f.dump(config);
    // f.writeModuleConfig(&config,"test","config","testconfig","42");
  }  
  //  f.dump(config);
  return 0;


}
