//
//      testslink.cc
//
//      Test program for sending slink frames.
//
//      Martin Kocian
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include "server/PgpModL.hh"
#include "HW/SerialPgpFei4.hh"
#include "HW/RCDImasterL.hh"
#include <iostream>

//////////////////////////////////////////
//
// Main function
//
//////////////////////////////////////////


int main ( int argc, char ** argv )
{
 
   //Serial IF
   PgpModL pgpl;
   pgpl.open();
   PgpTrans::RCDImaster* pgp= PgpTrans::RCDImaster::instance();
   new SerialPgpFei4;
   unsigned retVar[1024];
   int eventCnt=0;
   const int TIME_SLICE_SIZE_C=4;
   const int CHANNEL_SIZE_C=16; 
   const int HEADER_SIZE_C=9; 
   const int PAYLOAD_SIZE_C = TIME_SLICE_SIZE_C*CHANNEL_SIZE_C;
   const int FOOTER_SIZE_C=4;
   const int SIZE_C=(HEADER_SIZE_C+PAYLOAD_SIZE_C+FOOTER_SIZE_C);

   // ROD header marker
   retVar[0] = 0xEE1234EE;
   // Size of header always 9
   retVar[1] = 0x00000009;
   // Format version
   retVar[2] = 0x03010000;
   //  CSC source ID, slot 10
   retVar[3] = 0x0069000A;
   //  runNumber
   retVar[4] = 0x00000000;
   // Level 1 ID
   retVar[5] = eventCnt;
   // Bunch crossing - some marker for now
   retVar[6] = 0x11111111;
   // Trigger type - some pattern
   retVar[7] = 0x0D0D0D0D;
   // Detector event type - some pattern
   retVar[8] = 0xB0b0B0B0;
   ////////////////
   // End of Header
   ////////////////
   
   //////////////////-
   // Start of Payload
   //////////////////-
   for(int i=0;i<TIME_SLICE_SIZE_C;i++){
     for(int j=0;j<CHANNEL_SIZE_C;j++){
       retVar[i*CHANNEL_SIZE_C+j+HEADER_SIZE_C]=(i<<16)|j;
     }
   }
   ////////////////-
   // End of Payload
   ////////////////-
   
   //////////////////-
   // Start of Footer
   //////////////////-      
   // Status word - some marker 
   retVar[HEADER_SIZE_C+PAYLOAD_SIZE_C+0] = 0x0E0E0E0E;
   // one status word 
   retVar[HEADER_SIZE_C+PAYLOAD_SIZE_C+1] = 0x00000001;
   // Payload size
   retVar[HEADER_SIZE_C+PAYLOAD_SIZE_C+2] = PAYLOAD_SIZE_C;
   // Status word is located at the end of the payload
   retVar[HEADER_SIZE_C+PAYLOAD_SIZE_C+3] = 0x00000001;
   //////////////////-
   // End of Footer
   //////////////////-         
   unsigned val;
   SerialIF::readRegister(20, val);
   std::cout<<val<<std::endl;
   SerialIF::writeRegister(36, 0);
   SerialIF::readRegister(20, val);
   std::cout<<val<<std::endl;
   SerialIF::writeRegister(0xc81, 0);
   //SerialIF::writeRegister(0xc83, 0);
   //SerialIF::writeRegister(0x882, 0);
/*
   for(int i=0;i<100;i++){
   retVar[5] = i;
   pgp->sendFragment(retVar, SIZE_C);
   }
   for(unsigned i=0;i<9;i++){
     SerialIF::readRegister(i+2048, val);
     std::cout<<"Register "<<i<<" : "<<val<<std::endl;
   }
   for(unsigned i=0x7c;i<0x7f;i++){
     SerialIF::readRegister(i+2048, val);
     std::cout<<"Register "<<i<<" : "<<val<<std::endl;
   }
   SerialIF::readRegister(2048+0x40, val);
   std::cout<<"Register 40: "<<val<<std::endl;
   SerialIF::writeRegister(2048+0x82, 0);
   SerialIF::readRegister(2048+0x82, val);
   std::cout<<"Register 0x82: "<<val<<std::endl;
*/
}
