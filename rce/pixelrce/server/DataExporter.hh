#ifndef DATAEXPORTER_HH
#define DATAEXPORTER_HH

class TFile;
class ConfigGui;
class TDirectory;
#include <string>
#include <map>

class DataExporter{
public:
  DataExporter(ConfigGui** cfg): m_cfg(cfg){}
  ~DataExporter(){}
  void exportData(std::string &runname, std::string &topconfigname, std::string &rcdir, TFile* file, TFile* anfile);
private:
  void CopyDir(TDirectory *source, const std::map<int, int> &ids, const char* topdir);
  void CopyFile(const char *fname, const std::map<int, int> &ids, const char* dirname);
  static const std::string getExportBaseDir();
  ConfigGui** m_cfg;
  static const std::string basedir;
};

#endif
