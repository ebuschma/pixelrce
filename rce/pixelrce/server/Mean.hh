#ifndef MEAN_HH
#define MEAN_HH
#include <math.h>

class Mean{
public:
  Mean():sum(0),sum2(0),n(0){}
  void clear(){sum=0;sum2=0;n=0;}
  void accumulate(float val){
    sum+=val;
    sum2+=val*val;
    n++;
  }
  float mean(){
    if(n>0)return sum/n;
    else return 0;
  }
  float sigma(){
    if(n>0)return sqrt(1/(float)n*sum2-mean()*mean());
    else return 0;
  }
  int nEntries(){return n;}
private:
  float sum;
  float sum2;
  int n;
};
#endif
