//      low level bit stream test derived from sendbitstream 
//      without RCF and controller classes
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <assert.h>
#include "server/PgpModL.hh"
#include "HW/SerialPgpFei4.hh"
#include "HW/RCDImasterL.hh"
#include <iostream>
#include "config/FEI4/FECommands.hh"
#include "config/FEI4/FEI4BGlobalRegister.hh"
class  FEI4BModule {  // extract everything needed from FEI4::FEI4BModule into a new class
public:
  FEI4BModule( unsigned inlink, unsigned outlink) : m_inLink(inlink),m_outLink(outlink) {
    m_commands=new FEI4::FECommands;
    m_global=new FEI4:: FEI4BGlobalRegister(m_commands);
  }
  ~FEI4BModule() {
    delete  m_commands;
    delete m_global;
  }
  void switchModeHW(FEI4::FECommands::MODE mode){
    BitStream *bs=new BitStream;
    m_commands->switchMode(bs, mode);
    bs->push_back(0);
    SerialIF::send(bs, SerialIF::WAITFORDATA);
    delete bs;
  }  

  


uint32_t readHWglobalRegister(int32_t reg, uint16_t &val){
  SerialIF::sendCommand(0x10); //phase calibration
  m_commands->setAddr(8);
  SerialIF::setChannelInMask(1<<m_inLink);
  SerialIF::setChannelOutMask(1<<m_outLink);
  return m_global->readRegisterHW(reg, val);
  SerialIF::setChannelInMask(0);
  SerialIF::setChannelOutMask(0);
}


uint32_t writeHWglobalRegister(int32_t reg, uint16_t val){
  m_commands->setAddr(8);
  switchModeHW(FEI4::FECommands::CONF);
  SerialIF::setChannelInMask(1<<m_inLink);
  return m_global->writeRegisterHW(reg, val);
  SerialIF::setChannelInMask(0);
}


unsigned writeHWregister(unsigned addr, unsigned val){
  return SerialIF::writeRegister(addr,val);
}
unsigned readHWregister(unsigned addr, unsigned &val){
  return SerialIF::readRegister(addr, val);
}
unsigned writeHWblockData(std::vector<unsigned>& data){
  return SerialIF::writeBlockData(data);
}
unsigned readHWblockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec){
  return SerialIF::readBlockData(data, retvec);
}
unsigned readHWbuffers(std::vector<unsigned char>& retvec){
  return SerialIF::readBuffers(retvec);
}


private:
  unsigned m_inLink;
  unsigned m_outLink;
  FEI4::FECommands *m_commands;
  FEI4::FEI4BGlobalRegister *m_global;
};



int main ( int argc, char ** argv )
{
 
   //Init PGP and Serial IF
   PgpModL pgpl;
   pgpl.open();
   PgpTrans::RCDImaster* pgp= PgpTrans::RCDImaster::instance();
   new SerialPgpFei4;
   unsigned int inlink=0;
   unsigned int outlink=0;
   unsigned serstat, val;
   FEI4BModule controller(inlink,outlink);
   assert(controller.writeHWregister(3,0)==0); //emulator or loopback
   
   serstat=controller.writeHWglobalRegister(35, 0x5d4);
   serstat=controller.writeHWglobalRegister(34, 0x10);
   serstat=controller.writeHWglobalRegister(33, 0x0);
   serstat=controller.writeHWglobalRegister(32, 0x0);
   serstat=controller.writeHWglobalRegister(31, 0xf000);
   serstat=controller.writeHWglobalRegister(30, 0x0);
   serstat=controller.writeHWglobalRegister(29, 0x7);
   serstat=controller.writeHWglobalRegister(28, 0x8206);
   serstat=controller.writeHWglobalRegister(27, 0x8000);
   serstat=controller.writeHWglobalRegister(26, 0x8);
   serstat=controller.writeHWglobalRegister(25, 0xe00);
   serstat=controller.writeHWglobalRegister(24, 0x0);
   serstat=controller.writeHWglobalRegister(23, 0x0);
   serstat=controller.writeHWglobalRegister(22, 0xa0);
   serstat=controller.writeHWglobalRegister(21, 0x7ff);
   serstat=controller.writeHWglobalRegister(19, 0x600);
   serstat=controller.writeHWglobalRegister(18, 0xff);
   serstat=controller.writeHWglobalRegister(17, 0x2d);
   serstat=controller.writeHWglobalRegister(16, 0xd238);
   serstat=controller.writeHWglobalRegister(15, 0x1a96);
   serstat=controller.writeHWglobalRegister(14, 0xd54c);
   serstat=controller.writeHWglobalRegister(13, 0x0);
   serstat=controller.writeHWglobalRegister(12, 0x7200);
   serstat=controller.writeHWglobalRegister(11, 0x56d4);
   serstat=controller.writeHWglobalRegister(10, 0x284c);
   serstat=controller.writeHWglobalRegister(9, 0xaa);
   serstat=controller.writeHWglobalRegister(8, 0xf258);
   serstat=controller.writeHWglobalRegister(7, 0x6958);
   serstat=controller.writeHWglobalRegister(6, 0xd4);
   serstat=controller.writeHWglobalRegister(5, 0xd495);
   serstat=controller.writeHWglobalRegister(4, 0xc0);
   serstat=controller.writeHWglobalRegister(3, 0x4600);
   serstat=controller.writeHWglobalRegister(2, 0x0);
   serstat=controller.writeHWglobalRegister(1, 0xff);
   
   int command;
   unsigned short inp;
   unsigned nbt;
   int reg;
   BitStream *bs;
   std::vector<unsigned char> rep;
   FEI4::FECommands commands;
   std::vector<unsigned> dcoldata;
   std::vector<unsigned> retv;
   unsigned dcol, bit, xval;
   bool good;
   commands.setAddr(0);
   bs=new BitStream;
   commands.switchMode(bs, FEI4::FECommands::RUN);
   bs->push_back(0);
   //    for(size_t i=0;i<bs->size();i++)std::cout<<std::hex<<(*bs)[i]<<std::endl;
   
   while(true){
     std::cout<<"Choose a command"<<std::endl;
     std::cout<<"================"<<std::endl;
     std::cout<<" 1) Send command"<<std::endl;
     std::cout<<" 2) Write register"<<std::endl;
     std::cout<<" 3) Read register"<<std::endl;
     std::cout<<" 4) Write block data"<<std::endl;
     std::cout<<" 5) Write FE register"<<std::endl;
     std::cout<<" 6) Read FE register"<<std::endl;

     std::cin>>std::dec>>command;
     unsigned address, data;
     switch (command){
     case 5:
       std::cout<<"Address?"<<std::endl;
       std::cin>>address;
       std::cout<<"Data?"<<std::endl;
       std::cin>>data;
       serstat=controller.writeHWglobalRegister(address, data);
       if(serstat!=0)std::cout<<"Write FE register failed"<<std::endl;
        break;
     case 6:
       std::cout<<"Address?"<<std::endl;
       std::cin>>address;
       unsigned short val;
       serstat=controller.readHWglobalRegister(address, val);
       if(serstat!=0)std::cout<<"Read FE register failed"<<std::endl;
       else std::cout<<"Value="<<val<<std::endl;
       break;
     case 1:
       std::cout<<"Opcode?"<<std::endl;
       unsigned opcode;
       std::cin>>opcode;
       //assert(controller.sendHWcommand(opcode)==0);      
       break;
     case 2:
       unsigned address, data;
       std::cout<<"Address?"<<std::endl;
       std::cin>>std::hex>>address;
       std::cout<<"Data?"<<std::endl;
        std::cin>>std::hex>>data>>std::dec;
	std::cout<<controller.writeHWregister(address, data)<<std::endl;;
        break;
     case 3:
       unsigned addressr, datar;
       std::cout<<"Address?"<<std::endl;
       std::cin>>addressr;
       std::cout<<controller.readHWregister(addressr, datar)<<std::endl;
       std::cout<<"Data "<<std::hex<<datar<<std::dec<<std::endl;
       break;
     case 4:
       bs=new BitStream;
       commands.L1A(bs);
       serstat=controller.writeHWblockData(*bs);	
       if(serstat!=0)std::cout<<"Send Error "<<serstat<<std::endl;
       delete bs;
       break;
     default:
       break;
       
     }

   }

}
