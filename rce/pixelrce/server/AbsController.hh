#ifndef ABSCONTROLLER_HH
#define ABSCONTROLLER_HH

#include "PixelConfig.hh"
#include "Callback.hh"
#include <vector>
#include <map>

class CallbackInfo;
namespace ipc{
  class ScanOptions;
}

class AbsController{
public:
  AbsController() {};
  virtual bool guiRunning(){return 0;}
  virtual void setGuiRunning(bool on){};
  virtual void addRce(int rce)=0;
  void removeAllRces(){
    m_rces.clear();
  }
  virtual void downloadModuleConfig(int rce, int id, PixelConfig* config)=0;
  virtual void addModule(const char* name, const char* type, int id, int inLink, int outLink, int rce, const char* formatter)=0;
  virtual int setupTrigger(const char* type="default")=0;
  virtual void removeAllModules()=0;
  virtual void resetFE()=0;
  virtual void configureModulesHW()=0;
  virtual void configureModuleHW(int rce, int outlink)=0;

  virtual unsigned writeHWglobalRegister(const char* name, int reg, unsigned short val)=0;
  virtual unsigned readHWglobalRegister(const char* name, int reg, unsigned short& val)=0;
  virtual unsigned writeHWdoubleColumn(const char* name, unsigned bit, unsigned dcol, std::vector<unsigned> data, std::vector<unsigned> &retvec)=0;
  virtual unsigned readHWdoubleColumn(const char* name, unsigned bit, unsigned dcol, std::vector<unsigned> &retvec)=0;

  virtual unsigned writeHWregister(int rce, unsigned addr, unsigned val)=0;
  virtual unsigned readHWregister(int rce, unsigned addr, unsigned& val)=0;
  virtual unsigned sendHWcommand(int rce, unsigned char opcode)=0;
  virtual unsigned writeHWblockData(int rce, std::vector<unsigned> &data)=0;
  virtual unsigned readHWblockData(int rce, std::vector<unsigned> &data, std::vector<unsigned>& retvec)=0;
  virtual unsigned readHWbuffers(int rce, std::vector<unsigned char>& retvec)=0;
  
  virtual void downloadScanConfig(ipc::ScanOptions &scn)=0;
  virtual int verifyModuleConfigHW(int rce, int id)=0;
  
  virtual void waitForScanCompletion(ipc::Priority pr, CallbackInfo* callb)=0;
  virtual void runScan(ipc::Priority pr, CallbackInfo* callb)=0;
  virtual void startScan()=0;
  virtual void abortScan()=0;
  virtual void stopWaitingForData()=0;
  virtual void getEventInfo(unsigned* nevent)=0;
  virtual int getScanStatus()=0;
  virtual unsigned getNEventsProcessed()=0;
  virtual void resynch()=0;
  virtual void setupParameter(const char* par, int val)=0;
  virtual void setupMaskStage(int stage)=0;
protected:
  std::vector<int> m_rces;
};

#endif
