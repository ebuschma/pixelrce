#include "analysis/CalibAnalysis.hh"
#include "server/ConfigGui.hh"
#include "PixelFEI4BConfig.hh"
#include <TFile.h>
#include <regex>
#include <string>
#include <iostream>

std::string CalibAnalysis::getPath(TFile* file){
  std::string fn(file->GetName());
  std::string name="";
  std::regex re("[^/]*\\.root");
  std::string maskfilename = std::regex_replace (fn, re, name);
  return maskfilename;
}

PixelConfig* CalibAnalysis::findConfig(ConfigGui* cfg[], int id){
  for(int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(cfg!=0 && cfg[i]->isIncluded() && cfg[i]->getId()==id){
      cfg[i]->setConfig(); //restore config
      m_cfg=cfg[i];
      m_channel=i;
      return cfg[i]->getModuleConfig();
    }
  }
  return 0;
}

const char* CalibAnalysis::findFieldName(ConfigGui* cfg[], int id){
  for(int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(cfg!=0 && cfg[i]->isIncluded() && cfg[i]->getId()==id){
      return cfg[i]->getName();
    }
  }
  return "";
}

const std::string CalibAnalysis::findFEType(ConfigGui* cfg[], int id){
  for(int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(cfg!=0 && cfg[i]->isIncluded() && cfg[i]->getId()==id){
      return cfg[i]->getType();
    }
  }
  return "";
}

void CalibAnalysis::writeTopFile(ConfigGui* cfg[],TFile* anfile, int runno ){
  char key[16];
  sprintf(key, "%d", runno);
  std::string path=getPath(anfile)+"configUpdate";
  //std::cout << path << std::endl;
  std::ofstream topfile((path+"/top/config__"+key+".cfg").c_str());
  std::string name;
  for(int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(cfg[i]->isIncluded())
      name=path+"/"+cfg[i]->getConfdir()+"/configs/"+cfg[i]->getConfigName()+"__"+key+".cfg";
    else
      name=cfg[i]->getFilename();
    cfg[i]->copyConfig(topfile, name.c_str());
  }
  topfile.close();
  m_update=true;
}

void CalibAnalysis::writeConfig(TFile* anfile, int runno){
  if(m_cfg!=0){
    char key[16];
    sprintf(key, "%d", runno);
    std::string path=getPath(anfile)+"configUpdate";
    m_update=true;
    m_cfg->getModuleConfig()->writeModuleConfig(path.c_str(), m_cfg->getConfdir(), m_cfg->getConfigName(), key);
    m_cfg->setConfig(); //restore config
  }
}
  
void CalibAnalysis::clearFEI4Masks(PixelConfig* cfg){
  for(int i=0;i<ipc::IPC_N_I4_PIXEL_COLUMNS;i++){
    for(int j=0;j<ipc::IPC_N_I4_PIXEL_ROWS;j++){
      cfg->setFEMask(0,i,j, cfg->getFEMask(0,i,j)|0x1);//set bit 0 (enable)
      cfg->setFEMask(0,i,j, cfg->getFEMask(0,i,j)&0xf7);//reset bit 3 (hitbus)
    }
  }
}

std::string CalibAnalysis::addPosition(const char* hname, ConfigGui* cfg[]){
    std::cmatch matches;
    std::regex re("Mod_(\\d+)");
    if(std::regex_search(hname, matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      for(int i=0;i<ConfigGui::MAX_MODULES;i++){
	if(cfg[i]->isIncluded() && cfg[i]->getId()==id){
	  return std::string(Form("%d", 1+i/(ConfigGui::MAX_MODULES/2)))+std::string(cfg[i]->getName())+":"+hname;
	}
      }
    }
    return hname;
}
