#ifndef TOT_ANALYSIS_HH
#define TOT_ANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include "analysis/CfgFileWriter.hh"
#include "server/PixScan.hh"

class TFile;
class TH2;
namespace RCE{
  class PixScan;
}
class ConfigGui;

class TotAnalysis: public CalibAnalysis{
public:
  struct odata{
    TH2* occ;
    TH2* tot;
    TH2* tot2;
  };
  TotAnalysis(): CalibAnalysis(){}
  ~TotAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
