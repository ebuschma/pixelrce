#include "analysis/StuckPixelAnalysis.hh"
#include "server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <fstream>
using namespace RCE;

void StuckPixelAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  TIter nextkey(file->GetListOfKeys());
  TKey *key;
  std::regex re("_(\\d+)_Hitor");
  while ((key=(TKey*)nextkey())) {
    file->cd();
    std::string name(key->GetName());
    std::cout<<name<<std::endl;
    std::cmatch matches;
    if(std::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      TH2* histo = (TH2*)key->ReadObj();
      m_fw->writeMaskFile(Form("%sStuckPixelMask_Mod_%d", m_fw->getPath(anfile).c_str(), id), histo);
      PixelConfig* confb=findConfig(cfg, id);
      bool isfei4=(confb->getType()!="FEI3");
      if(isfei4){
	if(scan->clearMasks()==true)clearFEI4Masks(confb);
	for (int i=0;i<histo->GetNbinsX();i++){
	  for(int j=0;j<histo->GetNbinsY();j++){
	    if(histo->GetBinContent(i+1, j+1)==1){
	      confb->setFEMask(0,i,j, confb->getFEMask(0,i,j)&0xfe); //reset bit 0 (enable)
	      confb->setFEMask(0,i,j, confb->getFEMask(0,i,j)|0x8); //reset bit 3 (hitbus)
	    }
	  }
	}
	writeConfig(anfile, runno);
      }
      delete histo;
    }
  }  
  if(configUpdate())writeTopFile(cfg, anfile, runno);
}

