#ifndef OFFSETANALYSIS_HH
#define OFFSETANALYSIS_HH

#include "analysis/CalibAnalysis.hh"

class ConfigGui;
class TFile;
class TH2D;
class TH1;
namespace RCE{
  class PixScan;
}

class OffsetAnalysis: public CalibAnalysis{
public:
  OffsetAnalysis(): CalibAnalysis(){}
  ~OffsetAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
  void fillHit1d(std::string &name, TH1* hits1d, unsigned numval);
};


#endif
