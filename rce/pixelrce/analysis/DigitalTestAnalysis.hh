#ifndef DIGITALTESTANALYSIS_HH
#define DIGITALTESTANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include "analysis/CfgFileWriter.hh"

class ConfigGui;
class TFile;
class TH2D;
namespace RCE{
  class PixScan;
}

class DigitalTestAnalysis: public CalibAnalysis{
public:
  DigitalTestAnalysis(CfgFileWriter* fw): CalibAnalysis(), m_fw(fw){}
  ~DigitalTestAnalysis(){delete m_fw;}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
  void writeMaskFile(TH2D* his, TFile* anfile);
private:
  CfgFileWriter* m_fw;
};


#endif
