#include "analysis/CfgFileWriter.hh"
#include <TFile.h>
#include <regex>
#include <string>
#include <iostream>

std::string CfgFileWriter::getPath(TFile* file){
  std::string fn(file->GetName());
  std::string name="";
  std::regex re("[^/]*\\.root");
  std::string maskfilename = std::regex_replace (fn, re, name);
  return maskfilename;
}
