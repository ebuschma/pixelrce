#ifndef STUCKPIXELANALYSIS_HH
#define STUCKPIXELANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include "analysis/CfgFileWriter.hh"

class ConfigGui;
class TFile;
class TH2;
namespace RCE{
  class PixScan;
}

class StuckPixelAnalysis: public CalibAnalysis{
public:
  StuckPixelAnalysis(CfgFileWriter *fw): CalibAnalysis(), m_fw(fw){}
  ~StuckPixelAnalysis(){delete m_fw;}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
  void writeMaskFile(TH2* his, TFile* anfile);
private:
  CfgFileWriter* m_fw;
};


#endif
