#ifndef CALIBANALYSIS_HH
#define CALIBANALYSIS_HH

#include <string>
#include "config/PixelConfig.hh"

class TFile;
class ConfigGui;
namespace RCE{
  class PixScan;
}

class CalibAnalysis{
public:
  CalibAnalysis(): m_cfg(0), m_update(false){}
  virtual ~CalibAnalysis(){}
  virtual void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[])=0;
  std::string getPath(TFile* file);
  PixelConfig* findConfig(ConfigGui* cfg[], int id);
  void writeConfig(TFile *anfile, int runno);
  bool configUpdate(){return m_update;}
  void writeTopFile(ConfigGui* cfg[], TFile* anfile, int runno);
  const char* findFieldName(ConfigGui* cfg[], int id);
  const std::string findFEType(ConfigGui* cfg[], int id);
  void clearFEI4Masks(PixelConfig* cfg);
  static std::string addPosition(const char* hname, ConfigGui* cfg[]);
private:
  ConfigGui* m_cfg;
  int m_channel;
  bool m_update;
};

#endif
