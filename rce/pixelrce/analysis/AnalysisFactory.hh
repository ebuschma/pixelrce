#ifndef ANALYSISFACTORY_HH
#define ANALYSISFACTORY_HH

#include <string>

class CalibAnalysis;

class AnalysisFactory{
public:
  AnalysisFactory(){};
  ~AnalysisFactory(){};
  CalibAnalysis* getAnalysis(std::string &type);
};

#endif
