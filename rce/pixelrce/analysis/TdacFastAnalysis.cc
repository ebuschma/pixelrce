#include "analysis/TdacFastAnalysis.hh"
#include "server/PixScan.hh"
#include "config/FEI3/Frontend.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include <string>
#include <sstream>
#include "TH1D.h"
#include "TF1.h"
#include "TStyle.h"

namespace{
  const double BAD_THRESHOLD=150;
}


void TdacFastAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  int repetitions = scan->getRepetitions();
  double targetEff = 0.5;
  file->cd();
  TKey *key;
  char subdir[128];
  char name[128];
  char title[128];
  int binsx=0, binsy=0;
  size_t numvals=scan->getLoopVarValues(1).size();
  std::map<int, hdatafast> *histomap=new std::map<int, hdatafast>[numvals];
  double *val=new double[numvals];
  for (size_t i=0;i<numvals;i++){
    sprintf(subdir, "loop1_%d", i);
    file->cd(subdir); // TDAC steps
    TIter nextkey(gDirectory->GetListOfKeys()); // histos
    while ((key=(TKey*)nextkey())) {
      std::regex re("_(\\d+)_Occupancy_Point_000");
      std::cmatch matches;
      if(std::regex_search(key->GetName(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int lnm=strtol(match.c_str(),0,10);
	char tdachistoname[128];
	sprintf (tdachistoname, "TDAC_settings_Mod_%d_it_%d", lnm, i);
	TH2* histo = (TH2*)key->ReadObj();
	histo->SetMinimum(0); histo->SetMaximum(repetitions);
	TH2* tdachisto=(TH2*)gDirectory->Get(tdachistoname);
	assert(tdachisto);
	histomap[i][lnm].occupancy=histo;
	histomap[i][lnm].tdac=tdachisto;
	binsx=histo->GetNbinsX();
	binsy=histo->GetNbinsY();
      }
    } 
  }
  for(std::map<int, hdatafast>::iterator it=histomap[0].begin();it!=histomap[0].end();it++){
    int id=it->first;
    float pct = 100;
    sprintf(name, "BestOcc_Mod_%d", id);
    sprintf(title, "Best Occupancy Module %d at %s", id, findFieldName(cfg, id));
    TH2F* occBest=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
    occBest->SetMinimum(0); occBest->SetMaximum(pct);
    occBest->GetXaxis()->SetTitle("Column");
    occBest->GetYaxis()->SetTitle("Row");
    occBest->GetZaxis()->SetTitle("Occupancy[%]");
   
    sprintf(name, "BestTdac_Mod_%d", id);
    sprintf(title, "Best Tdacs Module %d at %s", id, findFieldName(cfg, id));
    TH2F* tdac=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
    tdac->GetXaxis()->SetTitle("Column");
    tdac->GetYaxis()->SetTitle("Row");
    tdac->GetZaxis()->SetTitle("Tdac");
    PixelConfig* confb=findConfig(cfg, id);
    for (int i=1;i<=occBest->GetNbinsX();i++){
      for(int j=1;j<=occBest->GetNbinsY();j++){
	double bestval = 10*targetEff; // must be larger than 1 (with possibilty of double hit)
	int index=0;
	for(size_t k=0;k<numvals;k++){
	  val[k] = histomap[k][id].occupancy->GetBinContent(i,j) / repetitions;
	  if(fabs(val[k]-targetEff)<bestval){
	    index=k;
	    bestval=fabs(val[k]-targetEff);
	  }
	}
	occBest->SetBinContent(i,j,pct*val[index]);
	tdac->SetBinContent(i,j,histomap[index][id].tdac->GetBinContent(i,j));
	int chip=0;
	int col=i-1;
	int row=j-1;
	if (confb->getType()=="FEI3"){
	  int chip=(i-1)/FEI3::Frontend::N_COLS;
	  int col=(i-1)%FEI3::Frontend::N_COLS;
	}
	confb->setThresholdDac(chip, col, row, histomap[index][id].tdac->GetBinContent(i,j)); 
      }
    }
    
    anfile->cd();
    occBest->Write();
    occBest->SetDirectory(gDirectory);
   
    tdac->Write();
    tdac->SetDirectory(gDirectory);
   
    m_fw->writeDacFile(Form("%sTdacs_Mod_%d", m_fw->getPath(anfile).c_str(), id), tdac);
    writeConfig(anfile, runno);
  }
  if(configUpdate())writeTopFile(cfg, anfile, runno);
  delete [] histomap;
  delete [] val;
}

