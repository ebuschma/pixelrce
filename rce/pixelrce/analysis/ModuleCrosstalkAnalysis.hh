#ifndef MODULECROSSTALKANALYSIS_HH
#define MODULECROSSTALKANALYSIS_HH

#include "analysis/CalibAnalysis.hh"

class ConfigGui;
class TFile;
class TH2D;
namespace RCE{
  class PixScan;
}

class ModuleCrosstalkAnalysis: public CalibAnalysis{
public:
  ModuleCrosstalkAnalysis(): CalibAnalysis(){}
  ~ModuleCrosstalkAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
