#include "analysis/TdacAnalysis.hh"
#include "server/PixScan.hh"
#include "config/FEI3/Frontend.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <regex>
#include <iostream>
#include "TH1D.h"
#include "TF1.h"
#include "TStyle.h"

namespace{
  const double BAD_THRESHOLD=150;
}


void TdacAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  //gStyle->SetOptStat(1110);
  gStyle->SetOptFit(10);
  gStyle->SetOptStat(0);
  file->cd();
  TKey *key;
  char subdir[128];
  char name[128];
  char title[128];
  int binsx=0, binsy=0;
  size_t numvals=scan->getLoopVarValues(1).size();
  std::map<int, hdata> *histomap=new std::map<int, hdata>[numvals];
  double *val=new double[numvals];
  for (size_t i=0;i<numvals;i++){
    sprintf(subdir, "loop1_%d", i);
    file->cd(subdir);
    TIter nextkey(gDirectory->GetListOfKeys()); // TDAC settings
    while ((key=(TKey*)nextkey())) {
      std::regex re("_(\\d+)_Mean");
      std::cmatch matches;
      if(std::regex_search(key->GetName(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int lnm=strtol(match.c_str(),0,10);
	char tdachistoname[128];
	sprintf (tdachistoname, "TDAC_settings_Mod_%d_it_%d", lnm, i);
	TH2* histo = (TH2*)key->ReadObj();
	TH2* tdachisto=(TH2*)gDirectory->Get(tdachistoname);
	assert(tdachisto);
	histomap[i][lnm].mean=histo;
	histomap[i][lnm].tdac=tdachisto;
	binsx=histo->GetNbinsX();
	binsy=histo->GetNbinsY();
      }
    } 
  }
  int target=scan->getThresholdTargetValue();
  for(std::map<int, hdata>::iterator it=histomap[0].begin();it!=histomap[0].end();it++){
    int id=it->first;
    sprintf(name, "threshdist_Mod_%d", id);
    sprintf(title, "Threshold distribution Module %d at %s", id, findFieldName(cfg, id));
    TH1F* thresh1d=new TH1F(name, title, 400, 1000, 5000); 
    thresh1d->GetXaxis()->SetTitle("Threshold");
    sprintf(name, "BestMean_Mod_%d", id);
    sprintf(title, "Best Mean Module %d at %s", id, findFieldName(cfg, id));
    TH2F* mean=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
    mean->GetXaxis()->SetTitle("Column");
    mean->GetYaxis()->SetTitle("Row");
    sprintf(name, "diff2d_Mod_%d", id);
    sprintf(title, "Distance wrt %d electrons Module %d at %s", target, id, findFieldName(cfg, id));
    TH2F* diff2d=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
    diff2d->GetXaxis()->SetTitle("Column");
    diff2d->GetYaxis()->SetTitle("Row");
    sprintf(name, "thresh1d_Mod_%d", id);
    sprintf(title, "Thresholds Module %d at %s", id, findFieldName(cfg, id));
    int nbins=binsx*binsy;
    TH1F* diff1d=new TH1F(name, title, nbins, 0, (float)nbins); 
    diff1d->GetXaxis()->SetTitle("Channel");
    diff1d->SetOption("p9");
    sprintf(name, "BestTdac_Mod_%d", id);
    sprintf(title, "Best Tdacs Module %d at %s", id, findFieldName(cfg, id));
    TH2F* tdac=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
    tdac->GetXaxis()->SetTitle("Column");
    tdac->GetYaxis()->SetTitle("Row");
    sprintf(name, "BadPixels_Mod_%d", id);
    sprintf(title, "Bad Pixels Module %d at %s", id, findFieldName(cfg, id));
    TH2F* bad=new TH2F(name, title, binsx, 0, binsx, binsy, 0, binsy);
    bad->GetXaxis()->SetTitle("Column");
    bad->GetYaxis()->SetTitle("Row");
    sprintf(name, "threshdiff_Mod_%d", id);
    sprintf(title, "Threshold distribution around target value Module %d at %s", id, findFieldName(cfg, id));
    TH1F* threshdist=new TH1F(name, title, 200, -500, 500); 
    threshdist->GetXaxis()->SetTitle("Threshold");
    PixelConfig* confb=findConfig(cfg, id);
    for (int i=1;i<=mean->GetNbinsX();i++){
      for(int j=1;j<=mean->GetNbinsY();j++){
	double bestval=10000;
	int index=0;
	for(size_t k=0;k<numvals;k++){
	  val[k]=histomap[k][id].mean->GetBinContent(i,j);
	  if(fabs(val[k]-target)<bestval){
	    index=k;
	    bestval=fabs(val[k]-target);
	  }
	}
	mean->SetBinContent(i,j,val[index]);
	diff2d->SetBinContent(i,j,fabs(val[index]-target));
	diff1d->SetBinContent((i-1)*mean->GetNbinsY()+j, val[index]);
	thresh1d->Fill(val[index]);
	threshdist->Fill(val[index]-target);
	tdac->SetBinContent(i,j,histomap[index][id].tdac->GetBinContent(i,j));
	int chip=0;
	int col=i-1;
	int row=j-1;
	if (confb->getType()=="FEI3"){
	  int chip=(i-1)/FEI3::Frontend::N_COLS;
	  int col=(i-1)%FEI3::Frontend::N_COLS;
	}
	confb->setThresholdDac(0, col, row, (int)histomap[index][id].tdac->GetBinContent(i,j)); 
	if(fabs(val[index]-target)>BAD_THRESHOLD)bad->SetBinContent(i,j,1);
      }
    }
    TF1 gauss("gauss", "gaus", 100, 10000);
    gauss.SetParameter(0, threshdist->GetMaximum());
    gauss.SetParameter(1, threshdist->GetMaximumBin()*threshdist->GetBinWidth(1));
    thresh1d->Fit(&gauss,"q", "", 100, 10000);
    anfile->cd();
    mean->Write();
    mean->SetDirectory(gDirectory);
    diff2d->Write();
    diff2d->SetDirectory(gDirectory);
    diff1d->Write();
    diff1d->SetDirectory(gDirectory);
    tdac->Write();
    tdac->SetDirectory(gDirectory);
    bad->Write();
    bad->SetDirectory(gDirectory);
    threshdist->Write();
    threshdist->SetDirectory(gDirectory);
    thresh1d->Write();
    thresh1d->SetDirectory(gDirectory);
    m_fw->writeDacFile(Form("%sTdacs_Mod_%d", m_fw->getPath(anfile).c_str(), id), tdac);
    writeConfig(anfile, runno);
  }
  if(configUpdate())writeTopFile(cfg, anfile, runno);
  delete [] histomap;
  delete [] val;
}

