#ifndef PRODUCERIF_HH
#define PRODUCERIF_HH
#include <eudaq/Event.hh>
#include <eudaq/Producer.hh>


class ProducerIF{
public:
  static void sendEvent(eudaq::Event *ev);
  static void setProducer(eudaq::Producer *producer);
private:
  static eudaq::Producer *m_producer;
  
};

#endif
