// MW: not this is an ugly hack to expose the SendPacket for raw data
// rce_eudaq must be in the include path before the eudaq release for this work
#ifndef EUDAQ_INCLUDED_DataSender
#define EUDAQ_INCLUDED_DataSender

#include "eudaq/Platform.hh"
#include <eudaq/TransportClient.hh>
#include <string>

namespace eudaq {

  //  class TransportClient;
  class Event;
  class AidaPacket;

  class DLLEXPORT DataSender {
  public:
    DataSender(const std::string &type, const std::string &name);
    ~DataSender();
    void Connect(const std::string &server);
    void SendEvent(const Event &);
    void SendPacket(const AidaPacket &);
    void SendPacket(unsigned char *data,int size) {m_dataclient->SendPacket(data,size);}
  private:
    std::string m_type, m_name;
    TransportClient *m_dataclient;
  };
}

#endif // EUDAQ_INCLUDED_DataSender
