#include "eudaq/DataSenderIF.hh"

#include "eudaq/DataSender.hh"
#include <eudaq/RawDataEvent.hh>

eudaq::DataSender * DataSenderIF::m_dataSender = 0;

void DataSenderIF::setDataSender(eudaq::DataSender * dataSender)
{ m_dataSender = dataSender; return; }

void DataSenderIF::SendEvent(eudaq::Event *event)
{
  if (m_dataSender != 0) 
  {
    m_dataSender->SendEvent(*event);
  }
  return;
}

void DataSenderIF::SendEvent(unsigned char *data, int size)
{
  if (m_dataSender != 0) 
  {    
    m_dataSender->SendPacket(data,size);
  }
  return;
}

unsigned DataSenderIF::m_run=0;
unsigned DataSenderIF::m_ev=0;
