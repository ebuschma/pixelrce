#ifndef SERIALHEXDUMP_HH
#define SERIALHEXDUMP_HH

#include "HW/SerialIF.hh"
#include <stdio.h>

class SerialHexdump: public SerialIF {
public:
  SerialHexdump();
  virtual ~SerialHexdump(){}
private:
  void Send(BitStream* bs, int opt);
  void SetChannelInMask(unsigned linkmask);
  void SetChannelOutMask(unsigned linkmask);
  int EnableTrigger(bool on);
  unsigned SendCommand(unsigned char opcode);
  unsigned WriteRegister(unsigned addr, unsigned val);
  unsigned ReadRegister(unsigned addr, unsigned &val);
  unsigned WriteBlockData(std::vector<unsigned>& data);
  unsigned ReadBlockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec);
  FILE* m_flog;
};


#endif
