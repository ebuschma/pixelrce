#ifdef RCE_V2
#include "datCode.hh"
#include DAT_PUBLIC( oldPpi, pic,       Tds.hh)
#include DAT_PUBLIC( oldPpi, pic,       Pool.hh)
#include DAT_PUBLIC( oldPpi, pgp,       Driver.hh)
#include DAT_PUBLIC( oldPpi, pgp,       DriverList.hh)
#include DAT_PUBLIC( service, fci,       Exception.hh)
#else
#include "rce/pic/Tds.hh"
#include "rce/pgp/Driver.hh"
#include "rce/pgp/DriverList.hh"
#include "rce/service/Exception.hh"
#include "rce/pic/Pool.hh"
#endif
#include "RCDImaster.hh"
#include "Headers.hh"
#include "Receiver.hh"
#include <stdio.h>
#include <assert.h>
#include <iostream>

using namespace PgpTrans;

RCDImaster* RCDImaster::_instance=0;
omni_mutex RCDImaster::_guard;

RCDImaster* RCDImaster::instance(){
  if( ! _instance){
    omni_mutex_lock ml(_guard);
    if( ! _instance){
      _instance=new RCDImaster;
    }
  }
  return _instance;
}
    
    

RCDImaster::RCDImaster():_pgp_driver(0),_receiver(0),_pool(0),
			 _tid(0),_data_cond(&_data_mutex),
                         _status(0), _data(0), _handshake(false),_blockread(false){
  try {
    const RcePic::Params Tx = {
      RcePic::NonContiguous,
      16,     // Header
      64*132, // Payload
      32     // Number of buffers
    };

    RcePic::Pool * pool = new RcePic::Pool::Pool(Tx); 
    RcePgp::DriverList * driverList =  RcePgp::DriverList::instance();
    setPool(pool);  // tell the handler so it can put back the buffers
    RcePgp::Driver * pgpd = 0;
    for (int i=0;i<4;i++){
       pgpd=driverList->handler(static_cast<RcePgp::port_number_t>(i), this);
    }
    pgpd=driverList->lookup(static_cast<RcePgp::port_number_t>(0));
     if (!pgpd){
       printf("serialPorts: Could not find the driver\n");
       assert(0);
     }
     setDriver(pgpd);
     rtems_task_wake_after(10);  // hack wait for initialization to complete
  } catch (RceSvc::Exception& e) {
    printf("*** rce exception %s", e.what());
  } catch (std::exception& e) {
    printf("*** c++ exception %s", e.what());
  }
  //printf("Handler %p\n",this);
  m_counter=0;

}



void RCDImaster::setPool(RcePic::Pool *p){_pool=p;}

void RCDImaster::setDriver(RcePgp::Driver *pgpd){_pgp_driver=pgpd;}

void RCDImaster::setReceiver(Receiver* receiver){_receiver=receiver;}
Receiver* RCDImaster::receiver(){return _receiver;}

unsigned int RCDImaster::writeRegister(unsigned address, unsigned value){
  //  std::cout<<"Register write address "<<address<<" value "<<value<<std::endl;
  //std::cout<<"RCDImaster"<<std::endl;
  RcePic::Tds* tds=_pool->allocate();
  assert (tds!=0); 
  _tid++;
  new(tds->header())RegTxHeader(_tid,address, RegHeader::Write, value);
  tds->payloadsize(0);
  // printf("Write register\n");
  omni_mutex_lock pl( _data_mutex );
  _pgp_driver->post(tds,RcePgp::Driver::Contiguous,RcePgp::Driver::DoNotComplete); 
  //printf("P %d",_pgp_driver->port_number());
  //printf("b\n");
  //printf("Posted  \n");
  unsigned long abs_sec,abs_nsec;
  omni_thread::get_time(&abs_sec,&abs_nsec,0,RECEIVETIMEOUT);
  int signalled=_data_cond.timedwait(abs_sec,abs_nsec);
  if(signalled==0){ //timeout. Something went wrong with the data.
    printf("PGP Write Register: No reply from front end.\n");
    _pool->deallocate(tds);
    return RECEIVEFAILED;
  }
  //printf("Status %d\n",stat);
  //printf("a\n");
  //printf("Write register done\n");
  _pool->deallocate(tds);
  return _status;
}
unsigned int RCDImaster::blockWrite(RcePic::Tds* tds){
  _pgp_driver->post(tds,RcePgp::Driver::NonContiguous,RcePgp::Driver::DoNotComplete);
  return 0;
}

unsigned int RCDImaster::blockWrite(unsigned* data, int size, bool handshake,bool byteswap){
  //printf("Size %d\n",size);
   RcePic::Tds* tds=_pool->allocate();
  assert (tds!=0); 
  char* header=(char*)tds->header();
  int headersize=tds->headersize();
  for (int i=0;i<headersize;i++)header[i]=0;
  new(tds->header())BlockWriteTxHeader(handshake);
  unsigned* payload=(unsigned*)tds->payload();
  for(int i=0;i<size;i++){
#ifdef SWAP_DATA
#warning Swapping of data turned on
    if(byteswap)
      payload[i]= ((data[i]&0xff)<<8) | ((data[i]&0xff00)>>8) |
	  ((data[i]&0xff0000)<<8) | ((data[i]&0xff000000)>>8);
    else
      payload[i]=data[i]<<16 | data[i]>>16;
#else
    if(byteswap)
      payload[i]= ((data[i]&0xff)<<24) | ((data[i]&0xff00)<<8) |
	((data[i]&0xff0000)>>8) | ((data[i]&0xff000000)>>24);
    else{
      payload[i]=data[i];
      //   std::cout<<"data "<<data[i]<<std::endl;
    }
#endif
  }
  tds->payloadsize(size*sizeof(unsigned));
  tds->flush(true);
  _handshake=handshake;
  //printf("Posting config data\n");
  if(handshake){
    _data_mutex.lock();
    //m_timer.Start();
  }
  //else {
  //  m_timer.Start();
  //  m_counter=0;
  //}
  _pgp_driver->post(tds,RcePgp::Driver::NonContiguous,RcePgp::Driver::DoNotComplete);
  if(handshake){
    unsigned long abs_sec,abs_nsec;
    omni_thread::get_time(&abs_sec,&abs_nsec,0,RECEIVETIMEOUT);
    int signalled=_data_cond.timedwait(abs_sec,abs_nsec);
    _data_mutex.unlock();
    if(signalled==0){ //timeout. Something went wrong with the data.
      std::cout<<"PGP Block R/W: No reply from frontend."<<std::endl;
      _pool->deallocate(tds);
      return RECEIVEFAILED;
    }
  }
  _pool->deallocate(tds);
  //printf("Done posting config data\n");
  return 0;
} 

unsigned int RCDImaster::blockRead(unsigned* data, int size, std::vector<unsigned>& retvec){
  _blockread=true;
  blockWrite(data,size,true,false);
  _blockread=false;
  if(nBuffers()!=0){
    unsigned char *header, *payload;
    unsigned headerlen, payloadlen;
    currentBuffer(header, headerlen, payload, payloadlen);
    payloadlen/=sizeof(unsigned);
    unsigned* ptr=(unsigned*)payload;
    for(unsigned i=0;i<payloadlen;i++) retvec.push_back(*ptr++);
    discardCurrentBuffer();
  }
  return 0;
}
unsigned int RCDImaster::readBuffers(std::vector<unsigned char>& retvec){
  unsigned char *header, *payload;
  unsigned headerlen, payloadlen;
  unsigned count=0;
  while(nBuffers()!=0){
    count++;
    currentBuffer(header, headerlen, payload, payloadlen);
    for(unsigned i=0;i<payloadlen;i++) retvec.push_back(payload[i]);
    if(payloadlen%3!=0){
      retvec.push_back(0);
      if(payloadlen%3==1) retvec.push_back(0);
    }
    discardCurrentBuffer();
  }
  return count;
}
 
unsigned int RCDImaster::readRegister(unsigned address, unsigned &value){
  RcePic::Tds* tds=_pool->allocate();
  _tid++;
  if(_tid>0xffffff)_tid=0;
  new(tds->header())RegTxHeader(_tid,address, RegHeader::Read);
  tds->payloadsize(0);
  //  printf("Read register tid=%d\n",_tid);
  omni_mutex_lock pl( _data_mutex );
  _pgp_driver->post(tds,RcePgp::Driver::Contiguous,RcePgp::Driver::DoNotComplete); 
  unsigned long abs_sec,abs_nsec;
  omni_thread::get_time(&abs_sec,&abs_nsec,0,RECEIVETIMEOUT);
  int signalled=_data_cond.timedwait(abs_sec,abs_nsec);
  if(signalled==0){ //timeout. Something went wrong with the data.
     printf("PGP Read Register: No reply from frontend.\n");
     _pool->deallocate(tds);
    return RECEIVEFAILED;
  }
  value=_data;
  //printf("Read register done tid=%d\n",_tid);
  _pool->deallocate(tds);
  return _status;
} 

unsigned int RCDImaster::sendCommand(unsigned char opcode, unsigned context){
  //  if(opcode==111){
     // m_timer.Print("RCDImaster");
     // m_timer.Reset();
     // return 0;
    //}
  RcePic::Tds* tds=_pool->allocate();
  new(tds->header())CmdTxHeader(opcode, context&0xffffff );
  tds->payloadsize(0);
  _pgp_driver->post(tds,RcePgp::Driver::Contiguous,RcePgp::Driver::DoNotComplete);  
  _pool->deallocate(tds);
  return 0;
}

unsigned RCDImaster::transferred(RcePic::Tds* tds) {
  return ((unsigned*)tds->result())[1];
}

void RCDImaster::completed(RcePic::Tds *tds, RcePgp::Driver* pgpd)
{
  // if(tds->edw()!=0)printf("Completion error %d\n",tds->edw());
  //  _pool->deallocate(tds);
}
//int ibuf=0;
void RCDImaster::received(RcePic::Tds *tds, RcePgp::Driver *pgpd){
  PgpHeader *pgpheader=(PgpHeader*)tds->header();
  if (pgpheader->vc()==1 && pgpheader->destination()==0){ // Register
    //std::cout<<"Register"<<std::endl;
    RegRxHeader* rxheader= (RegRxHeader*)tds->header();
    unsigned tid=rxheader->pgpHeader.tid();
    if(tid!=_tid){
      printf ("Bad tid\n");
    }
    _status=rxheader->regHeader.status();
    _data=rxheader->regHeader.data;
    if (rxheader->regHeader.fail()){
      printf("Register operation failed\n");
    } else if (rxheader->regHeader.timeout()){
      printf("Register operation timed out\n");
    }
    pgpd->release(tds,true);
    omni_mutex_lock pl( _data_mutex );
    _data_cond.signal();
  } else if (pgpheader->vc()==0 && pgpheader->destination()==0){ // data
        //std::cout<<"Data"<<std::endl;
    if(_receiver!=NULL && _blockread==false){
      //      if(_handshake==false){
	PgpData pgpdata;
	pgpdata.header=(unsigned char*)tds->header();
	pgpdata.payload=(unsigned*)tds->payload();
	unsigned size=transferred(tds);
	const unsigned headersize=8;
	pgpdata.payloadSize=size/sizeof(unsigned)- headersize;
	if(size%4!=0)std::cout<<"PGP Data size not a multiple of 32 bit."<<std::endl;
	_receiver->receive(&pgpdata);
	//	m_counter++;
		//if(m_counter==16){
		//  m_timer.Stop();
		//}
            //}
      //   printf("%d Did not release buffer\n",ibuf++);
      pgpd->release(tds,true);
    }else{
      _buffers.push_back(tds);
    }
    if(_handshake){
      _handshake=false;
      omni_mutex_lock pl( _data_mutex );
      _data_cond.signal();
    }
  }else{
   printf("Received message with vc = %d dest = %d\n",pgpheader->vc(),pgpheader->destination());
   //assert(0);
   omni_mutex_lock pl( _data_mutex );
   _data_cond.signal();
  }
}
      
unsigned RCDImaster::nBuffers(){
  return _buffers.size();
}

int RCDImaster::currentBuffer(unsigned char*& header, unsigned &headerSize, unsigned char*&payload, unsigned &payloadSize){
  int retval=1;
  if(_buffers.empty()){
    header=0;
    headerSize=0;
    payload=0;
    payloadSize=0;
    retval=1;
  }else{
    RcePic::Tds *tds=*_buffers.begin();
    header=(unsigned char*)tds->header();
    headerSize=tds->headersize();
    payload=(unsigned char*)tds->payload();
    payloadSize=transferred(tds)-headerSize;
    retval=0;
  }
  return retval;
}

int RCDImaster::discardCurrentBuffer(){
  int retval=1;
  if(_buffers.empty()){
    retval=1;
  }else{
    RcePic::Tds *tds=*_buffers.begin();
    _pgp_driver->release(tds,true);
    _buffers.pop_front();
    retval=0;
  }
  return retval;
}





