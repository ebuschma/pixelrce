#ifndef BITSTREAM_HH
#define BITSTREAM_HH

#include <vector>

typedef std::vector<unsigned> BitStream;

class BitStreamUtils{
public:
  static void byteSwap(BitStream* bs){
    for (unsigned int i=0;i<bs->size();i++){
      unsigned tmp=(*bs)[i];
      (*bs)[i] = ((tmp&0xff)<<24) | ((tmp&0xff00)<<8) |
        ((tmp&0xff0000)>>8) | ((tmp&0xff000000)>>24);
    }
  }
  static void prependZeros(BitStream *bs, int num=4){
    for(int i=0;i<num;i++)bs->push_back(0);
  }
};

#endif
