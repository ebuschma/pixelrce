// 
// IS monitoring
// 
// Martin Kocian, SLAC, 2/11/2016
//

#ifndef RCEMONITORING_HH
#define RCEMONITORING_HH

#include "config/FWRegisters.hh"


  class Monitoring{ 
  public:
    Monitoring(){};
    virtual ~Monitoring(){}
    virtual void Publish(){}
    virtual void Reset(){}
    virtual void SetOccupancy(int fe, float occ){}
    virtual void SetAverageOccupancy(float occ){}
    virtual void SetNumberOfEvents(unsigned nevt){}
    virtual void SetNumberOfCleanEvents(unsigned nevt){}
    virtual void SetNMissed(unsigned nevt){}
    virtual void SetDisabledMask(unsigned nevt){}
    virtual void SetDisabledInRunMask(unsigned nevt){}
    virtual void SetDisabledPermMask(unsigned nevt){}
    virtual void SetTtcBusyCounter(unsigned nevt){}
    virtual void SetErrorCounter(int fe, unsigned nevt, FWRegisters::EFBCOUNTER counter){}
    virtual unsigned GetErrorCounter(int fe, FWRegisters::EFBCOUNTER counter){return 0;}
    virtual void SetTtcClashCounter(unsigned nevt, FWRegisters::CLASH counter){}
    virtual void SetTimingScanInfo(bool enabled, bool running, int step, int nstep){};
    virtual void SetHptdcInfo(int hptdc, int reg, int val){};
    virtual int  GetRce(){return 0;}
  };
    
#endif
