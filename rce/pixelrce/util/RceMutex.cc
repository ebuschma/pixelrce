
#include <stdlib.h>
#include <errno.h>
#include "util/RceMutex.hh"
#include <sys/time.h>

///////////////////////////////////////////////////////////////////////////
//
// Mutex
//
///////////////////////////////////////////////////////////////////////////


rce_mutex::rce_mutex(void)
{
  pthread_mutex_init(&posix_mutex, 0);
}

rce_mutex::~rce_mutex(void)
{
  pthread_mutex_destroy(&posix_mutex);
}


///////////////////////////////////////////////////////////////////////////
//
// Condition variable
//
///////////////////////////////////////////////////////////////////////////


rce_condition::rce_condition(rce_mutex* m) : mutex(m)
{
  pthread_cond_init(&posix_cond, 0);
}

rce_condition::~rce_condition(void)
{
  pthread_cond_destroy(&posix_cond);
}

void
rce_condition::wait(void)
{
  pthread_cond_wait(&posix_cond, &mutex->posix_mutex);
}

int
rce_condition::timedwait(int32_t secs, int32_t nanosecs)
{
    timespec rqts = { secs, nanosecs };

again:
    int rc = pthread_cond_timedwait(&posix_cond, &mutex->posix_mutex, &rqts);
    if (rc == 0)
	return 1;

    if (rc == EINTR)
      goto again;

    if (rc == ETIMEDOUT)
	return 0;

}

void
rce_condition::signal(void)
{
  pthread_cond_signal(&posix_cond);
}

void
rce_condition::broadcast(void)
{
  pthread_cond_broadcast(&posix_cond);
}


void rce_get_time(int32_t* abs_sec, int32_t* abs_nsec,
		      int32_t rel_sec, int32_t rel_nsec)
{
    timespec abs;

    struct timeval tv;
    gettimeofday(&tv, NULL); 
    abs.tv_sec = tv.tv_sec;
    abs.tv_nsec = tv.tv_usec * 1000;

    abs.tv_nsec += rel_nsec;
    abs.tv_sec += rel_sec + abs.tv_nsec / 1000000000;
    abs.tv_nsec = abs.tv_nsec % 1000000000;


    *abs_sec = abs.tv_sec;
    *abs_nsec = abs.tv_nsec;
}

