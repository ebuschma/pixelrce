#include "util/RceName.hh"
#include <stdlib.h>
#include <assert.h>

int RceName::m_number=0;
bool RceName::m_initialized=false;

int RceName::getRceNumber(){
  if(m_initialized==false){
    char* name=getenv("RCE_NAME");
    char* endptr;
    if(name){
      m_number=(int)strtol(name, &endptr, 0);
      assert(*endptr=='\0');
    }
    else m_number=0;
    m_initialized=true;
  }
  return m_number;
}
