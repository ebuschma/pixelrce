
#include <map>
#include <stdint.h>
#include <iostream>
#include <iomanip>
#include <util/RceProfiling.hh>
static std::map<std::string,std::array<uint64_t,4>>  _profile_entry;
void _profile_::log(const std::string &name,uint64_t clk_ticks,uint64_t bytes_in,uint64_t bytes_out) {
  std::array<uint64_t,4> &cnt=_profile_entry[name];
  cnt[0]+=clk_ticks;
  cnt[1]+=bytes_in;
  cnt[2]+=bytes_out;
  cnt[3]++;
};  


void  _profile_::print() {
  std::cout << std::left 
	    << std::setw(20) << "Name"
	    << std::setw(10) << "calls" 
	    << std::setw(10) << "sec" 
	    << std::setw(10) << "mb_in"
	    << std::setw(10) << "mb_out"
	    << std::setw(10) << "mbps_in"
	    << std::setw(10) << "mbps_out" << std::endl;
  for(const auto &e:_profile_entry) {
    std::cout <<std::left << std::setw(20) << e.first;
    double ticks=e.second[0]/1e9;
    double in=e.second[2];
    double out=e.second[1];
    std::cout <<  std::setw(10) << e.second[3]
	      <<  std::setw(10) << ticks
	      <<  std::setw(10) << in/1000/1000
	      <<  std::setw(10) << out/1000/1000
	      <<  std::setw(10) << in/1000/1000/ticks*8
		<<  std::setw(10) << out/1000/1000/ticks*8
	      << std::endl;
  }
  
}
void  _profile_::clear() {_profile_entry.clear();}

