#ifndef HISTO_MANAGER_HH
#define HISTO_MANAGER_HH

#include <vector>
#include <string>
#include <map>
#include "util/RCFProvider.hh"




class AbsRceHisto;
class Provider;
#include <iostream>

class HistoManager{
public:
  HistoManager(Provider* p);
  ~HistoManager();
  static HistoManager* instance(){
    return m_manager;
  }
  void addToInventory(AbsRceHisto*);
  void removeFromInventory(AbsRceHisto*);
  void publish(const char* reg);
  std::vector<std::string> getHistoNames(const char* reg);
  std::vector<std::string> getPublishedHistoNames();
private:
  std::map<std::string,AbsRceHisto*> m_inventory;
  std::map<std::string,AbsRceHisto*> m_published;
  Provider* m_provider;
  static HistoManager* m_manager;
};

#endif
