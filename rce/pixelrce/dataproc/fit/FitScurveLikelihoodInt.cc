#include "dataproc/fit/FitScurveLikelihoodInt.hh"
#include "dataproc/fit/FitData.hh"
#include "config/ConfigIF.hh"
#include <iostream>
#include <stdio.h>

template<typename TP, typename TE>
FitScurveLikelihoodInt<TP, TE>::FitScurveLikelihoodInt(ConfigIF *cif,std::vector<std::vector<RceHisto2d<TP, TE>*> > &histo,std::vector<int> &vcal,int nTrigger,const char* name):AbsFit(cif,vcal,nTrigger),m_histo(histo),m_name(name) {
  initFit(&m_fit);
}

template<typename TP, typename TE>
FitScurveLikelihoodInt<TP, TE>::~FitScurveLikelihoodInt(){
  for(unsigned int module=0;module<m_fithisto.size();module++) {
    for(size_t i=0;i<m_fithisto[module].size();i++){
      delete m_fithisto[module][i];
    }
  }
}
      

template<typename TP, typename TE>
int FitScurveLikelihoodInt<TP, TE>::doFit(int fitopt) {
 FitData pfdi,pfdo;
  GaussianCurve gc;
  int nBins=m_vcal.size();
  if(!nBins) return -1;
  m_x=new UINT32[nBins];
  m_y=new UINT8[nBins];
 
  for(unsigned int module=0;module<m_config->getNmodules();module++) {
    int nRows= m_config->getModuleInfo(module).getNRows();
    int nCols=m_config->getModuleInfo(module).getNColumns();
    //int nHistos=m_histo[module].size();
    std::cout << "nTrigger " << m_nTrigger << std::endl;
    for(int chip=0;chip<m_config->getModuleInfo(module).getNFrontends();chip++) {
      if((fitopt&NOCONV)==0)  {
	/* apply dac to electron calibration */
	for(int bin=0;bin<nBins;bin++) {
	  /* for now we use float here */
	  m_x[bin]=(UINT32)m_config->dacToElectrons(module,chip,m_vcal[bin])*10;
	}
      } else {
	for(int bin=0;bin<nBins;bin++) {
	  /* for now we use float here */
	  m_x[bin]=m_vcal[bin]*10;
	}
      }
      for(int col=0;col<nCols;col++) {
	for(int row=0;row<nRows;row++) {
	  // extract S-Curve
	  for(int bin=0;bin<nBins;bin++) {
	    m_y[bin]=(UINT8)(*m_histo[module][bin])(row, chip*nCols+col);
	  }
	  pfdi.n=nBins;
	  pfdi.x=&m_x[0]; 
	  pfdi.y=&m_y[0];
	  int goodBins=extractGoodData(&pfdi,&pfdo,m_nTrigger);
	  if(goodBins<5) continue;
	  m_fit.curve=&gc;
	  m_fit.maxIters=100;
	  gc.a0=m_nTrigger;
	  int result=fitPixel(&pfdo,&m_fit);
	  if(!result) {
	    m_fithisto[module][0]->set(chip*nCols+col,row,(double)gc.mu/10000.);
	    m_fithisto[module][1]->set(chip*nCols+col,row,(double)gc.sigma/10000.);
	    m_fithisto[module][2]->set(chip*nCols+col,row,(double)m_fit.chi2/100000.);
	    m_fithisto[module][3]->set(chip*nCols+col,row,(double) m_fit.nIters);
	    //	    printf("%d %d %d %f %f %f %d\n",chip,col,row,gc.mu/10000.,gc.sigma/10000.,m_fit.chi2/100000.,m_fit.nIters);
	    }
	}  //rows
      } //columns
    } //chips
  } // modules
  delete [] m_x;
  delete [] m_y;
  return 0;
}

template<typename TP, typename TE>
typename FitScurveLikelihoodInt<TP, TE>::INT32 *FitScurveLikelihoodInt<TP, TE>::log_ext(INT32 x, GaussianCurve *psp) {
  INT32 t;
  INT32 u;
  int n;
  INT64 i64a,i64b;
  u=(x*100 - psp->mu/100);
  i64a=u;
  i64a*=10000000LL;
  i64b=(INT64) psp->sigma;
  i64a/=i64b;
  t = (INT32) i64a;
  n = (UINT32) LUT_WIDTH + (UINT32)t;
  if(n < 0) n = 0;
  if(n >= LUT_LENGTH) n = LUT_LENGTH-1; // truncate at last value  
  n = 2 * n;
  return (INT32*) &FitDataInt::data_logx[n];
}

template<typename TP, typename TE>
typename FitScurveLikelihoodInt<TP, TE>::UINT32 FitScurveLikelihoodInt<TP, TE>::errf_ext(INT32 x, GaussianCurve *psp) {
  INT32 t;
  INT32 u;
  INT64 i64a,i64b;
  int n;
  u = (x*100 - psp->mu/100);
  i64a=(INT64) u;
  i64a*=10000000LL;
  i64b=(INT64) psp->sigma;
  i64a/=i64b;
  t = (INT32) i64a;
  n = LUT_WIDTH + (UINT32)t;
  if(n < 0) n = 0;
  if(n >= LUT_LENGTH) n = LUT_LENGTH-1; // truncate at last value 
  return FitDataInt::data_errf[n];
}

template<typename TP, typename TE>
typename FitScurveLikelihoodInt<TP, TE>::UINT32 FitScurveLikelihoodInt<TP, TE>::logLikelihood(FitData *pfd, GaussianCurve *s) {
  INT32 r, N;
  INT64 acc,y2,y3;
  UINT32 *x; 
  UINT8 *y;
  UINT32 ret;
  INT32 *p;
  INT64 i64;
  int k, n;
  x = pfd->x;     y = pfd->y;     n = pfd->n;
  acc = 0;
  N = s->a0;
  for(k=0;k<n;++k) {
    r = *y++; /* data point */
    p = log_ext(*x++,s); /* pointer to log(probability) */
    y2=(INT64) r;
    i64=(INT64)*p++;
    y2*=i64;
    y3=(N - r);
    i64=*p;
    y3*=i64;    
    acc -= (y2 + y3);
  }
  acc/=10000ULL;
  ret=(UINT32) (acc);
  return (ret);
}

template<typename TP, typename TE>
int FitScurveLikelihoodInt<TP, TE>::initFit(Fit *pFit) {
  pFit->deltaSigma = 1000;
  pFit->deltaMu = 1000;
  pFit->maxIters = 100;
  pFit->muEpsilon = 1; /* 0.01% */
  pFit->sigmaEpsilon = 1; /* 0.01% */
  for (unsigned int i=0;i<m_config->getNmodules();i++){
    std::vector<RceHisto2d<float, float>*> vh;
    m_fithisto.push_back(vh);
    char name[128];
    char title[128];
    char suffix[128]="";
    if(std::string(m_name)!=""){
      sprintf(suffix,"%s_",m_name);
    }

    int rows=m_config->getModuleInfo(i).getNRows();
    int cols=m_config->getModuleInfo(i).getNColumns()*m_config->getModuleInfo(i).getNFrontends();
    int id=m_config->getModuleInfo(i).getId();
    std::string modName=m_config->getModuleInfo(i).getName();
    sprintf(name,"Mod_%d_%sMean", id, suffix);
    sprintf(title,"MOD %d %s at %s SCURVE MEAN", id, suffix, modName.c_str());
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,title,cols,0,cols,rows, 0, rows));
    sprintf(title,"MOD %d %s at %s SCURVE SIGMA", id, suffix, modName.c_str());
    sprintf(name,"Mod_%d_%sSigma", id, suffix);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,title,cols,0,cols,rows, 0, rows));
    sprintf(title,"MOD %d %s at %s SCURVE CHI2", id, suffix, modName.c_str());
    sprintf(name,"Mod_%d_%sChiSquare", id, suffix);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,title,cols,0,cols,rows, 0, rows));
    sprintf(title,"MOD %d %s at %s SCURVE ITER", id, suffix, modName.c_str());
    sprintf(name,"Mod_%d_%sIter", id, suffix);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,title,cols,0,cols,rows, 0, rows));
  }
  return 0;
}

template<typename TP, typename TE>
int FitScurveLikelihoodInt<TP, TE>::extractGoodData(FitData *pfdi,FitData *pfdo, UINT32 nTriggers) {
  int hitsStart, hitsEnd, nBins;
  UINT32 a0; 
  UINT8 *y;
  
  nBins = pfdi->n;
  y = pfdi->y;
  
  //new method to find hitsStart
  
  for(hitsStart = nBins-1;hitsStart >= 0; --hitsStart)
    if(y[hitsStart] == 0) break;
  
  if(hitsStart ==0)
    return 0;
  if(hitsStart == nBins)
    return 0;
  
  a0 = y[nBins - 1]; // last bin has at least 90% hits
  if(a0*999 < nTriggers*900)
    return 0;
  //failure mode, never reaches A0
  
  //dan's new method
  for(hitsEnd = hitsStart; hitsEnd < nBins; ++hitsEnd)
    if(y[hitsEnd] >= a0) break;
  
  // add 2 for old method or 1 for new method
  //      1 because of where we start comparing and
  //      1 because we want to include one instance of the maximum 
  nBins = 1 + hitsEnd - hitsStart; 
  
  if(hitsStart < 0)  hitsStart =0;
  
  if(hitsStart == hitsEnd) return 0;
  
  if(nBins < 2) {
    return 0;
    //failure mode :: not enough data points
  }  
  if(nBins < 0) nBins = 0;
  pfdo->n = nBins;
  pfdo->y = &y[hitsStart];
  pfdo->x = &pfdi->x[hitsStart];
  return nBins;
}


template<typename TP, typename TE>
int FitScurveLikelihoodInt<TP, TE>::initialGuess(FitData *pfd, GaussianCurve *pSParams) {
#define invRoot6 4082482904LL
#define invRoot2 7071067811ULL
  UINT32 pt1, pt2, pt3; 
  UINT8 *pdlo, *pdhi; 
  UINT32 y_lo, y_hi, sigma;
  UINT32 *x; 
  UINT8 *y; 
  UINT32 a0; 
  UINT32 minSigma;
  UINT32 j, k, n, lo1, lo2, lo3, hi1, hi2, hi3, status;
  UINT32 x1,x2,x3;
  UINT64 u64;
  INT64 i64;
  INT32  xdiff;
  
  n = pfd->n;
  if(n<5) return 1;
  pSParams->a0 = pfd->y[pfd->n-1]; // assumes last data point sets plateau 
  a0 = pSParams->a0*100000-1; // skim a token amount to ensure comparisons succeed 
  xdiff=pfd->x[1] - pfd->x[0]; 
  i64=(INT64) xdiff;
  i64*=invRoot6;
  i64/=100000000000LL;
  minSigma = (UINT32) i64;
  //     minSigma = (UINT32) (((INT64) (pfd->x[1] - pfd->x[0])) * invRoot6) / 1000000LL;
  
  x = pfd->x; y = pfd->y;
  pt1 = 16* a0;
  pt2 = 50* a0;
  pt3 = 84* a0;
  // find the range over which the data crosses the 16%, 50% and 84% points 
  hi1 = hi2 = hi3 = 0; // invalid values 
  lo1 = lo2 = lo3 = 0; // invalid values 
  pdlo = &y[0]; // y 
  pdhi = &y[n-1]; // y + n - 1 
  // important note: for the sake of speed we want to perform as few comparisons as
  //possible. Therefore, the integer comparison occurs first in each of the 
  //following operations. Depending on the logical value thereof, the next
  //comparison will be made only if necessary. To further expedite the code,
    //arrays are not used because there are only three members 
  j = n - 1;
  for(k=0;k<n;++pdlo, --pdhi) {
    y_lo = (*pdlo)*10000000;
    y_hi = (*pdhi)*10000000;
    if(!lo1 && (y_lo >= pt1)) lo1 = k;
    if(!lo2 && (y_lo >= pt2)) lo2 = k;
    if(!lo3 && (y_lo >= pt3)) lo3 = k;
    if(!hi1 && (y_hi <= pt1)) hi1 = j;
    if(!hi2 && (y_hi <= pt2)) hi2 = j;
    if(!hi3 && (y_hi <= pt3)) hi3 = j;
    --j;
    ++k;
  }
  x1 = (x[lo1] + x[hi1]) *5;
  x2 = (x[lo2] + x[hi2]) *5;
  x3 = (x[lo3] + x[hi3]) *5;
  
  // mu = threshold 
  
  pSParams->mu = (UINT32) x2;
  xdiff=(x3 - x1);
  u64=(UINT64) xdiff;
  u64*=invRoot2;
  u64/=100000000LL;
  sigma=(UINT32) u64;
  if(sigma < minSigma) {
    sigma = minSigma; 
  }  
  pSParams->sigma=sigma; 
  status = 0;
  return status;
}
template<typename TP, typename TE>
int FitScurveLikelihoodInt<TP, TE>::fitPixel(FitData *pfd,void *vpfit) {
  INT32 deltaSigma, deltaMu, sigma, chi2Min, *fptr;
  INT32 chi2[9];
  UINT64 u64;
  GaussianCurve sParams, sParamsWork, *psp;
  int k, dirMin;
  Fit *pFit;
  pFit = (Fit *)vpfit;
  
  pFit->nIters = 0;
  pFit->converge = 0; // assume no convergence 
  
  pFit->ndf = pfd->n - 2; // degrees of freedom 
  
  // take a guess at the S-curve parameters 
  
  if(initialGuess(pfd,&sParams)) {
    psp = (GaussianCurve *)pFit->curve;
    psp->a0 = sParams.a0;
    psp->mu = sParams.mu;
    psp->sigma = sParams.sigma;
    pFit->converge = 1;
    pFit->muConverge = 0;
    pFit->sigmaConverge = 0;
    pFit->chi2 = 0;
    return 0;
  }
  
  // initialize loop parameters 
  psp = &sParamsWork;
  psp->a0 = sParams.a0;
  deltaSigma = sParams.sigma*100; // scaled 
  deltaMu = sParams.mu*100; // scaled 
  sParams.sigma*= pFit->deltaSigma;
  sParams.mu*= pFit->deltaMu;
  
  
  //* the loop begins *
  //  printf("delta %d %d\n",deltaSigma,deltaMu);
  while(!pFit->converge && (pFit->nIters++ < pFit->maxIters)) {    
    dirMin = 0;
    fptr = chi2;
    for(k=0;k<9;++k) *fptr++ = 0;
    
    while((dirMin >= 0) && (pFit->nIters++ < pFit->maxIters)) {
      
      //* calculate neighboring points *
      //* calculate neighboring points *
      //  printf("!! %d %d\n",sParams.sigma,deltaSigma);
      psp->sigma=sParams.sigma - deltaSigma;
      psp->mu = sParams.mu - deltaMu;
      if(chi2[0] == 0)
        chi2[0] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[7] == 0)
        chi2[7] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;  
      if(chi2[6] == 0)
        chi2[6] = logLikelihood(pfd,psp);
      
      psp->sigma=sParams.sigma;
      psp->mu = sParams.mu - deltaMu;
      if(chi2[1] == 0)
        chi2[1] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[8] == 0)
        chi2[8] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;
      if(chi2[5] == 0)
	chi2[5] = logLikelihood(pfd,psp);
      
      psp->sigma=sParams.sigma+deltaSigma;
      psp->mu = sParams.mu - deltaMu;
      if(chi2[2] == 0)
        chi2[2] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[3] == 0)
        chi2[3] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;
      if(chi2[4] == 0)
        chi2[4] = logLikelihood(pfd,psp);
      
      dirMin = -1;
      chi2Min = chi2[8]; // first guess at minimum 
      for(k=0, fptr=chi2;k<8;++k, ++fptr) {
        if(*fptr < chi2Min) {
          chi2Min = *fptr;
          dirMin = k;
        }
      }
      //                 printf("\n");
      switch(dirMin) {
      case -1:
        deltaSigma = deltaSigma /10;
        deltaMu = deltaMu /10;
        break;
      case 0:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[0];
        chi2[3] = chi2[1];
        chi2[4] = chi2[8];
        chi2[5] = chi2[7];
        chi2[0] = chi2[1] = chi2[2] = 
          chi2[6] = chi2[7] = 0;
        break;
      case 1:
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[1];
        chi2[3] = chi2[2];
        chi2[4] = chi2[3];
        chi2[5] = chi2[8];
        chi2[6] = chi2[7];
        chi2[7] = chi2[0];
        chi2[0] = chi2[1] = chi2[2] = 0;
        break;
      case 2:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[2];
        chi2[5] = chi2[3];
        chi2[6] = chi2[8];
        chi2[7] = chi2[1];
        chi2[0] = chi2[1] = chi2[2] = 
          chi2[3] = chi2[4] = 0;
        break;
      case 3:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        chi2[8] = chi2[3];
        chi2[5] = chi2[4];
        chi2[6] = chi2[5];
        chi2[7] = chi2[8];
        chi2[0] = chi2[1];
        chi2[1] = chi2[2];
        chi2[2] = chi2[3] = chi2[4] = 0;
        break;
      case 4:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[4];
        chi2[7] = chi2[5];
        chi2[0] = chi2[8];
        chi2[1] = chi2[3];
        chi2[2] = chi2[3] = chi2[4] = 
          chi2[5] = chi2[6] = 0;
        break;
      case 5:
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[5];
        chi2[7] = chi2[6];
        chi2[0] = chi2[7];
        chi2[1] = chi2[8];
        chi2[2] = chi2[3];
        chi2[3] = chi2[4];
        chi2[4] = chi2[5] = chi2[6] = 0;
        break;
      case 6:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[6];
        chi2[1] = chi2[7];
        chi2[2] = chi2[8];
        chi2[3] = chi2[5];
        chi2[4] = chi2[5] = chi2[6] = 
          chi2[7] = chi2[0] = 0;
        break;
      case 7:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        chi2[8] = chi2[7];
        chi2[1] = chi2[0];
        chi2[2] = chi2[1];
        chi2[3] = chi2[8];
        chi2[4] = chi2[5];
        chi2[5] = chi2[6];
        chi2[6] = chi2[7] = chi2[0] = 0;
        break;
      }
    }
    
    
    if((unsigned int)deltaSigma/10 < ((pFit->sigmaEpsilon * sParams.sigma)/100000) &&
       (unsigned int)deltaMu/10 < (pFit->muEpsilon * sParams.mu)/100000) {
      pFit->converge = 1;
      break;
    }
  }
  psp = (GaussianCurve *)pFit->curve;
  *psp = sParams;
  pFit->chi2 = chiSquared(pfd,psp) / pFit->ndf;
  u64=(UINT64) sParams.sigma;
  u64*=70710678ULL;
  u64/=10000000000ULL;
  psp->sigma=(UINT32) (UINT64) u64;
  
  return pFit->converge ? 0 : 1; // 0 = success 
  
}

template<typename TP, typename TE>
typename FitScurveLikelihoodInt<TP, TE>::UINT32 FitScurveLikelihoodInt<TP, TE>::chiSquared(FitData *pfd,GaussianCurve *s) {
  UINT64 acc; 
  INT32 y0,y1,y2;
  INT64 y3; 
  INT64 ui64;
  UINT32 a0, chi2;
  UINT32 *x; 
  UINT8 *y;
  INT32 x0;
  int j, k, n;
  x = pfd->x;     y = pfd->y;     n = pfd->n;
  acc = 0; 
  a0 = s->a0;
  /* use binomial weighting */
  for(k=0;k<n;++k) {
    x0 = *x++; 
    y0 = *y++; 
    y1 = errf_ext(x0,s); 
    j = (int)(y1/1000); 
    y2 = y0*1000000 - (a0 * y1); 
    y2/=1000;
    y3=(INT64) y2;
    y3*=y3;
    ui64=(UINT64)FitDataInt::binomial_weight[j];
    ui64*=y3;
    acc+=ui64;
  }
  acc/=10000000ULL;
  chi2=(UINT32) acc;
  chi2/=a0; 
  return chi2;
}
