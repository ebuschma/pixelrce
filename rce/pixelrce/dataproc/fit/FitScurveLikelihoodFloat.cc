#include "dataproc/fit/FitScurveLikelihoodFloat.hh"
#include "dataproc/fit/FitData.hh"
#include "config/ConfigIF.hh"
#include <stdio.h>
#include <iostream> 

template<typename TP, typename TE>
FitScurveLikelihoodFloat<TP, TE>::FitScurveLikelihoodFloat(ConfigIF *cif,std::vector<std::vector<RceHisto2d<TP, TE>*> > &histo,std::vector<int> &vcal,int nTrigger):AbsFit(cif,vcal,nTrigger),m_histo(histo) {
  initFit(&m_fit);
}

template<typename TP, typename TE>
FitScurveLikelihoodFloat<TP, TE>::~FitScurveLikelihoodFloat()
{
  for(unsigned int module=0;module<m_config->getNmodules();module++) {
    for(size_t i=0;i<m_fithisto[module].size();i++){
      delete m_fithisto[module][i];
    }
  }
}

template<typename TP, typename TE>
int FitScurveLikelihoodFloat<TP, TE>::doFit() { 
  FitData pfdi,pfdo;
  GaussianCurve gc;
  int nBins=m_vcal.size();
  if(!nBins) return -1;
  m_x=new float[nBins];
  m_y=new float[nBins];
 
  for(unsigned int module=0;module<m_config->getNmodules();module++) {
    int nRows= m_config->getModuleInfo(module).getNRows();
    int nCols=m_config->getModuleInfo(module).getNColumns();
    //int nHistos=m_histo[module].size();
    std::cout << "nTrigger " << m_nTrigger << std::endl;
    for(int chip=0;chip<m_config->getModuleInfo(module).getNFrontends();chip++) {
      /* apply dac to electron calibration */
      for(int bin=0;bin<nBins;bin++) {
	m_x[bin]=m_config->dacToElectrons(module,chip,m_vcal[bin]);
      }
      for(int col=0;col<nCols;col++) {
	for(int row=0;row<nRows;row++) {
	  // extract S-Curve
	  for(int bin=0;bin<nBins;bin++) {
	    m_y[bin]=(*m_histo[module][bin])(row, chip*nCols+col);
	  }
	  pfdi.n=nBins;
	  pfdi.x=&m_x[0]; 
	  pfdi.y=&m_y[0];
	  int goodBins=extractGoodData(&pfdi,&pfdo,m_nTrigger);
	  if(goodBins<5) continue;
	  m_fit.curve=&gc;
	  m_fit.maxIters=100;
	  gc.a0=m_nTrigger;
	  int result=fitPixel(&pfdo,&m_fit);
	  if(!result) {
	    m_fithisto[module][0]->set(chip*nCols+col,row,gc.mu);
	    m_fithisto[module][1]->set(chip*nCols+col,row,gc.sigma);
	    m_fithisto[module][2]->set(chip*nCols+col,row,m_fit.chi2);
	    m_fithisto[module][3]->set(chip*nCols+col,row,(float) m_fit.nIters);
	    printf("%d %d %d %f %f %f %d\n",chip,col,row,gc.mu,gc.sigma,m_fit.chi2,m_fit.nIters);
	    }
	}  //rows
      } //columns
    } //chips
  } // modules
  delete [] m_x;
  delete [] m_y;
  return 0;  
}
template<typename TP, typename TE>
float* FitScurveLikelihoodFloat<TP, TE>::log_ext(float x, GaussianCurve *psp) {
  float u, t;
  int n;
  u = (x - psp->mu) / psp->sigma;
  t = u * inverse_lut_interval;
  n = LUT_WIDTH + (int)t;
  if(n < 0) n = 0;
  if(n >= LUT_LENGTH) n = LUT_LENGTH-1; // truncate at last value
  n = 2 * n;
  return (float*)&FitDataFloat::data_logx[n];
}
template<typename TP, typename TE>
float FitScurveLikelihoodFloat<TP, TE>::errf_ext(float x, GaussianCurve *psp) {
  float u, t;
  int n;
  
  u = (x - psp->mu) / psp->sigma;
  t = u * inverse_lut_interval;
  n = LUT_WIDTH + (int)t;
  if(n < 0) n = 0;
  if(n >= LUT_LENGTH) n = LUT_LENGTH-1; // truncate at last value 
  return FitDataFloat::data_errf[n];
}
template<typename TP, typename TE>
float FitScurveLikelihoodFloat<TP, TE>::logLikelihood(FitData *pfd, GaussianCurve *s) {
  float acc, r, y2, y3, N;
  float *x, *y, *p;
  int k, n;
  x = pfd->x;     y = pfd->y;     n = pfd->n;
  acc = 0.0f;
  N = s->a0;
  for(k=0;k<n;++k) {
    r = *y++; /* data point */
    p = log_ext(*x++,s); /* pointer to log(probability) */
    y2 = r * (*p++); /* log(prob) */
    y3 = (N - r) * (*p); /* log(1-prob) */
    acc -= (y2 + y3);
  }
  return acc;
}

template<typename TP, typename TE>
int FitScurveLikelihoodFloat<TP, TE>::initFit(Fit *pFit) {
  pFit->deltaSigma = 0.1f;
  pFit->deltaMu = 0.1f;
  pFit->maxIters = 100;
  pFit->muEpsilon = 0.0001f; /* 0.01% */
  pFit->sigmaEpsilon = 0.0001f; /* 0.01% */
  /* create histograms */
  for (unsigned int i=0;i<m_config->getNmodules();i++){
    std::vector<RceHisto2d<float, float>*> vh;
    m_fithisto.push_back(vh);
    char name[128];
    //char title[128];
    int rows=m_config->getModuleInfo(i).getNRows();
    int cols=m_config->getModuleInfo(i).getNColumns()*m_config->getModuleInfo(i).getNFrontends();
    int id=m_config->getModuleInfo(i).getId();
    sprintf(name,"Mod_%d_Mean",id);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,"SCURVE_MEAN",cols,0,cols,rows, 0, rows));
    sprintf(name,"Mod_%d_Sigma",id);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,"SCURVE_SIGMA",cols,0,cols,rows, 0, rows));
    sprintf(name,"Mod_%d_ChiSquare",id);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,"SCURVE_CHI2",cols,0,cols,rows, 0, rows));
    sprintf(name,"Mod_%d_Iter",id);
    m_fithisto[i].push_back(new RceHisto2d<float, float>(name,"SCURVE_ITER",cols,0,cols,rows, 0, rows));
  }
  // allocate fit data buffer


  return 0;
}
template<typename TP, typename TE>
int FitScurveLikelihoodFloat<TP, TE>::extractGoodData(FitData *pfdi,FitData *pfdo, float scanA0) {
  int hitsStart, hitsEnd, nBins;
  float a0, *y;
  
  nBins = pfdi->n;
  y = pfdi->y;
  
  //new method to find hitsStart  
  for(hitsStart = nBins-1;hitsStart >= 0; --hitsStart)
    if(y[hitsStart] == 0) break;
  
  if(hitsStart ==0)
    return 0;
  if(hitsStart == nBins)
    return 0;  
  a0 = 0.999f * y[nBins - 1]; // just under last bin 
  if(a0 < 0.9 * scanA0)
    return 0;
  //failure mode, never reaches A0

  for(hitsEnd = hitsStart; hitsEnd < nBins; ++hitsEnd)
    if(y[hitsEnd] > a0) break;
  nBins = 1 + hitsEnd - hitsStart;   
  if(hitsStart < 0)
    hitsStart =0;
  
  if(hitsStart == hitsEnd)
    return 0;
  
  if(nBins < 2) {
    return 0;
    //failure mode :: not enough data points
  }
  if(nBins < 0)  nBins = 0;
  pfdo->n = nBins;
  
  pfdo->y = &y[hitsStart];
  pfdo->x = &pfdi->x[hitsStart];
  return nBins;
}

template<typename TP, typename TE>
int FitScurveLikelihoodFloat<TP, TE>::initialGuess(FitData *pfd, GaussianCurve *pSParams){
#define invRoot6f  0.40824829046f
#define invRoot2f  0.7071067811f
  float pt1, pt2, pt3, *pdlo, *pdhi, y_lo, y_hi, x1, x2, x3, sigma;
  float *x, *y, a0, minSigma;
  int j, k, n, lo1, lo2, lo3, hi1, hi2, hi3, status;

  pSParams->a0 = pfd->y[pfd->n-1]; // assumes last data point sets plateau 
  a0 = pSParams->a0;
  a0 -= 0.00001f; // skim a token amount to ensure comparisons succeed 

  n = pfd->n; // number of samples 

  minSigma = (pfd->x[1] - pfd->x[0]) * invRoot6f;
 
    x = pfd->x; y = pfd->y;
    pt1 = 0.16f * a0;
    pt2 = 0.50f * a0;
    pt3 = 0.84f * a0;
        // find the range over which the data crosses the 16%, 50% and 84% points 
    hi1 = hi2 = hi3 = 0; // invalid values 
    lo1 = lo2 = lo3 = 0; // invalid values 
    pdlo = &y[0]; // y 
    pdhi = &y[n-1]; // y + n - 1 
    // important note: for the sake of speed we want to perform as few comparisons as
    //possible. Therefore, the integer comparison occurs first in each of the 
    //following operations. Depending on the logical value thereof, the next
    //comparison will be made only if necessary. To further expedite the code,
                //arrays are not used because there are only three members 
    j = n - 1;
    for(k=0;k<n;++pdlo, --pdhi) {
      y_lo = *pdlo;
      y_hi = *pdhi;
      if(!lo1 && (y_lo >= pt1)) lo1 = k;
      if(!lo2 && (y_lo >= pt2)) lo2 = k;
      if(!lo3 && (y_lo >= pt3)) lo3 = k;
                        if(!hi1 && (y_hi <= pt1)) hi1 = j;
                        if(!hi2 && (y_hi <= pt2)) hi2 = j;
                        if(!hi3 && (y_hi <= pt3)) hi3 = j;
                        --j;
                        ++k;
    }
    x1 = (x[lo1] + x[hi1]) * 0.5f;
    x2 = (x[lo2] + x[hi2]) * 0.5f;
    x3 = (x[lo3] + x[hi3]) * 0.5f;
    
    pSParams->mu = x2;
 
    sigma = (x3 - x1) * invRoot2f;

    if(sigma < minSigma) {
                        sigma = minSigma; 
    }
    pSParams->sigma=sigma;
    status = 0;
  
  return status;
}

template<typename TP, typename TE>
int FitScurveLikelihoodFloat<TP, TE>::fitPixel(FitData *pfd,void *vpfit)
{
  float deltaSigma, deltaMu, sigma, chi2Min, *fptr;
  float chi2[9];
  GaussianCurve sParams, sParamsWork, *psp;
  int k, dirMin;
  Fit *pFit;
  pFit = (Fit *)vpfit;
  
  pFit->nIters = 0;
  pFit->converge = 0; // assume no convergence 
  
  pFit->ndf = pfd->n - 2; // degrees of freedom 
  
  // take a guess at the S-curve parameters 
  
  if(initialGuess(pfd,&sParams)) {
    psp = (GaussianCurve *)pFit->curve;
    psp->a0 = sParams.a0;
    psp->mu = sParams.mu;
    psp->sigma = sParams.sigma;
    pFit->converge = 1;
    pFit->muConverge = 0;
    pFit->sigmaConverge = 0;
    pFit->chi2 = 0.0f;
    return 0;
  }
  
  // initialize loop parameters 
  psp = &sParamsWork;
  psp->a0 = sParams.a0;
  deltaSigma = pFit->deltaSigma * sParams.sigma; // scaled 
  deltaMu = pFit->deltaMu * sParams.mu; // scaled 
  //* the loop begins *
  //  printf("delta %f %f\n",deltaSigma,deltaMu);
  while(!pFit->converge && (pFit->nIters++ < pFit->maxIters)) {
    
    dirMin = 0;
    fptr = chi2;
    for(k=0;k<9;++k) *fptr++ = 0.0f;
    
    while((dirMin >= 0) && (pFit->nIters++ < pFit->maxIters)) {
      //* calculate neighboring points *
      //* calculate neighboring points *
      psp->sigma=sParams.sigma - deltaSigma;
      psp->mu = sParams.mu - deltaMu;
      if(chi2[0] == 0.0f)
        chi2[0] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[7] == 0.0f)
        chi2[7] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;
      if(chi2[6] == 0.0f)
        chi2[6] = logLikelihood(pfd,psp);
      
      psp->sigma=sParams.sigma;
      psp->mu = sParams.mu - deltaMu;
      if(chi2[1] == 0.0f)
        chi2[1] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[8] == 0.0f)
        chi2[8] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;
      if(chi2[5] == 0.0f)
        chi2[5] = logLikelihood(pfd,psp);
      
      psp->sigma=sParams.sigma+deltaSigma;
      psp->mu = sParams.mu - deltaMu;
      if(chi2[2] == 0.0f)
        chi2[2] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu;
      if(chi2[3] == 0.0f)
        chi2[3] = logLikelihood(pfd,psp);
      psp->mu = sParams.mu + deltaMu;
      if(chi2[4] == 0.0f)
        chi2[4] = logLikelihood(pfd,psp);
      
      dirMin = -1;
      chi2Min = chi2[8]; // first guess at minimum 
      for(k=0, fptr=chi2;k<8;++k, ++fptr) {
	if(*fptr < chi2Min) {
          chi2Min = *fptr;
          dirMin = k;
        }
      }
      switch(dirMin) {
      case -1:
        deltaSigma = deltaSigma * 0.1f;
        deltaMu = deltaMu * 0.1f;
        break;
      case 0:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[0];
        chi2[3] = chi2[1];
        chi2[4] = chi2[8];
        chi2[5] = chi2[7];
        chi2[0] = chi2[1] = chi2[2] = 
          chi2[6] = chi2[7] = 0.0f;
        break;
      case 1:
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[1];
        chi2[3] = chi2[2];
        chi2[4] = chi2[3];
        chi2[5] = chi2[8];
        chi2[6] = chi2[7];
        chi2[7] = chi2[0];
        chi2[0] = chi2[1] = chi2[2] = 0.0f;
        break;
      case 2:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu - deltaMu;
        chi2[8] = chi2[2];
        chi2[5] = chi2[3];
        chi2[6] = chi2[8];
        chi2[7] = chi2[1];
        chi2[0] = chi2[1] = chi2[2] = 
          chi2[3] = chi2[4] = 0.0f;
        break;
      case 3:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        chi2[8] = chi2[3];
        chi2[5] = chi2[4];
        chi2[6] = chi2[5];
        chi2[7] = chi2[8];
        chi2[0] = chi2[1];
        chi2[1] = chi2[2];
        chi2[2] = chi2[3] = chi2[4] = 0.0f;
        break;
      case 4:
        sigma = sParams.sigma + deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[4];
        chi2[7] = chi2[5];
        chi2[0] = chi2[8];
        chi2[1] = chi2[3];
        chi2[2] = chi2[3] = chi2[4] = 
          chi2[5] = chi2[6] = 0.0f;
        break;
      case 5:
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[5];
        chi2[7] = chi2[6];
        chi2[0] = chi2[7];
        chi2[1] = chi2[8];
        chi2[2] = chi2[3];
        chi2[3] = chi2[4];
        chi2[4] = chi2[5] = chi2[6] = 0.0f;
        break;
      case 6:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        sParams.mu = sParams.mu + deltaMu;
        chi2[8] = chi2[6];
        chi2[1] = chi2[7];
        chi2[2] = chi2[8];
        chi2[3] = chi2[5];
        chi2[4] = chi2[5] = chi2[6] = 
          chi2[7] = chi2[0] = 0.0f;
        break;
      case 7:
        sigma = sParams.sigma - deltaSigma;
        sParams.sigma=sigma;
        chi2[8] = chi2[7];
        chi2[1] = chi2[0];
        chi2[2] = chi2[1];
        chi2[3] = chi2[8];
        chi2[4] = chi2[5];
        chi2[5] = chi2[6];
        chi2[6] = chi2[7] = chi2[0] = 0.0f;
        break;
      }
    }
    
    if(deltaSigma < (pFit->sigmaEpsilon * sParams.sigma) &&
       deltaMu < (pFit->muEpsilon * sParams.mu)) {
      pFit->converge = 1;
      break;
    }
  }
  psp = (GaussianCurve *)pFit->curve;
  *psp = sParams;
  pFit->chi2=chiSquared(pfd,psp)/(float) pFit->ndf;
  //static int d=0;
 
  //  printf("%f %f %f \n",psp->mu,psp->sigma,pFit->chi2);
  
  return pFit->converge ? 0 : 1; // 0 = success 
}
template<typename TP, typename TE>
float FitScurveLikelihoodFloat<TP, TE>::chiSquared(FitData *pfd,GaussianCurve *s) {
 float acc, y0,y1,y2,y3, a0, chi2;
  float *x, *y;
  float x0;
  int j, k, n;  
  x = pfd->x;     y = pfd->y;     n = pfd->n;
  acc = 0.0f; 
  a0 = s->a0;  
  /* use binomial weighting */
  for(k=0;k<n;++k) {
    x0 = *x++; 
    y0 = *y++; 
    y1 = errf_ext(x0,s); 
    j = (int)(y1 * inverse_weight_lut_interval); 
    y2 = y0 - a0 * y1; 
    y3 = y2 * y2;
    acc += (y3 * FitDataFloat::binomial_weight[j]); 
  }
  chi2 = acc/ a0;
  return chi2;
}
