#ifndef FITSCURVELIKELIHOODFASTINT_HH
#define FITSCURVELIKELIHOODFASTINT_HH
#include "dataproc/fit/AbsFit.hh"
#include "util/RceHisto2d.cc"
#include <vector>

template<typename TP, typename TE>
class  FitScurveLikelihoodFastInt:public AbsFit {

typedef unsigned int UINT32;
typedef int INT32;
typedef unsigned long long UINT64;
typedef long long INT64;
typedef unsigned char UINT8;

typedef struct FitData_
{
  UINT32 n;
  UINT32 *x;
  UINT8 *y;
} FitData;

typedef struct FitResult_
{
 INT32 ndf;
 INT32 mu0; 
 INT32 sigma; //inverted sigma
 INT32 chi2;
} FitResult;

typedef struct GaussianCurve_
{
  UINT32 mu, a0;
  UINT32 sigma;
} GaussianCurve;

typedef struct {
  INT32 muEpsilon;
  INT32 sigmaEpsilon; /* user provides convergence criterion */
  INT32 nIters;
  INT32 ndf; /* number of degrees of freedom */
  INT32 chi2; /* final result for chi2 */
  INT32 converge;
  INT32 maxIters;
  void *curve;
} Fit;
#define sc_lut_width     (FAST_LUT_WIDTH << 16)
#define logx_length      (2*FAST_LUT_LENGTH)
#define  sc_lut_length   ((FAST_LUT_LENGTH - 1) << 16)

public:
FitScurveLikelihoodFastInt(ConfigIF *cif,std::vector<std::vector<RceHisto2d<TP, TE>*> > &histo,std::vector<int> &vcal, int nTrigger, const char* name="");
  ~FitScurveLikelihoodFastInt();
  int doFit(int);
protected:
  inline INT32 *log_ext(INT32 x, INT32 mu,INT32 sigma);
  UINT32 errf_ext(INT32 x, GaussianCurve *psp);
  inline UINT32 logLikelihood(FitData *pfd, GaussianCurve *s);
  int initFit(Fit *pFit);
  int extractGoodData(FitData *pfdi,FitData *pfdo, UINT32 nTriggers);
  int initialGuess(FitData *pfd, GaussianCurve *pSParams);
  int fitPixel(FitData *pfd,void *vpfit);
  UINT32 chiSquared(FitData *pfd,GaussianCurve *s);
  int sGuess(FitData *pfd,  void *vpfit);
  int roughFitRange(FitData *pfdi, FitData *pfdo,  int& maxVal);
  Fit m_fit;
  UINT32 *m_x;
  UINT8 *m_y;
  std::vector<std::vector<RceHisto2d<TP, TE>*> > &m_histo;
  std::vector<std::vector<RceHisto2d<float, float>*> > m_fithisto;
  const char* m_name;  

};



#endif
