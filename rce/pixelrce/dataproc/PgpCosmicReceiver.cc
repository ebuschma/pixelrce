#include "dataproc/PgpCosmicReceiver.hh"
#include "HW/RCDImasterL.hh"
#include <iostream>
#include "HW/Headers.hh"
#include "dataproc/Channeldefs.hh"

PgpCosmicReceiver::PgpCosmicReceiver(AbsDataHandler* handler):AbsReceiver(handler),PgpTrans::Receiver(){
  PgpTrans::RCDImaster::instance()->setReceiver(this);
  std::cout<<"Created pgp cosmic receiver"<<std::endl;
  m_buffer=new unsigned[2048];
  m_counter=0;
}

void PgpCosmicReceiver::receive(PgpTrans::PgpData *pgpdata){
  int link=pgpdata->header[2];
  if(link==PGPACK)return;
  m_counter++;
  //std::cout<<"Received data from link "<<link<<std::endl;
  //printf("Link %d Payloadsize %d Headersize headerSize %d\n",link, payloadSize,headerSize);
  unsigned size=pgpdata->payloadSize;
  if (size==0)return; //typically handshake from serialization command
  if(size>4096)std::cout<<"Large payload "<<size<<std::endl;
  unsigned* data;
  //  if(link==10&&payloadSize!=0)std::cout<<"Channel 10 payloadsize= "<<payloadSize<<std::endl;
  //if(link!=10){
  //data=(unsigned*)tds->header();
  //size=headerSize/sizeof(unsigned);
  //for (int i=0;i<size;i++){
    //std::cout<<"Header "<<i<<": "<<std::hex<<data[i]<<std::dec<<", ";
  //}
  //std::cout<<std::endl;
  //data=(unsigned*)tds->payload();
  //size=payloadSize/sizeof(unsigned)+ (payloadSize%4!=0);
  //for (int i=0;i<size;i++){
  //  std::cout<<"Data "<<i<<": "<<std::hex<<data[i]<<std::dec<<std::endl;
  //}
  //return;
  data=pgpdata->payload;
  bool marker=pgpdata->header[6]&0x80;
  if(marker){
    link|=1<<MARKERPOS;
  }
  //std::cout<<"Payloadsize "<<payloadSize<<std::endl;
  //unsigned char* payloadc=(unsigned char*)tds->payload();
#ifdef SWAP_DATA
  //byte swap data
  unsigned* ptr = data;
  unsigned* end = ptr+size;
  unsigned *swapped=new(m_buffer) unsigned[size];
    unsigned *sw=swapped;
    //for (int i=0;i<8;i++)std::cout<<std::hex<<header[i]<<std::endl;
  while (ptr<end) {
    unsigned tmp = *ptr;
    *sw = (tmp<<16) | (tmp >>16);
    //*sw = (tmp<<24) | (tmp >>24) | ((tmp&0xff00)<<8) | ((tmp&0xff0000)>>8);
    ptr++;
      sw++;
  }
  m_handler->handle(link,swapped,size);
  #else
  m_handler->handle(link,data,size);
  #endif
  //std::cout<<"Parsing done"<<std::endl;
}
