#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/OccupancyDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"
#include "dataproc/fit/FitFactory.cc"



int OccupancyDataProc::fit(std::string fitfun) {
  FitFactory<char, char> dfac; //template parameters are the type of the histogram and error
  std::cout << "Running: " << fitfun << std::endl;
  if(fitfun.size()>=6 && fitfun.substr(0,6)=="SCURVE") {
    //    m_fit=dfac.createFit("ScurveLikelihoodFloat",m_configIF, m_histo_occ,m_vcal,m_nTrigger);
    m_fit=dfac.createFit("ScurveLikelihoodFastInt",m_configIF, m_histo_occ,m_vcal,m_nTrigger);
    int opt=0;
    if(fitfun.size()>=13&&fitfun.substr(7,6)=="NOCONV")opt=AbsFit::NOCONV;
    if(fitfun.size()>=12&&fitfun.substr(7,5)=="XTALK")opt=AbsFit::XTALK;
    m_fit->doFit(opt);
  }
  return 0;
}

int OccupancyDataProc::processData(unsigned link, unsigned *data, int size){
  //  m_timer.Start();
  //    std::cout<<"Process data"<<std::endl;
  int module=m_linkToIndex[link];
  RceHisto2d<char, char>* histo=m_histo_occ[module][m_currentBin];
  for (int i=0;i<size;i++){
    FormattedRecord current(data[i]);
    /*
    if (current.isHeader()){
      l1id = current.getL1id();
      bcid = current.getBxid();
      //printf("bcid : %x \n", bcid);
      //printf("l1id : %x \n", l1id);
      if (l1id != l1id_last)
	{
	  l1id_last = l1id;
	  bcid_ref  = bcid;
	}
      bcid = bcid-bcid_ref;
      //printf("bcidafter : %x \n", bcid);
    }
    */
    if (current.isData()){
      unsigned int chip=current.getFE();
      unsigned int col=current.getCol();
      unsigned int row=current.getRow();
      //unsigned int tot=current.getToT();
      //printf("Hit col=%d row=%d tot=%d\n",col, row, tot);
      if((row<(unsigned)m_info[module].getNRows()) && (col<(unsigned)m_info[module].getNColumns())) {
	if(chip==0)histo->incrementFast(row,col);
	else if(chip<(unsigned)m_info[module].getNFrontends())histo->incrementFast(row, chip*m_info[module].getNColumns()+col);
      }
    }
  }
  //  m_timer.Stop();
  return 0;
}

OccupancyDataProc::OccupancyDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif),m_fit(0) {
  std::cout<<"Occupancy Data Proc"<<std::endl;
  try{ //catch bad scan option parameters
    m_nLoops = scanOptions->get<int>("nLoops");
    /* there is at least one parameter loop */
    /* TODO: fix in scan control */
    m_nPoints=1;
    if(m_nLoops>0){         
      m_nPoints=scanOptions->get<int>("scanLoop_0.nPoints");
      for(int i=0;i<m_nPoints;i++) {
	char pointname[10];
	sprintf(pointname,"P_%d",i);
	int vcal=scanOptions->get<int>(std::string("scanLoop_0.dataPoints.")+pointname);
	//std::cout << "point vcal " << vcal << std::endl;
	m_vcal.push_back(vcal);
      }
    }
    m_nTrigger=scanOptions->get<int>("trigOpt.nEvents");

    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      std::vector<RceHisto2d<char, char>* > vh;
      m_histo_occ.push_back(vh);
      char name[128];
      char title[128];
      RceHisto2d<char, char> *histo;
      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      unsigned int moduleId=m_info[module].getId();
      /* retrieve scan points - Vcal steps in this case */
      std::cout<<"Creating Occupancy histograms."<<std::endl;
      for (int point=0;point<m_nPoints;point++){
	sprintf(title,"OCCUPANCY");
	sprintf(name,"Mod_%d_Occupancy_Point_%03d", moduleId,point);
	histo=new RceHisto2d<char, char>(name,title,rows,0,rows,cols,0,cols, true, false);
	if(m_info[module].getNFrontends()==1)histo->setAxisTitle(1,"Column");
	else histo->setAxisTitle(1,"FE*N_COL+Column");
	histo->setAxisTitle(0, "Row");
	m_histo_occ[module].push_back(histo);
      }
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
  //  m_timer.Reset();
}

OccupancyDataProc::~OccupancyDataProc(){
  if(m_fit) delete m_fit;
  for (size_t module=0;module<m_histo_occ.size();module++)
    for(size_t i=0;i<m_histo_occ[module].size();i++)delete m_histo_occ[module][i];

  //  m_timer.Print("OccupancyDataProc");
}
  

