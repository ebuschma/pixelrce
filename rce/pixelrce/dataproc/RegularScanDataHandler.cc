#include "dataproc/RegularScanDataHandler.hh"
#include "dataproc/AbsDataProc.hh"
#include "config/AbsFormatter.hh"
#include "config/ConfigIF.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>

#include "util/DataCond.hh"

RegularScanDataHandler::RegularScanDataHandler(AbsDataProc* dataproc, 
					       DataCond& datacond,
					       ConfigIF* cif, 
					       boost::property_tree::ptree* scanOptions)
  :AbsDataHandler(dataproc, datacond, cif){
  m_nL1AperEv=scanOptions->get<int>("trigOpt.nL1AperEvent");
  int ntrigpergroup=scanOptions->get<int>("trigOpt.nTriggersPerGroup");
  if(ntrigpergroup!=0)m_nL1AperEv*=ntrigpergroup;
  std::cout<<"Number of expected triggers is "<<m_nL1AperEv<<std::endl;
  m_L1Acounters.clear();
  int maxlink=0;
  m_nModules=m_configIF->getNmodules();
  for (int i=0;i<m_nModules;i++){
    m_L1Acounters.push_back(0);
    if(m_configIF->getModuleInfo(i).getOutLink()>maxlink)maxlink=m_configIF->getModuleInfo(i).getOutLink();
    m_formatter.push_back(m_configIF->getModuleInfo(i).getFormatter());
  }
  maxlink++;
  m_linkToIndex=new int[maxlink];
  for (unsigned int i=0;i<m_configIF->getNmodules();i++){
    m_linkToIndex[m_configIF->getModuleInfo(i).getOutLink()]=i;
  }
  m_parsedData=new unsigned[16384];
  //m_timer.Reset();
}

RegularScanDataHandler::~RegularScanDataHandler(){
  delete [] m_parsedData;
  delete [] m_linkToIndex;
  // m_timer.Print("RegularScanDataHandler");
}

void RegularScanDataHandler::timeoutOccurred(){
  //print out which modules timed out
  for (int i=0;i<m_nModules;i++){
    if (m_L1Acounters[i]<m_nL1AperEv){
       std::cout<<m_configIF->getModuleInfo(i).getId()<<std::endl;
    }
  }
  resetL1counters();
}

void RegularScanDataHandler::resetL1counters(){
  assert(m_nModules==(int)m_L1Acounters.size());
  for (int i=0;i<m_nModules;i++){
    m_L1Acounters[i]=0;
  }
}

void RegularScanDataHandler::handle(unsigned link, unsigned *data, int size){
  // nL1A contains the number of L1A in the data chunk
  //std::cout<<"Data from Link "<<link<<std::endl;
  int nL1A=0;
  int parsedsize=0;
  int retval=m_formatter[m_linkToIndex[link]]->decode(data,size,m_parsedData, parsedsize, nL1A);
  //std::cout<<"parser"<<retval<<" "<<size<<std::endl;
    //if(size==448){
  if(retval!=0){
    std::cout<<"Parser error for link "<<link<<": Data size "<<size<<" number of triggers "<<nL1A<<std::endl;
    // if(nL1A==16){
    for (int i=0;i<size;i++)std::cout<<std::hex<<data[i]<<std::endl;
    std::cout<<std::dec;
    //     m_scan->pause();
    //}
  }
  if(parsedsize!=0){
    m_dataProc->processData(link, m_parsedData, parsedsize);
  }
  //  std::cout<<nL1A<<" events."<<std::endl;
    //  std::cout<<"Expecting "<<m_nL1AperEv<<std::endl;
  //for (int i=size-4;i<size;i++)std::cout<<std::hex<<data[i]<<std::endl;
  //m_timer.Start();
  if(nL1A!=0){
    rce_mutex_lock ml(m_nL1Alock);
    //assert(m_linkToIndex[link]<m_L1Acounters.size());
    m_L1Acounters[m_linkToIndex[link]]+=nL1A;
    //  std::cout<<m_L1Acounters[m_linkToIndex[link]]<<" events so far."<<std::endl;
    bool done=true;
    //check if the event is complete.
    for (int i=0;i<m_nModules;i++){
      //assert(m_L1Acounters[i]<=m_nL1AperEv);
      if(m_L1Acounters[i]>m_nL1AperEv)std::cout<<m_L1Acounters[i]<<" L1Atriggers"<<std::endl;
      if (m_L1Acounters[i]<m_nL1AperEv){
	done=false;
	break;
      }
    }
    if(done==true){
      //std::cout<<"Restarting scan "<<std::endl;
      resetL1counters();
      // Next trigger
      //      m_scan->stopWaiting();
      rce_mutex_lock pl( m_dataCond.mutex );
      if(m_dataCond.waitingForData==true)m_dataCond.cond.signal();
      else std::cout<<"Scan was not waiting for data!"<<std::endl;
    }
  }
  //m_timer.Stop();
}
