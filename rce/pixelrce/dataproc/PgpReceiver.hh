#ifndef PGPRECEIVER_HH
#define PGPRECEIVER_HH

#include "dataproc/AbsReceiver.hh"
#include "HW/Receiver.hh"



class PgpReceiver: public AbsReceiver, public PgpTrans::Receiver{
public:
  PgpReceiver(AbsDataHandler* handler);
  virtual ~PgpReceiver();
  void receive(PgpTrans::PgpData* pgpdata);
private:
  unsigned* m_buffer;
  unsigned m_counter;
};
#endif
