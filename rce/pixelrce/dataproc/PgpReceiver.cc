#include "dataproc/PgpReceiver.hh"
#include "HW/RCDImasterL.hh"
#include <iostream>
#include "HW/Headers.hh"
#include "dataproc/Channeldefs.hh"

PgpReceiver::PgpReceiver(AbsDataHandler* handler):AbsReceiver(handler),PgpTrans::Receiver(){
  PgpTrans::RCDImaster::instance()->setReceiver(this);
  std::cout<<"Created pgp receiver"<<std::endl;
  m_buffer=new unsigned[2048];
  m_counter=0;
}
PgpReceiver::~PgpReceiver(){
  delete [] m_buffer;
  std::cout<<"Received "<<m_counter<<" buffers in previous run"<<std::endl;
  //  m_timer.Print("PgpReceiver");
}

void PgpReceiver::receive(PgpTrans::PgpData *pgpdata){
  //int link=0;
  int link=pgpdata->header[2];
  //std::cout<<"Link is "<<link<<std::endl;
  //std::cout<<"Payload "<<std::hex<<pgpdata->payload[0]<<std::dec<<std::endl;
  int size=pgpdata->payloadSize;
  //std::cout<<"Size "<<size<<std::endl;
  if (link==PGPACK)return; //handshake from serialization command
  //printf("Payloadsize %d Headersize %d\n",payloadSize,headerSize);
  unsigned* data;
  data=pgpdata->payload;

#ifdef SWAP_DATA
  //byte swap data
  #warning Data swapping turned on
  unsigned* ptr = data;
  unsigned* end = ptr+size;
  unsigned *swapped=new(m_buffer) unsigned[size];
  unsigned *sw=swapped;
  //for (int i=0;i<8;i++)std::cout<<std::hex<<header[i]<<std::endl;
  while (ptr<end) {
    unsigned tmp = *ptr;
    *sw = (tmp<<16) | (tmp >>16);
    //*sw = (tmp<<24) | (tmp >>24) | ((tmp&0xff00)<<8) | ((tmp&0xff0000)>>8);
      //*ptr = (tmp<<16) | (tmp >>16);
    ptr++;
      sw++;
  }
  m_handler->handle(link,swapped,size);
#else
  m_counter++;
  //m_timer.Start();
  m_handler->handle(link,data,size);
   //m_timer.Stop();
#endif
 
  //std::cout<<"Parsing done"<<std::endl;
}
