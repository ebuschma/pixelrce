#ifndef PGPATLASRECEIVER_HH
#define PGPATLASRECEIVER_HH

#include "dataproc/AbsReceiver.hh"
#include "HW/Receiver.hh"
#include <iostream>

class PgpAtlasReceiver: public AbsReceiver, public PgpTrans::Receiver{
public:
  PgpAtlasReceiver(AbsDataHandler* handler);
  virtual ~PgpAtlasReceiver(){
    std::cout<<"Received "<<std::dec<<m_counter<<" buffers in previous run."<<std::endl;
  }
  void receive(PgpTrans::PgpData *pgpdata);
private:
  unsigned m_counter;
};
#endif
