#include "dataproc/PgpAtlasReceiver.hh"
#include "HW/RCDImasterL.hh"
#include <iostream>
#include "HW/Headers.hh"
#include "dataproc/Channeldefs.hh"

PgpAtlasReceiver::PgpAtlasReceiver(AbsDataHandler* handler):AbsReceiver(handler),PgpTrans::Receiver(){
  PgpTrans::RCDImaster::instance()->setReceiver(this);
  std::cout<<"Created Pgp Atlas receiver"<<std::endl;
  m_counter=0;
}

void PgpAtlasReceiver::receive(PgpTrans::PgpData *pgpdata){
  int link=pgpdata->header[2];
  if(link==PGPACK)return;
  m_counter++;
  //std::cout<<"Received data from link "<<link<<std::endl;
  //printf("Link %d Payloadsize %d Headersize headerSize %d\n",link, payloadSize,headerSize);
  unsigned size;
  unsigned* data;
  if(link==TDCREADOUT){
    size=3;
    data=(unsigned*)&pgpdata->header[4];
  }else{
    size=pgpdata->payloadSize;
    data=pgpdata->payload;
    if(size>4096)std::cout<<"Large payload "<<size<<std::endl;
    bool marker=pgpdata->header[6]&0x80;
    if(marker){
      link|=1<<MARKERPOS;
    }
  }
  m_handler->handle(link,data,size);
}
