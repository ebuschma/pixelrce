#ifndef RAWFEI4OCCUPANCYDATAPROC_HH
#define RAWFEI4OCCUPANCYDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include "dataproc/fit/AbsFit.hh"
#include <vector>
#include "util/RceHisto2d.cc"
#include "util/RceHisto1d.cc"
#include <stdio.h>

class RawFei4OccupancyDataProc: public AbsDataProc{
public:
  RawFei4OccupancyDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~RawFei4OccupancyDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);

protected:
  enum FEI4{N_ROW=336, N_COL=80};
  std::vector<std::vector<RceHisto2d<char, char>*> > m_histo_occ;
  std::vector<RceHisto1d<int, int>*> m_errhist;
  AbsFit *m_fit;
  std::vector<int> m_vcal;
  int m_nTrigger;
  int m_nLoops;
  int m_nPoints;
  unsigned short m_counter;
  //FILE* m_flog;
};

#endif
