#ifndef OCCUPANCYDATAPROC_HH
#define OCCUPANCYDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include "dataproc/fit/AbsFit.hh"
#include <vector>
#include "util/RceHisto2d.cc"


class OccupancyDataProc: public AbsDataProc{
public:
  OccupancyDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~OccupancyDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);

protected:

  std::vector<std::vector<RceHisto2d<char, char>*> > m_histo_occ;
  AbsFit *m_fit;
  std::vector<int> m_vcal;
  int m_nTrigger;
  int m_nLoops;
  int m_nPoints;
};

#endif
