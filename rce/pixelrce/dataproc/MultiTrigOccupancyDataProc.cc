#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/MultiTrigOccupancyDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"
#include "util/RceName.hh"
#include "dataproc/fit/FitFactory.cc"



int MultiTrigOccupancyDataProc::fit(std::string fitfun) {
  FitFactory<char, char> dfac; //template parameters are the type of the histogram and error
  std::cout << "Running: " << fitfun << std::endl;
  if(fitfun.size()>=6 && fitfun.substr(0,6)=="SCURVE") {
    //    m_fit=dfac.createFit("ScurveLikelihoodFloat",m_configIF, m_histo_occ,m_vcal,m_nTrigger);
    m_fit0=dfac.createFit("ScurveLikelihoodFastInt",m_configIF, m_histo_occ0,m_vcal,m_nTrigger, "Trig0");
    int opt=0;
    if(fitfun.size()>=13&&fitfun.substr(7,6)=="NOCONV")opt=AbsFit::NOCONV;
    if(fitfun.size()>=12&&fitfun.substr(7,5)=="XTALK")opt=AbsFit::XTALK;
    m_fit0->doFit(opt);

    m_fit1=dfac.createFit("ScurveLikelihoodFastInt",m_configIF, m_histo_occ1,m_vcal,m_nTrigger, "Trig1");
    opt=0;
    if(fitfun.size()>=13&&fitfun.substr(7,6)=="NOCONV")opt=AbsFit::NOCONV;
    if(fitfun.size()>=12&&fitfun.substr(7,5)=="XTALK")opt=AbsFit::XTALK;
    m_fit1->doFit(opt);
  }
  return 0;
}

int MultiTrigOccupancyDataProc::processData(unsigned link, unsigned *data, int size){
  //  m_timer.Start();
  //  std::cout<<"Process data with size = "<<size<<std::endl;
  int module=m_linkToIndex[link];
 
  int l1id = 0;
  int bcid = 0;

  for (int i=0;i<size;i++){
    FormattedRecord current(data[i]);

    
    if (current.isHeader()){
      l1id = current.getL1id();
      bcid = current.getBxid();


      //old code for deciding which trigger data belongs to.  Unfortunately, we've seen cases
      //where the l1id order gets messed up, in which case this doesn't work.
      /*
      if (l1id != m_l1id_prev) {
	// std::cout<<"new l1id = "<<l1id<<std::endl;

	  if (m_nbcid!=m_nbcid_total){
	    std::cout<<"VERY BAD last l1id had m_nbcid = "<<m_nbcid<<std::endl;
	  }

	  m_l1id_which++;
	  if(m_l1id_which>=2){
	    m_l1id_which=0;
	  }

	  // m_l1id_prev = l1id;
	  //  m_bcid_prev  = bcid;
	  m_nbcid=0;
      }
      else{
	int exp_bcid = m_bcid_prev+1;
	exp_bcid = exp_bcid%256;
	if ( (bcid%256) != exp_bcid) {
	  std::cout<<"SCREAM! bcid changed from "<<m_bcid_prev<<" to "<<bcid<<std::endl;
	}
      }
      */      

      //assume l1id starts counting from 1.  Empirically, this seems to work. 
      if((l1id%2)==0){
        m_l1id_which=1;
      }
      else{
        m_l1id_which=0;
      }
      
      
      //printf("bcid : %d ,   l1id : %d , l1id_which: %d\n", bcid,l1id,m_l1id_which);
 
      // bcid = bcid-bcid_ref;
      m_nbcid++;
      //if( (bcid+1) != m_nbcid || m_nbcid > m_nbcid_total || bcid >= m_nbcid_total){
      //	std::cout<<"SCREAM  bcid = "<<bcid<<" ; m_nbcid = "<<m_nbcid<<std::endl;
      //  }
      m_bcid_prev = bcid;
      m_l1id_prev = l1id;

    } //if(isHeader)
    
    if (current.isData()){
      unsigned int chip=current.getFE();
      unsigned int col=current.getCol();
      unsigned int row=current.getRow();
      //      unsigned int tot=current.getToT();
      //    if(col==10 && row==10){
      //      printf("Hit col=%d row=%d tot=%d\n",col, row, tot);
      // }
      if((row<(unsigned)m_info[module].getNRows()) && (col<(unsigned)m_info[module].getNColumns())) {

	RceHisto2d<char, char>* histo(0);

	if(m_l1id_which==0){
	  histo = m_histo_occ0[module][m_currentBin];
	}
	else if(m_l1id_which==1){
	  histo = m_histo_occ1[module][m_currentBin];
	}
	else{
	  std::cout<<"This can't happen!"<<std::endl;
	}

	if (chip==0) histo->incrementFast(row,col);
	else if (chip<(unsigned)m_info[module].getNFrontends()) histo->incrementFast(row, chip*m_info[module].getNColumns()+col);
      }

    } //if(isData)
 
  } //end for(int i=0; i<size; i++)

  //  m_timer.Stop();
  return 0;
}

MultiTrigOccupancyDataProc::MultiTrigOccupancyDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif),m_fit0(0),m_fit1(0),m_l1id_prev(-1),m_bcid_prev(0),m_nbcid(0),m_nbcid_total(0),m_l1id_which(-1) {
  std::cout<<"MultiTrigOccupancy Data Proc"<<std::endl;
  std::cout<<"Rce Number is "<<RceName::getRceNumber()<<std::endl;
  try{ //catch bad scan option parameters
    m_nLoops = scanOptions->get<int>("nLoops");
    /* there is at least one parameter loop */
    /* TODO: fix in scan control */
    m_nPoints=1;
    if(m_nLoops>0){         
      m_nPoints=scanOptions->get<int>("scanLoop_0.nPoints");
      for(int i=0;i<m_nPoints;i++) {
	char pointname[10];
	sprintf(pointname,"P_%d",i);
	int vcal=scanOptions->get<int>(std::string("scanLoop_0.dataPoints.")+pointname);
	//std::cout << "point vcal " << vcal << std::endl;
	m_vcal.push_back(vcal);
      }
    }
    m_nTrigger=scanOptions->get<int>("trigOpt.nEvents");
    m_nbcid_total = scanOptions->get<int>("trigOpt.nL1AperEvent"); 
    m_nbcid = m_nbcid_total;

    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      std::vector<RceHisto2d<char, char>* > vh;
      m_histo_occ0.push_back(vh);
      m_histo_occ1.push_back(vh);
 
      char name[128];
      char title[128];
      RceHisto2d<char, char> *histo0;
      RceHisto2d<char, char> *histo1;

      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();
      /* retrieve scan points - Vcal steps in this case */
      std::cout<<"Creating Occupancy histograms."<<std::endl;
      for (int point=0;point<m_nPoints;point++){
	sprintf(title,"OCCUPANCY Mod %d at %s", moduleId, moduleName.c_str());

	sprintf(name,"Mod_%d_Trig0_Occupancy_Point_%03d", moduleId,point);
        histo0=new RceHisto2d<char, char>(name,title,rows,0,rows,cols,0,cols, true, false);

	sprintf(name,"Mod_%d_Trig1_Occupancy_Point_%03d", moduleId,point);
	histo1=new RceHisto2d<char, char>(name,title,rows,0,rows,cols,0,cols, true, false);

	if(m_info[module].getNFrontends()==1){
	  histo0->setAxisTitle(0,"Column");
	  histo1->setAxisTitle(0,"Column");
	}
	else{ 
	  histo0->setAxisTitle(0,"FE*N_COL+Column");
	  histo1->setAxisTitle(0,"FE*N_COL+Column");
	}
	histo0->setAxisTitle(1, "Row");
	histo1->setAxisTitle(1, "Row");

	m_histo_occ0[module].push_back(histo0);
	m_histo_occ1[module].push_back(histo1);

      }
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
  //  m_timer.Reset();
}

MultiTrigOccupancyDataProc::~MultiTrigOccupancyDataProc(){
  if(m_fit0) delete m_fit0;
  if(m_fit1) delete m_fit1;

  for (size_t module=0;module<m_histo_occ0.size();module++){
    for(size_t i=0;i<m_histo_occ0[module].size();i++){
      delete m_histo_occ0[module][i];
      delete m_histo_occ1[module][i];
    }
  }

  //  m_timer.Print("MultiTrigOccupancyDataProc");
}
  

