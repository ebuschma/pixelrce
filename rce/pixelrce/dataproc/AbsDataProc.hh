#ifndef ABSDATAPROC_HH
#define ABSDATAPROC_HH

#include <string>
#include <vector>
#include <map>
#include "config/ModuleInfo.hh"

class ConfigIF;
class Scan;

class AbsDataProc{
public:
  AbsDataProc(ConfigIF* cif);
  virtual ~AbsDataProc();
  virtual int changeBin(int i);
  virtual int setMaskStage(int stage);
  virtual int processData(unsigned link, unsigned *data, int size);
  virtual int fit(std::string fitfun);
  virtual unsigned nEvents(){return m_nEvents;}
protected:
  ConfigIF* m_configIF;
  std::vector<ModuleInfo> m_info;
  int  *m_linkToIndex;
  int m_currentMaskStage;
  int m_currentBin;
  unsigned m_nEvents;
};

#endif
