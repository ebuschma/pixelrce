#ifndef FEI4REGISTERTESTDATAPROC_HH
#define FEI4REGISTERTESTDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "dataproc/AbsDataProc.hh"
#include <vector>
#include "util/RceHisto2d.cc"
#include "util/RceHisto1d.cc"


class Fei4RegisterTestDataProc: public AbsDataProc{
public:
  Fei4RegisterTestDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~Fei4RegisterTestDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);
  int setMaskStage(int stage);

protected:
  enum FEI4{N_ROW=336, N_COL=80};
  std::vector<RceHisto2d<int, int>*> m_pix;
  std::vector<RceHisto1d<int, int>*>m_glob;
  std::vector<int> m_counter;
};

#endif
