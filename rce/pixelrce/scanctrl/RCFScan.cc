
#include "scanctrl/RCFScan.hh"
#include <boost/property_tree/ptree.hpp>

#include <string>
#include <iostream>


void* RCFScan::startScanStatic(void* arg){
  ((RCFScan*)arg)->startScan();
  return 0;
}

RCFScan::RCFScan(): Scan() {
}

  

RCFScan::~RCFScan(){
}

void RCFScan::RCFpause(){
  std::cout<<"Pause"<<std::endl;
  pause();
}
void RCFScan::RCFresume(){
  std::cout<<"Resume"<<std::endl;
  resume();
}
void RCFScan::RCFabort(){
  std::cout<<"Abort"<<std::endl;
  abort();
}
void RCFScan::RCFstartScan(){
  pthread_t mthread;
  pthread_attr_t attr;
  int ret;
  // setting a new size
  int stacksize = (PTHREAD_STACK_MIN + 0x20000);
  pthread_attr_init(&attr);
  ret=pthread_attr_setstacksize(&attr, stacksize);
  pthread_create( &mthread, &attr , startScanStatic, this);
  pthread_detach(mthread);
  //startScan();
}
void RCFScan::RCFwaitForData(){
  waitForData();
}
void RCFScan::RCFstopWaiting(){
  stopWaiting();
}
int32_t RCFScan::RCFgetStatus(){
  return getStatus();
}
  
