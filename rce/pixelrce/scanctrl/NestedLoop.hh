#ifndef NESTED_LOOP_HH
#define NESTED_LOOP_HH

// Implements a nested for loop. 
// Works together with ScanLoop.hh 

// C 2009 SLAC Author: Martin Kocian 

#include <list>

class ScanLoop;

class NestedLoop{

public:
  NestedLoop():m_done(false),m_init(false){}
  ~NestedLoop();
  bool done(){return m_done;}
  void clear();
  void reset();
  void addNewInnermostLoop(ScanLoop*);
  void addNewOutermostLoop(ScanLoop*);
  void firstEvent();
  void next();
  void print();
private:
  std::list<ScanLoop*> m_looplist;
  bool m_done;
  bool m_init;
};

#endif
