#ifndef RCECALLBACK_HH
#define RCECALLBACK_HH

#include "Callback.hh"

class RceCallback{
public:
  static void sendMsg(ipc::Priority pr, const ipc::CallbackParams *msg);
  static void shutdown();
protected:
  RceCallback();
  virtual void SendMsg(ipc::Priority pr, const ipc::CallbackParams *msg)=0;
  virtual void Shutdown()=0;
  ipc::Priority m_pr;
  static bool m_configured;
  static RceCallback* m_callback;
};

#endif
