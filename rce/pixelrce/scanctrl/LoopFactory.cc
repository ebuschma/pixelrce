#include "scanctrl/LoopFactory.hh"
#include "scanctrl/RegularScanSetup.hh"
#include "scanctrl/RegularCfgScanSetup.hh"
#include "scanctrl/IfScanSetup.hh"
#include "scanctrl/SelftriggerSetup.hh"
#include "scanctrl/DelayScanSetup.hh"
#include "scanctrl/CosmicDataSetup.hh"
#include "scanctrl/AtlasDataSetup.hh"
#include "scanctrl/NoiseScanSetup.hh"
#include <stdio.h> /* for sprintf */
#include <iostream>
#include <assert.h>

LoopFactory::LoopFactory(){}


LoopSetup* LoopFactory::createLoopSetup(const char* type){
  
  if(std::string(type)=="Regular")
    return new RegularScanSetup;
  else if(std::string(type)=="RegularCfg")
    return new RegularCfgScanSetup;
  else if(std::string(type)=="IfScan")
    return new IfScanSetup;
  else if(std::string(type)=="Delay")
    return new DelayScanSetup;
  else if(std::string(type)=="CosmicData")
    return new CosmicDataSetup;
  else if(std::string(type)=="AtlasData")
    return new AtlasDataSetup;
  else if(std::string(type)=="Selftrigger")
    return new SelftriggerSetup;
  else if(std::string(type)=="Noisescan")
    return new NoiseScanSetup;
  else {
    char message[128];
    sprintf(message, "Scan loop setup for %s is not defined.", type);
    std::cout<<message<<std::endl;
    assert(0);
  }
  return 0;
}

