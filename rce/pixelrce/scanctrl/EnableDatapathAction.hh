#ifndef DATAPATHENABLE_HH
#define DATAPATHENABLE_HH

#include "scanctrl/LoopAction.hh"
#include "config/RceFWRegisters.hh"

class EnableDatapathAction: public LoopAction{
public:
  EnableDatapathAction(std::string name):
    LoopAction(name),m_fwregs(new RceFWRegisters){}
  ~EnableDatapathAction(){
    delete m_fwregs;
  }
  int execute(int i){
    m_fwregs->enableDatapath(0, true);
    return 0;
  }
private:
  RceFWRegisters* m_fwregs;
};

#endif
