#include "scanctrl/RCFRceCallback.hh"
#include <iostream>

RcfClient<I_RCFCallback>* RCFRceCallback::m_cb(0);

RCFRceCallback::RCFRceCallback(RCF::RcfServer &server): RceCallback(), m_server(server){
  m_server.setOnCallbackConnectionCreated(RCFRceCallback::onCallbackConnectionCreated);
}
RCFRceCallback::~RCFRceCallback(){
}
void RCFRceCallback::onCallbackConnectionCreated(RCF::RcfSessionPtr sessionPtr,
					  RCF::ClientTransportAutoPtr clientTransportPtr){
  delete m_cb;
  m_cb=new RcfClient<I_RCFCallback>(clientTransportPtr);
  m_configured=true;
}
void RCFRceCallback::SendMsg(ipc::Priority pr, const ipc::CallbackParams *msg){
  if(m_configured){
    if(pr>=m_pr){
      try {
	m_cb->notify( *msg );
      }
      catch(...) {
	std::cout << "callback : notification fails" << std::endl;
      }
    }
  }
}
void RCFRceCallback::RCFConfigureCallback(ipc::Priority pr){
  m_pr=pr;
}
void RCFRceCallback::Shutdown(){
  if(m_configured){
    try {
      m_cb->stopServer();
    }
    catch(...) {
      std::cout << "callback : notification fails" << std::endl;
    }
    m_configured=false;
  }
}
