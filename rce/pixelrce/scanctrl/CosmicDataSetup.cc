#include "scanctrl/CosmicDataSetup.hh"
#include "scanctrl/ScanLoop.hh"
#include <boost/property_tree/ptree.hpp>
#include "scanctrl/ActionFactory.hh"
#include "scanctrl/LoopAction.hh"
#include "scanctrl/EndOfLoopAction.hh"
#include <iostream>

int CosmicDataSetup::setupLoops( NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af){
  int retval=0; 
  loop.clear();
  try{ //catch bad scan option parameters
    // Data Taking has a single loop with one entry
    ScanLoop *scanloop=new ScanLoop("data",1);
    LoopAction* conf=af->createLoopAction("CONFIGURE_MODULES");
    scanloop->addLoopAction(conf);
    LoopAction* linkmask=af->createLoopAction("SETUP_TRIGGER", scanOptions);
    scanloop->addLoopAction(linkmask);
    LoopAction* enabletdc=0;
    if(scanOptions->get<int>("trigOpt.triggerDataOn")){
      enabletdc=af->createLoopAction("COSMIC_ENABLE_TDC");
      scanloop->addLoopAction(enabletdc);
    }
    LoopAction* enable=af->createLoopAction("ENABLE_TRIGGER");
    scanloop->addLoopAction(enable);
    // End of loop actions
    EndOfLoopAction* disabletrigger=af->createEndOfLoopAction("DISABLE_TRIGGER","");
    scanloop->addEndOfLoopAction(disabletrigger);
    EndOfLoopAction* closefile=af->createEndOfLoopAction("CLOSE_FILE","");
    scanloop->addEndOfLoopAction(closefile);
    EndOfLoopAction* resetfe=af->createEndOfLoopAction("RESET_FE","");
    scanloop->addEndOfLoopAction(resetfe);
    EndOfLoopAction* disablechannels=af->createEndOfLoopAction("DISABLE_ALL_CHANNELS","");
    scanloop->addEndOfLoopAction(disablechannels);
    // loop is the nested loop object
    loop.addNewInnermostLoop(scanloop);
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    retval=1;
  }
  //  loop.print();
  return retval;
}
