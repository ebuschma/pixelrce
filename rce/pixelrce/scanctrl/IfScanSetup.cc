#include "scanctrl/IfScanSetup.hh"
#include "scanctrl/ScanLoop.hh"
#include <boost/property_tree/ptree.hpp>
#include "scanctrl/ActionFactory.hh"
#include "scanctrl/LoopAction.hh"
#include "scanctrl/EndOfLoopAction.hh"

#include <stdio.h>
#include <iostream>

int IfScanSetup::setupLoops( NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af){
  int retval=0; 
  loop.clear();
  try{ //catch bad scan option parameters
    // Start with the trigger loop 
    int nEvents = scanOptions->get<int>("trigOpt.nEvents");
    ScanLoop* triggerloop=new ScanLoop("Triggerloop",nEvents);
    //    LoopAction* pause=af->createLoopAction("PAUSE");
    LoopAction* sendtrigger=af->createLoopAction("SEND_TRIGGER",scanOptions);
        //triggerloop->addLoopAction(pause);
    triggerloop->addLoopAction(sendtrigger);
    // loop is the nested loop object
    loop.addNewInnermostLoop(triggerloop);
    
    // The position of the mask stage loop is not 
    // explicitely specified by PixLib. Implicitely it's the innermost loop
    // if there is no other scan variable (nLoops==0) and next-to-innermost otherwise. 
    int nLoops = scanOptions->get<int>("nLoops");
    assert(nLoops==1);
    // now it's time for the mask stage loop
    int nMaskStages= scanOptions->get<int>("nMaskStages");
    ScanLoop* maskStageLoop = new ScanLoop("MaskStageLoop",nMaskStages);
    LoopAction* maskaction=af->createLoopAction("SETUP_MASK", scanOptions);
    maskStageLoop->addLoopAction(maskaction);
    loop.addNewOutermostLoop(maskStageLoop);
    //parameter loop is next
    int nPoints = scanOptions->get<int>("scanLoop_0.nPoints");
    ScanLoop *firstparloop=new ScanLoop("scanLoop_0",nPoints);
    LoopAction* wait=af->createLoopAction("IF_WAIT");
    LoopAction* setuppar0=af->createLoopAction("SETUP_PARAM",&scanOptions->get_child("scanLoop_0"));
    LoopAction* chgbin0=af->createLoopAction("CHANGE_BIN");
    firstparloop->addLoopAction(wait);
    firstparloop->addLoopAction(setuppar0);
    firstparloop->addLoopAction(chgbin0);
    loop.addNewOutermostLoop(firstparloop);
    std::string action=scanOptions->get<std::string>("scanLoop_0.endofLoopAction.Action");
    std::string fitfunc=scanOptions->get<std::string>("scanLoop_0.endofLoopAction.fitFunction");
    EndOfLoopAction* fitaction=af->createEndOfLoopAction(action,fitfunc);
    /* protect against no loop action returned */
    if(fitaction)maskStageLoop->addEndOfLoopAction(fitaction);
    ScanLoop *configloop=new ScanLoop("Configure modules",1);
    LoopAction* configaction=af->createLoopAction("CONFIGURE_MODULES_NO_ENABLE");
    configloop->addLoopAction(configaction);
    loop.addNewOutermostLoop(configloop);
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    retval=1;
  }
  //  loop.print();
  return retval;
}
