#ifndef RESETFEEOLACTION_HH
#define RESETFEEOLACTION_HH

#include "scanctrl/EndOfLoopAction.hh"
#include "config/ConfigIF.hh"

class ResetFEEoLAction: public EndOfLoopAction{
public:
  ResetFEEoLAction(std::string name, ConfigIF* cif) :
    EndOfLoopAction(name),m_configIF(cif){}
  int execute(){
    m_configIF->resetFE();
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
