#ifndef COSMICENABLETDC_HH
#define COSMICENABLETDC_HH

#include "scanctrl/LoopAction.hh"
#include "config/ConfigIF.hh"

class CosmicEnableTDCAction: public LoopAction{
public:
  CosmicEnableTDCAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    unsigned serstat= m_configIF->writeHWregister(4,1);
    assert(serstat==0);
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
