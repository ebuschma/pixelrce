#include "scanctrl/RceCallback.hh"
#include <iostream>

RceCallback* RceCallback::m_callback=0;
bool RceCallback::m_configured(false);

RceCallback::RceCallback(): m_pr(ipc::HIGH){
  m_callback=this;
}

void RceCallback::sendMsg(ipc::Priority pr, const ipc::CallbackParams *msg){
  if(m_callback==0)return;
  m_callback->SendMsg(pr, msg);
}
void RceCallback::shutdown(){
  if(m_callback==0)return;
  m_callback->Shutdown();
}
