
#include "ScanLoop.hh"
#include "NestedLoop.hh"
#include <iostream>
#include <sstream>

void NestedLoop::addNewInnermostLoop(ScanLoop* loop){
  ScanLoop *oldinnermost;
  if(m_looplist.empty()){
    oldinnermost=0;
  }else{
    oldinnermost=m_looplist.front();
  }
  m_looplist.push_front(loop);
  loop->setNextOuterLoop(oldinnermost);
}
void NestedLoop::addNewOutermostLoop(ScanLoop* loop){
  ScanLoop *oldoutermost;
  if(!m_looplist.empty()){
    oldoutermost=m_looplist.back();
    oldoutermost->setNextOuterLoop(loop);
  }
  m_looplist.push_back(loop);
}

void NestedLoop::next(){ // all for loops are chained together and call each other.
  // the first event is special because all loop actions have to be performed for setup
  if(!m_looplist.empty()){ 
    if(m_init==false){
      firstEvent();
      m_init=true;
    }else{
      m_done=m_looplist.front()->next(); //done comes from the outermost loop
    }
  }
}

void NestedLoop::firstEvent(){
  std::list<ScanLoop*>::reverse_iterator listit;
  // has to go from the outside to the inside
  for(listit = m_looplist.rbegin(); listit != m_looplist.rend(); listit++){
    (*listit)->firstEvent();
  }
}

void NestedLoop::clear(){
  std::list<ScanLoop*>::iterator listit;
  for(listit = m_looplist.begin(); listit != m_looplist.end(); listit++){
    delete *listit;
  }
  m_looplist.clear();
  m_init=false;
  m_done=false;
}  
void NestedLoop::reset(){
  m_init=false;
  m_done=false;
  std::list<ScanLoop*>::iterator listit;
  for(listit = m_looplist.begin(); listit != m_looplist.end(); listit++){
    (*listit)->reset();
  }
}
  
NestedLoop::~NestedLoop(){
  std::list<ScanLoop*>::iterator listit;
  for(listit = m_looplist.begin(); listit != m_looplist.end(); listit++){
    delete *listit;
  }
}

void NestedLoop::print(){
  std::cout<<std::endl;
  std::list<ScanLoop*>::iterator listit;
  std::stringstream spaces(" ");
  for(listit = m_looplist.begin(); listit != m_looplist.end(); listit++){
    ScanLoop* loop=(*listit);
    loop->print(spaces.str().c_str());
    spaces<<"   ";
    std::cout<<std::endl;
  }
}
