#ifndef COSMICDATASETUP_HH
#define COSMICDATASETUP_HH

#include "scanctrl/NestedLoop.hh"
#include "scanctrl/LoopSetup.hh"
#include "scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ConfigIF.hh"

class CosmicDataSetup: public LoopSetup{
public:
  CosmicDataSetup():LoopSetup(){};
  virtual ~CosmicDataSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
