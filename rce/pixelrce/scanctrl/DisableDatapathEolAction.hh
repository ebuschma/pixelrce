#ifndef DATAPATHDISABLEEOL_HH
#define DATAPATHDISABLEEOL_HH

#include "scanctrl/EndOfLoopAction.hh"
#include "config/RceFWRegisters.hh"

class DisableDatapathEolAction: public EndOfLoopAction{
public:
  DisableDatapathEolAction(std::string name):
    EndOfLoopAction(name),m_fwregs(new RceFWRegisters){}
  ~DisableDatapathEolAction(){
    delete m_fwregs;
  }
  int execute(){
    m_fwregs->enableDatapath(0, false);
    return 0;
  }
private:
  RceFWRegisters* m_fwregs;
};

#endif
