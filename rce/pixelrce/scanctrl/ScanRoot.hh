#ifndef SCANROOT_HH
#define SCANROOT_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include <pthread.h>

class ConfigIF;
class AbsDataProc;
class AbsDataHandler;
class AbsFormatter;
class AbsReceiver;
class Scan;

class ScanRoot{
public:
  ScanRoot(ConfigIF*, Scan*);
  virtual ~ScanRoot();
  virtual int configureScan(boost::property_tree::ptree* scanOptions);
protected:
  ConfigIF* m_configIF;
  AbsDataProc* m_dataProc;
  Scan* m_scan;
  AbsFormatter *m_formatter;
  AbsDataHandler* m_handler;
  AbsReceiver* m_receiver;

};
#endif
