#ifndef ABSTRIGGER_HH
#define ABSTRIGGER_HH

#include <list>
#include <boost/property_tree/ptree_fwd.hpp>

class AbsTrigger{
public:
  AbsTrigger():m_i(0){};
  virtual ~AbsTrigger(){}
  // configureScan and setupParameter can be overridden by daughter classes
  virtual int configureScan(boost::property_tree::ptree* scanOptions){
    m_i=0;
    return 0;
  }
  virtual int setupParameter(const char* name, int val){return 0;}
  virtual int sendTrigger()=0;
  virtual int getNTriggers(){return m_i;}
  virtual int enableTrigger(bool on)=0;
  virtual int resetCounters(){return 0;}
  bool lookupParameter(const char* name);
protected:
  int m_i;
  std::list<std::string> m_parameters;
};

#endif
