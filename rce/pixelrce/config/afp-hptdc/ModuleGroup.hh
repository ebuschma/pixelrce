#ifndef MODULEGROUPAFPHPTDC_HH
#define MODULEGROUPAFPHPTDC_HH
#include "config/AbsModuleGroup.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include <vector>

namespace afphptdc{

  class Module;

class ModuleGroup: public AbsModuleGroup{
public:
  ModuleGroup():AbsModuleGroup(){};
  virtual ~ModuleGroup(){}
  void addModule(Module* module);
  void deleteModules();
  int setupParameterHW(const char* name, int val, bool bcok);
  int setupMaskStageHW(int stage);
  void configureModulesHW();
  void configureModuleHW(int outlink);
  int verifyModuleConfigHW();
  void resetErrorCountersHW();
  int configureScan(boost::property_tree::ptree *scanOptions);
  void resetFE();
  void enableDataTakingHW();
  unsigned getNmodules(){return m_modules.size();}
private:
  void switchToConfigModeHW();
  std::vector<Module*> m_modules;

};
}

#endif
