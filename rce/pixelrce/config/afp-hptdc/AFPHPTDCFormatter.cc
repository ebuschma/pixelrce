#include "config/afp-hptdc/AFPHPTDCFormatter.hh"
#include "config/FormattedRecord.hh"
#include "config/afp-hptdc/AFPHPTDCRecord.hh"
#include "config/FEI4/FEI4BRecord.hh"
#include "scanctrl/RceCallback.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <stdio.h>

AFPHPTDCFormatter::AFPHPTDCFormatter(int id): AbsFormatter(id, "AFPHPTDC"){
}
AFPHPTDCFormatter::~AFPHPTDCFormatter(){
}
void AFPHPTDCFormatter::configure(boost::property_tree::ptree* config){
  try{
    for(int i=0;i<2;i++){
      for(int j=0;j<12;j++){
	for(int k=0;k<1024;k++){
	  char name[32];
	  sprintf(name, "c_%d_%d_%d", i, j, k);
	  m_calib[i][j][k]=config->get<float>(name);
	}
      }
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
  }
}

int AFPHPTDCFormatter::decode (const unsigned int *buffer, int buflen, unsigned* parsedData, int &parsedsize, int &nL1A){
  if(buflen==0)return FEI4::FEI4BRecord::Empty;
  for(int i=0;i<buflen;i++){
    //std::cout<<"Raw word: "<<std::hex<<buffer[i]<<std::dec<<std::endl;
    afphptdc::AFPHPTDCRecord rec(buffer[i]);
    if(rec.isHeader()){
      FormattedRecord fr(FormattedRecord::HEADER);
      fr.setL1id(rec.getL1id());
      fr.setBxid(rec.getBxid());
      parsedData[parsedsize++]=fr.getWord();
      nL1A++;
      //std::cout<<"Header L1id="<<rec.getL1id()<<" bxid="<<rec.getBxid()<<std::endl;
    }else if(rec.isData()){
      unsigned dataword=FormattedRecord::DATA;
      unsigned tdcid=rec.getTDCid();
      dataword|=tdcid<<24;
      unsigned chan=rec.getChannel();
      dataword|=chan<<22;
      unsigned edge=rec.getEdge();
      dataword|=edge<<28;
      unsigned data=rec.getData();
      if(tdcid>=0xa && tdcid<=0xc){ //TDC id should be 0xa, 0xb, or 0xc
	int chanid=(tdcid-0xa)*4+chan;
	//applying Inl calibration constants here.
       data=(data-(unsigned)m_calib[0][chanid][data&0x3FF])&0x1FFFFF;
      }else{
	std::cout<<"Bad TDC id "<<tdcid<<std::endl;
      }
      dataword|=data;
      //std::cout<<"Data TDC id="<<rec.getTDCid()<<" Channel="<<rec.getChannel()<<" Data="<<rec.getData()<<std::endl;
      FormattedRecord fr(dataword);
      //fr.setDataflag(1); // flags HPTDC data
      parsedData[parsedsize++]=fr.getWord();
    }else if(rec.isServiceRecord()){
      //std::cout<<"Received service record with code "<<rec.getErrorCode()<<"  and error count "<<rec.getErrorCount()<<std::endl; 
    }else if(rec.isTdcError()){
      int tdcNum = rec.getTDCid()-0xA;
      std::cout<<"Received TDC error for TDC id "<<tdcNum<<" with code 0x"<<std::hex<<rec.getTdcErrorCode()<<std::dec<<std::endl;	
    }else if(rec.isEmptyRecord()){
      //std::cout<<"Empty record"<<std::endl;
    }else{
      std::cout<<"FE "<<getId()<<": Unexpected record type: "<<std::hex<<buffer[i]<<std::dec<<std::endl;
      return FEI4::FEI4BRecord::BadRecord;
    }
  }
  return FEI4::FEI4BRecord::OK;
}

