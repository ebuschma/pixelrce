#ifndef FEI4ANALOGCOLPRMASKSTAGING_HH
#define FEI4ANALOGCOLPRMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class AnalogColprMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    AnalogColprMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
    int m_nColStages;
    int m_colpr_mode;
    int m_oldDcolStage;
  };
  
}
#endif
