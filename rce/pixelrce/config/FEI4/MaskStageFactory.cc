#include "config/MaskStageFactory.hh"
#include "config/FEI4/Module.hh"
#include "config/FEI4/PixelRegister.hh"
#include "config/FEI4/AnalogMaskStaging.hh"
#include "config/FEI4/FastAnalogMaskStaging.hh"
#include "config/FEI4/AnalogSingleMaskStaging.hh"
#include "config/FEI4/AnalogColprMaskStaging.hh"
#include "config/FEI4/AnalogSimpleColprMaskStaging.hh"
#include "config/FEI4/AnalogColpr2MaskStagingFei4a.hh"
#include "config/FEI4/DigitalMaskStaging.hh"
#include "config/FEI4/DigStepMaskStaging.hh"
#include "config/FEI4/CrosstalkMaskStaging.hh"
#include "config/FEI4/CrosstalkFastMaskStaging.hh"
#include "config/FEI4/NoiseMaskStaging.hh"
#include "config/FEI4/DiffusionMaskStaging.hh"
#include "config/FEI4/ModuleCrosstalkMaskStaging.hh"
#include "config/FEI4/MaskStaging26880.hh"
#include "config/FEI4/PatternMaskStaging.hh"
#include "config/Masks.hh"
#include <assert.h>
#include <string>

using namespace FEI4;
  
template<> Masks MaskStageFactory::createIOmasks<FEI4::Module>(const char* maskStagingMode){
  Masks m;
  m.oMask=m.iMask=m.xtalk_on=m.xtalk_off=m.crosstalking=0;
  std::string stagingMode(maskStagingMode);
  if(stagingMode=="FEI4_ENA_NOCAP"){
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj]);
    
    m.iMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj]);
  } else if(std::string(stagingMode)=="FEI4_ENA_SCAP"){
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap]);
    
    m.iMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap]);
  } else if(std::string(stagingMode)=="FEI4_ENA_LCAP"){
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap]);
    
    m.iMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap]);
  } else if(std::string(stagingMode)=="FEI4_ENA_BCAP"){
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap]);
    
    m.iMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap]);
  } else if(std::string(stagingMode)=="FEI4_ENA_HITBUS_DIG"){
    PixelRegister::setHitBusMode(1);
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::hitbus]);
    
    m.iMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::hitbus]);
  } else if(std::string(stagingMode)=="FEI4_ENA_HITBUS"){
    PixelRegister::setHitBusMode(1);
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::hitbus]);
    
    m.iMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::hitbus]);
  } else if(std::string(stagingMode)=="FEI4_MONLEAK"){
    PixelRegister::setHitBusMode(0);
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::hitbus]);
    
    m.iMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::hitbus]);
  } else if(std::string(stagingMode)=="FEI4_XTALK"){
    m.crosstalking = 1;
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]) 
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::hitbus])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap]);
    
    m.xtalk_off=(1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap]);
    m.xtalk_on=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]);
  } else if(std::string(stagingMode)=="FEI4_NOISE"){
    m.oMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::largeCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::smallCap])
      | (1<<PixelRegisterFields::fieldPos[PixelRegister::diginj]);

    m.iMask=(1<<PixelRegisterFields::fieldPos[PixelRegister::enable]);
  }else{
    std::cout<<"Mask stage mode "<<stagingMode<<" does not exist."<<std::endl;
    m.oMask=0;
    m.iMask=0;
  }
  return m;
}

template<>
MaskStaging<FEI4::Module>* MaskStageFactory::createMaskStaging(FEI4::Module* mod, const char* maskStagingType, const char* maskStagingMode){
  Masks iomask=createIOmasks<FEI4::Module>(maskStagingMode);
  std::string stagingType(maskStagingType);
  if(stagingType=="FEI4_COL_ANL_40" || stagingType=="FEI4_COL_ANL_40x8")
    return new FEI4::AnalogMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_COL_DIG_40")
    return new FEI4::DigitalMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_26880")
    return new FEI4::MaskStaging26880(mod, iomask, stagingType);
  else if(stagingType.substr(0,6)=="STEPS_")
    return new FEI4::DigStepMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_XTALK_26880")
    return new FEI4::CrosstalkMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_XTALK_40x8")
    return new FEI4::CrosstalkFastMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_DIFFUSION")
    return new FEI4::DiffusionMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_ALLCOLS")
    return new FEI4::NoiseMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_COL_ANL_1")
    return new FEI4::AnalogSingleMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_COLPR2x6FEI4A")
    return new FEI4::AnalogColpr2MaskStagingFei4a(mod, iomask, stagingType);
  else if(stagingType.substr(0,10)=="FEI4_COLPR")
    return new FEI4::AnalogColprMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_MODULECROSSTALK")
    return new FEI4::ModuleCrosstalkMaskStaging(mod, iomask, stagingType);
  else if(stagingType=="FEI4_PATTERN")
    return new FEI4::PatternMaskStaging(mod, iomask, stagingType);
  else{
    std::cout<<"Wrong staging type "<<stagingType<<std::endl;
    assert(0);
  }
}
