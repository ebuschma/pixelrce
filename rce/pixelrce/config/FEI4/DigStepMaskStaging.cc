#include "config/FEI4/DigStepMaskStaging.hh"
#include "config/FEI4/Module.hh"

namespace FEI4{
  
  DigStepMaskStaging::DigStepMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
    m_nStages=strtoul(type.substr(6).c_str(),0,10);
  }
  
  void DigStepMaskStaging::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    global->setField("Colpr_Addr", 0, GlobalRegister::SW); 
    global->setField("Colpr_Mode", 3, GlobalRegister::SW); 
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      if(m_masks.iMask&(1<<i)){
	pixel->setupMaskStage(i, maskStage, m_nStages);
	m_module->writeDoubleColumnHW(i, i, 0, 0); //stage 0 writes all bits (below)
      }
    }
    // enable all pixels for charge injection
    global->setField("Colpr_Mode", 3, GlobalRegister::HW); 
  }
}
