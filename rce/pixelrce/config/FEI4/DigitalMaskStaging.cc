#include "config/FEI4/DigitalMaskStaging.hh"
#include "config/FEI4/Module.hh"

namespace FEI4{
  
  DigitalMaskStaging::DigitalMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
  }
  
  void DigitalMaskStaging::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    // Digital injection dcol by dcol
    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      if(m_masks.iMask&(1<<i)){
	pixel->setBitDcol(maskStage, i, 1); //enable bit in double column
	pixel->setBitDcol(maskStage-1, i, 0); //disable previous column
	m_module->writeDoubleColumnHW(i, i, maskStage-1, maskStage); 
	global->setField("Colpr_Addr", maskStage, GlobalRegister::HW); 
      }
    }
  }
}
