#ifndef FEI4ANALOGSINGLEMASKSTAGING_HH
#define FEI4ANALOGSINGLEMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class AnalogSingleMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    AnalogSingleMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  };
  
}
#endif
