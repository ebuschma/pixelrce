#include "config/FEI4/FEI4BModule.hh"
#include "config/FEI4/FEI4BGlobalRegister.hh"
#include "HW/BitStream.hh"
#include "config/FEI4/FECommands.hh"
#include "config/MaskStageFactory.hh"
#include "HW/SerialIF.hh"
#include <boost/property_tree/ptree.hpp>
#include "util/VerifyErrors.hh"
#include <iostream>
#include <fstream>
#include <stdio.h>

namespace FEI4{


  FEI4BModule::FEI4BModule(const char* name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt):
    Module(name, id, inlink, outlink, fmt){
    //    std::cout<<"Module"<<std::endl;
    m_commands=new FECommands;
    m_global=new FEI4BGlobalRegister(m_commands);
    m_pixel=new PixelRegister(m_commands);
    m_pixel_readback=new PixelRegister(m_commands);
  }
  FEI4BModule::~FEI4BModule(){
    delete m_global;
    delete m_pixel;
    delete m_pixel_readback;
    delete m_commands;
    //m_timer.Print("Module");
  }
  void FEI4BModule::setupGlobalPulseHW(int pulsereg){
    m_global->setField("Efuse_sense",(pulsereg&GlobalRegister::Efuse_sense)!=0, GlobalRegister::SW);
    m_global->setField("Stop_Clk",(pulsereg&GlobalRegister::Stop_Clk)!=0, GlobalRegister::SW);
    m_global->setField("ReadErrorReq",(pulsereg&GlobalRegister::ReadErrorReq)!=0, GlobalRegister::SW);
    m_global->setField("CalEn",(pulsereg&GlobalRegister::CalEn)!=0, GlobalRegister::SW);
    m_global->setField("SR_clr",(pulsereg&GlobalRegister::SR_clr)!=0, GlobalRegister::SW);
    m_global->setField("Latch_en",(pulsereg&GlobalRegister::Latch_en)!=0, GlobalRegister::SW);
    m_global->setField("GADC_Enable",(pulsereg&GlobalRegister::GADC_Start)!=0, GlobalRegister::SW);
    m_global->setField("ShiftReadBack",(pulsereg&GlobalRegister::SR_Read)!=0, GlobalRegister::SW);
    m_global->setField("SR_Clock",(pulsereg&GlobalRegister::SR_Clock)!=0, GlobalRegister::HW); //do one HW write for all fields
  }
    
  //  void Frontend::pixelUsrToRaw(BitStream *bs, unsigned bit){
   // }

  void FEI4BModule::resetHW(){
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    m_commands->globalReset(bs);
    SerialIF::send(bs);
    delete bs;
    resetErrorCountersHW();
  }


  int FEI4BModule::verifyModuleConfigHW(){
    int retval=0;
    SerialIF::setChannelInMask(1<<m_inLink);
    SerialIF::setChannelOutMask(1<<m_outLink);
    std::vector <unsigned> readback;
    if(m_formatter){
      // global register
      std::cout<<"***FE "<<m_id<<"***"<<std::endl;
      m_formatter->setReadbackPointer(&readback);
      retval=m_global->verifyConfigHW(readback);
      // pixel registers
      m_global->setField("Colpr_Mode", 0, GlobalRegister::SW); // only write to 1 column pair
      BitStream *bs=new BitStream;
      bs->push_back(0);
      m_commands->globalPulse(bs, FECommands::PULSE_WIDTH);
      for (int dcol=0;dcol<PixelRegister::N_COLS/2;dcol++){
	for (int bit=0;bit<PixelRegister::N_PIXEL_REGISTER_BITS-1;bit++){
	  m_global->setField("Colpr_Addr", dcol, GlobalRegister::HW);
	  setupS0S1HitldHW(1, 1, 0);
	  setupGlobalPulseHW(GlobalRegister::SR_Clock); //SR clock
	  //set up strobe bit
	  setupPixelStrobesHW(1<<bit);
	  SerialIF::send(bs, SerialIF::DONT_CLEAR); //global pulse
	  setupS0S1HitldHW(0, 0, 0);
	  m_global->setField("ShiftReadBack",1, GlobalRegister::HW); //setupGlobalPulse does the HW write for that register
	  retval|=m_pixel->verifyDoubleColumnHW(bit, dcol, readback); //check double column
	  m_global->setField("ShiftReadBack",0, GlobalRegister::SW); //setupGlobalPulse does the HW write for that register
	}
      }
      setupGlobalPulseHW(0); //clear
      setupPixelStrobesHW(0); //clear
      delete bs;
      m_formatter->setReadbackPointer(0);
    }else{
      retval=ModuleVerify::NO_FORMATTER;
    }
    return retval;
  }


  void FEI4BModule::destroy(){
    delete this;
  }


};

