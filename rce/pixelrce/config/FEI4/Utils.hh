#ifndef UTILS_HH
#define UTILS_HH

inline unsigned flipBits(int nbits, unsigned val){
    unsigned tmp=0;
    unsigned old=val;
    for (int i=0;i<nbits;i++){
      tmp|=(old&0x1)<<(nbits-i-1);
      old>>=1;
    }
    return tmp;
}

#endif
