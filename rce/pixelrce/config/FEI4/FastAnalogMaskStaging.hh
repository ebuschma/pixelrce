#ifndef FEI4FASTANALOGMASKSTAGING_HH
#define FEI4FASTANALOGMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class FastAnalogMaskStaging: public MaskStaging<FEI4::Module>{
  public:
    FastAnalogMaskStaging(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
  };
  
}
#endif
