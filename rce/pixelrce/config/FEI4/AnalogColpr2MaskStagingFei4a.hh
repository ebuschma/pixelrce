#ifndef FEI4ANALOGCOLPR2MASKSTAGINGFEI4A_HH
#define FEI4ANALOGCOLPR2MASKSTAGINGFEI4A_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{

  class Module;
  
  class AnalogColpr2MaskStagingFei4a: public MaskStaging<FEI4::Module>{
  public:
    AnalogColpr2MaskStagingFei4a(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
  };
  
}
#endif
