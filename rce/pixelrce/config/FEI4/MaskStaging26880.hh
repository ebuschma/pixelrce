#ifndef FEI426880MASKSTAGING_HH
#define FEI426880MASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI4{
  class Module;
  class MaskStaging26880: public MaskStaging<FEI4::Module>{
  public:
    MaskStaging26880(FEI4::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  };
  
}
#endif
