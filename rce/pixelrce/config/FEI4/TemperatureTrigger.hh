#ifndef TEMPERATURETRIGGER_HH
#define TEMPERATURETRIGGER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "config/AbsTrigger.hh"
#include "HW/BitStream.hh"

namespace FEI4{

  class TemperatureTrigger: public AbsTrigger{
  public:
    TemperatureTrigger();
    ~TemperatureTrigger();
    int configureScan(boost::property_tree::ptree* scanOptions);
    int setupParameter(const char* name, int val);
    int sendTrigger();
    int enableTrigger(bool on);
    int resetCounters();
    void setupTriggerStream();
  private:
    BitStream m_triggerStream;
  };

};

#endif
