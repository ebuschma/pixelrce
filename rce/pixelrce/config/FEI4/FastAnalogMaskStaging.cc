#include "config/FEI4/FastAnalogMaskStaging.hh"
#include "config/FEI4/Module.hh"

namespace FEI4{
  
  FastAnalogMaskStaging::FastAnalogMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
    if(type=="FEI4_COL_ANL_40x8")m_nStages=6;
    else m_nStages=3;
  }
  
  void FastAnalogMaskStaging::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    const int nColStages=40;
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    unsigned dcolStage=maskStage%nColStages;
    unsigned stage=maskStage/nColStages;
    if((maskStage+nColStages)/nColStages!=(maskStage+nColStages-1)/nColStages){ //need to set up pixel mask 
      global->setField("Colpr_Mode", 3, GlobalRegister::SW); 
      for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
	if(m_masks.iMask&(1<<i)){
	  pixel->setupMaskStage(i, stage, m_nStages);
	  m_module->writeDoubleColumnHW(i, i, 0, 0); 
	}
      }
    }
    // stage 0-5: colpr_mode=2, colpr_addr= 1, 2, 3, 4, 5, 6
    // stage 6-9: colpr_mode=0, colpr_addr= 8, 16, 24, 32
    // stage 10-13: colpr_mode=0, colpr_addr= 7, 15, 23, 31
    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
    global->setField("Colpr_Addr", dcolStage , GlobalRegister::HW); 
}
};
