#ifndef RCFMODULEFACTORY_HH
#define RCFMODULEFACTORY_HH

#include "config/ModuleFactory.hh"
#include "config/FEI3/ModuleGroup.hh"
#include "config/FEI4/ModuleGroup.hh"
#include "config/hitbus/ModuleGroup.hh"
#include "config/afp-hptdc/ModuleGroup.hh"

class ConfigIF;
class AbsFormatter;
namespace RCF{
  class RcfServer;
}

class RCFModuleFactory: public ModuleFactory {
public:
  RCFModuleFactory(RCF::RcfServer &server);
  AbsModule* createModule(const char* name, const char* type, unsigned id, unsigned inpos, unsigned outPos, const char* formatter);
  AbsFormatter* createFormatter(const char* formatter, int id);
  AbsTrigger* createTriggerIF(const char* type, ConfigIF* cif);
private:
  FEI3::ModuleGroup m_modgroupi3;
  FEI4::ModuleGroup m_modgroupi4a;
  FEI4::ModuleGroup m_modgroupi4b;
  Hitbus::ModuleGroup m_modgrouphitbus;
  afphptdc::ModuleGroup m_modgroupafphptdc;
  RCF::RcfServer &m_server;
  
};


#endif
