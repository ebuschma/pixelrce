#ifndef ABSMODULEGROUP_HH
#define ABSMODULEGROUP_HH
#include <boost/property_tree/ptree_fwd.hpp>
#include <vector>

class AbsModuleGroup{
public:
  AbsModuleGroup(): m_channelInMask(0), m_channelOutMask(0){}
  virtual ~AbsModuleGroup(){};
  void setChannelInMask();
  void setChannelOutMask();
  unsigned getChannelInMask();
  unsigned getChannelOutMask();
  void disableAllOutChannels();
  void disableAllInChannels();
  virtual void deleteModules()=0;
  virtual int setupParameterHW(const char* name, int val, bool bcOK)=0;
  virtual int setupMaskStageHW(int stage)=0;
  virtual void configureModulesHW()=0;
  virtual void configureModuleHW(int outlink)=0;
  virtual int verifyModuleConfigHW()=0;
  virtual void resetErrorCountersHW()=0;
  virtual int configureScan(boost::property_tree::ptree *scanOptions)=0;
  virtual void resetFE()=0;
  virtual void enableDataTakingHW()=0;
  virtual unsigned getNmodules()=0;
protected:
  unsigned m_channelInMask;
  unsigned m_channelOutMask;

};

#endif
