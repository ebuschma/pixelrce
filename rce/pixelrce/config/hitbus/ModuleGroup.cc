
#include <boost/property_tree/ptree.hpp>
#include <stdio.h>
#include "config/hitbus/Module.hh"
#include "config/hitbus/ModuleGroup.hh"
#include "HW/SerialIF.hh"
#include <iostream>

namespace Hitbus{

void ModuleGroup::addModule(Module* module){
  m_modules.push_back(module);
  m_channelInMask|=1<<module->getInLink();
  m_channelOutMask=0;
}

void ModuleGroup::deleteModules(){
  for (unsigned i=0; i<m_modules.size();i++){
    //cannot call delete directly because IPC modules need to call _destroy() instead.
    m_modules[i]->destroy();
  }
  m_modules.clear();
  m_channelInMask=0;
  m_channelOutMask=0;
}

int ModuleGroup::setupParameterHW(const char* name, int val, bool bcOK){
  int retval=0;
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    retval+=m_modules[i]->setupParameterHW(name,val);
  }
  disableAllInChannels();
  return retval;
}

int ModuleGroup::setupMaskStageHW(int stage){
  // no mask staging for the hitbus chip.
  return 0;
}

void ModuleGroup::configureModulesHW(){
  //std::cout<<"Configure Modules HW"<<std::endl;
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    std::cout<<"Setting channel in mask for inlink "<<m_modules[i]->getInLink()<<std::endl;
    m_modules[i]->configureHW();
  }
  SerialIF::sendCommand(0x10); //phase calibration
  disableAllInChannels();
}

void ModuleGroup::resetFE(){
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    m_modules[i]->resetFE();
  }
  disableAllInChannels();
}

void ModuleGroup::enableDataTakingHW(){
  // do nothing
}

int ModuleGroup::verifyModuleConfigHW(){
  return 0; // does not exist for Hitbus
}
void ModuleGroup::resetErrorCountersHW(){
  //does not exist for Hitbus
}

int ModuleGroup::configureScan(boost::property_tree::ptree *scanOptions){
  int retval=0;
  for (unsigned int i=0;i<m_modules.size();i++){
    retval+=m_modules[i]->configureScan(scanOptions);
  }
  return retval;
}

}
