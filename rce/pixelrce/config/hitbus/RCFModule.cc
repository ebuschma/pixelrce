#include "util/RceName.hh"
#include "config/hitbus/RCFModule.hh"
#include <iostream>
namespace Hitbus{

  RCFModule::RCFModule(RCF::RcfServer &server, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt):
    Module(name, id, inlink, outlink, fmt), m_server(server){
    //  std::cout<<"RCFModule"<<std::endl;
    char binding[32];
    sprintf(binding, "I_RCFHitbusAdapter_%d", id);
    m_server.bind<I_RCFHitbusAdapter>(*this, binding);
}

RCFModule::~RCFModule(){
    char binding[32];
    sprintf(binding, "I_RCFHitbusAdapter_%d", m_id);
    m_server.unbind<I_RCFHitbusAdapter>(binding);
}

int32_t RCFModule::RCFdownloadConfig(ipc::HitbusModuleConfig config){
  setRegister("bpm", config.bpm);
  setRegister("delay_tam1", config.delay_tam1);
  setRegister("delay_tam2", config.delay_tam2);
  setRegister("delay_tam3", config.delay_tam3);
  setRegister("delay_tbm1", config.delay_tbm1);
  setRegister("delay_tbm2", config.delay_tbm2);
  setRegister("delay_tbm3", config.delay_tbm3);
  setRegister("bypass_delay", config.bypass_delay);
  setRegister("clock", config.clock);
  setRegister("function_A", config.function_A);
  setRegister("function_B", config.function_B);
  std::cout<<"Configure done"<<std::endl;
  return 0;
}


};

