#ifndef RCFHITBUSMODULE_HH
#define RCFHITBUSMODULE_HH

#include "config/hitbus/Module.hh"
#include "rcf/HitbusModuleConfig.hh"
#include "rcf/RCFHitbusAdapter.hh"

class AbsFormatter;

namespace Hitbus{
class RCFModule: public RCFHitbusAdapter, public Hitbus::Module {
public:
  RCFModule(RCF::RcfServer &server, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt);
  ~RCFModule();
  int32_t RCFdownloadConfig(ipc::HitbusModuleConfig config);    
private:
  RCF::RcfServer& m_server;

};
};
  

#endif
