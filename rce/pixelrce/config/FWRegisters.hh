#ifndef FWREGISTERS_HH
#define FWREGISTERS_HH


class FWRegisters{
private:
  static const unsigned int CHANNELMASK=0;
  static const unsigned int MODE=3;
  static const unsigned int CHANNELOUTMASK=4;
  static const unsigned int INCDISCDELAY=5;
  static const unsigned int MAXDELAY=63;
  static const unsigned int DISCOP=6;
  static const unsigned int RESETDELAYS=7;
  static const unsigned int TRIGGERMASK=11;
  static const unsigned int DEADTIME=15;
  static const unsigned int ENCODING=20;
  static const unsigned int HITBUSOP=21;
  static const unsigned int MULTOPT_0 = 22;
  static const unsigned int MULTOPT_1 =23;
  static const unsigned int L1ROUTE=27;
  static const unsigned int HBDELAY=28;
  static const unsigned int HBDELAYNEG=29;
  static const unsigned int TELESCOPEOP=30;
  static const unsigned int TEMPFREQ=31;
  static const unsigned int TEMPENABLE=32;
  static const unsigned int MAXBUFLENGTH=33;
  static const unsigned int CLKSEL=36;
  static const unsigned int CLKRB=20;
  static const unsigned int HITDISCCONFIG=96;
  static const unsigned int NEXP=37;
  static const unsigned int EFBTIMEOUT=38;
  static const unsigned int EFBTIMEOUTFIRST=39;
  static const unsigned int EFBMISSINGHEADERTIMEOUT=40;
  static const unsigned int RUNNUMBER=41;
  static const unsigned int DETEVTYPE=58;
  static const unsigned int NUMMON=42;
  static const unsigned int MONENABLED=43;
  static const unsigned int DATAPATH=44;
  static const unsigned int TTCSIM=45;
  static const unsigned int VETODISABLE=46;
  static const unsigned int BLOCKECRBCR=47;
  static const unsigned int ENABLEECRRESET=48;
  static const unsigned int ENABLEBCRVETO=49;
  static const unsigned int FIRSTVETOBCID=50;
  static const unsigned int NUMVETOBCID=51;
  static const unsigned int CDTENABLE=52;
  static const unsigned int CDTNBUF=53;
  static const unsigned int CDTWINDOW=54;
  static const unsigned int BCIDOFFSET=55;
  static const unsigned int EVENTLIMIT=56;
  static const unsigned int GBTICCHANNEL=57;
  static const unsigned int REENABLE=59;
  static const unsigned int MODIFYDISABLE=60;
  static const unsigned int DISABLEMASK=62;
  static const unsigned int TTCBUSYCOUNTER=61;
  static const unsigned int RODID=61;
  static const unsigned int CLEANEVENTS=63;
  
  static const unsigned int OUTPUTDELAYS=64;
  static const unsigned int EFBCOUNTERS=64;
  static const unsigned int NEVT=21;
  static const unsigned int NEVTNOMON=22;
  static const unsigned int DISABLED=23;
  static const unsigned int DISABLEDINRUN=24;
  static const unsigned int CLASHCOUNTERS=25;
  static const unsigned int DISABLEDPERM=28;
  static const unsigned int CLOCKMON=43;
  static const unsigned int GBTTXALIGNED=44;
  static const unsigned int HPTDCTEMPS=45;

  static const unsigned int BLOWOFF=0x882;

  static const unsigned int PRESETECR=0xc83;
  static const unsigned int PAUSEECR=0xc84;
  static const unsigned int RESETL1ID=0xcfc;
  static const unsigned int RESETTTC=0xcfd;
  static const unsigned int FORCEBUSY =0xc80;
  static const unsigned int IGNOREBUSY =0xc81;
  static const unsigned int READECR =0xc53;

  static const unsigned int CMD_PRESENT=17;
  
public:
  enum opmode{NORMAL, TDCCALIB, EUDAQ};
  enum trgmask{SCINTILLATORS=1, CYCLIC=2, EUDET=4, EXTERNAL=8, HITBUS=16};
  enum streamencoding{NONE, BIPHASEMARK, MANCHESTER};
  enum CLK{ATLAS, INTERNAL};
  enum routing{SINGLE, PATTERN};
  enum EFBCOUNTER{TIMEOUT, TOOMANYHEADERS, SKIPPEDTRIGGERS, BADHEADERS, MISSINGTRIGGERS, DATANOHEADER, DESYNCHED, ECRRESET, OCCUPANCY, DECERRORS, BUSYCOUNTER, GOODEVENTCOUNTER, RECONFIGCOUNTER};
  enum CLASH{BCRL1A, L1ABCR, L1AL1A};
  enum EVTYPE{PHYSICS=0, TIMING=0x10000000};
  FWRegisters(){}
  virtual void writeRegister(int rce, unsigned reg, unsigned val)=0;
  virtual unsigned readRegister(int rce, unsigned reg)=0;
  virtual void sendCommand(int rce, unsigned opcode)=0;

  void setChannelmask(int rce, unsigned mask);
  void setMode(int rce, opmode mode);
  void setChannelOutmask(int rce, unsigned mask);
  void setDiscDelay(int rce, int channel, int delay);
  void setDiscOpMode(int rce, unsigned mode);
  void resetDelays(int rce);
  void setTriggermask(int rce, unsigned mask);
  void setDeadtime(int rce, unsigned deadtime);
  void setEncoding(int rce, streamencoding enc);
  void setHitbusOp(int rce, unsigned hb);
  void setL1Type(int rce, routing rt);
  void setHitbusDataDelay(int rce, unsigned delay);
  void setHitbusDataNegativeDelay(int rce, unsigned delay);
  void setTelescopeOp(int rce, unsigned op);
  void setTemperatureReadoutFrequency(int rce, unsigned ticks);
  void setTemperatureReadoutEnable(int rce, bool on);
  void setNumberofFeFramesPgp(int rce, unsigned num);
  void setRcePresent(int rce);
  void setHitDiscConfig(int rce, int chan, unsigned val);
  void setNExp(int rce, unsigned val);
  void setEfbTimeout(int rce, unsigned val);
  void setEfbTimeoutFirst(int rce, unsigned val);
  void setEfbMissingHeaderTimeout(int rce, unsigned val);
  void setRunNumber(int rce, unsigned val);
  void setDetEvType(int rce, unsigned val);
  void selectClock(int rce, CLK clk);
  bool atlasClockPresent(int rce);
  bool externalClockSelected(int rce);
  void setOccNormalization(int rce, unsigned val);
  unsigned getOccNormalization(int rce);
  unsigned getClockmonCounters(int rce);
  unsigned getGbtStatus(int rce);
  void writeGbtIcRegister(int rce, unsigned gbtaddr, unsigned regaddr, unsigned val);
  void enableMonitoring(int rce, unsigned val);
  void enableDatapath(int rce, unsigned val);
  void enableTtcSim(int rce, unsigned val);
  void enableEcrReset(int rce, bool on);
  void enableBcrBusy(int rce, bool on);
  void setBcrBusyParams(int rce, unsigned first, unsigned num);
  void enableComplexDeadtime(int rce, bool on);
  void setComplexDeadtimeParams(int rce, unsigned first, unsigned num);
  void setBcidOffset(int rce, unsigned val);
  void setEventSizeLimit(int rce, unsigned val);
  void disableInternalBusy(int rce, bool on);
  unsigned getEfbCounter(int rce, int chan, EFBCOUNTER cnt);
  unsigned getHPTDCrecord(int rce, int hptdc, int reg);
  unsigned getTtcClashCounter(int rce, CLASH cnt);
  void setOutputDelay(int rce, unsigned inlink, unsigned val);
  void enableSLinkBlowoff(int rce, bool on);
  void blockEcrBcr(int rce, unsigned val);
  void resetTtc(int rce);
  void resetL1id(int rce);
  void presetECR(int rce, unsigned value);
  void pauseECR(int rce, bool on);
  unsigned getECR(int rce);
  void outputBusy(int rce, bool on);
  void forceBusy(int rce, bool on);
  void reenableFe(int rce, int link);
  void setDisableMask(int rce, unsigned value);
  void modifyDisableMask(int rce, unsigned outlink, bool on=true);
  void setRodId(int rce, unsigned value);
  unsigned getDisableMask(int rce);
  unsigned getNumberOfEvents(int rce);
  unsigned getNumberOfCleanEvents(int rce);
  unsigned getNumberOfMonMissed(int rce);
  unsigned getDisabledMask(int rce);
  unsigned getDisabledInRunMask(int rce);
  unsigned getDisabledPermMask(int rce);
  unsigned getTtcBusyCounter(int rce);

};
  
#endif
