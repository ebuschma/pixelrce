#include "config/TriggerReceiverIF.hh"
#include <assert.h>

TriggerReceiverIF::TriggerReceiverIF(){
  s_rec=this;
}
TriggerReceiverIF::~TriggerReceiverIF(){
  s_rec=0;
}
void TriggerReceiverIF::receive(unsigned *data, int size){
  assert(s_rec);
  s_rec->Receive(data, size);
}

TriggerReceiverIF* TriggerReceiverIF::s_rec=0;
