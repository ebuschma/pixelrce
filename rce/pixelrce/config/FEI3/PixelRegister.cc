#include "config/FEI3/PixelRegister.hh"

namespace FEI3{
  std::string PixelRegister::m_cachedName;
  PixelRegister::Field PixelRegister::m_cachedField(not_found);
  std::map<std::string, PixelRegister::Field> PixelRegister::m_parameters;
  bool PixelRegister::m_initialized = false;
  void PixelRegister::initialize(){
    // Translation from scan parameter to Global register field
    m_parameters["TDACS"]=tdac;
    m_parameters["FDACS"]=fdac;
    m_initialized=true;
  }
  PixelRegister::Field PixelRegister::lookupParameter(const char* name){
    // check if this is the last name used, return cached value
    if(std::string(name)==m_cachedName)return m_cachedField;
    // Now check if we can translate the name to a field name
    if(m_parameters.find(name)!=m_parameters.end()){
      //cache result
      m_cachedName=name;
      m_cachedField=m_parameters[name]; 
      return m_cachedField;
    }else return not_found;
  }
}
