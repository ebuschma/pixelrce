#ifndef JJFORMATTER_HH
#define JJFORMATTER_HH

#include "config/AbsFormatter.hh"

class JJFormatter:public AbsFormatter{
public:
  JJFormatter(int id):
    AbsFormatter(id, "JJ"){}
  virtual ~JJFormatter(){}
  int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A);
  
};
#endif
