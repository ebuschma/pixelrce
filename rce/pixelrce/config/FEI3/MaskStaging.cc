#include "config/MaskStaging.hh"
#include "config/FEI3/Module.hh"

using namespace FEI3;

template<> bool MaskStaging<FEI3::Module>::cLow(){
  return m_clow;
}
template<> bool MaskStaging<FEI3::Module>::cHigh(){
  return m_chigh;
}

template<> void MaskStaging<FEI3::Module>::clearBitsHW(){
  for (int k=0;k<Module::N_FRONTENDS;k++){
    for (int i=0;i<Frontend::N_COLS;i++){
      for (int j=0;j<Frontend::N_ROWS;j++){
	PixelRegister *pixel=m_module->frontend(k)->getPixelRegister(i,j);
	pixel->set(pixel->get() & m_masks.oMask);
      }
    }
  }
}
