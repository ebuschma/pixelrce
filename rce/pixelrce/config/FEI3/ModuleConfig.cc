#include "config/FEI3/ModuleConfig.hh"
#include <iostream>


#include <RCF/RCF.hpp>
#include "RCFFEI3Adapter.hh"
#include "util/RceName.hh"
int ModuleConfig::downloadConfig(int rce, int id){
  char binding[32];
  char rcename[32];
  sprintf(binding, "I_RCFFEI3Adapter_%d", id);
  sprintf(rcename, RCFHOST"%d", rce);
  try {
    RcfClient<I_RCFFEI3Adapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT), binding);
    client.RCFdownloadConfig( *m_config );
  }
  catch(const RCF::Exception & ex) {
    std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
  }
}

ModuleConfig::ModuleConfig(std::string filename): 
  PixelConfig("FEI3", filename, true, ipc::IPC_N_PIXEL_COLUMNS, ipc::IPC_N_PIXEL_ROWS, ipc::IPC_N_PIXEL_FE_CHIPS), 
  m_config(new ipc::PixelModuleConfig){
  TurboDaqFile turbo;
  turbo.readModuleConfig(m_config,filename);
  m_name=(const char*)m_config->idStr;
  m_id=strtol((const char*)m_config->idStr,0,10);
  m_valid=true;
}

FECalib ModuleConfig::getFECalib(int chip){
  FECalib fe;
  fe.cinjLo=m_config->FEConfig[chip].FECalib.cinjLo;
  fe.cinjHi=m_config->FEConfig[chip].FECalib.cinjHi;
  for(int i=0;i<4;i++) fe.vcalCoeff[i]=m_config->FEConfig[chip].FECalib.vcalCoeff[i];
  fe.chargeCoeffClo=m_config->FEConfig[chip].FECalib.chargeCoeffClo;
  fe.chargeCoeffChi=m_config->FEConfig[chip].FECalib.chargeCoeffChi;
  fe.chargeOffsetClo=m_config->FEConfig[chip].FECalib.chargeOffsetClo;
  fe.chargeOffsetChi=m_config->FEConfig[chip].FECalib.chargeOffsetChi;
  fe.monleakCoeff=m_config->FEConfig[chip].FECalib.monleakCoeff;
  return fe;
}
void ModuleConfig::setFECalib(int chip, FECalib fe){
  m_config->FEConfig[chip].FECalib.cinjLo=fe.cinjLo;
  m_config->FEConfig[chip].FECalib.cinjHi=fe.cinjHi;
  for(int i=0;i<4;i++) m_config->FEConfig[chip].FECalib.vcalCoeff[i]=fe.vcalCoeff[i];
  m_config->FEConfig[chip].FECalib.chargeCoeffClo=fe.chargeCoeffClo;
  m_config->FEConfig[chip].FECalib.chargeCoeffChi=fe.chargeCoeffChi;
  m_config->FEConfig[chip].FECalib.chargeOffsetClo=fe.chargeOffsetClo;
  m_config->FEConfig[chip].FECalib.chargeOffsetChi=fe.chargeOffsetChi;
  m_config->FEConfig[chip].FECalib.monleakCoeff=fe.monleakCoeff;
}
unsigned ModuleConfig::getThresholdDac(int chip, int col, int row){
  return m_config->FEConfig[chip].FETrims.dacThresholdTrim[row][col];
}
void ModuleConfig::setThresholdDac(int chip, int col, int row, int val){
  m_config->FEConfig[chip].FETrims.dacThresholdTrim[row][col]=val;
}
unsigned ModuleConfig::getFeedbackDac(int chip, int col, int row){
  return m_config->FEConfig[chip].FETrims.dacFeedbackTrim[row][col];
}
void ModuleConfig::setFeedbackDac(int chip, int col, int row, int val){
  m_config->FEConfig[chip].FETrims.dacFeedbackTrim[row][col]=val;
}
unsigned ModuleConfig::getIf(int chip){
  return m_config->FEConfig[chip].FEGlobal.dacIF;
}
void ModuleConfig::setIf(int chip, int val){
  m_config->FEConfig[chip].FEGlobal.dacIF=val;
}
unsigned ModuleConfig::getGDac(int chip){
  return m_config->FEConfig[chip].FEGlobal.gdac;
}
void ModuleConfig::setGDac(int chip, int val){
  m_config->FEConfig[chip].FEGlobal.gdac=val;
}
void ModuleConfig::setGDacCoarse(int chip, int val){
}
unsigned ModuleConfig::getFEMask(int chip, int col, int row){
  std::cout<<"getFEMask currently not implemented."<<std::endl;
  return 0;
}
void ModuleConfig::setFEMask(int chip, int col, int row, int val){
  std::cout<<"setFEMask currently not implemented."<<std::endl;
}
void ModuleConfig::writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key){
  TurboDaqFile cf;
  cf.writeModuleConfig(m_config, base, confdir, configname, key);
  //   this is now fixed?
  // std::cmatch matches;
  // std::string res="(.*)/[^/]*/[^/]*$"; // parse what's hopefully the module name
  // std::regex re;
  // re=res;
  // if(std::regex_search(m_filename.c_str(), matches, re)){
  //   if(matches.size()>1){
  //     std::string match(matches[1].first, matches[1].second);
  //     char cmd[512];
  //     sprintf(cmd,"cp -r %s %s", match.c_str(), base.c_str());
  //     system (cmd);
  //   }else
  //     std::cout<<"Bad config file name"<<std::endl;
  // }else
  //  std::cout<<"Bad config file name"<<std::endl;
}
