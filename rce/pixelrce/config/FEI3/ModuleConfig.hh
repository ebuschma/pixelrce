#ifndef MODULECONFIG_HH
#define MODULECONFIG_HH

#include "config/PixelConfig.hh"
#include "PixelModuleConfig.hh"
#include "config/FEI3/TurboDaqFile.hh"

class ModuleConfig: public PixelConfig{
public:
  ModuleConfig(std::string filename);
  virtual ~ModuleConfig(){
    delete m_config;
  }

  virtual int downloadConfig(int rce, int id);
  virtual void* getStruct(){return (void*)m_config;}
  virtual FECalib getFECalib(int chip);
  virtual void setFECalib(int chip, FECalib fe);
  virtual unsigned getThresholdDac(int chip, int col, int row);
  virtual void setThresholdDac(int chip, int col, int row, int val);
  virtual unsigned getFeedbackDac(int chip, int col, int row);
  virtual void setFeedbackDac(int chip, int col, int row, int val);
  virtual unsigned getIf(int chip);
  virtual void setIf(int chip, int val);
  virtual unsigned getGDac(int chip);
  virtual void setGDac(int chip, int val);
  virtual void setGDacCoarse(int chip, int val);
  virtual unsigned getFEMask(int chip, int col, int row);
  virtual void setFEMask(int chip, int col, int row, int val);
  virtual void writeModuleConfig(const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key);

private:
  ipc::PixelModuleConfig *m_config;
};

#endif
