#ifndef FECOMMANDS_FEI3_HH
#define FECOMMANDS_FEI3_HH

#include "HW/BitStream.hh"
#include "config/FEI3/Mcc.hh"

namespace FEI3{
  
  class FECommands{
  public:
    enum Command{
      FE_CMD_NULL=0x000000, 
      FE_CMD_REF_RESET=0x000002, 
      FE_CMD_SOFT_RESET=0x00000C , 
      FE_CMD_CLOCK_GLOBAL=0x000010, 
      FE_CMD_WRITE_GLOBAL=0x000060, 
      FE_CMD_READ_GLOBAL=0x000080, 
      FE_CMD_CLOCK_PIXEL=0x000100, 
      FE_CMD_WRITE_HITBUS=0x000200, 
      FE_CMD_WRITE_SELECT=0x000400, 
      FE_CMD_WRITE_MASK=0x000800, 
      FE_CMD_WRITE_TDAC0=0x001000, 
      FE_CMD_WRITE_TDAC1=0x002000, 
      FE_CMD_WRITE_TDAC2=0x004000, 
      FE_CMD_WRITE_TDAC3=0x008000, 
      FE_CMD_WRITE_TDAC4=0x010000, 
      FE_CMD_WRITE_TDAC5=0x020000, 
      FE_CMD_WRITE_TDAC6=0x040000, 
      FE_CMD_WRITE_FDAC0=0x080000, 
      FE_CMD_WRITE_FDAC1=0x100000, 
      FE_CMD_WRITE_FDAC2=0x200000, 
      FE_CMD_WRITE_KILL=0x400000, 
      FE_CMD_READ_PIXEL=0x800000}; 
    static void mccWriteRegister(BitStream* bs, Mcc::Register reg, unsigned value);
    static void resetMCC(BitStream *bs);
    static void resetFE(BitStream* bs, unsigned width);
    static void enableDataTaking(BitStream *bs);
    static void L1A(BitStream *bs);
    static void sendECR(BitStream *bs);
    static void sendBCR(BitStream *bs);
    static void feWriteCommand(BitStream* bs, unsigned chip, int cmd, bool rw=0);
  };

};
#endif

