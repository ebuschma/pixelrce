
#include "config/FEI3/Frontend.hh"
#include "config/FEI3/FECommands.hh"
#include "HW/SerialIF.hh"
#include <iostream>

namespace FEI3{

  Frontend::Frontend(unsigned chip):m_chip(chip){
    m_global=new GlobalRegister(chip, chip);
  }
  Frontend::~Frontend(){
    delete m_global;
  }
  void Frontend::setChipAddress(unsigned i, unsigned addr){
    if(i<16 && addr<16){
      if(m_chip!=addr){ // cannot use the cached global register
	delete m_global;
	m_global=new GlobalRegister(i, addr);
	std::cout<<"Creating new GlobalRegister position "<<i<<" address "<<addr<<std::endl;
      }
      m_chip=addr;
    }
  }

  void Frontend::writeGlobalRegisterHW(){
    //std::cout<<"write global register chip "<<m_chip<<std::endl;
    m_global->writeHW();
  }
  void Frontend::setGlobalRegField(const char* name, int val){
    m_global->setField(name,val);
  }

  void Frontend::setPixelParameter(PixelRegister::Field field, int val){
    for(int j=0;j<N_COLS;j++){
      for (int i=0;i<N_ROWS;i++){
	m_pixel[j][i].setField(field, val);
      }
    }
    for(unsigned int i=fieldPos[field]; i<fieldPos[field+1];i++){
      writePixelRegisterHW(i);
    }
  }

  void Frontend::writePixelRegisterHW(unsigned bit){
    //std::cout<<"write pixel register "<<bit<<" in chip "<<m_chip<<std::endl;
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    unsigned nPixelsPerChip = Frontend::N_COLS * Frontend::N_ROWS;
    unsigned dcnt = nPixelsPerChip; /* all column pairs */
    unsigned cnt = (dcnt << 3) | 4;
    FECommands::mccWriteRegister(bs, Mcc::MCC_CNT, cnt);
    FECommands::feWriteCommand(bs,m_chip, FECommands::FE_CMD_CLOCK_PIXEL);
    pixelUsrToRaw(bs, bit); 
    bs->push_back(0);// one clean word afterwards 
    cnt = 4;	
    FECommands::mccWriteRegister(bs, Mcc::MCC_CNT, cnt);
    FECommands::feWriteCommand(bs, m_chip, FECommands::FE_CMD_WRITE_HITBUS << bit);
    bs->push_back(0);
    FECommands::feWriteCommand(bs, m_chip, FECommands::FE_CMD_NULL);
    bs->push_back(0);
    SerialIF::send(bs);
    delete bs;
}

  void Frontend::pixelUsrToRaw(BitStream *bs, unsigned bit){
    int col;
    unsigned row;
    unsigned short *pixelReg, mask;
    mask = (1 << bit);
    col = 0;
    /* form the snake pattern */
    col = Frontend::N_COLS - 1; /* the last column */
    while(col > 0) { /* works because we are decrementing by 2 each loop. otherwise, > -1 */
      pixelReg = (unsigned short*)&m_pixel[col][0];
      --col;
      for(row=0;row<Frontend::N_ROWS/sizeof(unsigned);++row){
	unsigned word=0;
	for(int i=0;i<4;i++){
	  word<<=8;
	  word|= (*pixelReg++ & mask) ? 0xff : 0x00; /* upwards */
	}
	bs->push_back(word);
      }
      pixelReg = (unsigned short*)&m_pixel[col][Frontend::N_ROWS-1];
      --col;
      for(row=0;row<Frontend::N_ROWS/sizeof(unsigned);++row){
	unsigned word=0;
	for(int i=0;i<4;i++){
	  word<<=8;
	  word |= (*pixelReg-- & mask) ? 0xff : 0x00; /* backwards */	
	}
	bs->push_back(word);
      }
    }
  }
  void Frontend::setVcalCoeff(unsigned i, float val){
    if(i<4)m_vcalCoeff[i]=val;
  }
  void Frontend::setCapVal(unsigned i, float val){
    if(i<2)m_capVal[i]=val;
  }
  float Frontend::getVcalCoeff(unsigned i){
    if(i<4)return m_vcalCoeff[i];
    else return -999;
  }
  float Frontend::getCapVal(unsigned i){
    if(i<2)return m_capVal[i];
    else return -999;
  }

  // TODO: get rid of float 
  const float Frontend::dacToElectrons(int dac){
    bool isChigh=m_global->getField("enableCinjHigh");
    float capacity = 6.241495961*(isChigh ? m_capVal[1] : m_capVal[0]);
    float fldac=(float)dac;
    return capacity*(m_vcalCoeff[0] + (m_vcalCoeff[1] + (m_vcalCoeff[2] + m_vcalCoeff[3]*fldac)*fldac)*fldac);
  }

  const int Frontend::electronsToDac(float ne) {
    float min=10000.;
    int mini=-1;
    for(int i=0;i<1024;i++) {
      float diff=(ne-dacToElectrons(i));
      if(diff<0.) diff=-diff;
      if(diff<min) {
	min=diff;mini=i;
      }
    }   
    return mini;
  }

  
  PixelRegister* Frontend::getPixelRegister(unsigned col, unsigned row){
    return &m_pixel[col][row];
  }

  void Frontend::configureHW(){
    m_global->enableAll();
    //std::cout<<"write global register chip "<<m_chip<<std::endl;
    m_global->writeHW();
    for (int i=0;i<N_PIXEL_REGISTER_BITS;i++){
      writePixelRegisterHW(i);
    }
  }
      
  void Frontend::shiftEnablesHW(unsigned short bitmask){
    m_global->enableAll();
    //global bitstream is cached
    m_global->setField("doMux",11);
    BitStream* bitStream=new BitStream;
    for (int bit=0;bit<N_PIXEL_REGISTER_BITS;bit++){
      if(bitmask & (1<<bit)){
	m_global->writeHW();
	int cnt = 4;
	FECommands::mccWriteRegister(bitStream, Mcc::MCC_CNT, cnt);
	FECommands::feWriteCommand(bitStream, m_chip, FECommands::FE_CMD_READ_PIXEL);
        bitStream->push_back(0);
	FECommands::feWriteCommand(bitStream, m_chip, (FECommands::FE_CMD_WRITE_HITBUS << bit) | 
		FECommands::FE_CMD_READ_PIXEL);

        bitStream->push_back(0);
	int dcnt = 2;
	cnt = (dcnt << 3) | 4;
	FECommands::mccWriteRegister(bitStream, Mcc::MCC_CNT, cnt);
	FECommands::feWriteCommand(bitStream, m_chip, (FECommands::FE_CMD_WRITE_HITBUS << bit) | 
		FECommands::FE_CMD_READ_PIXEL | FECommands::FE_CMD_CLOCK_PIXEL);
	bitStream->push_back(0);
	bitStream->push_back(0);
	bitStream->push_back(0);

	cnt = 4;
	FECommands::mccWriteRegister(bitStream, Mcc::MCC_CNT, cnt);
	FECommands::feWriteCommand(bitStream, m_chip, (FECommands::FE_CMD_WRITE_HITBUS << bit) | 
		FECommands::FE_CMD_READ_PIXEL);
        
	bitStream->push_back(0);
	FECommands::feWriteCommand(bitStream, m_chip, FECommands::FE_CMD_READ_PIXEL);
	bitStream->push_back(0);
	FECommands::feWriteCommand(bitStream, m_chip, FECommands::FE_CMD_NULL);

	bitStream->push_back(0);
	dcnt = 1; // was npixels which always seems to be 1 
	cnt = (dcnt << 3) | 4;
	FECommands::mccWriteRegister(bitStream, Mcc::MCC_CNT, cnt);

	FECommands::feWriteCommand(bitStream, m_chip, FECommands::FE_CMD_CLOCK_PIXEL);
	bitStream->push_back(0);
	
	cnt = 4;	
	FECommands::mccWriteRegister(bitStream, Mcc::MCC_CNT, cnt);
	FECommands::feWriteCommand(bitStream, m_chip, FECommands::FE_CMD_WRITE_HITBUS << bit);
	bitStream->push_back(0);
	FECommands::feWriteCommand(bitStream, m_chip, FECommands::FE_CMD_NULL);
	bitStream->push_back(0);
	SerialIF::send(bitStream);
      }
    }
    delete bitStream;
    m_global->setField("doMux",8);
    m_global->writeHW();
  }

};
