#!/usr/bin/python
import subprocess
import paramiko
import sys
from scp import SCPClient
class Rce:
    def __init__(self,ip):
    	self.ip=ip

    def ssh_cmd(self,cmd):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(self.ip, username='root')
        except:
            ssh.connect(self.ip, username='root',password='root')
        stdin, stdout, stderr = ssh.exec_command(cmd)
        out=stdout.readlines()
        err=stdout.readlines()
        ssh.close()
        return (out,err)

    def ssh_get(self,source,dest,recursive=False):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(self.ip, username='root')
        except:
            ssh.connect(self.ip, username='root',password='root')
        scp = SCPClient(ssh.get_transport())
        scp.get(source, dest,recursive)
        ssh.close()
    def ssh_put(self,source,dest,recursive=False):
          ssh = paramiko.SSHClient()
          ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
          try:
              ssh.connect(self.ip, username='root')
          except:
              ssh.connect(self.ip, username='root',password='root')
          scp = SCPClient(ssh.get_transport())
          scp.put(source, dest,recursive)
          ssh.close()

    def stop(self):
        return self.ssh_cmd("systemctl stop calibserver")

    def start(self):
        return self.ssh_cmd("systemctl daemon-reload && systemctl start calibserver")

    def enable(self):
        return self.ssh_cmd("systemctl enable calibserver")
 
    def disable(self):
        return self.ssh_cmd("systemctl disable calibserver")
        
    def status(self):        
        print "calibserver status:"
        output=self.ssh_cmd("systemctl status calibserver")[0]
        for i in output: print i,
        print


    def install(self):
        self.stop()
        self.ssh_cmd("rm -rf /root/calibserver ;mkdir -p /root/calibserver")
        self.ssh_put("build.rce/lib","/root/calibserver/",True)
        self.ssh_put("build.rce/bin","/root/calibserver/",True)        
        self.ssh_put("scripts/calibserver.sh","/root/calibserver/bin")
        self.ssh_put("scripts/calibserver.service","/etc/systemd/system")
        self.start()
        self.enable()
        return "Done"

def usage():
    print "rce_talk_py <RCE hostname> <install|enable|disable|start|stop|status>"
    print "   install : copy calibserver and restart"
    print "   enable  : enable calibserver service"
    print "   disable : disable calibserver service"
    print "   stop    : stop calibserver service"
    print "   start   : start calibserver service"
    print "   status  : print calibserver log"
    exit(1)


if(len(sys.argv)!=3): usage()

rce=Rce(sys.argv[1])
cmd=sys.argv[2]
try:
    call=getattr(rce,cmd);
except:
    usage()
print call()







