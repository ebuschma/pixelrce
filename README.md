Pixel software for RCE controller

Requirements for compilation/running
  * Enterprise Linux 6 or 7 host (Centos 7 recommended)
  * gcc 4.9 or greater (devtoolset-4 with gcc 5.2 recommended)
    - refer to  https://www.softwarecollections.org/en/scls/rhscl/devtoolset-4 for installation
  * for the RCE talk python tools: paramik and python-scp
    - install with: bash
        - bash> sudo yum install python-paramiko python-scp
  * CERN ROOT version 5 or 6
    - available from EPEL 6 (ROOT 5) or EPEL 7 (ROOT 6) repositories
    - to install on EL6/7: 
        - bash> sudo yum install root root-physics
  * Atlas RCE SDK V0.11.0 in /opt/AtlasRceSdk
    - to install:
        - bash> wget -O - http://rceprojectportal.web.cern.ch/RceProjectPortal/software/SDK/V0.11.1.tar.gz | sudo tar xvfz 
  * or the new Atlas SDK (CentOS7 on RCE, gcc7 support)
       to instructions:  https://twiki.cern.ch/twiki/bin/viewauth/Atlas/RCEGen3SDK
Obtaining and compiling the software
   - bash> git clone --recurse-submodules https://:@gitlab.cern.ch:8443/rce/pixelrce.git
   - bash> cd pixelrce
   - bash> cd rce
   - bash> source scripts/setup-dev.sh oldsdk #ommit oldsdk argument for the new SDK
   - bash> ( cd build.rce && make ) && ( cd build.host && make)
   
Alternatively, ROOT and gcc can be taken from an LCG installation
For example on lxplus:
   - lxplus> source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh  tdaq-06-01-01
   - lxplus> git clone  https://:@gitlab.cern.ch:8443/rce/pixelrce.git
   - lxplus> source scripts/setup-dev.sh
   - lsplus> ( cd build.rce && make -j16 ) && ( cd build.host && make -j16 )

The following environment vaiables can be set before sourcing setup-devel.sh to override package location
   - LCG_gcc_home path to LCG gcc installation, if not set (default) CMake will look for devtoolset-4, then for gcc in PATH
   - ROOTSYS (default=/usr) will look for ROOT from EPEL
   - SDK_ROOT (default=/opt/AtlasRceSdk/V0.11.1)   

Running the software:
Your RCE (IP node) should be aliased or named rce0
   - install and start the calibration server on the RCE
       - bash> scripts/rce_talk.py rce0 install
   - check the status/output of calibserver
       - bash > rce_talk.py rce0 status  
   - run calibGui
       - bash> source scripts/setup-env.sh
       - bash> calibGui